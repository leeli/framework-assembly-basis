#!/bin/bash
#filename deploy.sh
set -x
export JAVA_HOME=/indi/software/jdk1.8.0_141
export TOMCAT_HOME=/indi/software/xxx-api-8083
pid=$(ps -ef|grep 8083|grep xxx-api|awk '{print $2}')
for temp_pid in ${pid}
do 
    kill -9 ${temp_pid}
done
war_file="/indi/software/xxx-api-8083/ROOT.war"
if [ -f "$war_file" ];
then
    echo "War file exists, deploy and start the server."
    rm -rf ${TOMCAT_HOME}/webapps/ROOT/
    rm -f ${TOMCAT_HOME}/webapps/ROOT.war
    cp -f ${war_file} ${TOMCAT_HOME}/webapps/ROOT.war
else
    echo "War file not exists, restart the server."
fi
set +x
/bin/bash ${TOMCAT_HOME}/bin/startup.sh

