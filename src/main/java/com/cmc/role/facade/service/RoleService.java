package com.cmc.role.facade.service;

import java.util.List;

import com.cmc.role.facade.model.RoleModel;

public interface RoleService {
    List<RoleModel> getRolePermissions();
}