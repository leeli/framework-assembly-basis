package com.cmc.role.facade.model;

import java.util.List;

import lombok.Data;

import com.cmc.permission.facade.entity.Permission;

@Data
public class RoleModel {
    private int roleId;
    private String roleName;
    private List<Permission> permissions;
}