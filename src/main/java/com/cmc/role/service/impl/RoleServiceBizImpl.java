package com.cmc.role.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cmc.role.facade.model.RoleModel;
import com.cmc.role.facade.service.RoleService;
import com.cmc.role.service.dao.RoleMapper;

public class RoleServiceBizImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<RoleModel> getRolePermissions() {
        return roleMapper.selectRolePermissions();
    }
}