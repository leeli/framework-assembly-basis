package com.cmc.role.service.dao;

import java.util.List;

import com.cmc.common.BaseMapper;
import com.cmc.permission.facade.entity.Permission;
import com.cmc.role.facade.model.RoleModel;

public interface RoleMapper extends BaseMapper<Permission> {
    List<RoleModel> selectRolePermissions();
}