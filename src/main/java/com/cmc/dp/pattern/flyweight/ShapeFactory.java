package com.cmc.dp.pattern.flyweight;

import java.util.HashMap;

public class ShapeFactory {

    /** Circle对象池，同一颜色的对象不用重新生成，可以直接使用. */
    private static final HashMap<String, Shape> CIRCLES = new HashMap<String, Shape>();

    public static Shape getCircle(String color) {
        Circle circle = (Circle) CIRCLES.get(color);
        if (circle != null) {
            return circle;
        }
        circle = new Circle(color);
        System.out.println("Create a new circle：" + color);
        CIRCLES.put(color, circle);
        return circle;
    }

}