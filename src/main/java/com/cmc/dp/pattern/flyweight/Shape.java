package com.cmc.dp.pattern.flyweight;

public interface Shape {
    void draw();
}