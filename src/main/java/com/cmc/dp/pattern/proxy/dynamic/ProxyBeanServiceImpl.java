package com.cmc.dp.pattern.proxy.dynamic;

public class ProxyBeanServiceImpl implements ProxyBeanService {
	@Override
	public boolean save(String name) {
		System.out.println("save business logic ..." + name);
		return false;
	}
}