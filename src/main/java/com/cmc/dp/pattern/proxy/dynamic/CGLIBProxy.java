package com.cmc.dp.pattern.proxy.dynamic;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

/**
 * CGLib Proxy.
 * <p>主要是对指定的类生成一个子类，覆盖其中的所有方法，所以该类或方法不能声明称final的。
 * @author  HT.LCB
 * @version 2016年11月21日 下午2:45:21
 * @version 2017年8月10日 下午3:22:18
 */
public class CGLIBProxy implements MethodInterceptor {
	private Object target;

	public Object getInstance(Object target) {
		this.target = target;
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(this.target.getClass());
		enhancer.setCallback(this);
		return enhancer.create();
	}

	@Override
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		Object rst = null;
		System.out.println("before method.");
		rst = proxy.invokeSuper(obj, args);
		System.out.println("after method.");
		return rst;
	}

	public static void main(String[] args) {
		ProxyBeanService proxyBeanServiceCGLibProxy = (ProxyBeanService) new CGLIBProxy().getInstance(new ProxyBeanServiceImpl());
		proxyBeanServiceCGLibProxy.save("LCB");
	}
}