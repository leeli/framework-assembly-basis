package com.cmc.dp.pattern.proxy.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK Proxy.
 * <p>JDK动态代理只能针对实现了接口的类生成代理。
 * @author  Thomas Lee
 * @version 2016年11月21日 下午2:44:54
 * @version 2017年2月14日 上午11:27:44
 * @version 2017年8月10日 下午3:19:44
 */
public class JDKProxy implements InvocationHandler {
	private Object target;

	public Object createProxy(Object target) {
		this.target = target;
		return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object rst = null;
		System.out.println("before method.");
		rst = method.invoke(target, args);
		System.out.println("after method.");
		return rst;
	}

	public static void main(String[] args) {
		ProxyBeanService proxyBeanService4JDKProxy = (ProxyBeanService) new JDKProxy().createProxy(new ProxyBeanServiceImpl());
		proxyBeanService4JDKProxy.save("");
	}
}