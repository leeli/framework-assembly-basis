package com.cmc.web.test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import com.cmc.common.constants.Constants;
import com.cmc.common.utils.CacheManager;
import com.cmc.common.utils.SerializationUtils;
import com.cmc.designer.facade.model.DesignerModel;
import com.cmc.designer.facade.service.DesignerService;
import com.cmc.user.facade.entity.User;
import com.cmc.user.facade.model.UserModel;

/**
 * 测试控制器.
 * <p>
 * 面试基础题：
 *     1. http://www.cnblogs.com/lanxuezaipiao/p/3371224.html.
 *     2. Interview Question Study Notes.
 *     3. Interview Programming Study Notes. 
 *          
 * </p>
 * @author Thomas Lee
 * @version 2017年5月8日 下午8:12:21
 */
@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {
	private static int INT_TEST = 1;
	@Autowired
	private DesignerService designerService;
	@Autowired
	private ShardedJedisPool shardedJedisPool;

	@GetMapping("/setrediscachemanager")
	public void setRedisCacheManager() {
		String key = "tmp_key";
		User user = new User();
		user.setId("1");
		user.setAge(20);
		CacheManager.set(key, user);
		User userTmp = CacheManager.get(key);
		log.info(userTmp.toString());

		CacheManager.addSet(key, user);
		CacheManager.addSet(key, user);
		Set<User> users = CacheManager.getSet(key);
		log.info(Integer.toString(users.size()));
	}

	/**
	 * 测试请求转发，需要结合com.cmc.interceptors.TestInterceptor.
	 * @param name
	 * @author Thomas Lee
	 * @version 2017年8月4日 下午1:06:50
	 */
	@ResponseBody
	@GetMapping("/testforward")
	public void testForward(String name) {
	}

	@ResponseBody
	@GetMapping("/testlog")
	public User testlog(String name) {
		log.info("loginfo");
		log.debug("logdebug");
		log.warn("logwarn");
		log.error("logerror");
		log.info("info");
		log.debug("debug");
		log.warn("warn");
		log.error("error");
		User user = new User();
		user.setAge(10);
		return user;
	}

	@GetMapping("/url-with-slash")
	public void testURLWithSlash() {
		log.info("/url-with-slash");
	}

	@GetMapping("/url-with-slash/")
	public void testURLWithSlash1() {
		log.info("/url-with-slash/");
	}

	public static void print() {
		System.out.println("d");
	}

	public static void testInitialization() {
		@SuppressWarnings("unused")
		int a;
		// 编译不通过.
		// System.out.println(a);
		String[] strs = new String[10];
		System.out.println(strs[0]);
		int[] ints = new int[10];
		System.out.println(ints[0]);
	}

	/**
	 * 测试静态成员变量.
	 * @author Thomas Lee
	 * @version 2017年5月8日 下午8:52:06
	 */
	@SuppressWarnings("static-access")
	public static void testStaticField() {
		TestController.INT_TEST = 2;
		new TestController().INT_TEST = 3;
		System.out.println(TestController.INT_TEST);
		TestController.INT_TEST = 5;
		System.out.println(new TestController().INT_TEST);
		new TestController().print();
		// System.out.println(TestConroller.INT_TEST);
	}

	/**
	 * 测试类型转换.
	 * <p>
	 * 往大的一方转换.
	 * char默认转换为对应ASCII对应的数值.
	 * </p>
	 * @author Thomas Lee
	 * @version 2017年5月8日 下午9:05:02
	 */
	@SuppressWarnings("unused")
	public static void testTypeConvert() {
		char a = 'a';

		double b = 10.2D;
		float c = 10.1F;
		System.out.println((a + b));
		System.out.println(a);
		System.out.println(b + c);

		byte byteA = 1;
		short shortB = 2;
		int intC = 1;
		long longD = 1L;
		int rstE = byteA + shortB;
	}

	/**
	 * 测试i++和++i.
	 * <p>
	 * i++后计算，++i先计算.
	 * 记忆：++和i的位置关系决定了先计算还是后计算.
	 * 注意：i++后计算也是针对于一个简单的表达式.
	 * </p>
	 * @author Thomas Lee
	 * @version 2017年5月8日 下午11:01:53
	 */
	public static void testIPlusPlusAndPlusPlusI() {

		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		}

		int i = 0;
		if (i++ > 0) {
			System.out.println("aaaaaaaaaaaaa");
		}

		i = 0;
		if (++i > 0) {
			System.out.println("bbbbbbbbbbbbb");
		}

		i = 0;
		// ( i++ > 0 ) 针对的是一个简单表达式，而不是一个复杂表达式. 
		if (i >= 1 || i++ > 0 || i >= 1) {
			System.out.println("ccccccccccccc");
		}

		// 后计算.
		System.out.println(i++);
		// 先计算.
		System.out.println(++i);
		System.out.println(i);

	}

	/**
	 * 测试switch.
	 * <p>
	 * 第一次匹配之后不再匹配后面的case.
	 * </p>
	 * @param i 参数.
	 * @return
	 * @author Thomas Lee
	 * @version 2017年5月9日 上午12:54:29
	 */
	public static int getValue(int i) {
		int result = 0;
		switch (i) {
		case 1:
			result = result + i;
		case 2:
			result = result + i * 2;
		case 3:
			result = result + i * 3;
		case 4:
			System.out.println("d");
		}
		return result;
	}

	public static void main(String[] args) {
		// testStaticField();
		// testTypeConvert();
		// testIPlusPlusAndPlusPlusI();
		testInitialization();
		int a = Integer.MAX_VALUE;
		System.out.println(a + 2);
		System.out.println(Double.NaN <= Double.NaN);

		// 注意下面两种方式.
		System.out.println('6' + 5);
		System.out.println("5" + 2);

		// 测试switch.
		System.out.println(getValue(2));
	}

	@RequestMapping(method = RequestMethod.GET, value = "testredis")
	public void testRedis() {
		// 存储java.lang.String类型.
		ShardedJedis jedis = shardedJedisPool.getResource();
		String key = "name";
		String value = "lcb";
		jedis.set(key, value);

		// 存储对象.
		User user = new User();
		user.setId(UUID.randomUUID().toString());
		user.setAge(18);
		try {
			byte[] btUser = SerializationUtils.serialize(user);
			jedis.set("user".getBytes(), btUser);
			byte[] btUserFromRedis = jedis.get("user".getBytes());
			@SuppressWarnings("unused")
			User userFromRedis = (User) SerializationUtils.deserialize(btUserFromRedis);
		} catch (IOException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 测试Hibernate.
	 * @author Thomas Lee
	 * @version 2017年3月21日 下午2:12:48
	 */
	@RequestMapping(value = "/testhibernate", method = RequestMethod.GET)
	public void testHibernate() {
		designerService.testHibernateCRUD();
		List<DesignerModel> mDesigners = designerService.getDesigners();
		if (!CollectionUtils.isEmpty(mDesigners)) {
			Iterator<DesignerModel> imDesigner = mDesigners.iterator();
			while (imDesigner.hasNext()) {
				DesignerModel mDesigner = imDesigner.next();
				log.info(mDesigner.toString());
				System.out.println();
			}
		}
	}

	/**
	 * 测试Hibernate的三种状态.
	 * @author Thomas Lee
	 * @version 2017年3月22日 下午4:38:07
	 */
	@RequestMapping(value = "/testhibernatethreestatus", method = RequestMethod.GET)
	public void testHibernateThreeStatus() {
		designerService.testHibernateThreeStatus();
	}

	private static User user = null;

	static {
		user = new User();
		user.setAge(11);
	}

	/**
	 * 测试注解：@PathVariable
	 * <p>
	 * 测试通过：主要功能，映射地址，默认是参数的名称，具体参考源码.
	 * @param name 姓名.
	 * @param age 年龄.
	 * @return String name + age.
	 * @author Thoma1s Lee
	 * @version Jun 6, 2017 2:09:59 PM
	 */
	// @PostMapping是@RequestMapping(method = RequestMethod.POST)的简称，其他的HTTP方法也是如此。
	// @RequestMapping(value = "/testpathvariable/{name}/{age}", method = RequestMethod.POST)
	@PostMapping("/testpathvariable/{name}/{age}")
	public @ResponseBody String testPathVariable(@PathVariable(name = "name") String name, @PathVariable String age) {
		log.info("name的值为：" + name);
		log.info("age的值为：" + age);
		return name + age;
	}

	/**
	 * 测试ZCGLCommonRequest公共请求类的使用.
	 * @param  UserModel 用户实例.
	 * @author           Thomas Lee
	 * @version  		  2017年7月10日 上午10:36:48
	 */
	@PostMapping("/testzcglcommonrequest")
	public void testZCGLCommonRequest(UserModel data) {
		log.info(data.toString());
	}

	/**
	 * 测试RequestBody的接收类型是不是只是application/json.
	 * @param mUser 用户实例.
	 * @author		 Thomas Lee
	 * @version 	 2017年7月10日 下午2:18:10
	 */
	@PostMapping("/testrequestbody")
	public void testRequestBody(@RequestBody UserModel mUser) {
		log.info(mUser.toString());
	}

	/**
	 * 测试动态和静态变量、方法在类加载的时候的执行顺序.
	 * @author  Thomas Lee
	 * @version 2017年7月15日 下午2:53:13
	 */
	@GetMapping("/staticdynamicorder")
	public void testStaticDynamicOrder() {
		@SuppressWarnings("unused")
		String tmp = Constants.TMP;
	}
}