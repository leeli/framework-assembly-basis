package com.cmc.web.test;

public class B extends A {

    static {
        System.out.println("b1");
    }

    B() {
        System.out.println("b2");
    }

    public static void main(String[] args) {
        new B();
    }

}