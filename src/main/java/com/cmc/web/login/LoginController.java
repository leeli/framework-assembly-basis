package com.cmc.web.login;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmc.common.constants.AjaxGeneralResult;
import com.cmc.common.constants.Constants;
import com.cmc.common.constants.LoginStatus;
import com.cmc.common.constants.SignUpStatus;
import com.cmc.service.login.LoginService;
import com.cmc.user.facade.model.UserModel;
import com.cmc.user.facade.service.UserService;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * 登录控制器.
 * @author  Thomas Lee
 * @version 2017年7月12日 上午10:36:41
 */
@Controller
@RequestMapping(value = "login")
public class LoginController {
	@Autowired
	private LoginService loginService;
	@Autowired
	private UserService userService;

	/**
	 * 渲染登录界面.
	 * @param req 					  HttpServletRequest.
	 * @param res 					  HttpServletResponse.
	 * @param name                   用户名.
	 * @param age                    年龄.
	 * @return                       渲染页面.
	 * @throws FileNotFoundException FileNotFoundException.
	 * @author                       Thomas Lee
	 * @version                      2017年7月12日 上午10:37:28
	 */
	@ApiOperation(value = "登录界面")
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest req, HttpServletResponse res, String name, String age) throws FileNotFoundException {
		// res.setHeader("Date", new Date().toLocaleString());
		// res.setHeader("Refresh", "5; URL=https://www.baidu.com");
		// InputStream ins =
		// this.getClass().getClassLoader().getResourceAsStream("config.properties");
		// ClassLoader.getSystemResourceAsStream ("");
		// req.getServletContext().getRealPath("");
		// InputStream in = new FileInputStream(news
		// File(req.getServletContext().getRealPath("WEB-INF\\classes\\config.properties")));
		// String tmp = Constants.TMP;
		return "login/login";
	}

	/**
	 * 返回登录情况.
	 * @param request HttpServletRequest.
	 * @param dto     UserModel.
	 * @param res     HttpServletResponse.
	 * @return        登录情况.
	 * @author        Thomas Lee
	 * @version       2017年7月12日 上午10:39:31
	 */
	@ResponseBody
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public AjaxGeneralResult login(HttpServletRequest request, UserModel dto, HttpServletResponse res) {
		LoginStatus status = loginService.login(dto);
		if (LoginStatus.SUCCESS == status) {
			UserModel userDTO = userService.getByName(dto);
			request.getSession().setAttribute(Constants.USER_SESSION, userDTO);
		}
		return AjaxGeneralResult.getGeneralRst(status.getCode(), status.getDesc());
	}

	/**
	 * 注册.
	 * @param dto UserModel.
	 * @return    是否成功标志.
	 * @author    Thomas Lee
	 * @version   2017年7月12日 上午10:40:16
	 */
	@ResponseBody
	@RequestMapping(value = "signup", method = RequestMethod.GET)
	public AjaxGeneralResult signUp(UserModel dto) {
		SignUpStatus status = loginService.signUp(dto);
		return AjaxGeneralResult.getGeneralRst(status.getCode(), status.getDesc());
	}

	/**
	 * 返回JSON数据.
	 * @return  JSON数据.
	 * @author  Thomas Lee
	 * @version 2017年4月11日 上午10:37:42
	 */
	@RequestMapping(value = "getjson", method = RequestMethod.GET)
	public @ResponseBody Object getJson() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("fd", "tt");
		return map;
	}
}