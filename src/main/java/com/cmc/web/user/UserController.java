package com.cmc.web.user;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmc.common.constants.AjaxGeneralResult;
import com.cmc.common.constants.AjaxResult;
import com.cmc.common.utils.FileUploadUtil;
import com.cmc.permission.facade.model.PermissionModel;
import com.cmc.permission.facade.service.PermissionService;
import com.cmc.user.facade.entity.User;
import com.cmc.user.facade.model.UserModel;
import com.cmc.user.facade.service.UserService;

/**
 * 用户相关操作业务类.
 * <p>
 * 注意：<br>
 * 1. 数据校验有待集成数据校验框架，参考数据校验项目.
 * </p>
 * @author Thomas Lee
 * @version 2017年4月18日 下午6:28:13
 * @version 2017年5月4日 上午10:50:18
 */
@Controller
@RequestMapping("user")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    /** 分页大小 */
    private static final long PAGE_SIZE = 10L;

    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;

    @RequestMapping(method = RequestMethod.GET, value = "add")
    public String add() {
        try {
            // EhCache DEMO.
            CacheManager cacheManager = CacheManager.create();
            Cache cache = cacheManager.getCache("sample");
            Element element = new Element("name", "Thomas");
            cache.put(element);
            cache.get("name");
        } catch (Exception e) {
            LOG.error("", e);
        }
        return "user/detail";
    }

    /**
     * 添加用户.
     * @param req HttpServletRequest.
     * @param user 用户信息.
     * @return AjaxGeneralResult.
     * @author Thomas Lee
     * @version 2017年5月4日 上午10:59:12
     */
    @RequestMapping(method = RequestMethod.POST, value = "add")
    public @ResponseBody AjaxResult add(HttpServletRequest req, UserModel user) {
        try {
            String avatarUrl = FileUploadUtil.upload(req);
            user.setTemp2(avatarUrl);
            userService.add(user);
            return AjaxResult.getSuccessInstance();
        } catch (RuntimeException e) {
            LOG.error(e.getMessage(), e);
            return AjaxResult.getFailInstance(e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "list")
    public String list(HttpServletRequest request, ModelMap map) throws Exception {
        String pageNo = request.getParameter("pageNo");
        if (null == pageNo) {
            pageNo = "1";
        }
        map.put("users", userService.list(Long.parseLong(pageNo), PAGE_SIZE));
        return "user/list";
    }

    @RequestMapping(method = RequestMethod.GET, params = "action=get")
    public String get(UserModel dto, ModelMap map) {
        map.put("user", userService.get(dto));
        return "user/detail";
    }

    @RequestMapping(method = RequestMethod.POST, params = "action=update")
    public @ResponseBody AjaxGeneralResult update(UserModel dto) {
        return userService.update(dto) ? AjaxGeneralResult.getSuccessRst("更新用户信息") : AjaxGeneralResult.getFailureRst("更新用户信息");
    }

    @RequestMapping(method = RequestMethod.POST, params = "action=delete")
    public @ResponseBody AjaxGeneralResult delete(UserModel dto) {
        return userService.delete(dto) ? AjaxGeneralResult.getSuccessRst("删除用户信息") : AjaxGeneralResult.getFailureRst("删除用户信息");
    }

    @RequestMapping(method = RequestMethod.GET, params = "action=getsampleuser")
    public @ResponseBody User getSampleUser() {
        User user = new User();
        user.setId("111111");
        return user;
    }

    /**
     * 数据校验：后端（通过AJAX）间接校验.
     * @author Thomas Lee
     * @since 2016年12月22日 上午10:16:56
     */
    @RequestMapping(method = RequestMethod.POST, params = "action=existstheuser")
    public @ResponseBody AjaxGeneralResult existsTheUser(String userId) {
        /* business logic is omitted. */
        return new Random().nextBoolean() ? AjaxGeneralResult.getSuccessRst() : AjaxGeneralResult.getFailureRst();
    }

    /**
     * 获取所有权限列表.
     * @author Thomas Lee
     * @version 2017年3月13日 下午5:55:31
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, params = "action=getpermissions")
    public @ResponseBody AjaxGeneralResult getAllPermissions() {
        // List<PermissionModel> mPermissions = permissionService.getAll();
        PermissionModel mPermission = permissionService.get(1L);
        LOG.info(mPermission.getPermName());
        return AjaxGeneralResult.getSuccessRst();
    }

}