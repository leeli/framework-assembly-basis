package com.cmc.permission.service.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cmc.common.BaseMapper;
import com.cmc.permission.facade.entity.Permission;

public interface PermissionMapper extends BaseMapper<Permission> {
    /**
     * 添加.
     * @param perms 权限列表.
     * @author      Thomas Lee
     * @version     2017年7月23日 下午8:41:55
     */
    void insertBatch(List<Permission> perms);

    /**
     * 通过权限ID获取权限信息
     * @param permId 权限ID
     * @author       Thomas Lee
     * @version      2017年3月14日 上午10:54:00
     * @return       权限信息
     */
    Permission selectByPermId(@Param("permId") long permId);

    /**
     * 添加.
     * <p>测试selectKey.
     * @param perm 权限信息.
     * @author     Thomas Lee
     * @version    2017年7月23日 下午8:54:45
     */
    void insertSelectiveWithId(Permission perm);
}