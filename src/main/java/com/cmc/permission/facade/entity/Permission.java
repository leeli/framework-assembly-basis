package com.cmc.permission.facade.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Table
@Entity
public class Permission {
    @Id
    @Column(name = "perm_id")
    private Long permId;
    @Column(name = "parent_perm_id")
    private Long parentPermId;
    @Column(name = "perm_name")
    private String permName;
    private String description;
}