package com.cmc.demo.net.socket.server;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;

import org.apache.http.util.ByteArrayBuffer;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 * 网络电话服务端.
 * 
 * <p>后期考虑：转发中心、多线程、通信技术升级、缓存、消息队列以及扩展业务功能等.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午11:00:42
 */
public class VoiceServer {
	private static final int VOICE_SERVER_PORT = 8888;
	private static final int BUF_SIZE = 1024;
	private static final int VOICE_INITIAL_CAPACITY = 1024 * 1024 * 10; // 10M

	public static void main(String[] args) {
//		new VoiceServer().start();
		new IVoiceService() {
			@Override
			public void listen() {
				try {
					try (ServerSocket ss = new ServerSocket(VOICE_SERVER_PORT)) {
						System.out.println("receive...");
						try (DataInputStream dis = new DataInputStream(ss.accept().getInputStream())) {
							byte[] buf = new byte[BUF_SIZE];
							ByteArrayBuffer voiceBuf = new ByteArrayBuffer(VOICE_INITIAL_CAPACITY);
							int size;
							for (; (size = dis.read(buf)) != -1;) {
								voiceBuf.append(buf, 0, size);
							}
							byte[] bVoice = voiceBuf.toByteArray();
							AudioStream as = new AudioStream(new ByteArrayInputStream(bVoice));
							AudioPlayer.player.start(as);
						}
					}
				} catch (IOException e) {
					// for now.
					e.printStackTrace();
				}
			}
		}.listen();
	}

	interface IVoiceService {
		/**
		 * 听语音.
		 * 
		 * @author Thomas Lee
		 * @version 2017年9月13日 下午11:02:37
		 */
		public void listen();
	}
}