package com.cmc.demo.net.socket.client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import lombok.Data;

import com.cmc.common.utils.StreamUtils;

/**
 * 网络电话客户端.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午6:14:39 v0.0.1
 */
public class VoiceClient {
	private static final String SERVER_IP = "127.0.0.1";
	private static final int SERVER_PORT = 8888;

	public static void main(String[] args) {
		byte[] bVoice = StreamUtils.stream2bytes(ClassLoader.getSystemResourceAsStream("audio/1.wav"));
		new IVoiceClientService() {
			@Override
			public void speak(Contact contact, byte[] voice) {
				try (Socket socket = new Socket(contact.getHost(), contact.getPort())) {
					try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {
						dataOutputStream.write(voice);
					}
				} catch (IOException e) {
					// for now.
					e.printStackTrace();
				}
			}
		}.speak(new Contact(SERVER_IP, SERVER_PORT), bVoice);
		
//		new VoiceClientService().speak(new Contact(SERVER_IP, SERVER_PORT), bVoice);
	}
}

@Deprecated
class VoiceClientService implements IVoiceClientService {
	@Override
	public void speak(Contact contact, byte[] voice) {
		try (Socket socket = new Socket(contact.getHost(), contact.getPort())) {
			try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream())) {
				dataOutputStream.write(voice);
			}
		} catch (IOException e) {
			// for now.
			e.printStackTrace();
		}
	}
}

interface IVoiceClientService {
	public void speak(Contact contact, byte[] voice);
}

@Data
class Contact {
	private String host;
	private int port;

	public Contact(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}
}