package com.cmc.demo.net.socket;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 网络语音电话工具类.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午11:05:22 v0.0.1
 */
public class VoiceBizUtils2 {
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				byte[] cons = listen(8888);
				String rst = "";
				for (int i = 0; i < cons.length; i++) {
					rst += (char) cons[i];
				}
				System.out.println(rst);
			}
		}).start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				speak(new Contact("127.0.0.1", 8888), new Voice("Hello Sever".getBytes()));
			}
		}).start();
	}

	/**
	 * 说.
	 * 
	 * @param voice
	 * @param contact
	 * @author Mike Lee
	 * @version 2017年9月13日 下午11:23:17
	 */
	public static void speak(Contact contact, Voice voice) {
		new IVoiceClientService() {
			@Override
			public void speak(Contact contact, Voice voice) {
				try {
					new DataOutputStream(new Socket(contact.getHost(), contact.getPort()).getOutputStream()).write(voice.getbVoice());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}.speak(contact, voice);
	}

	/**
	 * 听.
	 * 
	 * @param port
	 * @author Mike Lee
	 * @version 2017年9月13日 下午11:19:05
	 */
	public static byte[] listen(int port) {
		return new IVoiceService() {
			@Override
			public byte[] listen(int port) {
				try {
					return StreamUtils.stream2bytes(new ServerSocket(port).accept().getInputStream());
				} catch (IOException e) {
					e.printStackTrace();
				}
				return new byte[] {};
			}
		}.listen(port);
	}

	private static class StreamUtils {
		/**
		 * 
		 * @param is
		 * @return
		 * @author Thomas Lee
		 * @version 2017年9月13日 下午10:31:25
		 */
		public static byte[] stream2bytes(InputStream is) {
			// stream to bytes，肯定是读取流，所以使用OutputStream.
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				byte[] buf = new byte[1024];
				for (int len; (len = is.read(buf)) != -1;)
					baos.write(buf, 0, len);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					baos.close();
					baos = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return baos.toByteArray();
		}
	}

	private static interface IVoiceService {
		/**
		 * 
		 * @param port
		 * @author Mike Lee
		 * @version 2017年9月13日 下午11:31:05
		 */
		public byte[] listen(int port);
	}

	private static interface IVoiceClientService {
		/**
		 * 
		 * @param contact
		 * @param voice
		 * @author Mike Lee
		 * @version 2017年9月13日 下午11:31:00
		 */
		public void speak(Contact contact, Voice voice);
	}

	private static class Contact {
		private String host;
		private int port;

		public Contact(String host, int port) {
			super();
			this.host = host;
			this.port = port;
		}

		public String getHost() {
			return host;
		}

		public int getPort() {
			return port;
		}
	}

	private static class Voice {
		private byte[] bVoice;

		public Voice(byte[] bVoice) {
			super();
			this.bVoice = bVoice;
		}

		public byte[] getbVoice() {
			return bVoice;
		}
	}
}