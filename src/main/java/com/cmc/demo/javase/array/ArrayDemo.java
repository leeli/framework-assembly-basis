package com.cmc.demo.javase.array;

/**
 * 数组Demo.
 * @author  Thomas Lee
 * @version 2017年8月10日 下午2:09:10
 * @reference http://blog.csdn.net/u012110719/article/details/42746817
 */
public class ArrayDemo {
	public static void main(String[] args) {
		initArray();
	}

	/**
	 * 声明or初始化数组.
	 * <p>数组元素是被大括号：{}包装起来的.
	 * @author  Thomas Lee
	 * @version 2017年8月10日 下午2:09:50
	 */
	@SuppressWarnings("unused")
	private static void initArray() {
		int[] iArr = new int[10];
		int[] iArr1 = { 1, 2, 3, 4, 5 };
		int[] iArr2 = new int[] { 1, 2, 3, 4 };

		String[] strArr = new String[10];
		String[] strArr1 = { "1", "2", "3", "4" };
		String[] strArr2 = new String[] { "1", "2", "3", "4" };
	}
}