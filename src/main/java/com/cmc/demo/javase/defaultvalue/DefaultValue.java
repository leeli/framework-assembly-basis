package com.cmc.demo.javase.defaultvalue;

import com.cmc.user.facade.entity.User;

public class DefaultValue {

    private int a;
    private User user;

    public static void main(String[] args) {
        System.out.println(new DefaultValue().a);
        System.out.println(new DefaultValue().user);
        // 下面编译会报出错误.
        // int b;
        // User user1;
        // System.out.println(b);
        // System.out.println(user1);
    }

}