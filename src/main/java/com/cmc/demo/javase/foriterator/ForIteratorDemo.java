package com.cmc.demo.javase.foriterator;

import java.util.ArrayList;
import java.util.List;

/**
 * For循环和Iterator比较.
 * @author Thomas Lee
 * @version 2017年8月11日 下午5:09:00
 */
public class ForIteratorDemo {
	public static void main(String[] args) {
		List<ChildClass> childs = new ArrayList<ForIteratorDemo.ChildClass>();
		childs.add(new ForIteratorDemo().new ChildClass());
		childs.add(new ForIteratorDemo().new ChildClass());
		childs.add(new ForIteratorDemo().new ChildClass());
		for (ChildClass child : childs) {
			System.out.println(childs.size());
			childs.remove(0);
		}
	}

	private class ParentClass {
	}

	private class ChildClass extends ParentClass {
	}
}