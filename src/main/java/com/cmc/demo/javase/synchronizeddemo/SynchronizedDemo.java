package com.cmc.demo.javase.synchronizeddemo;

/**
 * 同步特性测试. 
 * @author Thomas Lee
 * @version 2017年5月16日 上午11:44:51
 */
public class SynchronizedDemo {

    public static void main(String[] args) {
        Runnable runner = new MyRunner();
        Thread thread1 = new Thread(runner, "thread=============1");
        Thread thread2 = new Thread(runner, "thread-------------2");

        SynchronizedDemo demo = new SynchronizedDemo();
        demo.test(thread1);
        demo.test(thread2);
    }

    public synchronized void test(Thread thread) {
        thread.start();
    }

}

class MyRunner implements Runnable {

    public void print() {
        System.out.println(Thread.currentThread().getName() + " is on ... ");
    }

    @Override
    public void run() {
        while (true) {
            print();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}