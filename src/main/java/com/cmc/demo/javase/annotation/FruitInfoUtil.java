package com.cmc.demo.javase.annotation;

import java.lang.reflect.Field;

/**
 * 通过反射获取注解信息.
 * 
 * @author  Thomas Lee
 * @version 2017年8月10日 下午2:30:16
 */
public class FruitInfoUtil {
	public static void getFruitInfo(Class<?> clazz) {
		String strFruitName = "水果名称：";
		String strFruitColor = "水果颜色：";
		String strFruitProvider = "供应商信息：";
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			FruitName fruitName = field.getAnnotation(FruitName.class);
			strFruitName += fruitName == null ? "" : fruitName.value();
			FruitColor fruitColor = field.getAnnotation(FruitColor.class);
			strFruitColor += fruitColor == null ? "" : fruitColor.fruitColor();
			FruitProvider fruitProvider = field.getAnnotation(FruitProvider.class);
			strFruitProvider += fruitProvider == null ? "" : fruitProvider.id() + fruitProvider.name() + fruitProvider.address();
		}
		System.out.println(strFruitName);
		System.out.println(strFruitColor);
		System.out.println(strFruitProvider);
	}
}