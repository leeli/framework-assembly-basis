package com.cmc.demo.javase.annotation.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class DemoRun {
	public static void main(String[] args) {
		DemoRun.testAnnotatinoTarget();
	}

	/*
	 * 测试获取注解不同@Target ElementType类型上的注解.
	 */
	private static void testAnnotatinoTarget() {
		Class<AnnotationTargetDemoClass> clazz = AnnotationTargetDemoClass.class;
		// 打印注解目标是ElementType.TYPE的内容.
		Desc targetType = clazz.getAnnotation(Desc.class);
		System.out.println(targetType == null ? "" : targetType.value());

		// 打印注解目标是ElementType.FIELD的内容.
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			Desc targetField = field.getAnnotation(Desc.class);
			System.out.println(targetField == null ? "" : targetField.value());
		}

		// 打印注解目标是ElementType.METHOD的内容.
		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			Desc targetMethod = method.getAnnotation(Desc.class);
			System.out.println(targetMethod == null ? "" : targetMethod.value());
		}
	}
}