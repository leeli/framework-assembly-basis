package com.cmc.demo.javase.annotation.projectexample;

import java.util.List;

/**
 * 
 * @author HT-LiChuanbin 
 * @version 2017年8月10日 下午4:52:42
 */
public interface ScoreRuleService {
    /**
     * 查询所有积分规则.
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月10日 下午4:53:35
     */
    public List<ScoreRule> list();
}