package com.cmc.demo.javase.annotation.projectexample;

import javax.persistence.Column;
import javax.persistence.Id;

import lombok.Data;

@Data
//@Entity
//@Table(name = "t_score_rule")
public class ScoreRule {
    @Id
    @Column(name = "score_rule_id")
    private Integer scoreRuleId;
    @Column(name = "score_name")
    private String scoreName;
    @Column(name = "score_value")
    private Integer scoreValue;
}