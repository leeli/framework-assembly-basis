package com.cmc.demo.javase.annotation.projectexample;

import java.util.List;

public class ScoreRuleServiceImpl implements ScoreRuleService {
    private ScoreRuleDao scoreRuleDao;

    @Override
    public List<ScoreRule> list() {
        return scoreRuleDao.selectAll();
    }
}