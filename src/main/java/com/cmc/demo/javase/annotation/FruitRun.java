package com.cmc.demo.javase.annotation;

/**
 * 
 * @author Thomas Lee
 * @version 2017年8月10日 下午2:28:43
 * @reference http://www.cnblogs.com/peida/archive/2013/04/23/3036035.html
 */
public class FruitRun {
	public static void main(String[] args) {
		FruitInfoUtil.getFruitInfo(Apple.class);
	}
}