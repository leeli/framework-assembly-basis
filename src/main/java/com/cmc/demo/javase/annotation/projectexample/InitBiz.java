package com.cmc.demo.javase.annotation.projectexample;

import java.util.Iterator;
import java.util.List;

/**
 * 业务初始化类.
 * @author  HT-LiChuanbin 
 * @version 2017年8月10日 下午4:36:30
 */
//@Component
//@Lazy(false)
public class InitBiz {
//    @Autowired
    private ScoreRuleService scoreRuleService;

    /**
     * 初始化积分规则.
     * @author  HT-LiChuanbin 
     * @version 2017年8月11日 下午4:00:15
     */
//    @PostConstruct
    public void initScore() {
        List<ScoreRule> scoreRules = scoreRuleService.list();
        Iterator<ScoreRule> iScoreRules = scoreRules.iterator();
        while (iScoreRules.hasNext()) {
            ScoreRule scoreRule = iScoreRules.next();
            for (ScoreRules strategy : ScoreRules.values()) {
                if (Integer.compare(strategy.getRuleId(), scoreRule.getScoreRuleId()) == 0) {
                    strategy.setDesc(scoreRule.getScoreName());
                    strategy.setScore(scoreRule.getScoreValue());
                }
            }
        }
    }
}