package com.cmc.demo.javase.annotation;

import lombok.Data;

import com.cmc.demo.javase.annotation.FruitColor.Color;

/**
 * 
 * TODO 补充注解对象范围各种的注解解析.
 * @author Thomas Lee
 * @version 2017年8月11日 下午1:30:05
 */
@Data
public class Apple {
	@FruitName("Apple")
	private String appleName;
	@FruitColor(fruitColor = Color.RED)
	private String appleColor;
	@FruitProvider(id = 1, name = "陕西红富士集团", address = "陕西省西安市延安路89号红富士大厦")
	private String appleProvider;

	public void displayName() {
		System.out.println("水果的名字是：苹果");
	}
}