package com.cmc.demo.javase.annotation.projectexample;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 通用Mapper接口
 * @param <T> Entity 泛型
 */
public interface BaseDao<T> extends Mapper<T>, MySqlMapper<T> {
}