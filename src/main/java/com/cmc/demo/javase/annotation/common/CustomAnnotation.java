package com.cmc.demo.javase.annotation.common;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解.
 * @author Thomas Lee
 * @version 2017年8月11日 下午2:15:55
 */
@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CustomAnnotation {
	/** 如果注解只有一个属性，建议名称为“value”，这样可以使用@CustomAnnotation("")更为简洁地代替@CustomAnnotation(value = "") */
	String value() default "";
}