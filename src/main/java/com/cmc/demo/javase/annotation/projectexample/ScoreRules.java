package com.cmc.demo.javase.annotation.projectexample;

public enum ScoreRules {
    NONE(0,"其他",0),
    LOGIN(1,"",0),
    ADD_QUESTION(2,"",0),
    ADD_REPLY(3,"",0),
    ADD_COMMNET(4,"",0),
    LIKE(5,"",0),
    ADD_NEWS(6, "", 0),
    AUDIT_ACTIVITY_JOINER(7, "", 0);

    private Integer ruleId;
    private String desc;
    private Integer score;

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    ScoreRules(Integer ruleId, String desc, Integer score) {
        this.ruleId = ruleId;
        this.desc = desc;
        this.score = score;
    }
}