package com.cmc.demo.javase.annotation.common;

@Desc("I'm a class annotation.")
public class AnnotationTargetDemoClass {
	@Desc("I'm a field annotation.")
	private String name;

	@Desc("I'm a method annotation.")
	public void printName() {
	}
}