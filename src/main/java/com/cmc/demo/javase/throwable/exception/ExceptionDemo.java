package com.cmc.demo.javase.throwable.exception;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * java.lang.Exception Demo.
 * @author Thomas Lee
 * @version 2017年5月11日 下午6:04:06
 */
public class ExceptionDemo {
	public static void main(String[] args) {
		// ExceptionDemo.testExceptionWithoutCatch();
		// readFirstLineFromFile("1");
		ExceptionDemo demo = new ExceptionDemo();
		System.out.println(demo.testReturn());
	}

	/**
	 * 测试finally语句块return和throw对其前面的return和异常处理的影响.
	 * 结果： the method testExceptionWithoutCatch throws the exception thrown from the finally block; the exception thrown from the try block is suppressed.
	 * 即，在finally语句块写return和throw会覆盖其前面的return和throw语句（所以不建议在finally语句块中进行return和throw操作，eclipse则会报出警告.）.
	 * @author Thomas Lee
	 * @version 2017年5月11日 下午7:44:27
	 */
	@SuppressWarnings("unused")
	private static void testExceptionWithoutCatch() {
		try {
			throw new RuntimeException("try.");
		} finally {
			// 当finally块中包含return语句时，Eclipse会给出警告“finally block does not complete normally”，原因分析如下：
			// 1、不管try块、catch块中是否有return语句，finally块都会执行。
			// 2、finally块中的return语句会覆盖前面的return语句（try块、catch块中的return语句），所以如果finally块中有return语句，Eclipse编译器会报警告“finally block does not complete normally”。
			// 3、如果finally块中包含了return语句，即使前面的catch块重新抛出了异常，则调用该方法的语句也不会获得catch块重新抛出的异常，而是会得到finally块的返回值，并且不会捕获异常。
			// 结论1：主要用来放置cleanup code，虽然可以放return、throw语句，但是不建议，return和throw的功能还是交给try、catch语句块进行处理，finally语句块是为了在系统发生异常（终止了程序执行流程）的时候处理一些需要处理的资源.
			// The finally block always executes when the try block exits. This ensures that the finally block is executed even if an unexpected exception occurs. 
			// But finally is useful for more than just exception handling — it allows the programmer to avoid having cleanup code accidentally bypassed by a return, 
			// continue, or break. Putting cleanup code in a finally block is always a good practice, even when no exceptions are anticipated.
			// 结论2：应避免在finally块中包含return语句。如果你在前面的语句中包含了return语句或重新抛出了异常，又在finally块中包含了return语句，说明你概念混淆，没有理解finally块的意义。
			// return;
		}
	}

	/**
	 * The try-with-resources Statement.
	 * <p>
	 * if exceptions are thrown from both the try block and the try-with-resources statement, 
	 * then method throws the exception thrown from the try block; 
	 * the exception thrown from the try-with-resources block is suppressed.     
	 * </p>     
	 * @param path
	 * @return
	 * @throws IOException
	 * @author Thomas Lee
	 * @version 2017年5月11日 下午7:52:30
	 */
	@SuppressWarnings("unused")
	private static void readFirstLineFromFile(String path) {
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			throw new RuntimeException("try-with-resources.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String s;

	/**
	 * 测试finally语句块中的赋值，为什么没有改变返回值.
	 * @return 结果.
	 * @author Thomas Lee
	 * @version 2017年5月12日 上午11:43:28
	 */
	public String testReturn() {
		try {
			s = "dev.";
			return s;
		} finally {
			s = "override variable s.";
			System.out.println("Entry in finally Block.");
		}
	}

}