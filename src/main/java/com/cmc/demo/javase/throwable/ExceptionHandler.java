package com.cmc.demo.javase.throwable;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * 统一异常处理.
 * @author  Thomas Lee
 * @version 2017年7月2日 下午12:43:07
 */
@Slf4j
@WebServlet("/exceptionhandler")
public class ExceptionHandler extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Throwable throwable = (Throwable) req.getAttribute("javax.servlet.error.exception");
        log.error(throwable.getMessage(), throwable);
        /*
        try (PrintWriter writer = resp.getWriter()) {
            writer.write("none");
        }
        */
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}