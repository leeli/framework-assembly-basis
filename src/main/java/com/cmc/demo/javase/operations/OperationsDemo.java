package com.cmc.demo.javase.operations;

/**
 * 基本运算.
 * @author  Mike Lee
 * @version 2017年8月30日 下午3:13:22
 * @version 2017年9月12日 下午2:50:12
 */
public class OperationsDemo {
	public static void main(String[] args) {
		testShift();

		// 下面实例一定程度上证明位运算比除法快.
		/*
		Integer a = 1000;
		Integer b = 1000;
		long curA = System.nanoTime();
		for (int i = 0; i < 1000000; i++)
			a /= 2;
		System.out.println(System.nanoTime() - curA);

		long curB = System.nanoTime();
		for (int i = 0; i < 1000000; i++)
			b >>= 2;
		System.out.println(System.nanoTime() - curB);
		*/
	}

	/**
	 * 测试++a和a++.
	 * @author  Mike Lee
	 * @version 2017年8月30日 下午3:11:13
	 */
	public static void testPlusPlus() {
		int a = 1;
		// a++先和其他数进行运算，然后自增.
		int b = a++ + 1;
		// ++a先自增，然后和其他数进行运算.
		// int b = ++a + 1;
		System.out.println(b);
		System.out.println(a);
	}

	/**
	 * 测试移位运算.
	 * @author  Mike Lee
	 * @version 2017年8月30日 下午3:14:34
	 */
	public static void testShift() {
		int a = 5;
		int b = a >> 1;
		System.out.println(b);
	}
}