package com.cmc.demo.javase.jvm.classloader.filesystemclassloder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 自定义文件系统类加载器.
 * 
 * <p>本质上是获取二进制流文件，从哪里获取不重要。文件系统类加载器、网络类加载器知识二进制流的来源不同而已.
 * 
 * @author  Mike Lee
 * @version 2017年11月8日 下午6:32:39
 */
public class FileSystemClassLoader extends ClassLoader {
    public static void main(String[] args) throws ClassNotFoundException {
        /*
         * loadClass(String)方法执行步骤：
         * 
         * 1. Invoke findLoadedClass(String) to check if the class has already been loaded.
         * 2. Invoke the loadClass method on the parent class loader. If the parent is null the class loader built-in to the virtual machine is used, instead.
         * 3. Invoke the findClass(String) method to find the class.
         */
        // 一般的ClassLoader默认的模式是双亲委托代理模式，可以通过调用自定义的findClass(String)改变这种代理模式.
        Class<?> c = new FileSystemClassLoader("C:/Users/Chuanbinli/Desktop/").loadClass("com.cmc.demo.javase.jvm.classloader.Foo");
        System.err.println(c.hashCode());
    }

    private String rootDir;

    public FileSystemClassLoader(String rootDir) {
        this.rootDir = rootDir;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class<?> c = findLoadedClass(name);
        if (c != null)
            return c;
        try {
            /*
             * 该步骤可以自定义，以取代JVM默认的双亲委托代理模式. 
             */
            c = getParent().loadClass(name);
        } catch (ClassNotFoundException e) {
        }
        if (c != null)
            return c;
        byte[] b = this.getClassBytes(name);

        /*
         * Converts an array of bytes into an instance of class Class. Before the Class can be used it must be resolved.
         * 这一步是关键步骤：把二进制类文件转换为Class对象，即在堆中生成方法区中类信息的引用对象.
         */
        return defineClass(name, b, 0, b.length);
    }

    private byte[] getClassBytes(String name) {
        String path = rootDir + "/" + name.replace(".", "/") + ".class";
        try (FileInputStream fis = new FileInputStream(new File(path)); ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            byte[] b = new byte[1024];
            int len = 0;
            while ((len = fis.read(b)) != -1)
                baos.write(b, 0, len);
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}