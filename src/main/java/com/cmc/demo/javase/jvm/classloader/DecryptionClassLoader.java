package com.cmc.demo.javase.jvm.classloader.decryptionclassLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 解密类加载器.
 * 
 * @author  Mike Lee
 * @version 2017年11月10日 下午5:04:08
 */
public class DecryptionClassLoader extends ClassLoader {
    public static void main(String[] args) throws ClassNotFoundException {
        String name = "com.cmc.demo.javase.jvm.classloader.Foo";
        EncyptionUtils.encrypt(new File("C:/Users/Chuanbinli/Desktop/" + name.replace(".", "/") + ".class"), new File("C:/Users/Chuanbinli/Desktop/temp/" + name.replace(".", "/") + ".class"));
        Class<?> c = new DecryptionClassLoader().loadClass("com.cmc.demo.javase.jvm.classloader.Foo");
        System.out.println(c);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] b = getClassData(name);
        return defineClass(name, b, 0, b.length);
    }

    private byte[] getClassData(String name) {
        String inFilepath = "C:/Users/Chuanbinli/Desktop/temp/" + name.replace(".", "/") + ".class";
        try (InputStream is = new FileInputStream(new File(inFilepath)); ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
            for (int b; (b = is.read()) != -1;)
                baos.write(b ^ 0xff);
            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private static class EncyptionUtils {
        public static void encrypt(File in, File out) {
            try (InputStream is = new FileInputStream(in); OutputStream os = new FileOutputStream(out);) {
                for (int b; (b = is.read()) != -1;)
                    // 取反加密.实际情况下为了安全，要复杂的多.
                    // os.write(b ^ 0xff);
                    os.write(~b);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}