package com.cmc.demo.javase.jvm.reference;

import java.lang.ref.WeakReference;

/**
 * 测试对象引用.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午2:13:36
 */
public class ReferenceDemo {
	public static void main(String[] args) {
		new ReferenceDemo().testWeakReference();
	}

	/**
	 * 测试弱引用.
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午2:28:06
	 */
	public void testWeakReference() {
		// 字符串常量池中共享的常量，不能回收.
//		String name = "Mike";
		String name = new String("Mike");
		// 建立对name（name则是对字符串对象"Mike"的引用.）的弱引用对象.
		WeakReference<String> weakReference = new WeakReference<String>(name);
		System.out.println("GC运行前，变量name的值为：" + weakReference.get());
		// 断开引用.
		name = null;
		// 通知回收.
		System.gc();
		System.runFinalization();
		System.out.println("GC运行前，变量name的值为：" + weakReference.get());
		// GC运行前，变量name的值为：Mike
		// GC运行前，变量name的值为：null
	}
}