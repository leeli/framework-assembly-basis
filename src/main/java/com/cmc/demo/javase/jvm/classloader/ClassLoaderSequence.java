package com.cmc.demo.javase.jvm.classloader;

class A {
    static {
        System.out.println(1);
    }

    public A() {
        System.out.println(3);
    }
}

class B extends A {
    static {
        System.out.println(2);
    }

    public B() {
        System.out.println(4);
    }
}

public class ClassLoaderSequence {

    public static void main(String[] args) {
        new B();
    }

}