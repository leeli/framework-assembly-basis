package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池——自定义线程池.
 * <p>
 * 处理任务的优先级为：
 *     核心线程corePoolSize、
 *     任务队列workQueue、
 *     最大线程maximumPoolSize，
 * 如果三者都满了，使用handler处理被拒绝的任务。
 * </p>
 * @author Thomas Lee
 * @version 2017年5月7日 下午3:52:41
 */
public class CustomThreadPool {

    private static final int CORE_POOL_SIZE = 1;
    private static final int MAXIMUM_POOL_SIZE = 3;
    private static final long KEEP_ALIVE_TIME_SECONDS = 3;
    private static final int BLOCKING_QUEUE_CAPACITY = 4;

    public static void main(String[] args) {
        // 创建等待队列.
        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(BLOCKING_QUEUE_CAPACITY);
        // 创建线程不能被执行时候的处理策略.
        RejectedExecutionHandler rejExecHandler = new ThreadPoolExecutor.AbortPolicy();
        // 创建一个单线程执行程序，它可安排在给定延迟后运行命令或者定期地执行. 
        ThreadPoolExecutor pool = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAXIMUM_POOL_SIZE, 
                KEEP_ALIVE_TIME_SECONDS, 
                TimeUnit.SECONDS,
                blockingQueue,
                rejExecHandler);
        // 创建实现了Runnable接口对象，Thread对象当然也实现了Runnable接口. 
        Thread t1 = new Thread(new MyRunner());
        Thread t2 = new Thread(new MyRunner());
        Thread t3 = new Thread(new MyRunner());
        Thread t4 = new Thread(new MyRunner());
        Thread t5 = new Thread(new MyRunner());
        Thread t6 = new Thread(new MyRunner());
        Thread t7 = new Thread(new MyRunner());

        // 将线程放入池中进行执行.
        pool.execute(t1);
        pool.execute(t2);
        pool.execute(t3);
        pool.execute(t4);
        pool.execute(t5);
        pool.execute(t6);
        pool.execute(t7);
        // 关闭线程池.
        pool.shutdown();
    }

}