package com.cmc.demo.javase.thread.unsafedemo;

public class ThreadUnsafeDemo {

    public static void main(String[] args) {
        Calculator calculator = new ThreadUnsafeDemo().new Calculator();
        Runnable runner1 = new ThreadUnsafeDemo().new MyRunner(calculator, 1000);
        Runnable runner2 = new ThreadUnsafeDemo().new MyRunner(calculator, 5000);
        new Thread(runner1, "ThreadX").start();
        new Thread(runner2, "ThreadY").start();
    }

    private class Calculator {
        private int num = 1000;

        /*
        // 此时该方法线程不安全，也就是说当ThreadX还没有执行完成，ThreadY可能就进入了该方法中，导致实际操作期望的结果和实际不一样.
        // 即，可能出现连个数字一样，这样也就是说并行执行该方法.
        public int substract() {
            return --this.NUM;
        }
        */

        // 把一个同步的方法修改为非同步的方法.
        public int substract() {
            synchronized (this) {
                return --this.num;
            }
        }

    }

    private class MyRunner implements Runnable {
        private Calculator calculator;

        public MyRunner(Calculator calculator, long mills) {
            this.calculator = calculator;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(20);
                    System.out.println(Thread.currentThread().getName() + "减1的结果为：" + this.calculator.substract());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}