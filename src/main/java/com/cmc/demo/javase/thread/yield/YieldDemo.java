package com.cmc.demo.javase.thread.yield;

/**
 * Java线程：线程的调度-让步. 
 * @author Thomas Lee
 * @version 2017年5月16日 下午8:54:11
 */
public class YieldDemo {
    public static void main(String[] args) {
        Thread t1 = new MyThread1();
        Thread t2 = new Thread(new MyRunnable());
        t1.start();
        t2.start();
    }
}

class MyThread1 extends Thread {
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("线程1第" + i + "次执行！");
        }
    }
}

class MyRunnable implements Runnable {
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("线程2第" + i + "次执行！");
            Thread.yield();
        }
    }
}