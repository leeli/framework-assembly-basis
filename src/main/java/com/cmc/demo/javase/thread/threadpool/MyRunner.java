package com.cmc.demo.javase.thread.threadpool;

/**
 * MyRunner for thread pool.
 * @author Thomas Lee
 * @version 2017年5月12日 下午3:33:31
 */
class MyRunner implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " is on ...");
    }
}