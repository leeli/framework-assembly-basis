package com.cmc.demo.javase.thread.priority;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 继承Thread接口.
 * @author Thomas Lee
 * @version 2017年3月28日 下午5:23:32
 */
public class PriorityDemo extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(PriorityDemo.class);
    public static final long INTERVAL = 1000;

    public static void main(String[] args) {
        Thread myThred = new PriorityDemo();
        myThred.start();
        System.out.println("主线程main的优先级：" + Thread.currentThread().getPriority());
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(INTERVAL);
                System.out.println("heart beats ... ");
                System.out.println("当前线程的优先级为：" + Thread.currentThread().getPriority());
            }
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
    }

}