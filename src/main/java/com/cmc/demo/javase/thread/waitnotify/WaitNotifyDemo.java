package com.cmc.demo.javase.thread.waitnotify;

/** 
* 获取计算结果并输出 
* @author leizhimin 2008-9-20 11:15:22 
*/
public class WaitNotifyDemo extends Thread {
    Calculator calculator;

    public WaitNotifyDemo(Calculator c) {
        this.calculator = c;
    }

    public void run() {
        synchronized (calculator) {
            try {
                System.out.println(Thread.currentThread() + "等待计算结果。。。");
                calculator.wait();
                System.out.println(Thread.currentThread() + "计算结果为：" + calculator.total);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        // 启动三个线程，分别获取计算结果 
        new WaitNotifyDemo(calculator).start();
        new WaitNotifyDemo(calculator).start();
        new WaitNotifyDemo(calculator).start();
        // 为什么这里没有进行notify()就执行了？
        calculator.start();
        // calculator.notify();
    }

}