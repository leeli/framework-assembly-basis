package com.cmc.demo.javase.thread.sleep;

/**
 * 线程调度——休眠实例.
 * @author Thomas Lee
 * @version 2017年5月16日 下午8:35:54
 */
public class SleepDemo {

    public static void main(String[] args) {
        new Thread(new SleepDemo().new MyRunner()).start();
    }

    private class MyRunner implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " is on ...");
            }
        }
    }

}