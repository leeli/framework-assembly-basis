package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

/**
 * 固定大小线程池实例.
 * @author Thomas Lee
 * @version 2017年5月7日 下午2:59:37
 */
public class FixedThreadPool {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.shutdown();
    }
}