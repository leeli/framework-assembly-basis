package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 可变尺寸线程池.
 * 
 * Creates a thread pool that creates new threads as needed, but
 * will reuse previously constructed threads when they are available.
 * 可以使用已经创建、还没有被销毁的线程，具体请参考源码.
 * @author Thomas Lee
 * @version 2017年5月19日 下午7:42:48
 */
public class CachedThreadPool {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        Thread.sleep(1000);
        pool.execute(new Thread(new MyRunner()));
    }
}