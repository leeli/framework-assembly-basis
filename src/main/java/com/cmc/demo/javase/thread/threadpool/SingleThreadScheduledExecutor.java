package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SingleThreadScheduledExecutor {
    
    public static void main(String[] args) {
        ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.shutdown();
    }
    
}