package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单任务线程池. 
 * @author Thomas Lee
 * @version 2017年5月19日 下午7:30:11
 */
public class SingleThreadExecutor {
    public static void main(String[] args) {
        ExecutorService pool = Executors.newSingleThreadExecutor();
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.shutdown();
    }
}