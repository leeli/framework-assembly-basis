package com.cmc.demo.javase.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 延迟加载线程池.
 * @author Thomas Lee
 * @version 2017年5月22日 下午2:18:28
 */
public class ScheduledThreadPool {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService pool = Executors.newScheduledThreadPool(2);
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.execute(new Thread(new MyRunner()));
        pool.shutdown();
    }
}