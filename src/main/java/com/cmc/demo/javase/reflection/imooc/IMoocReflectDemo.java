package com.cmc.demo.javase.reflection.imooc;

/**
 * 慕课网反射实例. 
 * @site: http://www.imooc.com/learn/199
 * @author Thomas Lee
 * @version Jun 9, 2017 10:40:41 AM
 */
public class IMoocReflectDemo {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		/* 下面介绍反射之Class类的使用. */
		// Foo的（实例）对象通过foo来表示.
		Foo foo = new Foo();
		// Foo这个类也是一个对象，是Class类的对象，如何表示呢？
		// 任何一个类都是java.lang.Class的实例对象，这个实例对象有三种表示方式.
		// 官网：classFoo1和classFoo2表示了Foo类的（类）类型（class type）.
		// 万事万物皆对象.
		// 第一种表示方式（实际上告诉我们任何一个类都有一个隐含的静态变量class）：通过类的隐藏（静态）变量
		Class<Foo> classFoo1 = Foo.class;
		// 第二种表示方式：通过类对象的getClass方法.
		Class<? extends Foo> classFoo2 = foo.getClass();
		System.out.println(classFoo1 == classFoo2);
		// 第三种表示方式：通过静态方法Class.forName().
		@SuppressWarnings("unused")
		Class<?> classFoo3 = Class.forName("com.cmc.demo.javase.reflection.imooc.Foo");

		// 可以通过类的类型创建该类的对象，即通过classFoo1、classFoo2、classFoo3创建
		Foo foo1 = classFoo1.newInstance();
		foo1.print();

		/* 下面介绍反射之动态加载类. */
		// 运行的时候加载类.
	}
}