package com.cmc.demo.javase.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.cmc.user.facade.entity.User;

/**
 * JavaSE Reflect demo.
 * 
 * @author  Mike Lee
 * @version 2017年2月16日 下午3:20:02
 * @version 2017年9月15日 下午3:45:30
 */
public class ReflectDemo {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
//		String clazzQualifier = "com.cmc.user.facade.entity.User";
//		new ReflectionDemo().printFields("com.cmc.user.facade.entity.User");
//		new ReflectionDemo().printClazz(clazzQualifier);
//		new ReflectDemo().printInstance(clazzQualifier);
		new ReflectDemo().testReflectPerformance();
	}

	/**
	 * 打印类类型.
	 * 
	 * @param clazzQualifier
	 * @throws ClassNotFoundException
	 * @author Thomas Lee
	 * @version 2017年4月14日 下午3:41:19
	 */
	public void printClazz(String clazzQualifier) throws ClassNotFoundException {
		// 方法一；通过类的方法forName()获取
		Class<?> clazz = Class.forName(clazzQualifier);

		// 方法二：通过类直接获取
		// Class<?> clazz = User.class;

		// 方法三：通过对象获取
		// User user = new User();
		// Class<?> clazz = user.getClass();
		System.out.println(clazz.toString());
	}

	/**
	 * 打印类的实例.
	 * 
	 * @param clazzQualifier
	 * @throws ClassNotFoundException
	 * @author Thomas Lee
	 * @version 2017年4月14日 下午3:44:50
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 */
	public void printInstance(String clazzQualifier) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		// 方法一：通过类对象调用newInstance()方法
		Class<?> clazz = Class.forName(clazzQualifier);
		@SuppressWarnings("unused")
		User user = (User) clazz.newInstance();
		System.out.println(clazz.newInstance().toString());

		// 方法二：反射调用构造方法，通过类对象的getConstructor()或getDeclaredConstructor()方法获得构造器（Constructor）对象并调用其newInstance()方法创建对象.
		System.out.println(String.class.getConstructor(String.class).newInstance("hello"));
	}

	/**
	 * 打印类的属性.
	 * 
	 * <p>Class: Instances of the class Class represent classes and interfaces in a running Java application.
	 * 
	 * @param clazzQualifier 限定名称
	 * @throws ClassNotFoundException
	 * @author Thomas Lee
	 * @version 2017年2月16日 下午5:35:43
	 * @version 2017年4月14日 下午3:17:51
	 */
	public void printFields(String clazzQualifier) throws ClassNotFoundException {
		// 第一种方式；通过类的方法forName()获取
		Class<?> clazz = Class.forName(clazzQualifier);

		// 第二种方式：通过类直接获取
		// Class<?> clazzUser = User.class;

		// 第三种方式：通过对象获取
		// User user = new User();
		// Class<?> clazzUser = user.getClass();

		Field[] classFields = clazz.getDeclaredFields();

		StringBuilder sb = new StringBuilder();
		sb.append(Modifier.toString(clazz.getModifiers()) + " class " + clazz.getSimpleName() + " {\n");
		for (Field userField : classFields) {
			sb.append("\t" + Modifier.toString(userField.getModifiers()) + " " + userField.getType().getSimpleName() + " " + userField.getName() + ";\n");
		}
		sb.append("}");

		System.out.println(sb.toString());
	}

	/**
	 * 测试直接调用方法、通过反射调用以及去除访问检查三种情况下进行方法调用程序性能比较.
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月14日 下午4:27:45
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public void testReflectPerformance() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		// 普通方式调用方法.
		Aux fooA = new Aux();
		long currentA = System.currentTimeMillis();
		for (int i = 0; i < 100000000L; i++)
			fooA.method();
		System.out.println("普通方式调用10亿次方法执行时间为：" + (System.currentTimeMillis() - currentA) + "ms.");
		
		// 通过普通的反射形式调用方法.
		Method methodB = Aux.class.getDeclaredMethod("method");
//		methodB.setAccessible(Boolean.TRUE);
		long currentB = System.currentTimeMillis();
		for (int i = 0; i < 100000000L; i++)
			methodB.invoke(fooA);
		System.out.println("普通反射方式调用10亿次方法执行时间为：" + (System.currentTimeMillis() - currentB) + "ms.");
		
		// 通过去除访问检查的反射形式调用方法.
		Method methodC = Aux.class.getDeclaredMethod("method");
		methodC.setAccessible(Boolean.TRUE);
		long currentC = System.currentTimeMillis();
		for (int i = 0; i < 100000000L; i++)
			methodC.invoke(fooA);
		System.out.println("去除访问检查的反射方式调用10亿次方法执行时间为：" + (System.currentTimeMillis() - currentC) + "ms.");
		// JDK8 
		// 普通方式调用10亿次方法执行时间为：42ms.
		// 普通反射方式调用10亿次方法执行时间为：652ms.
		// 去除访问检查的反射方式调用10亿次方法执行时间为：427ms.
	}

	
	
	private static class Aux {
		public void method() {
		}
	}
}