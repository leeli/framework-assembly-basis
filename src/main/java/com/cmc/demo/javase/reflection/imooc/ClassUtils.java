package com.cmc.demo.javase.reflection.imooc;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import lombok.extern.slf4j.Slf4j;

/**
 * 慕课网反射实例 -- Class工具类. 
 * @site: http://www.imooc.com/learn/199
 * @author Thomas Lee
 * @version Jun 9, 2017 10:40:41 AM
 * @version 2017年6月19日 上午11:09:56
 */
@Slf4j
public class ClassUtils {
	public static void main(String[] args) {
		// Integer b = 0;
		// printClassMessage(b);
		A a = new ClassUtils().new A();
		method(a);
	}

	/**
	 * 打印成员变量信息.
	 * @param obj 对象.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 1:27:04 PM
	 */
	public static void pringFieldMessage(Object obj) {
		log.info("开始打印成员变量信息...");
		Class<? extends Object> c = obj.getClass();
		Field[] fs = c.getDeclaredFields();
		for (Field field : fs) {
			// 得到成员变量类型的类类型.
			Class<?> fieldType = field.getType();
			String typeName = fieldType.getSimpleName();
			// String typeName = fieldType.getName();
			System.out.print(typeName + " ");
			// 得到成员变量的名称.
			String fieldName = field.getName();
			System.out.print(fieldName);
			System.out.println();
		}
	}

	/**
	 * 打印成员方法信息.
	 * @param   obj 对象.
	 * @author  Thomas Lee
	 * @version Jun 12, 2017 1:28:11 PM
	 * @version 2017年6月20日 上午10:26:29
	 */
	public static void printMethodMessage(Object obj) {
		System.out.println("\n");
		log.info("开始打印成员方法信息...");
		// 要获取类的信息，首先要获取类的（类）类型.
		// 传递的是哪个子类的对象，c就是该子类的类型.
		Class<? extends Object> c = obj.getClass();
		// log.info(c.getName());

		/*
		 * Method类，方法对象.
		 * 一个成员方法就是一个Method对象.
		 * getMethods()方法获取的是素有public的方法，包含父类的.
		 * getDeclaredMethods()方法获取的是所有该类自己声明的方法.
		 * getName()方法获取的是binary name（科学命名法）
		 * getSimpleName()方法获取的是关键词对应的名称.
		 */
		Method[] ms = c.getMethods(); // c.getDeclaredMethods();
		for (Method method : ms) {
			// method.getModifiers();
			// 获取方法的返回值类型.
			Class<?> returnType = method.getReturnType();
			System.out.print(returnType.getSimpleName() + " ");
			// 得到方法的名称.
			System.out.print(method.getName() + "(");
			// 获取参数类型，即参数列表的类型的（类）类型.
			Class<?>[] paramTypes = method.getParameterTypes();
			if (paramTypes.length == 0)
				System.out.print(")");
			for (Class<?> class1 : paramTypes) {
				if (class1 == paramTypes[paramTypes.length - 1])
					System.out.print(class1.getSimpleName() + ")");
				else
					System.out.print(class1.getSimpleName() + ", ");
			}
			System.out.println();
		}
	}

	/**
	 * 打印对象的构造方法信息.
	 * @param obj 对象.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 1:46:01 PM
	 */
	public static void printConstructorMessage(Object obj) {
		System.out.println("\n");
		log.info("开始打印构造方法信息...");
		Class<? extends Object> c = obj.getClass();
		/*
		 * 构造方法也是对象.
		 * java.lang.Constructor中封装了构造方法的信息.
		 */
		Constructor<?>[] cs = c.getDeclaredConstructors();
		for (Constructor<?> constructor : cs) {
			System.out.print(constructor.getName() + "(");
			Class<?>[] paramTypes = constructor.getParameterTypes();
			for (Class<?> class1 : paramTypes) {
				if (class1 == paramTypes[paramTypes.length - 1])
					System.out.print(class1.getSimpleName() + ")");
				else
					System.out.print(class1.getSimpleName() + ", ");
			}
			System.out.println();
		}
	}

	/**
	 * 打印类的信息，包括类的成员变量和成员方法. 
	 * @param obj 对象.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 11:07:27 AM
	 */
	public static void printClassMessage(Object obj) {
		pringFieldMessage(obj);
		printConstructorMessage(obj);
		printMethodMessage(obj);
	}

	/**
	 * 方法的反射以及操作.
	 * @param obj 对象.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 1:59:27 PM
	 */
	public static void method(Object obj) {
		Class<? extends Object> c = obj.getClass();
		try {
			// 对方法进行反射操作.
			Method m = c.getMethod("print", new Class[] { int.class, int.class });
			m.invoke(obj, 1, 2);
			// 对于没有参数的方法.
			// 方法一：传入一个空的数组.
			Method methodForNoArg1 = c.getMethod("print", new Class[] {});
			methodForNoArg1.invoke(obj, new Object[] {});
			// 方法二：可以不传Class<?>...定义的参数.
			Method methodForNoArg2 = c.getMethod("print");
			methodForNoArg2.invoke(obj);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 反射辅助类.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 2:27:39 PM
	 */
	class A {
		/**
		 * 无参方法.
		 * @author Thomas Lee
		 * @version Jun 12, 2017 2:27:29 PM
		 */
		public void print() {
			System.out.println("Hello World.");
		}

		public void print(int a, int b) {
			System.out.println(a + b);
		}

		public void print(String a, String b) {
			System.out.println(a + b);
		}
	}
}