package com.cmc.demo.javase.reflection.imooc;

/**
 * 伪（相对于真实）类.
 * <p> 程序员倾向于使用术语“foo”(FOO的发音)作为在讨论想法或者是提出例子时一些确实存在的东西的普遍替代语。
 * @author Thomas Lee
 * @version Jun 9, 2017 10:53:34 AM
 */
public class Foo {

	public void print() {
		System.out.println("new Foo().print()...");
	}

}