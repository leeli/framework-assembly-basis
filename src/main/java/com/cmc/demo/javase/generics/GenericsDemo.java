package com.cmc.demo.javase.generics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

/**
 * 泛型工具类.
 * 
 * @author  Mike Lee
 * @author  Thomas Lee
 * @version 2017年9月15日 下午4:43:14
 * @version 2017年9月5日 下午6:59:52
 * @version Jun 13, 2017 10:30:29 AM
 */
public class GenericsDemo {
	public static void main(String[] args) {
	}

	/**
	 * 通过反射了解泛型的本质.
	 * 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public void testEssenceByReflect() throws Exception {
		// 可以存放任意类型对象.
		ArrayList<?> list1 = new ArrayList<>();
		// 只可以存放String类型.
		ArrayList<String> list2 = new ArrayList<String>();
		list2.add("hello");

		Class<?> c1 = list1.getClass();
		Class<?> c2 = list2.getClass();

		/*
		 * 结果为true.
		 * 说明编译之后集合是去泛型化的，也就是说编译之后泛型是不算在类类型中的。
		 * 结论：Java中集合的泛型，是为了防止错误输入的，只是在编译期间有效，运行期间无效。
		 */
		System.out.println(c1 == c2);

		// 验证泛型是不是只在编译期间有效.
		Method m = c2.getMethod("add", Object.class);
		// 此处向只能存放String类型的数组存放整型.
		m.invoke(list2, 100);
		System.out.println(list2.size());
		// 此时for循环会报出ClassCaseException.
		for (String member : list2) {
			System.out.println(member);
		}
	}

	/**
	 * 测试super和extends关键字.
	 * 
	 * <p>结果：super和extends都包含本身.
	 */
	public <T extends Aux> void testSuperExtends(T t) {
		System.out.println(t);
	}

	/**
	 * 泛型转换.
	 * 
	 * <p>暂时放到反射之后进行研究 - 学习完成之后进一步封装基本的CRUD业务操作（面向接口编程）.
	 * 
	 * @param entity 实体类类型.
	 * @return 
	 */
	public Class<?> getActualType(Class<?> entity) {
		ParameterizedType parameterizedType = (ParameterizedType) entity.getGenericSuperclass();
		return (Class<?>) parameterizedType.getActualTypeArguments()[0];
	}

	private static class Aux {
	}
}