package com.cmc.demo.javase.generics;

import lombok.Data;

/**
 * 通用返回结果.
 * @author  Thomas Lee
 * @version 2017年6月8日 上午12:11:49
 */
@Data
public class Result<T> {
	/** 错误码 */
	private Integer code;
	/** 提示信息 */
	private String msg;
	/** 具体内容 */
	private T data;
}