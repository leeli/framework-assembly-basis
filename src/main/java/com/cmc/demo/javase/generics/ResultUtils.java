package com.cmc.demo.javase.generics;

/**
 * 通用返回结果工具类.
 * 
 * @author  Mike Lee
 * @version 2017年6月8日 上午12:20:46
 */
public class ResultUtils<E> {
	/**
	 * 返回通用成功信息.
	 * 
	 * @param t          泛型.
	 * @return Result<T> 通用返回结果.
	 * @author  		  Mike Lee
	 * @version 		  2017年6月8日 上午12:23:33
	 * @version 2017年6月26日 下午3:00:28
	 */
	public static <T> Result<T> success(T t) {
		Result<T> result = new Result<T>();
		result.setCode(ResultEnum.SUCCESS.getCode());
		result.setMsg(ResultEnum.SUCCESS.getDesc());
		result.setData(t);
		return result;
	}

	/**
	 * 返回通用失败信息.
	 * 
	 * @param resultEnum 错误结果枚举.
	 * @return Result<T> 通用返回结果.
	 * @author 			  Mike Lee
	 * @version 		  2017年6月8日 上午12:25:38
	 * @version 2017年6月26日 下午3:00:21
	 */
	public static <T> Result<T> failure(ResultEnum resultEnum) {
		Result<T> result = new Result<T>();
		result.setCode(resultEnum.getCode());
		result.setMsg(resultEnum.getDesc());
		return result;
	}

	/**
	 * 返回通用失败结果.
	 * 
	 * @param code 		    错误代码.
	 * @param message 	    错误信息.
	 * @return Result<T> 通用返回结果.
	 * @author 			  Mike Lee
	 * @version    	  2017年6月26日 下午3:01:39
	 */
	public static <T> Result<T> failure(Integer code, String message) {
		Result<T> result = new Result<T>();
		result.setCode(code);
		result.setMsg(message);
		return result;
	}
}