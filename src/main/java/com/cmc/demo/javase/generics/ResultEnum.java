package com.cmc.demo.javase.generics;

import lombok.Getter;

/**
 * 通用结果枚举.
 * @author  Thomas Lee
 * @version 2017年6月26日 下午2:55:59
 */
@Getter
public enum ResultEnum {
	SERVER_ERROR(500, "未知错误"), 
	SUCCESS(200, "成功"),
	VALIDATION_FAILURE(600, "数据不符合要求");
	
	private Integer code;
	private String desc;

	ResultEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}
}