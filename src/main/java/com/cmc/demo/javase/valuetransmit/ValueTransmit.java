package com.cmc.demo.javase.valuetransmit;

import com.cmc.user.facade.entity.User;

/**
 * Java值传递.
 * <p>
 * Java中的参数传递都是值传递，只不过对象传递的是指针.
 * 需要深入理解JVM.
 * </p>
 * @author Thomas Lee
 * @version 2017年5月16日 上午9:55:33
 */
public class ValueTransmit {

    public static void main(String[] args) {
        User user = new User();
        user.setAge(10);
        changeUser(user);
        int a = 9;
        changeBasicValue(a);
        System.out.println(user.getAge());
        System.out.println(a);
    }

    private static void changeBasicValue(int a) {
        a = 10;
    }

    private static void changeUser(User user) {
        user.setAge(11);
        User userMirror = new User();
        userMirror.setAge(12);
        user = userMirror;
        System.out.println("tmp");
    }

}