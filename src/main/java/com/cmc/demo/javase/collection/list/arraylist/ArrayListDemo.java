package com.cmc.demo.javase.collection.list.arraylist;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/**
 * ArrayList Demo.
 * 
 * <p>实现功能：
 * 1. 构造方法；
 * 2. 添加方法；
 * 3. 删除方法.
 * 
 * @author  Mike Lee
 * @version 2017年9月11日 上午10:59:44
 */
public class ArrayListDemo {
	public static void main(String[] args) {
		List<Integer> strings = new ArrayListDemo().new ArrayList<Integer>();
		for (int i = 0; i < 15; i++)
			strings.add(i);
		System.out.println(strings);
	}

	@SuppressWarnings({ "serial" })
	private class ArrayList<E> extends AbstractList<E> implements List<E>, RandomAccess, Cloneable, java.io.Serializable {
		private Object[] elementData;
		private int size;

		public ArrayList() {
			this(5);
		}

		public ArrayList(int initialCapacity) {
			// validate initialCapacity.
			elementData = new Object[initialCapacity];
		}

		@Override
		public boolean add(Object e) {
			if (size + 1 >= elementData.length)
				grow();
			elementData[size++] = e;
			return true;
		}

		private void grow() {
			int oldCapacity = elementData.length;
			int newCapacity = oldCapacity + (oldCapacity >> 1);
			Object[] newElementData = new Object[newCapacity];
			System.arraycopy(elementData, 0, newElementData, 0, elementData.length);
			elementData = newElementData;
		}

		@Override
		public E remove(int index) {
			// validate.
			E e = elementData(index);
			System.arraycopy(elementData, index + 1, elementData, index, size - index - 1);
			elementData[--size] = null;
			return e;
		}

		@Override
		public int size() {
			return size;
		}

		@Override
		public boolean isEmpty() {
			return size == 0;
		}

		@Override
		public boolean contains(Object o) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public Iterator iterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object[] toArray(Object[] a) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean remove(Object o) {
			// 此处不使用二分查找法是因为该方法需要元素“可比较”.
			for (int index = 0; index < elementData.length; index++) {
				if (o.equals(elementData[index])) {
					fastRemove(index);
				}
			}
			return false;
		}

		private void fastRemove(int index) {
			System.arraycopy(elementData, index + 1, elementData, index, size - index - 1);
			elementData[--size] = null;
		}

		@SuppressWarnings("unchecked")
		private E elementData(int index) {
			return (E) elementData[index];
		}

		@Override
		public boolean containsAll(Collection c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean addAll(Collection c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean addAll(int index, Collection c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean removeAll(Collection c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean retainAll(Collection c) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void clear() {
			// TODO Auto-generated method stub

		}

		@Override
		public E get(int index) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object set(int index, Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void add(int index, Object element) {
			// TODO Auto-generated method stub

		}

		@Override
		public int indexOf(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int lastIndexOf(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public ListIterator listIterator() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ListIterator listIterator(int index) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List subList(int fromIndex, int toIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String toString() {
			String rst = "";
			for (int i = 0; i < elementData.length; i++)
				rst += elementData[i] == null ? "" : elementData[i];
			return rst;
		}

		@Override
		protected Object clone() throws CloneNotSupportedException {
			// TODO Auto-generated method stub
			return super.clone();
		}
	}
}