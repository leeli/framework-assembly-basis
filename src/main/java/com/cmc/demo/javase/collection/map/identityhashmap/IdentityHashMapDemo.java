package com.cmc.demo.javase.collection.map.identityhashmap;

import java.util.IdentityHashMap;

/**
 * 
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午2:50:06
 */
public class IdentityHashMapDemo {
	public static void main(String[] args) {
		new IdentityHashMapDemo().testIdentityHashMap();
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午2:52:23
	 */
	private void testIdentityHashMap() {
		IdentityHashMap<String, Object> identityHashMap = new IdentityHashMap<String, Object>();
		identityHashMap.put("a", "A");
		identityHashMap.put("a", "A");
		identityHashMap.put(new String("a"), "A");
		identityHashMap.put(new String("a"), "A");
		System.out.println(identityHashMap.size()); // 3
	}
}