package com.cmc.demo.javase.collection.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import net.sf.json.JSONObject;

/**
 * Map实例.
 * @author  Thomas Lee
 * @author  Mike Lee
 * @version 2017年2月9日 下午3:40:48
 * @version 2017年4月1日 下午1:41:09
 * @version 2017年4月21日 下午4:09:09
 * @version 2017年9月1日 下午3:32:38
 * @version 2017年9月4日 下午4:21:42
 * @reference http://blog.csdn.net/ns_code/article/details/37867985.
 */
public class MapDemo {
	public static void main(String[] args) {
		testMapOutputOrder();
	}

	public static void testMapCapicity() {
		Map<String, String> hashMap = new HashMap<String, String>(100);
		hashMap.put("a", "bv");
		System.out.println(hashMap.size());
	}

	/**
	 * Hash table based implementation of the Map interface. 
	 * This implementation provides all of the optional map operations, and permits null values and the null key. 
	 * (The HashMap class is roughly equivalent to Hashtable, except that it is unsynchronized and permits nulls.) 
	 * This class makes no guarantees as to the order of the map; 
	 * in particular, it does not guarantee that the order will remain constant over time.
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月1日 上午11:12:09
	 */
	public static void testKey() {
		// HashMap允许key或者value为null.
		Map<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("key", 0);
		hashMap.put(null, 1);
		hashMap.put(null, 2);
		hashMap.put("key1", null);
		hashMap.put("key1", 3);
		hashMap.put(null, null);
		// {null=null, key1=3, key=0}.
		System.out.println(hashMap.size());

		// HashTable不允许key或者value为null.
		Map<String, Object> hashTable = new Hashtable<String, Object>();
		hashTable.put("key", 0);
		// hashTable.put(null, 1);
		// hashTable.put(null, 2);
		// hashTable.put("key1", null);
		hashTable.put("key1", 3);
		// hashTable.put(null, null);
		System.out.println(hashTable.size());
	}

	/**
	 * 测试任意对象作为key.
	 * Note: great care must be exercised if mutable objects are used as map keys. 
	 * The behavior of a map is not specified if the value of an object is changed 
	 * in a manner that affects equals comparisons while the object is a key in the map. 
	 * 
	 * A special case of this prohibition is that it is not permissible for a map to 
	 * contain itself as a key. While it is permissible for a map to contain itself as a value, 
	 * extreme caution is advised: the equals and hashCode methods are no longer well defined on such a map. 
	 */
	// TODO 问题：	It is not permissible for a map to contain itself as a key. 经测试可以的呀，为什么呢？
	public static void testPutUsingObj() {
		Map<Object, String> map = new HashMap<Object, String>();
		map.put(new MapDemo().new Aux(), "value1");
		map.put(new MapDemo().new Aux(), "value2");
		System.out.println(map.size());
		Integer intKey = 0;
		Integer intKey1 = 1;
		map.put(intKey, "0");
		map.put(intKey1, "1");
		intKey = 2;
		map.put(1, "2");
		System.out.println(map.size());
	}

	/**
	 * 迭代Map.
	 * @author Thomas Lee
	 * @version 2017年2月9日 下午3:44:20
	 */
	public static void demoMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "a");
		map.put("2", "b");
		map.put("3", "c");
		/* 一般的遍历方式 */
		for (Map.Entry<String, String> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
		/* 优先通过这个方式进行遍历 */
		for (Iterator<Map.Entry<String, String>> it = map.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, String> entry = it.next();
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}
		for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			System.out.println(key + " = " + map.get(key));
		}
	}

	/**
	 * 排序Map.
	 * @author Thomas Lee
	 * @version 2017年3月29日 下午3:54:32
	 */
	public static void compareMap() {
		List<Map<String, Integer>> maps = new ArrayList<Map<String, Integer>>();
		Map<String, Integer> map1 = new HashMap<String, Integer>();
		map1.put("order", 1);
		maps.add(map1);
		Map<String, Integer> map2 = new HashMap<String, Integer>();
		map2.put("order", 2);
		maps.add(map2);
		// 调用Java集合比较器，实现匿名类.
		Collections.sort(maps, new Comparator<Map<String, Integer>>() {
			@Override
			public int compare(Map<String, Integer> map1, Map<String, Integer> map2) {
				return map2.get("order") - map1.get("order");
			}
		});
		// 调用自定义的比较器.
		// Collections.sort(maps, new MyComparator<Map<String, Integer>>());
		Iterator<Map<String, Integer>> iMaps = maps.iterator();
		while (iMaps.hasNext()) {
			Map<String, Integer> map = iMaps.next();
			System.out.println(map.get("order").toString());
		}
	}

	/**
	 * 测试HashMap、TreeMap和LinkedHashMap的输出顺序.
	 * Hash table based implementation of the Map interface. 
	 * This implementation provides all of the optional map operations, and permits null values and the null key. 
	 * (The HashMap class is roughly equivalent to Hashtable, except that it is unsynchronized and permits nulls.) 
	 * This class makes no guarantees as to the order of the map; 
	 * in particular, it does not guarantee that the order will remain constant over time. TODO 此时输出“始终”是一样的顺序？
	 * 
	 * @author Thomas Lee
	 * @version 2017年4月1日 下午2:00:32
	 */
	public static void testMapOutputOrder() {
		// HashMap按照随机顺序进行输出（随机遍历）
		Map<String, String> hashMap = new HashMap<String, String>();
		hashMap.put("aa", "1");
		hashMap.put("ke", "1");
		hashMap.put("c", "1");
		hashMap.put("da", "1");
		hashMap.put("b", "1");
		for (Iterator<Entry<String, String>> ieHashMap = hashMap.entrySet().iterator(); ieHashMap.hasNext();) {
			Entry<String, String> eHashMap = ieHashMap.next();
			System.out.println("HashMap输出值：" + eHashMap.getKey() + " " + eHashMap.getValue());
		}

		System.out.println("\n");

		// TreeMap按照自然顺序或者自定顺序输出（public TreeMap(Comparator<? super K> comparator);）（自然顺序或者自定顺序遍历）.
		Map<String, String> treeMap = new TreeMap<String, String>();
		treeMap.put("aa", "1");
		treeMap.put("b", "1");
		treeMap.put("ke", "1");
		treeMap.put("da", "1");
		treeMap.put("c", "1");
		for (Iterator<Entry<String, String>> ieTreeMap = treeMap.entrySet().iterator(); ieTreeMap.hasNext();) {
			Entry<String, String> eTreeMap = ieTreeMap.next();
			System.out.println("TreeMap输出值：" + eTreeMap.getKey() + " " + eTreeMap.getValue());
		}

		System.out.println("\n");

		// LinkedHashMap按照输入的顺序进行输出（输入顺序遍历）.
		Map<String, String> linkedHashMap = new LinkedHashMap<String, String>();
		linkedHashMap.put("aa", "1");
		linkedHashMap.put("b", "1");
		linkedHashMap.put("ke", "1");
		linkedHashMap.put("da", "1");
		linkedHashMap.put("c", "1");
		for (Iterator<Entry<String, String>> ieLinkedHashMap = linkedHashMap.entrySet().iterator(); ieLinkedHashMap.hasNext();) {
			Entry<String, String> eLinkedHashMap = ieLinkedHashMap.next();
			System.out.println("LinkedHashMap输出值：" + eLinkedHashMap.getKey() + " " + eLinkedHashMap.getValue());
		}
	}

	/**
	 * Map转换为JSONObject.
	 * @author Thomas Lee
	 * @version 2017年4月14日 下午2:25:27
	 */
	public static void Map2JSONObject() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("key1", "value1");
		map.put("key2", "value2");
		map.put("key3", "value3");
		map.put("key4", "value4");

		JSONObject obj = new JSONObject();
		obj.put("map", map);
		System.out.println(obj);
	}

	/**
	 * 辅助类.
	 * @author  Mike Lee
	 * @version 2017年8月30日 下午2:15:19
	 */
	private class Aux {
	}
}

/**
 * 自定义比较器.
 * @param <T>
 * @author Thomas Lee
 * @version 2017年3月29日 下午5:01:31
 */
@SuppressWarnings({ "unchecked" })
class MyComparator<T> implements Comparator<T> {
	@Override
	public int compare(T o1, T o2) {
		if (o1 instanceof Map && o2 instanceof Map) {
			Map<String, Integer> map1 = (Map<String, Integer>) o1;
			Map<String, Integer> map2 = (Map<String, Integer>) o2;
			return map2.get("order") - map1.get("order");
		} else {
			return 0;
		}
	}
}