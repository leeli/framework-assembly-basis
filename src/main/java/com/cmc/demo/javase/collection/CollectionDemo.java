package com.cmc.demo.javase.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Collection demo.
 * @author Thomas Lee
 * @version 2017年2月9日 下午3:40:08
 */
public class CollectionDemo {
	public static void main(String[] args) {
		new CollectionDemo().testCollections();
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午3:05:14
	 */
	public void testSynchronize() {
		List<String> strings = new ArrayList<String>();
		strings.add("a");
		strings.add("b");
		strings.add("c");
		// 线程安全.
		@SuppressWarnings("unused")
		List<String> syncStrings = Collections.synchronizedList(strings);
	}

	/**
	 * 测试java.utils.Collections，对集合进行查找、排序和线程安全等操作.
	 * 
	 * @author Thomas Lee
	 * @version 2017年3月27日 下午9:39:08
	 */
	@SuppressWarnings("unchecked")
	public void testCollections() {
		List<String> list = Collections.synchronizedList(new ArrayList<String>());
		Collections.binarySearch(list, "key");
		Collections.sort(list);
		Collections.synchronizedList(list);
		
		// 空的集合.
		List<String> emptyList = (List<String>) Collections.EMPTY_LIST;
		System.out.println(emptyList.size());
		
		// 只读集合， final只能保证引用不变，通过unmodifiableList()方法可以保证引用不变的情况下，集合内容不变.
		Collections.unmodifiableList(emptyList);
//		emptyList.add("A"); // java.lang.UnsupportedOperationException
		
		// 一个元素的集合.
		List<String> oneList = Collections.singletonList("A");
		oneList.add("B"); // java.lang.UnsupportedOperationException
	}
}