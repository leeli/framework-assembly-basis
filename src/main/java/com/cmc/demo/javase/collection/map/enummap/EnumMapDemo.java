package com.cmc.demo.javase.collection.map.enummap;

import java.util.EnumMap;

/**
 * 
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午2:52:42
 */
public class EnumMapDemo {
	public static void main(String[] args) {
		new EnumMapDemo().testEnumMap();
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午2:56:48
	 */
	private void testEnumMap() {
		EnumMap<Season, Object> enumMap = new EnumMap<Season, Object>(Season.class);
		enumMap.put(Season.SPRING, "SPRING");
		enumMap.put(Season.SUMMER, "SUMMER");
		enumMap.put(Season.AUTUMN, "AUTUMN");
		enumMap.put(Season.WINTER, "WINTER");
		System.out.println(enumMap.size()); // 4
//		System.out.println(Season.SPRING instanceof Enum);
	}

	private enum Season {
		SPRING, SUMMER, AUTUMN, WINTER;
	}

}