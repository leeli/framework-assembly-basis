package com.cmc.demo.javase.collection.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Java Collections Framework List Interface Demo.
 * @author  Mike Lee
 * @version 2017年8月24日 下午4:01:50
 * @param <E>
 */
public class ListDemo<E> {
	public static void main(String[] args) {
		testListIterator();
	}

	/**
	 * List可以存放null元素.
	 * @author  Mike Lee
	 * @version 2017年8月24日 下午4:11:46
	 */
	public static void test存储多个null元素() {
		List<String> strings = new ArrayList<String>();
		strings.add(null);
		strings.add(null);
		strings.add(null);
		System.out.println(strings.size());
	}

	/**
	 * 通过listIterator()方法修改List内容.
	 * @author  Mike Lee
	 * @version 2017年8月24日 下午5:06:44
	 */
	public static void testListIterator() {
		List<String> strings = new ArrayList<String>();
		strings.add(null);
		strings.add(null);
		strings.add(null);
		System.out.println(strings.size());
		for (ListIterator<String> listItr = strings.listIterator(); listItr.hasNext();) {
			String string = listItr.next();
			listItr.set("ddd");
			listItr.remove();
			// listItr.add("new");
			System.out.println(string);
		}
	}

	/**
	 * @author  Mike Lee
	 * @version 2017年8月24日 下午5:06:44
	 */
	public static void testIterator() {
		List<String> strings = new ArrayList<String>();
		strings.add(null);
		strings.add(null);
		strings.add(null);
		System.out.println(strings.size());
		for (Iterator<String> iterator = strings.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			// listItr.remove();
			// listItr.add("new");
			System.out.println(string);
		}
	}

	/**
	 * List排序.
	 * @author  Mike Lee
	 * @version 2017年8月25日 上午11:06:18
	 */
	public static void testSort() {
		List<String> strings = new ArrayList<String>(20);
		for (int i = 0; i < 20; i++) {
			strings.add(String.valueOf(i));
		}
		System.out.println(strings.size());
		Collections.sort(strings);
		Collections.sort(strings, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		strings.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年8月29日 上午11:12:15
	 */
	public static void testConcurrentModificationException() {
		List<String> list = new LinkedList<String>();
		list.add("a");
		for (ListIterator<String> iterator = list.listIterator(); iterator.hasNext();) {
			String string = iterator.next();
			iterator.add("b");
			System.out.println(string);
		}
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年8月29日 下午1:11:20
	 */
	public void testTmp() {
		List<String> strings = new LinkedList<String>();
		strings.add(null);
		strings.add(null);
		strings.add(null);
		System.out.println(strings.size());
		for (Iterator<String> iterator = strings.listIterator(); iterator.hasNext();) {
			String string = iterator.next();
			// listItr.remove();
			// listItr.add("new");
			System.out.println(string);
		}
	}
}