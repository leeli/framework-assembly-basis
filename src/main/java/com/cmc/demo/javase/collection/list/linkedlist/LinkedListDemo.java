package com.cmc.demo.javase.collection.list.linkedlist;

/**
 * LinkedList Demo.
 * 
 * <>
 * 
 * @author  Mike Lee
 * @version 2017年9月11日 上午10:59:40
 */
public class LinkedListDemo<E> {
	private Node<E> first;
	private Node<E> last;
	private int size;

	public static void main(String[] args) {
		LinkedListDemo<Object> demo = new LinkedListDemo<Object>();
		demo.add(1);
		demo.add(2);
		demo.add(3);
		System.out.println(demo);
		System.out.println(demo.get(1));
	}

	public void add(E e) {
		Node<E> l = last;
		Node<E> node = new Node<E>(e, l, null);
		last = node;
		if (first == null) {
			first = node;
		} else {
			l.next = node;
		}
		size++;
	}

	public void remove(E e) {
	}

	public int get(E e) {
		Node<E> x = first;
		for (int index = 1; index < size; index++)
			if (e.equals(x))
				return index;
		return -1;
	}
	
	/**
	 * 这里注意一下：比较index和size的关系，决定是从头结点往中间找，还是尾节点往中间找，提升查找功能50%左右性能.
	 * @param index 要查找元素索引.
	 * @return
	 * @author      Mike Lee
	 * @version     2017年9月12日 上午11:04:40
	 */
	public E get(int index) {
		Node<E> x;
		if (index < size >> 1) {
			x = first;
			for (int i = 0; i < index; i++)
				x = first.next;
		} else {
			x = last;
			for (int i = size - 1; i > index; i--)
				x = x.prev;
		}
		return x.value;
	}

	public int size() {
		return size;
	}

	class Node<E> {
		E value;
		Node<E> prev;
		Node<E> next;

		public Node(E value, Node<E> prev, Node<E> next) {
			this.value = value;
			this.prev = prev;
			this.next = next;
		}
	}

	@Override
	public String toString() {
		String rst = "";
		if (first == null)
			return "";
		for (Node<E> e = first; e != last; e = e.next) {
			rst += e.value == null ? "" : e.value;
		}
		rst += last.value;
		return rst;
	}
}