package com.cmc.demo.javase.collection.map.weakhashmap;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * WeakHashMap Demo.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午2:34:35
 */
public class WeakHashMapDemo {
	public static void main(String[] args) {
		new WeakHashMapDemo().testWeakHashMap();
	}

	/**
	 * 
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午2:42:04
	 */
	public void testWeakHashMap() {
		Map<String, String> weakHashMap = new WeakHashMap<String, String>();
		weakHashMap.put("a", "A");
		weakHashMap.put("b", "B");
		// 弱引用键值，进行垃圾回收之后就会被回收.
		weakHashMap.put(new String("c"), "C");
		weakHashMap.put(new String("d"), "D");
		System.out.println(weakHashMap.size()); // 4
		System.gc();
		System.runFinalization();
		System.out.println(weakHashMap.size()); // 2
	}
}