package com.cmc.demo.javase.collection.map.hashmap;

import java.util.Map;

/**
 * HashMap Demo.
 * 
 * @author  Mike Lee
 * @version 2017年9月11日 上午10:59:54
 */
public class HashMapDemo<D, E> {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		HashMap<String, String> hashMap = new HashMapDemo.HashMap<String, String>();
		HashMapDemo.HashMap.Node<String, String> node = new HashMapDemo.HashMap.Node<String, String>(0, null, null, null);
	}

	static class HashMap<K, V> {
		private int size;
		private Node<K, V>[] table;

		public HashMap() {
			this(1 << 4);
		}

		@SuppressWarnings("unchecked")
		public HashMap(int initialCapacity) {
			size = initialCapacity;
			table = (Node<K, V>[]) new Node[initialCapacity];
		}

		public void put(K k, V v) {
		}

		static class Node<K, V> implements Map.Entry<K, V> {
			final int hash;
			final K key;
			V value;
			Node<K, V> next;

			Node(int hash, K key, V value, Node<K, V> next) {
				this.hash = hash;
				this.key = key;
				this.value = value;
				this.next = next;
			}

			@Override
			public K getKey() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public V getValue() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public V setValue(V value) {
				// TODO Auto-generated method stub
				return null;
			}
		}
	}
}