package com.cmc.demo.javase.collection.set.hashset;

/**
 * HashSet Demo. 
 * 
 * <p>通过HashMap实现，Set中的值放到Map中的key，因为后者key和set中的value一样不能重复.
 * <pre>private transient HashMap<E,Object> map;</pre>
 * 
 * @author  Mike Lee
 * @version 2017年9月11日 上午11:00:05
 */
public class HashSetDemo {
	public static void main(String[] args) {
	}

}