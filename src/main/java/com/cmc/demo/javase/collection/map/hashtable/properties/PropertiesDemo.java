package com.cmc.demo.javase.collection.map.hashtable.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Properties Demo.
 * <p>主要通过Properties类获取项目配置文件信息.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 上午11:17:55
 */
public class PropertiesDemo {
	private static final String FILE_PATH = "C:/Users/Chuanbinli/Desktop/tmp.txt";

	public static void main(String[] args) throws IOException {
		store(new File(FILE_PATH));
		read(new File(FILE_PATH));
		getProperty("config/config1.properties", "tmp");
	}

	/**
	 * 
	 * @param file
	 * @throws IOException
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午1:29:05
	 */
	private static void store(File file) throws IOException {
		Properties props = new Properties();
		props.setProperty("name", "lcb");
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		props.store(fileOutputStream, "save");
	}

	/**
	 * 
	 * @param file
	 * @throws IOException
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午1:29:09
	 */
	private static void read(File file) throws IOException {
		Properties props = new Properties();
		props.load(new FileInputStream(file));
		System.out.println(props.getProperty("name"));
	}

	/**
	 * 获取项目中的配置文件（而不是从系统中直接获取）.
	 * @throws IOException
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午1:49:09
	 */
	private static void getProperty(String propFilename, String propName) throws IOException {
		Properties props = new Properties();
		// props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config1.properties"));
		// 推荐这个方式获取系统配置文件.
		props.load(ClassLoader.getSystemResourceAsStream(propFilename));
		System.out.println(props.getProperty(propName));
	}
}