package com.cmc.demo.javase.collection.queue;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Queue Demo.
 * 
 * <p>add|remove|element
 * <p>offer|poll|peek
 * <p>通常认为上面一行的方法好，实际使用中也基本使用offer和poll方法.
 * 
 * @author  Mike Lee
 * @version 2017年9月12日 下午4:29:54
 */
public class QueueDemo {
	public static void main(String[] args) {
		BankBiz.dealWith(new BankBiz().deposit(10));
	}

	/**
	 * 使用队列模拟银行存款和处理存款业务.
	 * 
	 * @author  Mike Lee
	 * @version 2017年9月12日 下午4:52:38
	 */
	private static class BankBiz {
		/**
		 * 存款业务.
		 * @param size
		 * @return
		 * @author  Mike Lee
		 * @version 2017年9月12日 下午4:51:16
		 */
		public Queue<Request> deposit(int size) {
			Queue<Request> queue = new ArrayBlockingQueue<Request>(size);
			for (int i = 1; i <= size; i++) {
				final int num = i;
				// 匿名类.
				queue.offer(new Request() {
					@Override
					public void deposit() {
						System.out.println("第" + num + "存款了" + Math.random() * 100000 + "元。");
					}
				});
			}
			return queue;
		}

		/**
		 * 处理存款.
		 * @param queue
		 * @author  Mike Lee
		 * @version 2017年9月12日 下午4:53:27
		 */
		public static void dealWith(Queue<Request> queue) {
			Request req;
			// 循环可以基本使用for循环代替while，do while的场景也不多，所以实际使用中普遍使用for循环即可.
			for (; queue != null && (req = queue.poll()) != null;)
				req.deposit();
		}

		/**
		 * 存款请求.
		 * 
		 * @author  Mike Lee
		 * @version 2017年9月12日 下午4:53:43
		 */
		private static interface Request {
			public void deposit();
		}
	}
}