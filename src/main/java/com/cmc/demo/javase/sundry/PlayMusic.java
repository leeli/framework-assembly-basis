package com.cmc.demo.javase.sundry;

import java.applet.AudioClip;

import javax.swing.JApplet;

public class PlayMusic {
    public static void main(String[] args) {
        PlayMusic p = new PlayMusic();
        p.play();
    }

    public static AudioClip loadSound(String filename) {
        return JApplet.newAudioClip(ClassLoader.getSystemResource("audio/1.wav"));
    }

    public void play() {
        AudioClip christmas = loadSound("C:/Users/Thomas Lee/Desktop/1.wav");
        christmas.play();
    }
}