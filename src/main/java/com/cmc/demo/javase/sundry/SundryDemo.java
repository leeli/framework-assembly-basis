package com.cmc.demo.javase.sundry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Sundry Demo.
 * @author  Thomas Lee
 * @version 2017年7月3日 下午6:01:06
 */
@Slf4j
public class SundryDemo {
	private final static String KEY = "assistant";

	public static void main(String[] args) throws LineUnavailableException, IOException, UnsupportedAudioFileException {
        // int a = -3;
        // System.out.println(a / 2);
        // System.out.println(a >> 1);
        // System.out.println((1 < 2));
        // testTransmission();
        // byte a = (byte) (0xf + 1);
        // System.out.println(a);
        // int a = 3;
        // System.out.println(a <<= 1);
        // System.out.println(9 ^ 2);
        // System.out.println(new Integer(0).equals(new Integer(0)));
    	byte[] cons = "你好".getBytes("utf8");
    	String rst = "";
		for (int i = 0; i < cons.length; i++) {
			rst += (char) cons[i];
		}
		String b = "fdaf" + '里';
		System.out.println(rst);
		System.out.println(b);
    }

	/**
	 * 测试Object和其子类（例如，Integer和String）的关系.
	 * Class Object is the root of the class hierarchy. 
	 * Every class has Object as a superclass. 
	 * All objects, including arrays, implement the methods of this class.
	 * @author  Mike Lee
	 * @version 2017年8月30日 下午3:20:28
	 */
	public static void testTmp() {
		int a = 1;
		String b = "1";
		List<Object> objs = new ArrayList<Object>();
		objs.add(a);
		objs.add(b);
		System.out.println(objs.get(0).getClass().getSimpleName());
		System.out.println(objs.get(1).getClass().getSimpleName());
	}

	/**
	 * 测试Java中的变量传递.
	 * @author  Thomas Lee
	 * @version 2017年7月3日 下午6:12:49
	 */
	@SuppressWarnings("unused")
	private static void testTransmission() {
		Map<String, Object> map = new HashMap<String, Object>();
		Assistant assistant = new SundryDemo().new Assistant();
		assistant.setId(10);
		map.put(KEY, assistant);
		// 此时传递的是栈中对象的引用，修改其堆中的值，会影响（map）.
		assistant.setId(20);
		Assistant a = (Assistant) map.get(KEY);
		log.info(String.valueOf(a.getId()));

		Assistant assistant1 = new SundryDemo().new Assistant();
		assistant1.setId(30);
		// 此时修改的是栈中存放assistant引用的变量的值（为assistant1），assistant引用的内容没有改变，并且还是存活的，所以map的值不会变化.
		assistant = assistant1;
		Assistant b = (Assistant) map.get(KEY);
		log.info(String.valueOf(b.getId()));
	}

	@Data
	private class Assistant {
		private int id;
	}
}