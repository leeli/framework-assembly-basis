package com.cmc.demo.cache.redis;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import com.cmc.common.utils.SerializationUtils;

/**
 * Redis实例.
 * @author Thomas Lee
 * @version 2017年4月5日 下午1:50:36
 */
@Slf4j
public class RedisDemo {
    public static void main(String[] args) {
        testRedis();
    }

    @Autowired
    private ShardedJedisPool shardedJedisPool;

    /**
     * 直接通过Jedis进行缓存测试.
     * @author  Thomas Lee
     * @version 2017年3月12日 下午12:36:37
     */
    public static void testRedis() {
        try (Jedis jedis = new Jedis("192.168.176.128", 6379)) {
            log.info(jedis.ping());
            String key = "name";
            String value = "lcb";
            jedis.set(key, value);
            log.info(jedis.get(key));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 通过Spring和Redis集成.
     * @author  Thomas Lee
     * @version 2017年3月12日 下午12:35:52
     */
    public void testSpringRedis() {
        ShardedJedis jedis = shardedJedisPool.getResource();
        String key = "name";
        String value = "lcb";
        jedis.set(key, value);
        log.info(jedis.get(key));
    }

    /**
     * 存储、获取（各种类型）数据.
     * @author  Thomas Lee
     * @version 2017年6月29日 上午11:30:36
     * @throws IOException 
     * @throws ClassNotFoundException 
     */
    @SuppressWarnings({ "unused", "unchecked" })
    public void setData() throws IOException, ClassNotFoundException {
        ShardedJedis jedis = shardedJedisPool.getResource();
        // 存储获取java.lang.String.
        String strKey = "java.lang.String";
        jedis.set(strKey, "string value");
        jedis.expire(strKey, 3600);
        jedis.ttl(strKey);
        String javaLangString = jedis.get("java.lang.String");
        // 存储获取（业务）对象.
        byte[] keyForUserThomas = "user-thomas".getBytes();
        User user = new RedisDemo().new User();
        user.setId(0);
        user.setName("Thomas");
        user.setAge(18);
        jedis.set(keyForUserThomas, SerializationUtils.serialize(user));
        User userDeserialized = (User) SerializationUtils.deserialize(jedis.get(keyForUserThomas));
        log.info(userDeserialized.toString());

        // 存储获取（语法）对象（eg. Collection.）.
        List<User> users = new ArrayList<User>();
        users.add(user);
        byte[] keyForUsers = "users".getBytes();
        jedis.set(keyForUsers, SerializationUtils.serializeList(users));
        // 泛型只在编译期进行检查，运行期的变量没有泛型.
        // if((SerializationUtils.deserializeList(jedis.get(keyForUsers))) instanceof List<?>){
        List<User> deserializedUsers = (List<User>) SerializationUtils.deserializeList(jedis.get(keyForUsers));
        for (User deserializedUser : deserializedUsers) {
            log.info(deserializedUser.toString());
        }
    }

    /**
     * 当前公共类（RedisDemo）辅助类.
     * @author  Thomas Lee
     * @version 2017年6月28日 下午4:07:29
     */
    @Data
    private class User implements Serializable {
        private static final long serialVersionUID = 1L;
        private Integer id;
        private String name;
        private Integer age;
    }
}