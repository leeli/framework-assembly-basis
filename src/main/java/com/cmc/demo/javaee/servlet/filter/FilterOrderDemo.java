package com.cmc.demo.javaee.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * Filter Order Demo. 
 * @author  Thomas Lee
 * @version 2017年7月2日 上午11:30:54
 */
@Slf4j
public class FilterOrderDemo implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String initParamDemoValue = filterConfig.getInitParameter("init-param-demo-name");
        log.info(initParamDemoValue);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("FilterOrderDemo before.");
        chain.doFilter(request, response);
        log.info("FilterOrderDemo after.");
    }

    @Override
    public void destroy() {
    }
}