package com.cmc.demo.javaee.servlet.cookie;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * Cookie Demo.
 * <p>Cookie三要素：
 *      1. 一个键值对；
 *      2. 一个GMT日期；
 *      3. 作用路径（一个路径和一个域）（默认与当前页面所在目录，包括子目录下，例如当前例子中所在的目录就是/）.
 * @Reference http://www.runoob.com/servlet/servlet-cookies-handling.html.
 * @author    Thomas Lee
 * @version   2017年7月2日 下午5:04:13
 */
@Slf4j
@WebServlet("/cookiedemo")
public class CookieDemo extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置cookie.
        Cookie cookie = new Cookie("name", "lcb");
        resp.addCookie(cookie);
        PrintWriter writer = resp.getWriter();
        final String content = "Cookie Demo.";
        writer.write(content);
        writer.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 读取Cookie.
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            String cookieName = cookie.getName();
            String cookieValue = cookie.getValue();
            String msg = "Cookie的名称是" + cookieName + "，Cookie的值是" + cookieValue;
            log.info(msg);
        }
    }
}