package com.cmc.demo.javaee.servlet.email;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

/**
 * Email Demo.
 * @author   Thomas Lee
 * @version  2017年7月2日 下午9:20:51
 * @refercen SMTP Server 暂时没有找到合适的.
 */
@Slf4j
@WebServlet("/emaildemo")
@SuppressWarnings("serial")
public class EmailDemo extends HttpServlet {
    private String host;
    private String from;
    private String to;
    private String subject;
    private String text;
    private Address fromAddress;
    private Address recipAddress;
    private MimeMessage msg;
    private Properties props;

    @Override
    public void init() throws ServletException {
        host = "localhost";
        from = "525940290@qq.com";
        to = "2250672088@qq.com";
        subject = "Email Demo.";
        text = "this is a email test.";
        try {
            fromAddress = new InternetAddress(from);
            recipAddress = new InternetAddress(to);
            props = new Properties();
            props.setProperty("mail.smtp.host", host);
            Session session = Session.getDefaultInstance(props);
            msg = new MimeMessage(session);
        } catch (AddressException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            msg.setFrom(fromAddress);
            msg.setRecipient(RecipientType.TO, recipAddress);
            msg.setSubject(subject);
            msg.setText(text);
            Transport.send(msg);
        } catch (MessagingException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}