package com.cmc.demo.javaee.servlet.session;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Session Demo.
 * @author  Thomas Lee
 * @version 2017年7月2日 下午5:42:34
 */
@WebServlet("/sessiondemo")
public class SessionDemo extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private int count;
    
    @Override
    public void init() throws ServletException {
        // count默认值是0.
        count = 0;
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.setAttribute("name", "lcb");
        session.setAttribute("userId", "Romeo");

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTime = df.format(session.getCreationTime());
        String lastAccessTime = df.format(session.getLastAccessedTime());
        int maxInactiveInterval = session.getMaxInactiveInterval();
    
        // 点击数，也可以在init()方法中初始点击数.
        String visitCount = "visitCount";
        if (session.isNew()) {
            session.setAttribute(visitCount, ++count);
        } else {
            session.setAttribute(visitCount, (int) session.getAttribute(visitCount) + 1);
        }
        
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = resp.getWriter();
        String content = 
                  "<br>ID：" + session.getId()
                + "<br>创建时间：" + createTime
                + "<br>用户ID：" + session.getAttribute("userId")
                + "<br>访问次数：" + session.getAttribute(visitCount)
                + "<br>访问最大间隔：" + maxInactiveInterval
                + "<br>最后访问时间：" + lastAccessTime;
        writer.write(content);
        
        resp.setHeader("Refresh", "0.01");
        // 移除session内容.
        // session.removeAttribute("name");
        // session.invalidate();
        // session.setMaxInactiveInterval(0);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}