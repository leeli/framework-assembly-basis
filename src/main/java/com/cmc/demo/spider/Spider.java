package com.cmc.demo.net.demo.spider;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * 网络爬虫Demo.
 * 
 * @author  Mike Lee
 * @version 2017年11月16日 下午2:49:05
 */
public class Spider {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://www.baidu.com");
        InputStream is = url.openStream();
        int len = 0;
        while ((len = is.read()) != -1) {
            System.out.print((char) len);
        }
    }
}