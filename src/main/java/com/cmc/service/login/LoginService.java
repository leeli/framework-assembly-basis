package com.cmc.service.login;

import com.cmc.common.constants.LoginStatus;
import com.cmc.common.constants.SignUpStatus;
import com.cmc.user.facade.model.UserModel;

/**
 * 登录服务.
 * @author  Thomas Lee
 * @version 2017年7月12日 上午10:52:33
 */
public interface LoginService {
	/**
	 * 登录.
	 * @param name     用户名.
	 * @param password 密码.
	 * @return         登录结果标志.
	 */
	LoginStatus login(UserModel dto);

	boolean logout(String name);

	SignUpStatus signUp(UserModel dto);
}