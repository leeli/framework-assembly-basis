package com.cmc.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 序列化工具.
 * @author  Thomas Lee
 * @version 2017年5月7日 下午10:01:03
 * @version 2017年6月28日 下午4:10:48
 */
public class SerializationUtils {
	/**
	 * 序列化.
	 * @param object       对象.
	 * @return       	        对象字节数组.
	 * @throws IOException 异常.
	 * @author             Thomas Lee
	 * @version            2017年5月7日 下午10:05:46
	 */
	public static byte[] serialize(Object object) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
				oos.writeObject(object);
				return baos.toByteArray();
			}
		}
	}

	/** 
	 * 序列化对象集合. 
	 * @param list 			对象集合.
	 * @return 			  	对象结合字节数组.
	 * @throws IOException 异常.
	 */
	public static byte[] serializeList(List<?> list) throws IOException {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
				for (Object obj : list) {
					oos.writeObject(obj);
				}
				return baos.toByteArray();
			}
		}
	}

	/**
	 * 反序列化.
	 * @param bytes                   对象字节数组.
	 * @return 						      对象.
	 * @throws IOException 			      异常.
	 * @throws ClassNotFoundException 
	 * @author 						   Thomas Lee
	 * @version 					   2017年5月7日 下午10:05:59
	 */
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes)) {
			try (ObjectInputStream ois = new ObjectInputStream(bais)) {
				return ois.readObject();
			}
		}
	}

	/** 
	 * 反序列化对象集合.
	 * @param bytes 				    对象集合字节数组.
	 * @return 						    对象集合.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static List<?> deserializeList(byte[] bytes) throws IOException, ClassNotFoundException {
		List<Object> list = new ArrayList<Object>();
		try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes)) {
			try (ObjectInputStream ois = new ObjectInputStream(bais)) {
				while (bais.available() > 0) {
					Object obj = (Object) ois.readObject();
					if (obj == null) {
						break;
					}
					list.add(obj);
				}
				return list;
			}
		}
	}
}