package com.cmc.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cmc.user.facade.entity.User;

/**
 * Microsoft Office Excel Utilities
 * <p>有待全部使用POI替换JXL.
 * @author Thomas Lee
 * @since 2016年11月20日 下午8:09:35
 * @version 2017年4月27日 上午10:27:27
 */
public class ExcelUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ExcelUtils.class);

	/**
	 * 导出.
	 * <p>使用POI.</p> 
	 * @param data 传入的字符串二维数组
	 * @return 解析结果.
	 */
	@SuppressWarnings("deprecation")
	public static ByteArrayOutputStream create(String[][] data) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("统计表");
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		HSSFFont font = workbook.createFont();
		font.setColor(HSSFColor.BLUE.index);
		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setIndention((short) 10);
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		cellStyle.setBorderLeft((short) 10);
		
		for (int x = 0; x < data.length; x++) {
			HSSFRow row = sheet.createRow(x);
			for (int y = 0; y < data[x].length; y++) {
				row.createCell(y).setCellValue(data[x][y]);
			}
		}
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			workbook.write(byteArrayOutputStream);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return byteArrayOutputStream;
	}

	/**
	 * 导入
	 * <p>使用POI进行导入.</p>
	 * @param file
	 * @author Thomas Lee
	 * @version 2017年4月27日 上午10:28:14
	 */
	public static void readByPOI(File file) {
		try (XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file))) {
			XSSFSheet sheet = wb.getSheetAt(0);
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				XSSFRow row = sheet.getRow(i);
				System.out.println(row.getCell(0).getRawValue());
				System.out.println(row.getCell(1).getRawValue());
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/*
	private Map<String, String> packageMsgs(MultipartFile mfMsgs) throws BiffException, IOException {
	    Map<String, String> mMsgs = new IdentityHashMap<String, String>();
	    String suffix = this.getFileSuffix(mfMsgs.getOriginalFilename());
	    ExcelSuffix excelSuffix = ExcelSuffix.parseByDesc(suffix);
	    // 如果是office 2007及以上版本
	    if (ExcelSuffix.XLSX.equals(excelSuffix)) {
	        try (XSSFWorkbook wb = new XSSFWorkbook(mfMsgs.getInputStream())) {
	            XSSFSheet sheet = wb.getSheetAt(0);
	            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
	                XSSFRow row = sheet.getRow(i);
	                mMsgs.put(row.getCell(0).getRawValue().trim(), row.getCell(1).toString().trim());
	            }
	        }
	    } else { // 如果是office 2003及以下版本
	        Workbook book = Workbook.getWorkbook(mfMsgs.getInputStream());
	        Sheet sheet = book.getSheet(0);
	        for (int i = 1; i < sheet.getRows(); i++) {
	            mMsgs.put(sheet.getCell(0, i).getContents().trim(), sheet.getCell(1, i).getContents().trim());
	        }
	    }
	    return mMsgs;
	}
	*/
	public static void main(String[] args) {
		String filePath = "C:/Users/ucmed/Desktop/tmp.xlsx";
		File file = new File(filePath);
		String filename = file.getName();
		System.out.println(filename.substring(filename.indexOf(".") + 1, filename.length()));
	}

	/**
	 * 导入
	 * <p>使用JXL导入.</p>
	 * @param file 文件
	 * @return 解析结果.
	 */
	public static List<User> read(File file) {
		List<User> users = new ArrayList<User>();
		Workbook book = null;
		try {
			book = Workbook.getWorkbook(file);
		} catch (BiffException | IOException e) {
			LOG.error(e.getMessage(), e);
		}
		Sheet sheet = book.getSheet(0);
		for (int x = 0; x < sheet.getRows(); x++) {
			User user = new User();
			user.setName(sheet.getCell(0, x).getContents());
			user.setSex(sheet.getCell(1, x).getContents());
			users.add(user);
		}
		book.close();
		return users;
	}

}