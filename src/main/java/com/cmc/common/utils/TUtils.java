package com.cmc.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;

/**
 * 泛型工具类.
 * @author Thomas Lee
 * @version Jun 13, 2017 10:30:29 AM
 */
public class TUtils {

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		// System.out.println(getActualType(new UserServiceBizImpl().getClass()));
		showEssenceByReflect();
	}

	/**
	 * 泛型转换
	 * <p> 暂时放到反射之后进行研究 - 学习完成之后进一步封装基本的CRUD业务操作（面向接口编程）.
	 * @param entity 实体类类型.
	 * @return 
	 * @author HT.LCB
	 * @version 2016年10月27日 下午1:55:32
	 */
	public static Class<?> getActualType(Class<?> entity) {
		ParameterizedType parameterizedType = (ParameterizedType) entity.getGenericSuperclass();
		return (Class<?>) parameterizedType.getActualTypeArguments()[0];
	}

	/**
	 * 通过反射了解泛型的本质.
	 * @author Thomas Lee
	 * @version Jun 13, 2017 10:34:00 AM
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public static void showEssenceByReflect() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		// 可以存放任意类型对象.
		ArrayList<?> list1 = new ArrayList<>();
		// 只可以存放String类型.
		ArrayList<String> list2 = new ArrayList<String>();
		list2.add("hello");

		Class<?> c1 = list1.getClass();
		Class<?> c2 = list2.getClass();

		/*
		 * 结果为true.
		 * 说明编译之后集合是去泛型化的，也就是说编译之后泛型是不算在类类型中的。
		 * 结论：Java中集合的泛型，是为了防止错误输入的，只是在编译期间有效，运行期间无效。
		 */
		System.out.println(c1 == c2);

		// 验证泛型是不是只在编译期间有效.
		Method m = c2.getMethod("add", Object.class);
		// 此处向只能存放String类型的数组存放整型.
		m.invoke(list2, 100);
		System.out.println(list2.size());
		// 此时for循环会报出ClassCaseException.
		for (String member : list2) {
			System.out.println(member);
		}
	}

}