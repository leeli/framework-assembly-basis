package com.cmc.common.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 图片压缩处理
 * @author  崔素强
 * @author  Thomas Lee
 * @version 2017年7月29日 上午10:17:57
 */
public class ImageCompressUtils {
	private Image img;
	private int width;
	private int height;
	private String toFilepath;

	public static void main(String[] args) throws Exception {
		String filepath = "C:/Users/Thomas Lee/Desktop/beach.jpeg";
		String toFilepath = "C:/Users/Thomas Lee/Desktop/beach_compress.jpeg";
		ImageCompressUtils imgCom = new ImageCompressUtils(filepath, toFilepath);
		imgCom.resizeFix(100, 100);
	}

	public ImageCompressUtils(String fileName, String toFilepath) throws IOException {
		File file = new File(fileName);
		this.img = ImageIO.read(file);
		this.width = img.getWidth(null);
		this.height = img.getHeight(null);
		this.toFilepath = toFilepath;
	}

	/**
	 * 强制压缩、放大图片到固定的大小.
	 * @param w int 新宽度.
	 * @param h int 新高度.
	 */
	public void resize(int w, int h) throws IOException {
		// SCALE_SMOOTH的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢.
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		image.getGraphics().drawImage(img, 0, 0, w, h, null);
		File destFile = new File(this.toFilepath);
		try (FileOutputStream out = new FileOutputStream(destFile)) {
			// 可以正常实现bmp、png、gif转jpg.
			JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
			encoder.encode(image); // JPEG编码
		}
	}

	/**
	 * 按照宽度、高度进行压缩.
	 * @param w int 最大宽度.
	 * @param h int 最大高度.
	 */
	public void resizeFix(int w, int h) throws IOException {
		if (width / height > w / h) {
			resizeByWidth(w);
		} else {
			resizeByHeight(h);
		}
	}

	/**
	 * 以宽度为基准，等比例放缩图片.
	 * @param w int 新宽度.
	 */
	public void resizeByWidth(int w) throws IOException {
		int h = (int) (height * w / width);
		resize(w, h);
	}

	/**
	 * 以高度为基准，等比例缩放图片.
	 * @param h int 新高度.
	 */
	public void resizeByHeight(int h) throws IOException {
		int w = (int) (width * h / height);
		resize(w, h);
	}
}