package com.cmc.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Stream Utilities.
 * 
 * @author  Mike Lee
 * @version 2017年9月13日 下午10:27:24
 */
public class StreamUtils {
	public static void main(String[] args) {
	}

	/**
	 * 
	 * @param buf
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午10:28:44
	 * @return 
	 */
	public static ByteArrayInputStream bytes2stream(byte[] buf) {
		// bytes to stream，肯定是写入流，所以使用InputStream.
		return new ByteArrayInputStream(buf);
	}

	/**
	 * 
	 * @param is
	 * @return
	 * @author  Mike Lee
	 * @version 2017年9月13日 下午10:31:25
	 */
	public static byte[] stream2bytes(InputStream is) {
		// stream to bytes，肯定是读取流，所以使用OutputStream.
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			byte[] buf = new byte[1024];
			for (int len; (len = is.read(buf)) != -1;)
				baos.write(buf, 0, len);
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[] {};
	}
}