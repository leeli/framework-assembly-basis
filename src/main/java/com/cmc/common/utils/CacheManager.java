package com.cmc.common.utils;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.context.ContextLoader;

/**
 * Redis CacheManager.
 * @author  Thomas Lee
 * @version 2017年7月27日 下午10:39:23
 */
public class CacheManager {
	private static ApplicationContext CONTEXT = ContextLoader.getCurrentWebApplicationContext();

	@SuppressWarnings("unchecked")
	public static <T> void set(String key, T obj) {
		RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) CONTEXT.getBean("redisTemplate");
		ValueOperations<String, Object> opsValue = redisTemplate.opsForValue();
		opsValue.set(key, obj);
	}

	@SuppressWarnings("unchecked")
	public static <T> void set(String key, T obj, long timeout, TimeUnit unit) {
		RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) CONTEXT.getBean("redisTemplate");
		redisTemplate.expire(key, timeout, unit);
		ValueOperations<String, Object> opsValue = redisTemplate.opsForValue();
		opsValue.set(key, obj);
	}

	@SuppressWarnings("unchecked")
	public static <T> T get(String key) {
		RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
		ValueOperations<String, T> opsValue = redisTemplate.opsForValue();
		return opsValue.get(key);
	}

	@SuppressWarnings("unchecked")
	public static <T> void addSet(String key, T obj) {
		RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
		SetOperations<String, T> opsSet = redisTemplate.opsForSet();
		opsSet.add(key, obj);
	}

	@SuppressWarnings("unchecked")
	public static <T> Set<T> getSet(String key) {
		RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
		SetOperations<String, T> opsSet = redisTemplate.opsForSet();
		return opsSet.members(key);
	}
}