package com.cmc.common.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Domain和DTO转换工具.
 * @author LiChuanbin
 * @version Jun 8, 2017 2:48:55 PM
 */
@Slf4j
public class DomainDTOUtil {

	public static <T> T dto2do(Object dto, Class<T> domainClass) {
		if (dto == null || domainClass == null)
			return null;
		T domain = null;
		try {
			domain = domainClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}
		if (domain == null)
			return null;

		List<Field> fields = getAllField(new ArrayList<Field>(), domain.getClass(), 1);
		for (Field field : fields) {
			try {
				// 此字段标记为转换时忽略.
				if (field.isAnnotationPresent(TransIgnore.class))
					continue;
				if (field.isAnnotationPresent(Desensitize.class)) {
					Desensitize desensitize = field.getAnnotation(Desensitize.class);
					String dtoFieldName = desensitize.fieldName();
					if (StringUtils.isBlank(dtoFieldName)) {
						dtoFieldName = field.getName();
					}
					field.setAccessible(true);
					Field dtoField = getFiledByName(dto.getClass(), dtoFieldName);
					dtoField.setAccessible(true);
					Object fieldValue = null;
					Object dtoFieldValue = dtoField.get(dto);
					if (null == dtoFieldValue) {
						fieldValue = null;
					} else {
						String fieldStrValue = IdHandler.idDecrypt(dtoFieldValue.toString());
						// field 类型暂时只能处理 Long，Integer，String
						if (Long.class.equals(field.getType())) {
							fieldValue = new Long(fieldStrValue);
						} else if (Integer.class.equals(field.getType())) {
							fieldValue = new Integer(fieldStrValue);
						} else {
							fieldValue = fieldStrValue;
						}
						log.debug(dtoFieldName + ":" + fieldValue);
					}
					field.set(domain, fieldValue);
				} else {
					// model field handle
					Field modelField = getFiledByName(dto.getClass(), field.getName());
					field.setAccessible(true);
					modelField.setAccessible(true);
					field.set(domain, modelField.get(dto));
				}
			} catch (NumberFormatException e) {
				log.error("数据转换错误", e);
			} catch (Exception e) {
				log.warn(e.getMessage());
				continue;
			}
		}
		return domain;
	}

	public static <T> T do2dto(Object domain, Class<T> dtoClass) {
		if (domain == null || null == dtoClass) {
			return null;
		}
		T model = null;
		try {
			model = dtoClass.newInstance();
		} catch (InstantiationException e1) {
			log.error(e1.getMessage(), e1);
		} catch (IllegalAccessException e1) {
			log.error(e1.getMessage(), e1);
		}

		List<Field> fields = new ArrayList<Field>();
		getAllField(fields, domain.getClass(), 1);
		for (Field field : fields) {
			try {
				// 此字段标记为转换时忽略
				if (field.isAnnotationPresent(TransIgnore.class)) {
					continue;
				}
				if (field.isAnnotationPresent(Desensitize.class)) {
					Desensitize desensitize = field.getAnnotation(Desensitize.class);
					String dtoFieldName = desensitize.fieldName();
					if (StringUtils.isBlank(dtoFieldName)) {
						dtoFieldName = field.getName();
					}
					field.setAccessible(true);
					Object fieldValue = field.get(domain);
					Object dtoFieldValue;
					if (null == fieldValue) {
						dtoFieldValue = null;
					} else {
						dtoFieldValue = IdHandler.idEncrypt(fieldValue.toString());
						log.debug(dtoFieldName + ":" + dtoFieldValue.toString());
					}
					Field dtoField = getFiledByName(model.getClass(), dtoFieldName);
					dtoField.setAccessible(true);
					dtoField.set(model, dtoFieldValue);
				} else {
					Field modelField = getFiledByName(model.getClass(), field.getName());

					field.setAccessible(true);
					modelField.setAccessible(true);
					modelField.set(model, field.get(domain));
				}
			} catch (Exception e) {
				log.warn(e.getMessage());
				continue;
			}
		}

		return model;

	}

	/**
	* @Description model列表转换dataobject列表
	* @param dtos
	*            model列表
	* @param domainClass
	*            dataobject.class
	* @return dataobject列表
	*/
	public static <T, M> List<T> dtos2dos(List<M> dtos, Class<T> domainClass) {
		if (null == dtos) {
			return null;
		}
		List<T> dataObjects = new ArrayList<T>();
		for (M model : dtos) {
			dataObjects.add(dto2do(model, domainClass));
		}
		return dataObjects;
	}

	/**
	 * @Description dataobject列表转换model列表
	 * @param domains
	 *            dataobject列表
	 * @param dto
	 *            model.class
	 * @return model列表
	 */
	public static <T, M> List<T> dos2dtos(List<M> domains, Class<T> dto) {
		if (null == domains) {
			return null;
		}
		List<T> models = new ArrayList<T>();
		for (M dataobject : domains) {
			models.add(do2dto(dataobject, dto));
		}
		return models;
	}

	/**
	 * 递归获取实体（含多重继承，此处最多5层）的所有自定义成员变量.
	 * @param fields 成员变量数组.
	 * @param currentClass 类类型.
	 * @param recursionLevel 递归层数.
	 * @return 成员变量数组.
	 * @author Thomas Lee
	 * @version Jun 12, 2017 3:14:29 PM
	 */
	private static List<Field> getAllField(List<Field> fields, Class<?> currentClass, int recursionLevel) {
		if (fields == null) {
			fields = new ArrayList<Field>();
		}
		if (recursionLevel > 5 || null == currentClass) {
			return fields;
		}
		try {
			Field[] field = currentClass.getDeclaredFields();
			if (field != null) {
				CollectionUtils.addAll(fields, field);
			}
			Class<?> superClass = currentClass.getSuperclass();
			return getAllField(fields, superClass, recursionLevel++);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return fields;
		}
	}

	private static Field getFiledByName(Class<?> clazz, String fieldName) {
		if (null == clazz || StringUtils.isBlank(fieldName)) {
			return null;
		}
		Field field = null;
		try {
			field = clazz.getDeclaredField(fieldName);
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchFieldException e) {
			log.warn("Can not find field namded " + fieldName + " in class" + clazz);
		}
		if (field == null && clazz.getSuperclass() != null) {
			return getFiledByName(clazz.getSuperclass(), fieldName);
		} else {
			return field;
		}
	}

}
