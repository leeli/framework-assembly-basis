package com.cmc.common.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Dto和Entity转换工具.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:51:11
 */
@Slf4j
public class DtoEntityUtils {
	/**
	 * Dto到Entity的转换.
	 * @param dto         Dto.
	 * @param entityClazz Entity类类型.
	 * @return
	 * @author            HT-LiChuanbin 
	 * @version           2017年7月19日 下午3:48:21
	 */
	public static <T> T dto2entity(Object dto, Class<T> entityClazz) {
		try {
			if (null == dto || null == entityClazz) {
				return null;
			}

			T entity = entityClazz.newInstance();
			if (entity == null) {
				return null;
			}
			List<Field> fields = new ArrayList<Field>();
			getAllFields(new ArrayList<Field>(), entity.getClass(), 1);
			for (Field field : fields) {
				if (field.isAnnotationPresent(TransIgnore.class)) {
					continue;
				}
				Field dtoField = getFiledByName(dto.getClass(), field.getName());
				if (dtoField == null) {
					// 这里说明dto没有entity中的field属性.
					continue;
				}
				field.setAccessible(true);
				dtoField.setAccessible(true);
				field.set(entity, dtoField.get(dto));
			}
			return entity;
		} catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Entity到Dto的转换.
	 * @param entity   Entity.
	 * @param dtoClazz Dto类类型.
	 * @return
	 * @author 		    HT-LiChuanbin 
	 * @version 		2017年7月19日 下午3:49:32
	 */
	public static <T> T entity2dto(Object entity, Class<T> dtoClazz) {
		try {
			if (entity == null || null == dtoClazz) {
				return null;
			}

			T dto = dtoClazz.newInstance();
			List<Field> fields = new ArrayList<Field>();
			getAllFields(fields, entity.getClass(), 1);
			for (Field field : fields) {
				if (field.isAnnotationPresent(TransIgnore.class)) {
					continue;
				}
				Field dtoField = getFiledByName(dto.getClass(), field.getName());
				if (dtoField == null) {
					// 这里说明dto中没有entity中的属性field.
					continue;
				}
				field.setAccessible(true);
				dtoField.setAccessible(true);
				dtoField.set(dto, field.get(entity));
			}
			return dto;
		} catch (InstantiationException | IllegalAccessException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Dto列表到Entity的转换.
	 * @param dtos        Dto列表.
	 * @param entityClazz Entity类类型.
	 * @return
	 * @author            HT-LiChuanbin 
	 * @version           2017年7月19日 下午3:50:32
	 */
	public static <T, M> List<T> dtoList2doList(List<M> dtos, Class<T> entityClazz) {
		if (null == dtos) {
			return null;
		}
		List<T> entitys = new ArrayList<T>();
		for (M dto : dtos) {
			entitys.add(dto2entity(dto, entityClazz));
		}
		return entitys;
	}

	/**
	 * Entity列表到Dto的转换.
	 * @param entitys  Entity列表.
	 * @param dtoClazz Dto类类型.
	 * @return
	 * @author         HT-LiChuanbin 
	 * @version        2017年7月19日 下午3:51:13
	 */
	public static <T, M> List<T> doList2dtoList(List<M> entitys, Class<T> dtoClazz) {
		if (null == entitys) {
			return null;
		}
		List<T> dtos = new ArrayList<T>();
		for (M entity : entitys) {
			dtos.add(entity2dto(entity, dtoClazz));
		}
		return dtos;
	}

	/**
	 * 获取所有属性.
	 * @param fields 所有属性列表.
	 * @param clazz  类类型.
	 * @param i      类父类的层数.
	 * @author       HT-LiChuanbin 
	 * @version      2017年7月19日 下午3:52:08
	 */
	private static void getAllFields(List<Field> fields, Class<?> clazz, int i) {
		if (i >= 5 || null == clazz) {
			return;
		}
		try {
			Class<?> superClazz = clazz.getSuperclass();
			Field[] field = clazz.getDeclaredFields();
			if (null != field) {
				CollectionUtils.addAll(fields, field);
			}
			getAllFields(fields, superClazz, ++i);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return;
		}
	}

	/**
	 * 通过属性名称获取属性.
	 * @param clazz     类类型.
	 * @param fieldName 属性名称.
	 * @return          属性.
	 * @author          HT-LiChuanbin 
	 * @version         2017年7月19日 下午3:53:24
	 */
	private static Field getFiledByName(Class<?> clazz, String fieldName) {
		if (null == clazz || StringUtils.isBlank(fieldName)) {
			return null;
		}
		try {
			Field field = clazz.getDeclaredField(fieldName);
			return field == null && clazz.getSuperclass() != null ? getFiledByName(clazz.getSuperclass(), fieldName) : field;
		} catch (NoSuchFieldException | SecurityException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}
}