package com.cmc.common;

import lombok.Data;

@Data
public class ZCGLCommonReqParams {
	private String client;
	private String version;
	private String device;
	private String token;
}