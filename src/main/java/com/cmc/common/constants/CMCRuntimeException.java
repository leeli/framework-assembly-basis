package com.cmc.common.constants;

/**
 * 业务运行时异常.
 * @author Thomas Lee
 * @version 2017年5月4日 下午3:11:31
 */
public class CMCRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private int code;
    private String message;

    public CMCRuntimeException() {
    }

    public CMCRuntimeException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public CMCRuntimeException(ResCode resCode) {
        this(resCode.getCode(), resCode.getDesc());
    }

    public CMCRuntimeException(String message) {
        super(message);
        this.code = ResCode.SERVER_ERROR.getCode();
        this.message = message;
    }

    public CMCRuntimeException(Throwable cause) {
        super(cause);
        this.code = ResCode.SERVER_ERROR.getCode();
        this.message = cause.getMessage();
    }

    public CMCRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.code = ResCode.SERVER_ERROR.getCode();
        this.message = message;
    }

    public CMCRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = ResCode.SERVER_ERROR.getCode();
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
