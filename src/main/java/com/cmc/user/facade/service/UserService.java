package com.cmc.user.facade.service;

import java.util.List;

import com.cmc.common.utils.PaginationResult;
import com.cmc.user.facade.model.UserModel;

/**
 * User Interface
 * 
 * @author Thomas Lee
 * @since 2016年12月22日 上午9:50:01
 */
public interface UserService {
    
    /**
     * 通过存储过程查询用户.
     * @param age
     * @return
     * @author Thomas Lee
     * @version 2017年8月13日 下午10:06:40
     */
    List<UserModel> listUsersWithStoreProcedure(int age);
    
    /**
     * 添加用户
     * @param dto
     * @return
     */
    boolean add(UserModel dto);

    public String getArg1();

    public String getCommon();

    /**
     * 显示用户列表
     * @return
     */
    PaginationResult<UserModel> list(Long pageNo, Long pageSize);

    /**
     * 显示用户
     * @param dto
     * @return
     */
    UserModel get(UserModel dto);

    /**
     * 修改用户
     * @param dto
     * @return
     */
    boolean update(UserModel dto);

    /**
     * 删除用户
     * @param dto
     * @return
     */
    boolean delete(UserModel dto);
    
    /**
     * 删除用户（临时）.
     * @author Thomas Lee
     * @version 2017年5月5日 下午2:17:44
     */
    void deleteTMP();
    
    /**
     * 通过用户名获取用户信息
     * @param dto UserDTO{name}
     * @return
     */
    UserModel getByName(UserModel dto);

}