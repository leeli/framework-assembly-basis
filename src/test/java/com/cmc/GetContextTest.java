package com.cmc;

import java.util.Date;
import java.util.Random;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.ContextLoader;

import com.cmc.user.facade.entity.User;

/**
 * 获取上下文测试. 
 * @author  Thomas Lee
 * @version 2017年7月27日 下午9:45:43
 */
@Slf4j
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:config/spring/spring-context.xml" })
public class GetContextTest {
    @Test
    @SuppressWarnings("unchecked")
    public void getContext() {
        ApplicationContext context = ContextLoader.getCurrentWebApplicationContext();
        RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) context.getBean("redisTemplate");
        String randomKey = this.generateKey();
        ValueOperations<String, Object> opsValue = redisTemplate.opsForValue();
        opsValue.set(randomKey, this.getDefaultUser());
        User user = (User) opsValue.get(randomKey);
        log.info(user.toString());
    }

    private String generateKey() {
        return new Random().toString();
    }

    private Object getDefaultUser() {
        User user = new User();
        user.setId("1");
        user.setName("凯旋基诺");
        user.setAge(20);
        user.setCreateTime(new Date());
        return user;
    }
}