package com.cmc.permission.service.dao;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cmc.common.constants.Constants;
import com.cmc.permission.facade.entity.Permission;

@Slf4j
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:config/spring/spring-context.xml" })
public class PermissionMapperTest {
    @Autowired
    private PermissionMapper permissionMapper;

    @Test
    public void testInsertSelectiveWithId() {
        Permission perm = new Permission();
        perm.setDescription("desc");
        permissionMapper.insertSelectiveWithId(perm);
        log.info("selectKey");
    }

    @Test
    public void testInsertBatch() {
        List<Permission> perms = new ArrayList<Permission>();
        Permission perm = new Permission();
        perm.setDescription("desc");
        perms.add(perm);
        Permission perm1 = new Permission();
        perm1.setDescription("desc1");
        perms.add(perm1);
        permissionMapper.insertBatch(perms);
        log.info("foreach");
        log.info(Constants.logBase);
    }
}