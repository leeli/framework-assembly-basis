package com.cmc.cache.redis;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import com.cmc.user.facade.entity.User;

/**
 * Redis Test.
 * @author  Thomas Lee
 * @version 2017年7月23日 下午4:47:52
 */
@Slf4j
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:config/spring/spring-context.xml" })
public class RedisTest {
	@Autowired
	private ShardedJedisPool shardedJedisPool;
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	@Autowired
	private RedisTemplate<String, Object> redisTemplateForObj;

    /**
     * 测试spring-data-redis.
     * @author  Thomas Lee
     * @version 2017年7月23日 下午4:42:43
     */
    @Test
    public void testSpringRedis() {
        User user = new User();
        user.setName("lcb");
        user.setAge(20);

        // 操作字符串.
        ValueOperations<String, String> opsValue = redisTemplate.opsForValue();
        final String strKey = "strkey";
        opsValue.set(strKey, "strvalue");
        redisTemplate.expire(strKey, 5, TimeUnit.SECONDS);
        log.info("Redis String : " + opsValue.get(strKey));

        ValueOperations<String, Object> opsValueForObj = redisTemplateForObj.opsForValue();
        opsValueForObj.set("token_string", user);
        User userValue = (User) opsValueForObj.get("token_string");
        log.info(userValue.toString());

        // 操作哈希-存储字符串.
        HashOperations<String, String, String> opsHash = redisTemplate.opsForHash();
        final String hashKey = "hashkey";
        opsHash.put(hashKey, "name", "lcb");
        opsHash.put(hashKey, "age", "18");
        log.info("Redis Hash : " + opsHash.get(hashKey, "name"));
        // 操作哈希-存储对象.
        HashOperations<String, String, Object> opsHashForObj = redisTemplateForObj.opsForHash();
        final String hashKeyForObj = "hashkeyforobj";
        // 过期时间为30分钟.
        redisTemplateForObj.expire(hashKeyForObj, 30, TimeUnit.MINUTES);
        opsHashForObj.put(hashKeyForObj, "token", user);
        User userHash = (User) opsHashForObj.get(hashKeyForObj, "token");
        log.info(userHash.toString());

		// 操作列表.
		ListOperations<String, String> opsList = redisTemplate.opsForList();
		final String listKey = "listkey";
		opsList.leftPush(listKey, "1");
		opsList.leftPush(listKey, "2");
		opsList.leftPush(listKey, "3");
		List<String> list = opsList.range(listKey, 0, opsList.size(listKey) - 1);
		for (String v : list) {
			log.info("Redis List : " + v);
		}

		// 操作集合.
		SetOperations<String, String> opsSet = redisTemplate.opsForSet();
		final String setKey = "setkey";
		opsSet.add(setKey, "1");
		opsSet.add(setKey, "1");
		opsSet.add(setKey, "2");
		opsSet.add(setKey, "3");
		Set<String> sets = opsSet.members(setKey);
		for (String v : sets) {
			log.info("Redis Set : " + v);
		}

		// 操作有序集合.
		ZSetOperations<String, String> opsZSet = redisTemplate.opsForZSet();
		final String zSetKey = "zsetkey";
		opsZSet.add(zSetKey, "1", 1);
		opsZSet.add(zSetKey, "1", 2);
		opsZSet.add(zSetKey, "2", 4);
		opsZSet.add(zSetKey, "3", 5);
		opsZSet.add(zSetKey, "4", 1);
		Set<String> zSets = opsZSet.range(zSetKey, 0, -1);
		for (String v : zSets) {
			log.info("Redis ZSet : " + v);
		}
	}

	/**
	 * 直接通过Jedis进行缓存测试 
	 * @author Thomas Lee
	 * @version 2017年3月12日 下午12:36:37
	 */
	@Test
	public void testJedis() {
		Jedis jedis = null;
		try {
			jedis = new Jedis("192.168.98.128", 6379);
			String key = "name";
			String value = "lcb";
			jedis.set(key, value);
			log.info(jedis.get(key));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (null != jedis) {
				jedis.close();
			}
		}
	}

	/**
	 * 通过@ContextConfiguration进行Spring和Redis集成测试
	 * @author Thomas Lee
	 * @version 2017年3月12日 下午12:35:52
	 */
	@Test
	public void testJedisRedis() {
		// 通过ClassPathXmlApplicationContext加载Spring容器
		// ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/config/spring-context.xml");
		// context.start();

		// ShardedJedisPool shardedJedisPool = (ShardedJedisPool) context.getBean("shardedJedisPool");
		ShardedJedis jedis = shardedJedisPool.getResource();
		String key = "name";
		String value = "lcb";
		jedis.set(key, value);
		log.info(jedis.get(key));
		// context.stop();
		// context.close();
	}
}