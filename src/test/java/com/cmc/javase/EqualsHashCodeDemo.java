package com.cmc.javase;

import org.junit.Test;

public class EqualsHashCodeDemo {
	private String prop;

	public static void main(String[] args) {
		// 2147483647
		// Object o;
		// System.out.println((o = (1 << 31) - 1).hashCode());
		// Long l = 2147483648L;
		// System.out.println(l);
		System.out.println(new Aux().hashCode());
		System.out.println(new Aux().hashCode());
		System.out.println(new Aux().hashCode());
		System.out.println(new Aux().hashCode());
	}

	private static class Aux {
	}

	@Test
	public void testEqualsHashCode() {
		EqualsHashCodeAssit eha1 = new EqualsHashCodeAssit();
		EqualsHashCodeAssit eha2 = new EqualsHashCodeAssit();
		System.out.println(eha1.equals(eha2));
	}

	private static class EqualsHashCodeAssit {
		@Override
		public boolean equals(Object obj) {
			return Boolean.TRUE;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result;
		result = prime * (result = 1) + ((this.prop == null) ? 0 : prop.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EqualsHashCodeDemo other = (EqualsHashCodeDemo) obj;
		if (prop == null) {
			if (other.prop != null)
				return false;
		} else if (!prop.equals(other.prop))
			return false;
		return true;
	}
}