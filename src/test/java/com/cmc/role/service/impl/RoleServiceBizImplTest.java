package com.cmc.role.service.impl;

import java.util.Iterator;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cmc.permission.facade.entity.Permission;
import com.cmc.role.facade.model.RoleModel;
import com.cmc.role.facade.service.RoleService;

@Slf4j
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:config/spring/spring-context.xml" })
public class RoleServiceBizImplTest {
    @Autowired
    private RoleService roleService;

    @Test
    public void testGetRolePermissions() {
        List<RoleModel> mRoles = roleService.getRolePermissions();
        if (CollectionUtils.isEmpty(mRoles)) {
            return;
        }
        Iterator<RoleModel> imRoles = mRoles.iterator();
        while (imRoles.hasNext()) {
            RoleModel mRole = imRoles.next();
            log.info(mRole.toString());
            List<Permission> perms = mRole.getPermissions();
            for (Permission perm : perms) {
                if (perm == null) {
                    continue;
                }
                log.info(perm.toString());
            }
        }
    }
}