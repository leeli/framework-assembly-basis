set -x
set -e
mvn clean install -DskipTests
chmod 766 "./target/ssbkdemo-0.0.1-SNAPSHOT.war"
scp -P 2222 ./target/ssbkdemo-0.0.1-SNAPSHOT.war root@183.129.255.165:/opt/ROOT.war
chmod 766 ./deploy.sh
scp -P 2222 ./deploy.sh root@183.129.255.165:/opt/script
ssh -p 2222 root@183.129.255.165 "/opt/script/deploy.sh"