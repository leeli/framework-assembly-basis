package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Invitation;

/**
 * Created by work_pc on 2017/8/8.
 */
public interface InvitationDao extends BaseDao<Invitation>{
}
