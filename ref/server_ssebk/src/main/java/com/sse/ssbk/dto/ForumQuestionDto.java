package com.sse.ssbk.dto;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

@Data
public class ForumQuestionDto {
    // t_forum_question
    private transient Integer questionId;
    @NotBlank
    private String title;
    @NotBlank
    private String content;
    @Range(min = 0, max = 1)
    private String isAnonymous;
    private String[] imageArr;

    // t_topic_question_map
    private String[] topicIds;
}