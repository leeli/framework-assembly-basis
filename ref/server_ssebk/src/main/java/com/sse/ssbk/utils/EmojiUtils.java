package com.sse.ssbk.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmojiUtils {
    /**
    * @Description 将字符串中的emoji表情转换成可以在utf-8字符集数据库中保存的格式（表情占4个字节，需要utf8mb4字符集）
    * @param content
    *            待转换字符串
    * @return 转换后字符串
    * @throws UnsupportedEncodingException
    *             exception
    */
    public static String encode(String content) {
        String patternString = "([\\x{10000}-\\x{10ffff}\ud800-\udfff])";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(content);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            try {
                matcher.appendReplacement(sb, "[[" + URLEncoder.encode(matcher.group(1), "UTF-8") + "]]");
            } catch (UnsupportedEncodingException e) {
                log.error("emojiConvert error", e);
            }
        }
        matcher.appendTail(sb);
        log.debug("emojiConvert " + content + " to " + sb.toString() + ", len：" + sb.length());
        return sb.toString();
    }

    /**
     * @Description 还原utf8数据库中保存的含转换后emoji表情的字符串
     * @param content
     *            转换后的字符串
     * @return 转换前的字符串
     * @throws UnsupportedEncodingException
     *             exception
     */
    public static String decode(String content) {
        String patternString = "\\[\\[(.*?)\\]\\]";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(content);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            try {
                matcher.appendReplacement(sb, URLDecoder.decode(matcher.group(1), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                log.error("emojiRecovery error", e);
            }
        }
        matcher.appendTail(sb);
        log.debug("emojiRecovery " + content + " to " + sb.toString());
        return sb.toString();
    }
}