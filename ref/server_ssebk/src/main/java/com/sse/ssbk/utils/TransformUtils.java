package com.sse.ssbk.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 转换工具类.
 * @author  HT-LiChuanbin 
 * @version 2017年8月1日 上午11:30:45
 */
public class TransformUtils {
    /**
     * 
     * @param buf
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月1日 上午11:30:43
     */
    public static final InputStream bytes2Inputstream(byte[] buf) {
        return new ByteArrayInputStream(buf);
    }

    /**
     * 
     * @param inStream
     * @return
     * @throws IOException
     * @author  HT-LiChuanbin 
     * @version 2017年8月1日 上午11:31:01
     */
    public static final byte[] inputstream2bytes(InputStream inStream) throws IOException {
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while ((rc = inStream.read(buff, 0, 100)) > 0) {
            swapStream.write(buff, 0, rc);
        }
        return swapStream.toByteArray();
    }
}