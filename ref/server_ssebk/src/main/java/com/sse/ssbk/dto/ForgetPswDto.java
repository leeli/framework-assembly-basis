package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by work_pc on 2017/8/9.
 */
@Data
public class ForgetPswDto {
    @NotBlank
    private String newPwd;
    @NotBlank
    private String code;
    @NotBlank
    private String phone;
}
