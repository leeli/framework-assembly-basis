package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_upgrade")
public class Upgrade {
    @Id
    @Column(name = "upgrade_id")
    private Integer upgradeId;
    @Column(name = "upgrade_desc")
    private String upgradeDesc;
    @Column(name = "version_no")
    private String versionNo;
    @Column(name = "is_force")
    private String isForce;
    @Column(name = "url")
    private String url;
    @Column(name = "create_time")
    private Date createTime;
}