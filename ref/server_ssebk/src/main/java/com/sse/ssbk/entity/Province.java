package com.sse.ssbk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_province")
public class Province {
    @Id
    @Column(name = "province_id")
    private Integer provinceId;
    @Column(name = "province_name")
    private String provinceName;
    @Column(name = "short_name")
    private String shortName;
}