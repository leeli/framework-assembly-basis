package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ForumTopicDao;
import com.sse.ssbk.dao.UserConcernDao;
import com.sse.ssbk.dao.UserConcernTopicDao;
import com.sse.ssbk.dao.UserConcernUserDao;
import com.sse.ssbk.entity.ConcernMyQuestionReply;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.UserConcernTopic;
import com.sse.ssbk.entity.UserConcernUser;
import com.sse.ssbk.enums.ConcernTargetType;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ConcernService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ConcernMyQuestionReplyVo;

@Slf4j
@Service
public class ConcernServiceImpl implements ConcernService {
    @Autowired
    private UserConcernUserDao userConcernUserDao;
    @Autowired
    private UserConcernTopicDao userConcernTopicDao;
    @Autowired
    private UserConcernDao userConcernDao;
    @Autowired
    private ForumTopicDao forumTopicDao;

    @Override
    public void add(Integer targetId, ConcernTargetType targetType) {
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        Date now = new Date();
        if (ConcernTargetType.USER.equals(targetType)) {
            Set<Integer> concernUserTopics = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + userId);
            if (BizUtils.contains(concernUserTopics, targetId)) {
                log.error("用户已经关注了" + "targetType：" + targetType + ", targetId：" + targetId + "。");
                return;
            }
            UserConcernUser record = new UserConcernUser();
            record.setConcernUserId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            userConcernUserDao.insertSelective(record);
            RedisCacheUtils.addForSet(RedisConstants.CONCERN_USER_USER + userId, targetId);
        } else if (ConcernTargetType.TOPIC.equals(targetType)) {
            Set<Integer> concernUserTopics = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_TOPIC + userId);
            if (BizUtils.contains(concernUserTopics, targetId)) {
                log.error("用户已经关注了" + "targetType：" + targetType + ", targetId：" + targetId + "。");
                return;
            }
            UserConcernTopic record = new UserConcernTopic();
            record.setTopicId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            userConcernTopicDao.insertSelective(record);
            RedisCacheUtils.addForSet(RedisConstants.CONCERN_USER_TOPIC + userId, targetId);
            RedisCacheUtils.addForSet(RedisConstants.CONCERN_TOPIC_USER + targetId, userId);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public void del(Integer targetId, ConcernTargetType targetType) {
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        if (ConcernTargetType.USER.equals(targetType)) {
            RedisCacheUtils.delForSet(RedisConstants.CONCERN_USER_USER + userId, targetId);
            UserConcernUser record = new UserConcernUser();
            record.setConcernUserId(targetId);
            record.setUserId(userId);
            userConcernUserDao.delete(record);
        } else if (ConcernTargetType.TOPIC.equals(targetType)) {
            RedisCacheUtils.delForSet(RedisConstants.CONCERN_USER_TOPIC + userId, targetId);
            RedisCacheUtils.delForSet(RedisConstants.CONCERN_TOPIC_USER + targetId, userId);
            UserConcernTopic record = new UserConcernTopic();
            record.setTopicId(targetId);
            record.setUserId(userId);
            userConcernTopicDao.delete(record);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    /**
     * 
     * @param session
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:27:25
     */
    private void validateSession(UserSession session) {
        if (session == null || session.getUserId() == null) {
            throw new SSEBKRuntimeException(AppReturnCode.LoginOutDate);
        }
    }

    @Override
    public List<ConcernMyQuestionReplyVo> listMyQuestionReplys(long pageNo, long pageSize) {
        this.validateMyQuestionReplysInput(pageNo, pageSize);
        List<ConcernMyQuestionReplyVo> voConcernMyQuestionReplys = new ArrayList<ConcernMyQuestionReplyVo>();
        UserSession session = BizUtils.getCurrentUser();
        Integer userId = session.getUserId();
        long offset = pageSize * (pageNo - 1);
        long limit = pageSize;
        List<ConcernMyQuestionReply> concernMyQuestionReplys = userConcernDao.selectMyQuestionReplys(userId, offset, limit);

        // 一个问题即是关注人提问的也是关注版本的，需要去重，保留人的样式.
        List<Integer> myConcernUserQuestionIds = new ArrayList<Integer>();
        for (int i = 0; i < concernMyQuestionReplys.size(); i++) {
            if ("1".equals(concernMyQuestionReplys.get(i).getSource())) {
                myConcernUserQuestionIds.add(concernMyQuestionReplys.get(i).getQuestionId());
            }
        }
        Iterator<ConcernMyQuestionReply> iConcernMyQuestionReplys = concernMyQuestionReplys.iterator();
        while (iConcernMyQuestionReplys.hasNext()) {
            ConcernMyQuestionReplyVo voConMyQuesReply = new ConcernMyQuestionReplyVo();
            ConcernMyQuestionReply concernMyQuestionReply = iConcernMyQuestionReplys.next();
            Integer questionId = concernMyQuestionReply.getQuestionId();
            if ("0".equals(concernMyQuestionReply.getSource()) && myConcernUserQuestionIds.contains(questionId)) {
                continue;
            }
            voConMyQuesReply.setQuestionId(String.valueOf(questionId));
            voConMyQuesReply.setReplyId(concernMyQuestionReply.getReplyId() == null ? "" : concernMyQuestionReply.getReplyId().toString());
            voConMyQuesReply.setQuestionTitle(concernMyQuestionReply.getQuestionTitle());
            voConMyQuesReply.setContent(concernMyQuestionReply.getReplyId() == null ? concernMyQuestionReply.getQuestionContent() : concernMyQuestionReply.getReplyContent());
            voConMyQuesReply.setImageArr(BizUtils.getArrImages(concernMyQuestionReply.getImages()));

            Is isAnonymous = Is.parse(concernMyQuestionReply.getIsAnonymous());
            if (Is.YES.equals(isAnonymous) && !userId.equals(concernMyQuestionReply.getUserId())) {
                voConMyQuesReply.setUserId("");
                voConMyQuesReply.setUserName("匿名");
                voConMyQuesReply.setUserPhoto("");
                voConMyQuesReply.setCompany("未公开");
            } else {
                voConMyQuesReply.setUserId(concernMyQuestionReply.getUserId() == null ? "" : concernMyQuestionReply.getUserId().toString());
                voConMyQuesReply.setUserName(concernMyQuestionReply.getUserName());
                voConMyQuesReply.setUserPhoto(concernMyQuestionReply.getUserPhoto());
                voConMyQuesReply.setCompany(concernMyQuestionReply.getCompany());
            }

            List<ForumTopic> forumTopics = forumTopicDao.selectByQuestionIdUserId(questionId, userId);
            if (!CollectionUtils.isEmpty(forumTopics)) {
                ForumTopic forumTopic = forumTopics.get(0);
                voConMyQuesReply.setTopicId(forumTopic.getTopicId().toString());
                voConMyQuesReply.setTopicName(forumTopic.getTopicName());
                voConMyQuesReply.setImgUrl(forumTopic.getTopicPicture());
                Set<Integer> setConcernTopicUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER + forumTopic.getTopicId());
                // voConMyQuesReply.setIsConcern(setConcernUserUsers.contains(concernMyQuestionReply.getUserId()) ? Is.YES.getCode() : Is.NO.getCode());
                voConMyQuesReply.setConcernNum(CollectionUtils.isEmpty(setConcernTopicUsers) ? 0 : setConcernTopicUsers.size());
            } else {
                log.error("questionId：" + questionId + "，userId：" + userId + "此时没有对应的板块信息，实际上应该存在");
            }
            this.setTopics(voConMyQuesReply, questionId);

            voConMyQuesReply.setSource(concernMyQuestionReply.getSource());

            Set<Integer> setLikeQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.LIKE_QUESTION_USER + questionId);
            voConMyQuesReply.setIsLike(setLikeQuestionUsers.contains(userId) ? Is.YES.getCode() : Is.NO.getCode());
            voConMyQuesReply.setLikeNum(CollectionUtils.isEmpty(setLikeQuestionUsers) ? 0 : setLikeQuestionUsers.size());
            Set<Integer> sQuestionReplys = RedisCacheUtils.getForSet(RedisConstants.QUESTION_REPLY + questionId);
            voConMyQuesReply.setReplyNum(CollectionUtils.isEmpty(sQuestionReplys) ? 0 : sQuestionReplys.size());
            voConcernMyQuestionReplys.add(voConMyQuesReply);
        }
        return voConcernMyQuestionReplys;
    }

    /**
     * <p>后来添加的，最多两条.
     * @param voForumQuestionList
     * @param questionId
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午6:09:24
     */
    private void setTopics(ConcernMyQuestionReplyVo voConMyQuesReply, Integer questionId) {
        List<ForumTopic> forumTopics = forumTopicDao.selectByQuestionId(questionId);
        List<String> topics = new ArrayList<String>();
        for (int i = 0; i < forumTopics.size() && i < 2; i++) {
            topics.add(forumTopics.get(i).getTopicName());
        }
        voConMyQuesReply.setTopics(topics);
    }

    /**
     * 
     * @param pageNo
     * @param pageSize
     * @author HT-LiChuanbin 
     * @version 2017年8月4日 下午7:58:16
     */
    private void validateMyQuestionReplysInput(long pageNo, long pageSize) {
    }

    public static void main(String[] args) {
        Set<Integer> setTmps = new HashSet<Integer>();
        setTmps.add(1);
        setTmps.add(2);
        setTmps.add(3);
        setTmps.add(null);
        boolean bool = setTmps.contains(null);
        System.out.println(bool);
    }
}