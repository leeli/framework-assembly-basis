package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/7/31.
 */
@Data
public class CollectNewsVo {
    private String newsId;
    private String title;
    private String readNum;     //阅读数
    private String createTime;
    private String collectNum;   //收藏数
    private String commentNum;   //评论数
}
