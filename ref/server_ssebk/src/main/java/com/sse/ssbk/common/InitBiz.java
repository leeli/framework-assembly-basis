package com.sse.ssbk.common;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.sse.ssbk.entity.ScoreRule;
import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.service.ScoreRuleService;

/**
 * 业务初始化类.
 * @author  HT-LiChuanbin 
 * @version 2017年8月10日 下午4:36:30
 */
@Component
@Lazy(false)
public class InitBiz {
    @Autowired
    private ScoreRuleService scoreRuleService;

    /**
     * 初始化积分规则.
     * @author  HT-LiChuanbin 
     * @version 2017年8月11日 下午4:00:15
     */
    @PostConstruct
    public void initScore() {
        List<ScoreRule> scoreRules = scoreRuleService.list();
        Iterator<ScoreRule> iScoreRules = scoreRules.iterator();
        while (iScoreRules.hasNext()) {
            ScoreRule scoreRule = iScoreRules.next();
            for (ScoreRules strategy : ScoreRules.values()) {
                // 此处最好根据积分规则的名称（最好是英文）填充积分枚举的值，毕竟此处的ID不是业务属性.
                if (Integer.compare(strategy.getRuleId(), scoreRule.getScoreRuleId()) == 0) {
                    strategy.setDesc(scoreRule.getScoreName());
                    strategy.setScore(scoreRule.getScoreValue());
                }
            }
        }
    }
}