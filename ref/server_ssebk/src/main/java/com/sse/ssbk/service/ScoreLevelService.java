package com.sse.ssbk.service;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ScoreLevel;

/**
 * Created by yxf on 2017/7/7.
 */
public interface ScoreLevelService{

    ScoreLevel getScoreLevelByLevelId(String levelId);
}
