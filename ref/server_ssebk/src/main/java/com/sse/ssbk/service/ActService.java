package com.sse.ssbk.service;

import com.sse.ssbk.dto.ActDto;
import com.sse.ssbk.entity.ActSubject;
import com.sse.ssbk.vo.*;
import com.sse.ssbk.entity.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by yxf on 2017/7/17.
 */
public interface ActService {

    /**
     * 获取活动轮播图
     * @return
     */
    List<CircleActVo> getCircleAct();


    /**
     * 写入活动接口
     * @param actDto
     * @return
     */
    void addAct(ActDto actDto);

    /**
     * 获取活动主题列表  设计图02e
     * @param lastSubjectId
     * @param pageSize
     * @param sortBy
     * @return
     */
    List<ActSubject> getActSubjects(Integer lastSubjectId, Integer pageSize, String sortBy);
    //todo 未完成
    /**
     * 获取活动详情
     * @param actId  活动id
     * @return
     */
    ActivityDetailsVo getActDetailsByActId(Integer actId);

    /**
     * 获取报名人/参与人列表
     * @param lastUserId
     * @param actId
     * @param pageSize
     * @param status
     * @return
     */
    List<UserEnrollVo> getEnrollUserList(String lastUserId, String actId, String pageSize,String status);

    /**
     * 4.4.5.	获取某个区域或主题的活动列表
     * @param lastActId
     * @param pageSize
     * @param subjectId
     * @param cityId
     * @param status
     * @return
     */
    List<ActivityCitySubjectVo> getCityOrSubjectActivity(String lastActId, String pageSize, String subjectId, String cityId, String status);

    /**
     * 获取我的活动列表
     * @param lastActId
     * @param pageSize
     * @return
     */
    List<UserActivityCitySubjectVo> getMyActivity(String lastActId,String pageSize);

    /**
     * 获取我参与的活动列表
     * @param lastActId
     * @param pageSize
     * @param status
     * @return
     */
    List<UserActivityCitySubjectVo> getIJoinActivity(String lastActId, String userId,String status, String pageSize);


    Map<String,Object> getHotActivity();

}
