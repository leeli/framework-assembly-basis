package com.sse.ssbk.dto;

import lombok.Data;

/**
 * Created by ciyua on 2017/8/8.
 */
@Data
public class SmsCaptchaDto {
    private String type;
    private String userName;
    private String picCaptcha;
    private String session;
}
