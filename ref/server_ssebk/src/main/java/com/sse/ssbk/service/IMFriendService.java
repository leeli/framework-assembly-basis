package com.sse.ssbk.service;

import com.sse.ssbk.dto.AgreeFriendDto;
import com.sse.ssbk.dto.RequestFriendDto;
import com.sse.ssbk.entity.IMFriend;
import com.sse.ssbk.vo.IMFriendVo;
import com.sse.ssbk.vo.IMReqFriendsVo;

import java.util.List;
import java.util.Map;

/**
 * Created by work_pc on 2017/8/9.
 */
public interface IMFriendService {

    int isFriend(Integer friendUserId);

    /**
     * 放入redis中
     * 请求添加好友
     * @param requestFriendDto
     */
    void requesTAddFrined(RequestFriendDto requestFriendDto);

    /**放入mysql
     * 同意添加好友
     * @param agreeFriendDto
     */
    int agressAddFrined(AgreeFriendDto agreeFriendDto);

    /**
     * 查找用户：手机号或姓名
     * @param searchKeys
     * @return
     */
    List<IMFriendVo> searchFriend(String searchKeys);

    /**
     * 请求好友列表
     * @return
     */
    List<IMReqFriendsVo> reqFriends();

    /**
     * 通讯录列表
     * @return
     */
    Map<String,Object> contactList();
}
