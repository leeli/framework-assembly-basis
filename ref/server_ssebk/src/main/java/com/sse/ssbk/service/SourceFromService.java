package com.sse.ssbk.service;

import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.entity.SourceFrom;
import com.sse.ssbk.vo.SourceFromVo;

import java.util.List;

public interface SourceFromService {
	List<SourceFromVo> getSourceFromList();

	SourceFrom getSourceFromById(String id);
}