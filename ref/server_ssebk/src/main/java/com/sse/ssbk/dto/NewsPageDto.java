package com.sse.ssbk.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewsPageDto {
	@NotNull
	private Integer lastNewsId;
	@NotNull
	private Integer pageSize;
	@NotBlank
	private String sourceFromId;
}
