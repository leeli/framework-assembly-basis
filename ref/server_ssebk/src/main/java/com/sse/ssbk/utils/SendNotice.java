package com.sse.ssbk.utils;

import com.sse.ssbk.enums.NoticeTypes;
import com.sse.ssbk.enums.ScoreRules;

import java.lang.annotation.*;

/**
 * Created by work_pc on 2017/8/21.
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SendNotice {
        NoticeTypes value() default NoticeTypes.Other;
}
