package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.IMFriend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by work_pc on 2017/8/7.
 */
public interface IMFriendDao extends BaseDao<IMFriend> {
     int isFriend(@Param("userId") int userId, @Param("friendUserId") Integer friendUserId);

     void requestAddFrined(Integer userId,Integer friendUserId);

     void agressAddFrined(Integer userId,Integer friendUserId);

     List<IMFriend> searchFriend(String searchKeys);
}
