package com.sse.ssbk.enums;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/18.
 */
public enum NoticeTypes {
    Other("其他","-1","其他"),
    ActivityNeedAudit("活动需审核","1","活动报名"),
    ActivityAuditPass("活动报名审核通过","1","审核活动报名"),
    ActivityAuditUnpass("活动报名审核不通过","1","审核活动报名"),

    QuestionHasReply("问题被回答","2","问题被回答"),

    QuestionHasComment("问题被评论","3","问答被评论"),
    ReplyHasComment("回答被评论","3","问答被评论"),
    CommentHasComment("评论被评论","3","问答被评论"),

    QuestionHasLike("问题被点赞","2","点赞"),
    ReplyHasLike("回答被点赞","4","点赞"),
    CommentHasLike("评论被点赞","3","点赞"),

    /*NewsAuditPass("资讯被审核通过",7),
    NewsAuditUnPass("资讯审核不通过",8),*/

    UserHasConcerned("用户被关注","5","关注"),
    SystemMessage("系统消息","6","系统消息");

    private String desc;
    private String type;
    private String typeDesc;

    private NoticeTypes(String desc,String type,String typeDesc){
        this.desc = desc;
        this.type = type;
        this.typeDesc = typeDesc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }
}
