package com.sse.ssbk.vo;

import lombok.Data;

/**
 * 供ForumQuestionVo中的topics属性使用.
 * @author  HT-LiChuanbin 
 * @version 2017年7月25日 上午11:25:22
 */
@Data
public class ForumReplyGetForumTopicVo {
    private String topicId;
    private String topicName;
    private String imgUrl;
    private String isConcern;
    private String concernNum;

    public ForumReplyGetForumTopicVo() {
        this.topicId = "";
        this.topicName = "";
        this.imgUrl = "";
        this.isConcern = "";
        this.concernNum = "";
    }
}