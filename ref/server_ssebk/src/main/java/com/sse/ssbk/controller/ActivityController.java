package com.sse.ssbk.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.dto.ActDto;
import com.sse.ssbk.dto.ForumQuestionListDto;
import com.sse.ssbk.entity.EnrollActivity;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ActSubjectService;
import com.sse.ssbk.service.EnrollActService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.utils.SendNotice;
import com.sse.ssbk.vo.*;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.service.ActService;
import com.sse.ssbk.service.UserConcernService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.nio.cs.UnicodeEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by yxf on 2017/7/7.
 */

@Controller
@RequestMapping("/ssebk")
public class ActivityController extends BaseController {
    @Autowired
    private ActService actService;

    @Autowired
    private ActSubjectService actSubjectService;

    @Autowired
    private UserConcernService userConcernService;

    @Autowired
    private EnrollActService enrollActService;

    //todo 接口设计文档未写
    @PostMapping(value = "/act/activity")
    @ResponseBody
    public Result<?> addActivity(@RequestBody @Valid ActDto actDto) {
        actService.addAct(actDto);
        return ResultUtils.success();
    }

    @GetMapping(value = "act/circleimage")
    @ResponseBody
    public Result<?> getCircleAct() {
        List<CircleActVo> circleImageList = actService.getCircleAct();
        Map<String, Object> data = new HashMap<>();
        data.put("circleImageList", circleImageList);
        return ResultUtils.success("活动轮播图", data);
    }

    //主题列表
    @GetMapping(value = "act/subjects")
    @ResponseBody
    public Result<?> getActSubjects(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastSubjectId = jData.getString("lastSubjectId");
        String pageSize = jData.getString("pageSize");

        List<ActSubjectVo> actSubjectVoList = actSubjectService.getActSubjects(lastSubjectId, pageSize);
        if (actSubjectVoList == null || actSubjectVoList.size() == 0) {
            lastSubjectId = lastSubjectId;
        } else {
            lastSubjectId = actSubjectVoList.get(actSubjectVoList.size() - 1).getSubjectId();
        }
        Map<String, Object> subjectList = new HashMap<>();
        subjectList.put("lastSubjectId", Integer.valueOf(lastSubjectId));
        subjectList.put("subjectList", actSubjectVoList);
        return ResultUtils.success("活动主题列表", subjectList);
    }

    @PostMapping(value = "act/enroll")
    @ResponseBody
    @SendNotice
    public Result<?> enrollAct(@RequestBody @Valid EnrollActivity enrollActivity) {
        enrollActivity = enrollActService.enrollAct(enrollActivity);
        Map<String, Object> data = new HashMap<>();
        if ("0".equals(enrollActivity.getStatus())) {
            data.put("status", "0");
            data.put("reason", "需要审核");
        } else if ("1".equals(enrollActivity.getStatus())) {
            data.put("status", "1");
            data.put("reason", "无需审核，报名成功");
        } else {
            data.put("status", "2");
            data.put("reason", "报名失败");
        }
        return ResultUtils.success(data);
    }

    @GetMapping(value = "act/enrolls")
    @ResponseBody
    public Result<?> getEnrollUserList(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastUserId = jData.getString("lastUserId");
        String pageSize = jData.getString("pageSize");
        String actId = jData.getString("actId");
        String enrollStatus = jData.getString("enrollStatus");

        if (lastUserId == null || pageSize == null || actId == null || enrollStatus == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }
        List<UserEnrollVo> enrollUserList = actService.getEnrollUserList(lastUserId, actId, pageSize, enrollStatus);
        // @author LiChuanbin，为了兼容客户端，这里需呀修改一下。
        Map<String,Object> dataSource = new HashMap<>();
        if (enrollUserList != null && enrollUserList.size() != 0){
            dataSource.put("lastUserId",enrollUserList.get(enrollUserList.size()-1).getUserId());
        }else{
            dataSource.put("lastUserId",lastUserId);
        }
        dataSource.put("enrollList",enrollUserList);
        /*if ("0".equals(enrollStatus)){
            return ResultUtils.success("审核列表",dataSource);
        }else if ("1".equals(enrollStatus)){
            return ResultUtils.success("参与人列表",dataSource);
        }else if ("2".equals(enrollStatus)){
            return ResultUtils.success("未通过列表",dataSource);
        }else{
            return ResultUtils.success("报名人",dataSource);
        }*/
        return ResultUtils.success(dataSource);
    }

    /**
     * 主题活动查询
     * @return
     */
    @GetMapping(value = "act/subjectActivitys")
    @ResponseBody
    public Result<?> getActivitySubjects(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastSubjectId = jData.getString("lastSubjectId");
        String pageSize = jData.getString("pageSize");

        if (lastSubjectId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        List<ActivitySubjectVo> activitySubjects = actSubjectService.getActivitySubjects(lastSubjectId, pageSize);
        if (activitySubjects != null && activitySubjects.size() != 0) {
            lastSubjectId = activitySubjects.get(activitySubjects.size() - 1).getSubjectId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastSubjectId", lastSubjectId);
        dataSource.put("subjectActivityList", activitySubjects);
        return ResultUtils.success("主题活动查询", dataSource);
    }

    /**
     * 更多区域活动接口查询
     * @return
     */
    @GetMapping(value = "act/citys")
    @ResponseBody
    public Result<?> getActivityCity(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastCityId = jData.getString("lastCityId");  //provinceId
        String pageSize = jData.getString("pageSize");

        if (lastCityId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        List<ActivityCityVo> activityCityList = actSubjectService.getActivityCity(lastCityId, pageSize);
        if (activityCityList != null && activityCityList.size() != 0) {
            lastCityId = activityCityList.get(activityCityList.size() - 1).getCityId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastCityId", lastCityId);
        dataSource.put("cityList", activityCityList);
        return ResultUtils.success("区域活动查询", dataSource);
    }

    @RequestMapping(value = "act/{actId}", method = RequestMethod.GET)
    @ResponseBody
    public Result<?> getActById(@PathVariable("actId") Integer id) {
        ActivityDetailsVo activityDetailsVo = actService.getActDetailsByActId(id);
        return ResultUtils.success("活动详情", activityDetailsVo);
    }

    @GetMapping(value = "act/city/activitys")
    @ResponseBody
    public Result<?> getCityOrSubjectActivity(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastActId = jData.getString("lastActId");
        String pageSize = jData.getString("pageSize");
        String subjectId = jData.getString("subjectId");
        String cityId = jData.getString("cityId");
        String status = jData.getString("status");

        if (lastActId == null || pageSize == null || status == null || (subjectId == null && cityId == null)) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        Map<String, Object> dataSource = new HashMap<>();
        List<ActivityCitySubjectVo> cityOrSubjectActivity = actService.getCityOrSubjectActivity(lastActId, pageSize, subjectId, cityId, status);
        if (cityOrSubjectActivity != null && cityOrSubjectActivity.size() != 0) {
            lastActId = cityOrSubjectActivity.get(cityOrSubjectActivity.size() - 1).getActId();
        }
        dataSource.put("lastActId", lastActId);
        dataSource.put("actList", cityOrSubjectActivity);
        return ResultUtils.success("活动列表", dataSource);
    }

    /**
     * 首页热门活动列表
     * @return
     */
    @GetMapping(value = "act/city/indexactivitys")
    @ResponseBody
    public Result<?> hotActivityList(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        //暂时不用
        String cityId = jData.getString("cityId");

        Map<String, Object> dataSource = actService.getHotActivity();
        /*Map<String,Object> dataSource = new HashMap<>();
        List<ActivityCitySubjectVo> cityOrSubjectActivity = actService.getCityOrSubjectActivity(lastActId, pageSize, subjectId, cityId, status);
        if (cityOrSubjectActivity != null && cityOrSubjectActivity.size() !=0){
            lastActId = cityOrSubjectActivity.get(cityOrSubjectActivity.size()-1).getActId();
        }
        dataSource.put("actList",cityOrSubjectActivity);
        return ResultUtils.success("活动列表",dataSource);*/
        return ResultUtils.success("热门活动", dataSource);
    }

    /*  @RequestMapping(value = "news",method = RequestMethod.GET)
      @ResponseBody
      public Object getNews(){

          return null;
      }*/
}
