package com.sse.ssbk.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.dto.ForumQuestionListDto;
import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ForumQuestionService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.CountScore;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.ForumQuestionGetVo;
import com.sse.ssbk.vo.ForumQuestionListVo;
import com.sse.ssbk.vo.ForumQuestionSaveVo;

/**
 * 你问我答模块提问控制器.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:03:15
 */
@RestController
@RequestMapping("/ssebk/forum")
public class ForumQuestionController extends BaseController {
    @Autowired
    private ForumQuestionService forumQuestionService;

    /**
     * 添加问题.
     * @param dto ForumQuestionDto.
     * @return
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:46:51
     */
    @PostMapping("/question")
    @CountScore(ScoreRules.ADD_QUESTION)
    public Result<ForumQuestionSaveVo> add(HttpServletRequest req, @RequestBody @Valid ForumQuestionDto dto) {
        ForumQuestionSaveVo voForumQuestionSave = forumQuestionService.save(dto);
        return ResultUtils.success(voForumQuestionSave);
    }

    /**
     * 删除问题.
     * @param id 问题ID.
     * @return
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:47:02
     */
    @DeleteMapping("/question/{questionId}")
    public Result<?> del(@PathVariable("questionId") Integer id) {
        forumQuestionService.del(id);
        return ResultUtils.success();
    }

    /**
     * 修改问题.
     * @param id  问题ID.
     * @param dto ForumQuestionDto.
     * @return
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:47:11
     */
    @PutMapping("/question/{questionId}")
    public Result<?> upt(@PathVariable("questionId") Integer id, @RequestBody @Valid ForumQuestionDto dto) {
        dto.setQuestionId(id);
        forumQuestionService.upt(dto);
        return ResultUtils.success();
    }

    /**
     * 获取问题.
     * @param id 问题ID.
     * @return
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:47:19
     */
    @GetMapping("/question/{questionId}")
    public Result<ForumQuestionGetVo> get(@PathVariable("questionId") Integer id) {
        ForumQuestionGetVo voForumQuestion = forumQuestionService.get(id);
        return ResultUtils.success(DtoEntityVoUtils.voFieldFromNull2Empty(voForumQuestion));
    }

    /**
     * 获取问题列表.
     * @return
     * @author  HT-LiChuanbin 
     * @version 2017年7月24日 下午3:06:22
     */
    @GetMapping("/questions")
    public Result<List<ForumQuestionListVo>> list(String data) {
        if (StringUtils.isBlank(data)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        ForumQuestionListDto dto = this.getForumQuestionListDto(data);

        String lastId = dto.getLastId();
        String pageSize = dto.getPageSize();
        String title = dto.getTitle();
        String topicId = dto.getTopicId();
        String userId = dto.getUserId();
        Page page = BizUtils.getBizPage(lastId, pageSize);

        boolean boolTitle = StringUtils.isBlank(title);
        boolean boolTopicId = StringUtils.isBlank(topicId);
        boolean boolUserId = StringUtils.isBlank(userId);

        List<ForumQuestionListVo> voForumQuestionLists = new ArrayList<ForumQuestionListVo>();
        if (boolTitle && boolTopicId && boolUserId) {
            voForumQuestionLists = forumQuestionService.list(page);
        } else if (!boolTitle && boolTopicId && boolUserId) {
            voForumQuestionLists = forumQuestionService.listByTitle(dto.getTitle(), page);
        } else if (boolTitle && !boolTopicId && boolUserId) {
            voForumQuestionLists = forumQuestionService.listByTopicId(Integer.parseInt(topicId), page);
        } else if (boolTitle && boolTopicId && !boolUserId) {
            voForumQuestionLists = forumQuestionService.listByUserId(Integer.parseInt(userId), page);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        return ResultUtils.success(voForumQuestionLists);
    }

    /**
     * 
     * @param data
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月25日 下午1:34:18
     */
    private ForumQuestionListDto getForumQuestionListDto(String data) {
        ForumQuestionListDto dto = new ForumQuestionListDto();
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastId = jData.getString("lastId");
        String pageSize = jData.getString("pageSize");
        String title = jData.getString("title");
        String topicId = jData.getString("topicId");
        String userId = jData.getString("userId");
        dto.setLastId(lastId);
        dto.setPageSize(pageSize);
        dto.setTitle(title);
        dto.setTopicId(topicId);
        dto.setUserId(userId);
        return dto;
    }
}