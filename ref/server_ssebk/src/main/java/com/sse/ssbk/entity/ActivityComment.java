package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_activity_comment")
public class ActivityComment {
    @Id
    @Column(name = "act_comment_id")
    private Integer actCommentId;
    @Column(name = "content")
    private String content;
    @Column(name = "act_comment_images")
    private String actCommentImages;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "act_id")
    private Integer actId;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
    @Column(name = "relation")
    private String relation;
    @Column(name = "user_photo")
    private String userPhoto;
}