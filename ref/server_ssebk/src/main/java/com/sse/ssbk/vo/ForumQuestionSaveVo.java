package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class ForumQuestionSaveVo {
    private String questionId;

    public ForumQuestionSaveVo() {
        this.questionId = "";
    }
}