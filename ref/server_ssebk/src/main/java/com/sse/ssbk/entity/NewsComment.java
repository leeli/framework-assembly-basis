package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_news_comment")
public class NewsComment {
    @Id
    @Column(name = "news_comment_id")
    private Integer newsCommentId;
    @Column(name = "content")
    private String content;
    @Column(name = "news_id")
    private Integer newsId;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_photo")
    private String userPhoto;
    @Column(name = "company_id")
    private Integer companyId;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
    @Column(name = "relation")
    private String relation;
}