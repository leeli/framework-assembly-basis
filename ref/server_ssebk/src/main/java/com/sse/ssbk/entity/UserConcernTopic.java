package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_user_concern_topic")
public class UserConcernTopic {
    @Column(name = "topic_id")
    private Integer topicId;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "create_time")
    private Date createTime;
}