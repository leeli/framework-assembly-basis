package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.UserConcernTopic;

public interface UserConcernTopicDao extends BaseDao<UserConcernTopic> {
}