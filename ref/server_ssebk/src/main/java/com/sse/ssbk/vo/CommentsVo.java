package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CommentsVo {
    
    private String commentId;
    private String parentCommentId;
    private String commentTopId;
    private String content;
    private List<String> imageArr;
    private Integer commentCount;
    private Integer likeCount;
    private String isLike;
    private String createTime;

    private String userId;
    private String userName;
    private String userPhoto;
    private String company;
    private String isConcern;

    private String toUserName;

    public CommentsVo() {
        this.commentId = "";
        // TODO 使用默认常量替代，消除硬编码.
        this.parentCommentId = "";
        this.commentTopId = "";
        this.content = "";
        this.imageArr = new ArrayList<String>();
        this.commentCount = 0;
        this.likeCount = 0;
        this.isLike = "";
        this.createTime = "";
        this.userId = "";
        this.userName = "";
        this.userPhoto = "";
        this.company = "";
        this.isConcern = "";
        this.toUserName = "";
    }
}