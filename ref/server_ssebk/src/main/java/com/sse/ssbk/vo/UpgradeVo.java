package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class UpgradeVo {
    private String upgradeDesc;
    private String versionNo;
    private String isForce;
    private String isUpgrade;
    private String url;
}