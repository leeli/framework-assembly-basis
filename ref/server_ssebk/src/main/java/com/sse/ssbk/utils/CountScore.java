package com.sse.ssbk.utils;

import java.lang.annotation.*;

import com.sse.ssbk.enums.ScoreRules;

/**
 * Created by work_pc on 2017/8/10.
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CountScore {
    ScoreRules value() default ScoreRules.NONE;
}