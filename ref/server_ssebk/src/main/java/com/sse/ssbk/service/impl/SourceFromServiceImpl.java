package com.sse.ssbk.service.impl;

import com.sse.ssbk.dao.SourceFromDao;
import com.sse.ssbk.entity.SourceFrom;
import com.sse.ssbk.service.SourceFromService;
import com.sse.ssbk.vo.SourceFromVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yxf on 2017/7/20.
 */
@Service
public class SourceFromServiceImpl implements SourceFromService {

    @Autowired
    private SourceFromDao sourceFromDao;

    @Override
    public List<SourceFromVo> getSourceFromList() {
        return sourceFromDao.getSourceFromList();
    }

    @Override
    public SourceFrom getSourceFromById(String id) {
        SourceFrom sourceFrom = new SourceFrom();
        sourceFrom.setSourceFromId(Integer.parseInt(id));
        return sourceFromDao.selectByPrimaryKey(sourceFrom);
    }
}
