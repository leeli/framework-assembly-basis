package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
@Entity
@Table(name = "t_user_collection_reply")
public class UserCollectReply {
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "reply_id")
    private Integer replyId;
    @Column(name = "create_time")
    private Date createTime;
}
