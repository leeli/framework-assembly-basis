package com.sse.ssbk.vo;

import com.sse.ssbk.entity.Activity;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
public class CircleActVo{
    private Integer actId;
    private String actName;
    private String imgUrl;
    private String index;
}
