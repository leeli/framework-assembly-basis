package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import com.sse.ssbk.utils.TransIgnore;

@Data
@Entity
@Table(name = "t_news")
public class News {
    @Id
    @Column(name = "news_id")
    private Integer newsId;
    private String userName;
    private Integer userId;
    private String title;
    private String content;
    private String status;
    private Date createTime;
    private Date updateTime;
    @Column(name = "image")
    @TransIgnore
    private String images;
    private String sourceFrom;
    private Integer companyId;
    private String companyName;
    private String userPhoto;
    @Column(name = "source_from_id")
    private String sourceFromId;
    @TransIgnore
    //获取我的资讯列表时，手动赋值
    private String reason;
}
