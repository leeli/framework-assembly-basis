package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.*;
import com.sse.ssbk.entity.*;
import com.sse.ssbk.service.EnrollActService;
import com.sse.ssbk.service.UserCollectService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DateUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ActivityCitySubjectVo;
import com.sse.ssbk.vo.CollectNewsVo;
import com.sse.ssbk.vo.CollectQuestionVo;
import com.sse.ssbk.vo.CollectReplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by work_pc on 2017/7/31.
 */
@Service
public class UserCollectServiceImpl implements UserCollectService {

    @Autowired
    private UserCollectQuestionDao userCollectQuestionDao;

    @Autowired
    private UserCollectReplyDao userCollectReplyDao;

    @Autowired
    private UserCollectActDao userCollectActDao;

    @Autowired
    private UserCollectNewsDao userCollectNewsDao;

    @Autowired
    private ForumReplyDao forumReplyDao;

    @Autowired
    private UserConcernDao userConcernDao;

    @Autowired
    private ReplyCommentDao replyCommentDao;

    @Autowired
    private EnrollActService enrollActService;
    
    @Override
    public List<CollectQuestionVo> getMyCollectQuestion(String lastQuestionId, String pageSize) {
//        User logingUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Integer> columns = userCollectQuestionDao.getCollectQuestionColumnsInOrder(loginUser.getUserId(), Integer.valueOf(lastQuestionId), Integer.valueOf(pageSize));
        if (columns == null || columns.size() == 0){
            return new ArrayList<CollectQuestionVo>();
        }
        List<CollectQuestionVo> collectQuestions = userCollectQuestionDao.getCollectQuestion(columns);
        for (CollectQuestionVo collectQuestionVo :
             collectQuestions) {
            ForumReply forumReply = new ForumReply();
            forumReply.setQuestionId(Integer.valueOf(collectQuestionVo.getQuestionId()));
            int replyNum = forumReplyDao.selectCount(forumReply);
            collectQuestionVo.setReplyNum(String.valueOf(replyNum));
            String likeNum = "0";
            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_QUESTION_USER + collectQuestionVo.getQuestionId());
            if (forSet!= null){
                likeNum = String.valueOf(forSet.size());
            }
            collectQuestionVo.setLikeNum(likeNum);
            long createTime = DateUtils.StringToDate4(collectQuestionVo.getCreateTime()).getTime();
            collectQuestionVo.setCreateTime(String.valueOf(createTime));
            collectQuestionVo.setIsCollect("1");
            Integer isConcern = 0;
            if ("1".equals(collectQuestionVo.getIsAnonymous()) && Integer.valueOf(collectQuestionVo.getUserId()).intValue() != loginUser.getUserId().intValue()){  //匿名且非当前用户查看
                //匿名
                collectQuestionVo.setUserId("");
                collectQuestionVo.setUserPhoto("");
                collectQuestionVo.setUserName("匿名");
                collectQuestionVo.setCompany("未公开");
                //todo 对于匿名用户暂定为已关注
                collectQuestionVo.setIsConcern("1");
            }else {
                isConcern =  userConcernDao.isConcernedUser(loginUser.getUserId(), Integer.valueOf(collectQuestionVo.getUserId()));
                collectQuestionVo.setIsConcern(String.valueOf(isConcern));
            }
            collectQuestionVo.setIsCollect("1");
        }

        return collectQuestions;
    }

    @Override
    public List<CollectReplyVo> getMyCollectReply(String lastReply, String pageSize) {
//        User logingUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Integer> columns = userCollectReplyDao.getCollectReplyColumnsInOrder(loginUser.getUserId(), Integer.valueOf(lastReply), Integer.valueOf(pageSize));
        if (columns == null || columns.size() == 0){
            return new ArrayList<CollectReplyVo>();
        }
        List<CollectReplyVo> collectReplyVos = userCollectReplyDao.getCollectReply(columns);
        for (CollectReplyVo collectReplyVo  :
                collectReplyVos) {
            ReplyComment replyComment = new ReplyComment();
            replyComment.setReplyId(Integer.valueOf(collectReplyVo.getReplyId()));
            int commnetNum = replyCommentDao.selectCount(replyComment);

            int likeNum = 0;
            Set<Object> forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + collectReplyVo.getReplyId());
            if (forSet!= null){
                likeNum = forSet.size();
            }
            collectReplyVo.setLikeNum(String.valueOf(likeNum));
            collectReplyVo.setCommentNum(String.valueOf(commnetNum));
            long createTime = DateUtils.StringToDate4(collectReplyVo.getCreateTime()).getTime();
            collectReplyVo.setCreateTime(String.valueOf(createTime));
            collectReplyVo.setIsCollect("1");
            Integer isConcern = 0;
            if ("1".equals(collectReplyVo.getIsAnonymous()) && replyComment.getUserId().intValue() != loginUser.getUserId().intValue()){  //匿名且不是作者查看
                //匿名
                collectReplyVo.setUserId("");
                collectReplyVo.setUserPhoto("");
                collectReplyVo.setUserName("匿名");
                collectReplyVo.setCompany("未公开");
                //todo 对于匿名用户暂定为已关注
                collectReplyVo.setIsConcern("1");
            }else {
                isConcern =  userConcernDao.isConcernedUser(loginUser.getUserId(), Integer.valueOf(collectReplyVo.getUserId()));
                collectReplyVo.setIsConcern(String.valueOf(isConcern));
            }
//            collectReplyVo.setIsCollect(String.valueOf(isConcern));
        }

        return collectReplyVos;
    }

    @Override
    public List<ActivityCitySubjectVo> getMyCollectActivity(String lastActId, String pageSize) {
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Integer> columns = userCollectActDao.getCollectActColumnsInOrder(loginUser.getUserId(), Integer.valueOf(lastActId), Integer.valueOf(pageSize));
        if (columns == null || columns.size() == 0){
            return new ArrayList<ActivityCitySubjectVo>();
        }

        List<Activity> activityList = userCollectActDao.getCollectAct(columns);

        List<ActivityCitySubjectVo> activityCitySubjectVos = new ArrayList<>();

        ActivityCitySubjectVo activityCitySubjectVo;
        String[] actUserPhotos;
        for (Activity activity:activityList){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,ActivityCitySubjectVo.class);
            activityCitySubjectVo.setActImages(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            if (actUserNum!=null && Integer.valueOf(actUserNum)>= Constants.ISHOTNUM){
                isHot = "1";
            }

            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);
            activityCitySubjectVos.add(activityCitySubjectVo);
        }
        return activityCitySubjectVos;
    }

    @Override
    public List<CollectNewsVo> getMyCollectNews(String lastNewsId, String pageSize) {
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Integer> columns = userCollectNewsDao.getCollectNewsColumnsInOrder(loginUser.getUserId(), Integer.valueOf(lastNewsId), Integer.valueOf(pageSize));
        if (columns == null || columns.size() == 0){
            return new ArrayList<CollectNewsVo>();
        }
        List<CollectNewsVo> collectNews = userCollectNewsDao.getCollectNews(columns);
        for (CollectNewsVo collectNewsVo :
                collectNews) {

            Integer readCount = RedisCacheUtils.getInt(RedisConstants.User.ReadCount.READ_COUNT_NEWS+ collectNewsVo.getNewsId());
            Integer collectionCount = RedisCacheUtils.getInt(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + collectNewsVo.getNewsId());
            Integer commentCount = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS+ collectNewsVo.getNewsId());

            if(readCount == null){
                readCount = 0;
            }

            if(collectionCount == null){
                collectionCount = 0;
            }

            if(commentCount == null){
                commentCount = 0;
            }

            collectNewsVo.setReadNum(String.valueOf(readCount));
            collectNewsVo.setCollectNum(String.valueOf(collectionCount));
            collectNewsVo.setCommentNum(String.valueOf(commentCount));
            long createTime = DateUtils.StringToDate4(collectNewsVo.getCreateTime()).getTime();
            collectNewsVo.setCreateTime(String.valueOf(createTime));
        }
        return collectNews;
    }
}
