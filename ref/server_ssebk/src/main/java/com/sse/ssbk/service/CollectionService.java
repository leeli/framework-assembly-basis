package com.sse.ssbk.service;

import com.sse.ssbk.enums.CollectionTargetType;

/**
 * 
 * @author HT-LiChuanbin 
 * @version 2017年7月31日 下午1:35:26
 */
public interface CollectionService {
    /**
     * 
     * @param targetId
     * @param targetType
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:11:15
     */
    public void add(Integer targetId, CollectionTargetType targetType);

    /**
     * 
     * @param targetId
     * @param targetType
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:11:18
     */
    public void del(Integer targetId, CollectionTargetType targetType);
}