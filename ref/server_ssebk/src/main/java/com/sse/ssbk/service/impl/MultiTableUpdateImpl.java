package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.dao.MultiTableUpdateDao;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.entity.UserInfo;
import com.sse.ssbk.service.MultiTableUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by work_pc on 2017/8/16.
 */
@Service
public class MultiTableUpdateImpl implements MultiTableUpdateService {

    @Autowired
    private MultiTableUpdateDao multiTableUpdateDao;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateWithUser(User user) {
        if (user.getUserPhoto() != null){
            for (String table : Constants.UPDATE_PHOTO_TABLE) {
                multiTableUpdateDao.updatePhoto(user.getUserPhoto(), user.getUserId(), table);
            }
        }

        if (user.getCompanyName() != null){
            for (String table: Constants.UPDATE_COMPANY_NAME_TABLE) {
                multiTableUpdateDao.updateCompanyName(user.getCompanyName(), user.getUserId(),table);
            }
        }

        if (user.getCompanyId() != null){
            for (String table :Constants.UPDATE_COMPANY_ID_TABLE) {
                multiTableUpdateDao.updateCompanyId(user.getCompanyId(), user.getUserId(), table);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    @Deprecated
    public void updateWithUserInfo(UserInfo userInfo) {
        if (userInfo.getUserPhoto() != null){
            for (String table : Constants.UPDATE_PHOTO_TABLE) {
                multiTableUpdateDao.updatePhoto(userInfo.getUserPhoto(), userInfo.getUserId(), table);
            }
        }

        if (userInfo.getCompany() != null){
            for (String table : Constants.UPDATE_PHOTO_TABLE) {
                multiTableUpdateDao.updatePhoto(userInfo.getUserPhoto(), userInfo.getUserId(), table);
            }        }
    }
}
