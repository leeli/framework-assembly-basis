package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by yxf on 2017/7/28.
 */
@Data
public class UpdateUserInfoDto {
    private String photo;
    @NotBlank
    private String companyName;
    @NotBlank
    private String jobPosition;
    @NotBlank
    private String provinceId;
    private String profession;
    private String education;
    private String sex;
    private String email;
    private String selfDesc;
}
