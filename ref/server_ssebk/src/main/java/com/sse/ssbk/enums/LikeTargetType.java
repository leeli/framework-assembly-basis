package com.sse.ssbk.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 点赞枚举.
 * @author  HT-LiChuanbin 
 * @version 2017年7月26日 下午8:35:41
 */
public enum LikeTargetType {
    REPLY, REPLYCOMMENT, QUESTION;
    public static LikeTargetType parse(String targetType) {
        if (StringUtils.isBlank(targetType)) {
            return null;
        }
        targetType = targetType.trim();
        for (LikeTargetType type : LikeTargetType.values()) {
            if (StringUtils.equalsIgnoreCase(type.toString(), targetType)) {
                return type;
            }
        }
        return null;
    }
}