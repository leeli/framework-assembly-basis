package com.sse.ssbk.service;

import java.util.List;

import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.vo.ForumQuestionListVo;
import com.sse.ssbk.vo.ForumQuestionSaveVo;
import com.sse.ssbk.vo.ForumQuestionGetVo;

/**
 * ForumQuestion服务.
 * @author  HT-LiChuanbin 
 * @version 2017年7月20日 下午1:31:44
 */
public interface ForumQuestionService {
    /**
     * 添加.
     * @param dto ForumQuestion Dto.
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:29:34
     */
    ForumQuestionSaveVo save(ForumQuestionDto dto);

    /**
     * 删除.
     * <p>注意：需要级联删除，避免脏数据.
     * @param id 问题ID.
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:30:56
     */
    void del(Integer id);

    /**
     * 修改.
     * @param dto ForumQuestion Dto.
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:31:06
     */
    void upt(ForumQuestionDto dto);

    /**
     * 获取.
     * @param id 问题ID.
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:31:16
     */
    ForumQuestionGetVo get(Integer id);

    /**
     * 获取列表.
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午4:03:47
     */
    List<ForumQuestionListVo> list(Page page);

    /**
     * 通过标题获取问题列表.
     * @param title 问题标题.
     * @param page
     * @return
     * @author      HT-LiChuanbin 
     * @version     2017年7月24日 下午3:13:56
     */
    List<ForumQuestionListVo> listByTitle(String title, Page page);

    /**
     * 通过板块ID获取问题列表
     * @param topicId
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月24日 下午3:14:58
     */
    List<ForumQuestionListVo> listByTopicId(Integer topicId, Page page);

    /**
     * 通过用户ID获取问题列表
     * @param userId
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月24日 下午3:15:01
     */
    List<ForumQuestionListVo> listByUserId(Integer userId, Page page);
}