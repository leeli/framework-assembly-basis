package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class ForumTopicsDto {
    private String lastId;
    private String pageSize;
    private String topicName;
}