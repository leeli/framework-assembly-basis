package com.sse.ssbk.dto;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

@Data
public class CommentsDto {
    @NotBlank
    private String targetId;
    @NotBlank
    private String targetType;
    @NotBlank
    private String lastId;
    @NotBlank
    private String pageSize;
}