package com.sse.ssbk.controller;

import javax.validation.Valid;

import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.utils.CountScore;
import com.sse.ssbk.utils.SendNotice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sse.ssbk.dto.LikeAddDto;
import com.sse.ssbk.dto.LikeDelDto;
import com.sse.ssbk.enums.LikeTargetType;
import com.sse.ssbk.service.LikeService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;

@RestController
@RequestMapping("/ssebk/common")
public class LikeController {
    @Autowired
    private LikeService likeService;

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:38
     */
    @PostMapping("/like")
    @CountScore(ScoreRules.LIKE)
    @SendNotice
    public Result<?> add(@RequestBody @Valid LikeAddDto dto) {
        likeService.add(Integer.parseInt(dto.getTargetId()), LikeTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:41
     */
    @DeleteMapping("/like")
    public Result<?> del(@RequestBody @Valid LikeDelDto dto) {
        likeService.del(Integer.parseInt(dto.getTargetId()), LikeTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }
}