package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by yxf on 2017/7/26.
 */
@Data
@Entity
@Table(name = "t_user_score_level")
public class ScoreLevel {
    //用户等级
    @Id
    @Column(name = "level_id")
    private Integer levelId;
    //用户等级名
    @Column(name = "level_name")
    private String levelName;
    //等级分上限
    @Column(name = "level_score")
    private String levelScore;
    //用户等级徽章
    @Column(name = "level_img")
    private String levelImg;
}
