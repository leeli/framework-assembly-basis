package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by yxf on 2017/7/19.
 */
@Data
@Entity
@Table(name = "t_sourcefrom")
public class SourceFrom {
    @Id
    @Column(name = "sourcefrom_id")
    private Integer sourceFromId;
    private String sourceName;
    private Integer sort;
    private Date createTime;
    private Date updateTime;
}
