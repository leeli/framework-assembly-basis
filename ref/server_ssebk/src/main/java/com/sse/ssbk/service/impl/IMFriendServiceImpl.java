package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.IMFriendDao;
import com.sse.ssbk.dao.UserDao;
import com.sse.ssbk.dto.AgreeFriendDto;
import com.sse.ssbk.dto.RequestFriendDto;
import com.sse.ssbk.entity.IMFriend;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.IMFriendService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.HanZi2PinYin;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.IMContactFriendVo;
import com.sse.ssbk.vo.IMFriendVo;
import com.sse.ssbk.vo.IMReqFriendsVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * Created by work_pc on 2017/8/9.
 */
@Slf4j
@Service
public class IMFriendServiceImpl implements IMFriendService {

    @Autowired
    private IMFriendDao imFriendDao;

    @Autowired
    private UserDao userDao;

    @Override
    public int isFriend(Integer friendUserId) {
        UserSession currentUser = BizUtils.getCurrentUser();
        return imFriendDao.isFriend(currentUser.getUserId(), friendUserId);
    }

    /**
     * 放入redis中
     * 请求添加好友
     */
    @Override
    public void requesTAddFrined(RequestFriendDto requestFriendDto) {

        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        Integer friendUserId = Integer.valueOf(requestFriendDto.getUserId());
        if (userId.intValue() == friendUserId.intValue()){
            throw new SSEBKRuntimeException(AppReturnCode.FriendReqSelf);
        }
        //是否已经发送过好友请求
        Set<Integer> requestAddFriend = RedisCacheUtils.getForSet(RedisConstants.User.Frined.REQUEST_FUSER_TUSER + userId);
        Boolean boolRequestAddFriend = BizUtils.contains(requestAddFriend, friendUserId);
        if (boolRequestAddFriend) {
            log.error("用户 " + userId +"已经向用户 " + friendUserId + " 发送过好友请求！" );
            throw new SSEBKRuntimeException(AppReturnCode.FriendReqExists);
        }

        //是否已经是好友
        int isFriend = imFriendDao.isFriend(userId, friendUserId);
        if (isFriend > 0){
            log.error(" 用户" + userId +" 和用户 " + friendUserId + " 已经是好友！");
            throw new SSEBKRuntimeException(AppReturnCode.FriendExists);
        }
        //发送好友请求
        RedisCacheUtils.addForSet(RedisConstants.User.Frined.REQUEST_FUSER_TUSER+userId,friendUserId);
        //反向添加一次 用来查找 请求添加我的好友
        //保存id
        RedisCacheUtils.addForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSERID+friendUserId,userId);
        User user = new User();
        user.setUserId(userId);
        user = userDao.selectByPrimaryKey(user);
        //保存user信息
        RedisCacheUtils.addForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSER+friendUserId,user);
    }

    /**放入mysql
     * 同意添加好友
     * @param agreeFriendDto
     */
    @Override
    public int agressAddFrined(AgreeFriendDto agreeFriendDto) {
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        Integer friendUserId = Integer.valueOf(agreeFriendDto.getUserId());
        if (userId.intValue() == friendUserId.intValue()){
            throw new SSEBKRuntimeException(AppReturnCode.FriendReqSelf);
        }
        Set<Integer> requestAddFriend = RedisCacheUtils.getForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSERID + userId);
        Boolean boolRequestAddFriend = BizUtils.contains(requestAddFriend, friendUserId);
        if (!boolRequestAddFriend) {
            log.error("用户 " + friendUserId +"未向用户 " + userId + " 发送过好友请求！" );
            throw new SSEBKRuntimeException(AppReturnCode.FriendReqNotExists);
        }

        int isFriend = imFriendDao.isFriend(userId, friendUserId);
        if (isFriend > 0){
            log.error(" 用户" + userId +" 和用户 " + friendUserId + " 已经是好友！");
            throw new SSEBKRuntimeException(AppReturnCode.FriendExists);
        }
        IMFriend imFriend = new IMFriend();
        imFriend.setUserId(userId);
        imFriend.setFriendUserId(friendUserId);
        imFriend.setCreateTime(new Date());
        return imFriendDao.insert(imFriend);
    }

    @Override
    public List<IMFriendVo> searchFriend(String searchKeys) {
        searchKeys = searchKeys.trim();
        Example searchKeyExample = new Example(User.class);
        Example.Criteria criteriaOr1 = searchKeyExample.createCriteria();
        criteriaOr1.andEqualTo("userName",searchKeys);
        Example.Criteria criteriaOr2 = searchKeyExample.createCriteria();
        criteriaOr2.andEqualTo("realName",searchKeys);
        searchKeyExample.or(criteriaOr2);
        List<User> users = userDao.selectByExample(searchKeyExample);
        List<IMFriendVo> imFriendVos = new ArrayList<>();
        for (User user:users){
            IMFriendVo imFriendVo = DtoEntityVoUtils.entity2vo(user,IMFriendVo.class);
            imFriendVo.setCompany(user.getCompanyName() );
            imFriendVos.add(DtoEntityVoUtils.voFieldFromNull2Empty(imFriendVo));
        }
        return imFriendVos;
    }

    /**
     * 好友列表
     * @return
     */
    @Override
    public Map<String, Object> contactList() {
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();

        //好友列表
        List<IMContactFriendVo> imFriendVoList = new ArrayList<>();

        Example example1 = new Example(IMFriend.class);
        Example.Criteria criteria1 = example1.createCriteria();
        criteria1.andEqualTo("userId",userId);
        //userId == userId
        List<IMFriend> userIdFriends = imFriendDao.selectByExample(example1);

        for (IMFriend imFriend:userIdFriends){
            User user = new User();
            user.setUserId(imFriend.getFriendUserId());
            user = userDao.selectByPrimaryKey(user);
            IMContactFriendVo imContactFriendVo = DtoEntityVoUtils.entity2vo(user, IMContactFriendVo.class);
            imContactFriendVo.setCompany(user.getCompanyName());
            imFriendVoList.add(imContactFriendVo);
            String pinYin = HanZi2PinYin.getPinyin(imContactFriendVo.getRealName());
            imContactFriendVo.setPinYin(pinYin.toUpperCase());
        }

        Example example2 = new Example(IMFriend.class);
        Example.Criteria criteria2 = example2.createCriteria();
        criteria2.andEqualTo("friendUserId",userId);
        //friendUserId = userId
        List<IMFriend> friendIdFriends = imFriendDao.selectByExample(example2);

        for (IMFriend imFriend:friendIdFriends){
            User user = new User();
            user.setUserId(imFriend.getUserId());
            user = userDao.selectByPrimaryKey(user);
            IMContactFriendVo imContactFriendVo = DtoEntityVoUtils.entity2vo(user, IMContactFriendVo.class);
            imContactFriendVo.setCompany(user.getCompanyName());
            imFriendVoList.add(imContactFriendVo);
            String pinYin = HanZi2PinYin.getPinyin(imContactFriendVo.getRealName());
            imContactFriendVo.setPinYin(pinYin.toUpperCase());
        }

        //通讯录列表上有个数字 表明有多少请求未处理

        //我收到的总请求数
        int totalReq = RedisCacheUtils.getForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSER+userId).size();
        
        //已经处理的请求数
        int processReq = userIdFriends.size();

        int unProcessReq = totalReq - processReq;

        Map<String,Object> data = new HashMap<>();
        data.put("unProcessNum",String.valueOf(unProcessReq));
        data.put("fiends",imFriendVoList);
        return data;
    }

    /**
     * 好友请求列表
     * @return
     */
    @Override
    public List<IMReqFriendsVo> reqFriends() {
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
//        会有缓存数据延迟问题
//        Set<User> users = RedisCacheUtils.getForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSER + userId);
        Set<Integer> ids = RedisCacheUtils.getForSet(RedisConstants.User.Frined.REQUEST_TUSER_FUSERID + userId);
        List<IMReqFriendsVo> imReqFriendsVos = new ArrayList<>();
        for (Integer id:ids){
            User user = new User();
            user.setUserId(id);
            user = userDao.selectByPrimaryKey(user);
            IMReqFriendsVo imReqFriendsVo = DtoEntityVoUtils.entity2vo(user,IMReqFriendsVo.class);
            imReqFriendsVo.setCompany(user.getCompanyName());
            imReqFriendsVo.setIsAccept(String.valueOf(imFriendDao.isFriend(userId, user.getUserId())));
            imReqFriendsVos.add(DtoEntityVoUtils.voFieldFromNull2Empty(imReqFriendsVo));
        }
        return imReqFriendsVos;
    }
}
