package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.entity.UserCollectAct;
import com.sse.ssbk.entity.UserCollectNews;
import com.sse.ssbk.vo.CollectNewsVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/27.
 */
public interface UserCollectNewsDao extends BaseDao<UserCollectNews> {

    List<Integer> getCollectNewsColumnsInOrder(@Param("userId") Integer userId, @Param("lastNewsId") Integer lastNewsId, @Param("pageSize") Integer pageSize);

    List<CollectNewsVo> getCollectNews(@Param("columns") List<Integer> columns);
}
