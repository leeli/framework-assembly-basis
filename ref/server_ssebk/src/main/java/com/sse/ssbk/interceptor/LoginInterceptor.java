package com.sse.ssbk.interceptor;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.utils.BizUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.utils.RedisCacheUtils;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
        String currentToken = BizUtils.getCurrentToken();
        Object forValue = RedisCacheUtils.getForValue(currentToken);
        if (forValue == null){
            throw new SSEBKRuntimeException(AppReturnCode.LoginOutDate);
        }
        return true;
    }
}
