package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/9.
 */
@Data
public class IMReqFriendsVo {
    private String userId;
    private String userName;
    private String realName;
    private String company;
    private String userPhoto;
    private String isAccept;
}
