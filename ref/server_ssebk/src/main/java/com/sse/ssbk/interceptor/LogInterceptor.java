package com.sse.ssbk.interceptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sse.ssbk.service.AppSdkRelationService;

/**
 * 日志拦截器.
 * @version 2017年7月18日 下午5:38:11
 */
@Slf4j
public class LogInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private AppSdkRelationService appSdkRelationService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        this.log(request);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

    /**
     * 
     * @param request
     * @author HT-LiChuanbin 
     * @version 2017年8月16日 下午7:56:45
     */
    private void log(HttpServletRequest request) {
        try {
            String base = request.getHeader("base");
            String strParam = "";
            String method = request.getMethod();
            if (RequestMethod.GET.toString().equals(method)) {
                Map<String, String[]> mParams = request.getParameterMap();
                Set<Map.Entry<String, String[]>> smParams = mParams.entrySet();
                Iterator<Map.Entry<String, String[]>> ismParams = smParams.iterator();
                while (ismParams.hasNext()) {
                    Map.Entry<String, String[]> entryParam = ismParams.next();
                    strParam += entryParam.getKey() + "=" + entryParam.getValue()[0].toString();
                }
            } else if (RequestMethod.POST.toString().equals(method) || RequestMethod.PUT.toString().equals(method)) {
                try (ServletInputStream sis = request.getInputStream()) {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(sis))) {
                        StringBuilder sb = new StringBuilder();
                        String line = "";
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        strParam += sb.toString();
                    }
                }
            } else if (RequestMethod.DELETE.toString().equals(method)) {
            } else {
                log.error("不支持的请求方法！");
                return;
            }
            log.info("base信息：\n" + URLDecoder.decode(base, "utf-8"));
            log.info("请求信息：\n" + strParam.substring(0, strParam.length()));
            log.error("请求URI：\n" + method.toString());
        } catch (IOException ioe) {
            log.error("", ioe);
        }
    }
}