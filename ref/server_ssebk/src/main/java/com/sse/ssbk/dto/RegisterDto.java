package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by ciyua on 2017/8/3.
 */
@Data
public class RegisterDto {

    @NotBlank
    private String userName;
    @NotBlank
    private String smsCaptcha;
    @NotBlank
    private String password;
}
