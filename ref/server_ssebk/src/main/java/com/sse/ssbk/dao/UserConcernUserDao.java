package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.UserConcernUser;

public interface UserConcernUserDao extends BaseDao<UserConcernUser> {
}