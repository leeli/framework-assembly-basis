package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
@Entity
@Table(name = "t_user_score")
public class UserScore {
    @Id
    private Integer userId;
    private Integer score;
}
