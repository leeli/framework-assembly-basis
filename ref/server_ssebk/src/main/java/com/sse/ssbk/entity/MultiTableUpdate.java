package com.sse.ssbk.entity;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/16.
 */
@Data
public class MultiTableUpdate {
    private String userPhoto;
    private String companyName;
    private String companyId;
}
