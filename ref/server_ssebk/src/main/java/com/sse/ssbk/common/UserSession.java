package com.sse.ssbk.common;

import java.io.Serializable;
import java.util.Map;

import com.sse.ssbk.utils.TransIgnore;
import lombok.Data;

/**
 * 用户会话信息.
 * @author  HT-LiChuanbin 
 * @version 2017年7月24日 下午2:04:46
 */
@Data
public class UserSession implements Serializable {
    @TransIgnore
    private static final long serialVersionUID = -393569817652589187L;
    private Integer userId;
    private String userName;
    private String realName;
    private String userPhoto;
    private String userStatus;
    private Integer cityId;
    private String province;        //省id

    private Integer companyId;
    private String companyName;

    private Map<String, Boolean> perm;
    private String loginStatus;
    private String reason;

    private String token;
}