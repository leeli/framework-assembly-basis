package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;


@Data
public class NewsDto{
	@NotBlank
	private String title;
	@NotBlank
	private String content;
	private String[] images;
	@NotBlank
	private String sourceFromId;
}
