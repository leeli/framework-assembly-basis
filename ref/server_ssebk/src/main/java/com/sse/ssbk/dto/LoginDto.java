package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by ciyua on 2017/7/27.
 */
@Data
public class LoginDto {
    @NotBlank
    private String loginName;
    @NotBlank
    private String password;
}