package com.sse.ssbk.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by work_pc on 2017/8/21.
 */
@Data
public class ScoreIndexVo {
    private String userName;
    private String userPhoto;
    private String company;

    private String lastLevelId;
    private String lastLevelName;
    private String lastLevelScore;
    private String lastLevelImg;

    private String currentId;
    private String levelName;   //用户等级名
    private String currentScore;  //当前积分
    private String currentImg;

    private String upgradeId;
    private String upgradeName;
    private String upgradeLevelScore;   //当前用户等级的下一级的积分
    private String upgradeImg;
    private String upgradeToScore;  //当前用户等级的积分与下一级积分的差值


    private String next2LevelId;
    private String next2LevelName;
    private String next2LevelScore;
    private String next2LevelImg;
    private String next2LevelUpgradeToScore;

    private List<UserTopVo> userTops;

    public ScoreIndexVo(){
        lastLevelId  = "";
        lastLevelName ="";
        lastLevelScore ="";
        lastLevelImg ="";
        currentId ="";
        levelName ="";   //用户等级名
        currentScore ="";  //当前积分
        currentImg ="";
        upgradeId ="";
        upgradeName ="";
        upgradeLevelScore ="";   //当前
        upgradeImg ="";
        upgradeToScore ="";  //当前用户等级
        next2LevelId ="";
        next2LevelName ="";
        next2LevelScore ="";
        next2LevelImg ="";
        next2LevelUpgradeToScore ="";
    }
}
