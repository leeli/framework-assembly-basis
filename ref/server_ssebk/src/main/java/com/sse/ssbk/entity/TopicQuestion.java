package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_topic_question_map")
public class TopicQuestion {
    @Column(name = "topic_id")
    private Integer topicId;
    @Column(name = "question_id")
    private Integer questionId;
    @Column(name = "is_delete")
    private String isDelete;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "delete_time")
    private Date deleteTime;
}