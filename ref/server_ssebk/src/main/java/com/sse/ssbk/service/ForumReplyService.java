package com.sse.ssbk.service;

import com.sse.ssbk.dto.ForumReplyAddDto;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.vo.ForumReplyGetVo;
import com.sse.ssbk.vo.ForumReplyListVo;

import java.util.List;

/**
 * ForumReply服务.
 * @author  HT-LiChuanbin 
 * @version 2017年7月20日 下午1:31:44
 */
public interface ForumReplyService {
    /**
     * 添加.
     * @param dto ForumReply Dto.
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:29:34
     */
    void save(ForumReplyAddDto dto);

    /**
     * 删除.
     * @param id 回答ID.
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:30:56
     */
    void del(Integer id);

    /**
     * 修改.
     * @param dto ForumReply Dto.
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:31:06
     */
    void upt(ForumReplyAddDto dto);

    /**
     * 获取.
     * @param id 回答ID.
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:31:16
     */
    ForumReplyGetVo get(Integer id);


    /**
     * 获取某个问题的回答列表.
     * @return
     * @author HT-LiChuanbin
     * @version 2017年7月26日 下午4:03:47
     */
    List<ForumReplyListVo> listByQuesionId(Integer questionId,Page page);

    /**
     * 获取用户的回答列表.
     * @return
     * @author HT-LiChuanbin
     * @version 2017年7月26日 下午4:03:47
     */
    List<ForumReplyListVo> listByUserId(Integer userId,Page page);


    /**
     * 获取用户某问题的回答列表.
     * @return
     * @author HT-LiChuanbin
     * @version 2017年7月26日 下午4:03:47
     */
    List<ForumReplyListVo> listByUserIdAndQuesionId(Integer userId,Integer questionId,Page page);
}