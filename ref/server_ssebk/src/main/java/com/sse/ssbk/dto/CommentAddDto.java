package com.sse.ssbk.dto;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

@Data
public class CommentAddDto {
    @NotBlank
    private String targetType;
    @NotBlank
    private String targetId;
    private String[] imageArr;
    @NotBlank
    private String content;
    private String isAnonymous;
}