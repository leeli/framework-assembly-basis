package com.sse.ssbk.service;

import java.util.List;

import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.vo.ForumTopicsVo;

public interface ForumTopicService {
    /**
     * 
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午7:20:55
     */
    List<ForumTopicsVo> list(Page page);

    /**
     * 
     * @param topicName
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午7:20:58
     */
    List<ForumTopicsVo> listByTopicName(String topicName, Page page);
}