package com.sse.ssbk.dto;

import java.util.Date;

import lombok.Data;

@Data
public class TopicQuestionDto {
	private Integer topicId;
	private Integer questionId;
	private Date createTime;
}