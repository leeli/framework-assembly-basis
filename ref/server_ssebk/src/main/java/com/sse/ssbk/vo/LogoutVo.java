package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by ciyua on 2017/8/7.
 */
@Data
public class LogoutVo {
    private String logoutStatus;
}
