package com.sse.ssbk.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.vo.ForumQuestionGetVo;

/**
 * Dto和Entity转换工具.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:51:11
 */
@Slf4j
public class DtoEntityVoUtils {
    /**
     * Dto到Entity的转换.
     * @param dto         Dto.
     * @param entityClazz Entity类类型.
     * @return
     * @author            HT-LiChuanbin 
     * @version           2017年7月19日 下午3:48:21
     */
    public static <T> T dto2entity(Object dto, Class<T> entityClazz) {
        try {
            if (null == dto || null == entityClazz) {
                return null;
            }

            T entity = entityClazz.newInstance();
            if (entity == null) {
                return null;
            }
            List<Field> fields = new ArrayList<Field>();
            getAllFields(fields, entity.getClass(), 1);
            for (Field field : fields) {
                if (field.isAnnotationPresent(TransIgnore.class)) {
                    continue;
                }
                Field dtoField = getFiledByName(dto.getClass(), field.getName());
                if (dtoField == null) {
                    // 这里说明dto没有entity中的field属性.
                    continue;
                }
                field.setAccessible(true);
                dtoField.setAccessible(true);
                String simpleName = field.getType().getSimpleName();

                // 处理String和Integer的转换.
                if (StringUtils.equals(simpleName, "Integer")) {
                    if (dtoField.get(dto) != null) {
                        field.set(entity, Integer.parseInt(dtoField.get(dto).toString()));
                    }
                    // 处理Date和String的转换.
                } else if (StringUtils.equals(simpleName, "Date")) {
                    if (dtoField.get(dto) != null) {
                        Date date = new Date(Long.parseLong(dtoField.get(dto).toString()));
                        field.set(entity, date);
                    }
                } else {
                    field.set(entity, dtoField.get(dto));
                }

            }
            return entity;
        } catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Entity到Vo的转换.
     * @param entity   Entity.
     * @param voClazz  Vo类类型.
     * @return
     * @author 		    HT-LiChuanbin 
     * @version 		2017年7月19日 下午3:49:32
     */
    public static <T> T entity2vo(Object entity, Class<T> voClazz) {
        try {
            if (entity == null || null == voClazz) {
                return null;
            }

            // TODO 实例化内部类.
            // voClazz.getSuperclass();

            T vo = voClazz.newInstance();
            List<Field> fields = new ArrayList<Field>();
            getAllFields(fields, entity.getClass(), 1);
            for (Field field : fields) {
                field.setAccessible(true);
                // 如果entity中的值为null，没有必要进行转换.
                if (field.get(entity) == null) {
                    continue;
                }
                if (field.isAnnotationPresent(TransIgnore.class)) {
                    continue;
                }
                Field voField = getFiledByName(vo.getClass(), field.getName());
                if (voField == null) {
                    // 这里说明vo中没有entity中的属性field.
                    continue;
                }
                voField.setAccessible(true);
                String simpleName = field.getType().getSimpleName();
                if (StringUtils.equals(simpleName, "Integer")) {
                    voField.set(vo, String.valueOf(field.get(entity)));
                } else if (StringUtils.equals(simpleName, "Date")) {
                    if (field.get(entity) != null) {
                        voField.set(vo, String.valueOf(((Date) field.get(entity)).getTime()));
                    }
                } else {
                    voField.set(vo, field.get(entity));
                }
            }
            return vo;
        } catch (InstantiationException | IllegalAccessException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static <T> T voFieldFromNull2Empty(T entity) {
        try {
            if (entity == null) {
                return null;
            }
            List<Field> fields = new ArrayList<Field>();
            getAllFields(fields, entity.getClass(), 5);
            for (Field field : fields) {
                field.setAccessible(true);
                // 如果entity中的值为null，进行转换.
                if (field.get(entity) == null) {
                    if (field.getType().getSimpleName().equals("String")) {
                        field.set(entity, "");
                    } else if (field.getType().getSimpleName().equals("List")) {
                        field.set(entity, new ArrayList<>());
                    } else if (field.getType().getSimpleName().equals("String[]")) {
                        field.set(entity, new String[0]);
                    } else if (field.getType().getSimpleName().equals("Integer")) {
                        field.set(entity, 0);
                    }
                }
            }
            return entity;
        } catch (IllegalAccessException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Dto列表到Entity的转换.
     * @param dtos        Dto列表.
     * @param entityClazz Entity类类型.
     * @return
     * @author            HT-LiChuanbin 
     * @version           2017年7月19日 下午3:50:32
     */
    public static <T, M> List<T> dtoList2doList(List<M> dtos, Class<T> entityClazz) {
        if (null == dtos) {
            return null;
        }
        List<T> entitys = new ArrayList<T>();
        for (M dto : dtos) {
            entitys.add(dto2entity(dto, entityClazz));
        }
        return entitys;
    }

    /**
     * Entity列表到Vo的转换.
     * @param entitys  Entity列表.
     * @param voClazz  Vo类类型.
     * @return
     * @author         HT-LiChuanbin 
     * @version        2017年7月19日 下午3:51:13
     */
    public static <T, M> List<T> entityList2voList(List<M> entitys, Class<T> voClazz) {
        if (null == entitys) {
            return null;
        }
        List<T> vos = new ArrayList<T>();
        for (M entity : entitys) {
            vos.add(entity2vo(entity, voClazz));
        }
        return vos;
    }

    /**
     * 获取所有属性.
     * @param fields 所有属性列表.
     * @param clazz  类类型.
     * @param i      类父类的层数.
     * @author       HT-LiChuanbin 
     * @version      2017年7月19日 下午3:52:08
     */
    private static void getAllFields(List<Field> fields, Class<?> clazz, int i) {
        if (i >= 5 || null == clazz) {
            return;
        }
        try {
            Class<?> superClazz = clazz.getSuperclass();
            Field[] field = clazz.getDeclaredFields();
            if (null != field) {
                CollectionUtils.addAll(fields, field);
            }
            getAllFields(fields, superClazz, ++i);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return;
        }
    }

    /**
     * 通过属性名称获取属性.
     * @param clazz     类类型.
     * @param fieldName 属性名称.
     * @return          属性.
     * @author          HT-LiChuanbin 
     * @version         2017年7月19日 下午3:53:24
     */
    private static Field getFiledByName(Class<?> clazz, String fieldName) {
        if (null == clazz || StringUtils.isBlank(fieldName)) {
            return null;
        }
        try {
            Field field = clazz.getDeclaredField(fieldName);
            return field == null && clazz.getSuperclass() != null ? getFiledByName(clazz.getSuperclass(), fieldName) : field;
        } catch (NoSuchFieldException e) {
            log.info(clazz.getSimpleName() + "中没有" + fieldName + "属性。");
            return null;
        } catch (SecurityException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }


    /**
     * 将Dto的非空值合并到Entity.
     * @param dto         Dto.
     * @return
     * @author            HT-LiChuanbin
     * @version           2017年7月19日 下午3:48:21
     */
    public static <T> T combineDtoAndEntity(Object dto,T entity) {
        try {
            if (null == dto || null == entity) {
                return null;
            }

            List<Field> fields = new ArrayList<Field>();
            getAllFields(fields, entity.getClass(), 1);
            for (Field field : fields) {
                if (field.isAnnotationPresent(TransIgnore.class)) {
                    continue;
                }
                Field dtoField = getFiledByName(dto.getClass(), field.getName());
                if (dtoField == null) {
                    // 这里说明dto没有entity中的field属性.
                    continue;
                }
                field.setAccessible(true);
                dtoField.setAccessible(true);
                String simpleName = field.getType().getSimpleName();

                // 处理String和Integer的转换.
                if (StringUtils.equals(simpleName, "Integer")) {
                    if (dtoField.get(dto) != null) {
                        field.set(entity, Integer.parseInt(dtoField.get(dto).toString()));
                    }
                    // 处理Date和String的转换.
                } else if (StringUtils.equals(simpleName, "Date")) {
                    if (dtoField.get(dto) != null) {
                        Date date = new Date(Long.parseLong(dtoField.get(dto).toString()));
                        field.set(entity, date);
                    }
                } else if (dtoField.get(dto)!=null && !"".equals(dtoField.get(dto))){
                    field.set(entity, dtoField.get(dto));
                }
            }
            return entity;
        } catch (IllegalArgumentException | IllegalAccessException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }


    public static void main(String[] args) {

    }

    @SuppressWarnings("unused")
    private static void testDto2Entity() {
        ForumQuestionDto dtoForumQuestion = new ForumQuestionDto();
        dtoForumQuestion.setContent("con");
        dtoForumQuestion.setIsAnonymous("1");
        ForumQuestion forumQuestion = DtoEntityVoUtils.dto2entity(dtoForumQuestion, ForumQuestion.class);
        log.info(forumQuestion.toString());
    }

    @SuppressWarnings("unused")
    private static void testEntity2Dto() {
        ForumQuestion forumQuestion = new ForumQuestion();
        forumQuestion.setContent("con");
        forumQuestion.setIsAnonymous("1");
        ForumQuestionGetVo voForumQuestion = DtoEntityVoUtils.entity2vo(forumQuestion, ForumQuestionGetVo.class);
        log.info(voForumQuestion.toString());

    }

    private static void testvoFieldFromNull2EmptyString() {
        //        TestNull2EmptyStringVo testNull2EmptyStringVo = new TestNull2EmptyStringVo();
        //        TestNull2EmptyStringVo testNull2EmptyStringVo1 = voFieldFromNull2Empty(testNull2EmptyStringVo);
        //        System.out.println(testNull2EmptyStringVo1);
    }
}