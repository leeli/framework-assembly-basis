package com.sse.ssbk.common;

import lombok.Data;

/**
 * 
 * @author HT-LiChuanbin 
 * @version 2017年7月28日 下午6:09:42
 */
@Data
public class BaseInfo {
    private String deviceId;
    private String token;
    private String OSType;
    private String deviceType;
    private String version;
}