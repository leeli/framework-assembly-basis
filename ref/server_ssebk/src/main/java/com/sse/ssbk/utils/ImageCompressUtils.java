 package com.sse.ssbk.utils;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

/**
 * 图片压缩处理.
 * @author  崔素强
 * @author  Thomas Lee
 * @version 2017年7月29日 上午10:17:57
 */
public class ImageCompressUtils {
    private static Image img;
    private static int width;
    private static int height;
    private static String toFilepath;

    public static void main(String[] args) throws Exception {
        String filepath = "C:/Users/Chuanbinli/Desktop/tomato.jpg";
        String toFilepath = "C:/Users/Chuanbinli/Desktop/tomato1.jpg";
        File file = new File(filepath);
        byte[] bytes = FileUtils.file2bytes(file);
        ImageCompressUtils.resize(bytes, toFilepath, 100, 100);
    }

    /**
     * 强制压缩、放大图片到固定的大小.
     * @param w int 新宽度.
     * @param h int 新高度.
     */
    public static void resizeFix(int w, int h) throws IOException {
        // SCALE_SMOOTH的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢.
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        image.getGraphics().drawImage(img, 0, 0, w, h, null);
        File destFile = new File(toFilepath);
        try (FileOutputStream out = new FileOutputStream(destFile)) {
            // 可以正常实现bmp、png、gif转jpg.
            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            encoder.encode(image); // JPEG编码
        }
    }

    /**
     * 按照宽度、高度进行压缩.
     * @param w int 最大宽度.
     * @param h int 最大高度.
     */
    public static void resize(byte[] bytes, String toFilepath, int w, int h) throws IOException {
        img = ImageIO.read(TransformUtils.bytes2Inputstream(bytes));
        width = img.getWidth(null);
        height = img.getHeight(null);
        ImageCompressUtils.toFilepath = toFilepath;
        if (width / height > w / h) {
            resizeByWidth(w);
        } else {
            resizeByHeight(h);
        }
    }

    /**
     * 以宽度为基准，等比例放缩图片.
     * @param w int 新宽度.
     */
    public static void resizeByWidth(int w) throws IOException {
        int h = (int) (height * w / width);
        resizeFix(w, h);
    }

    /**
     * 以高度为基准，等比例缩放图片.
     * @param h int 新高度.
     */
    public static void resizeByHeight(int h) throws IOException {
        int w = (int) (width * h / height);
        resizeFix(w, h);
    }
}