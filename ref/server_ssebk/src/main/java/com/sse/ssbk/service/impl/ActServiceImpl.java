package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.*;
import com.sse.ssbk.dto.ActDto;
import com.sse.ssbk.entity.*;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ActService;
import com.sse.ssbk.service.EnrollActService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DateUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import com.sse.ssbk.exception.AppReturnCode;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by yxf on 2017/7/17.
 */
@Service
public class ActServiceImpl implements ActService {

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private UserConcernDao userConcernDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ActivitySubjectDao activitySubjectDao;

    @Autowired
    private ProvinceDao provinceDao;

    @Autowired
    private EnrollActService enrollActService;

    @Autowired
    private ActivityCommentDao activityCommentDao;

    @Autowired
    private UserCollectActDao userCollectActDao;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 最新的五张活动轮播图，按index从小到大的顺序排列
     * @return
     */
    @Override
    public List<CircleActVo> getCircleAct() {
        return activityDao.getCircleAct();
    }

    @Override
    public void addAct(ActDto actDto) {
        //新活动 需要有isAudit 和isPublicer
        if ("1".equals(actDto.getActType())){
            if (actDto.getIsAudit() == null || actDto.getIsPublicActor() == null){
                throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
            }
        }

        Activity activity = DtoEntityVoUtils.dto2entity(actDto,Activity.class);
        activity.setCreateTime(new Date());
        if (actDto.getActImages()!= null && !"".equals(actDto.getActImages())){
            activity.setActImages(BizUtils.getStrImages(actDto.getActImages()));
        }
        activity.setStartTime(DateUtils.StringToDate1(actDto.getStartTime()));
        activity.setEndTime(DateUtils.StringToDate1(actDto.getEndTime()));

        //区域活动排序需要，精确到毫秒，防止具有同样的开始或结束时间
        activity.setStartTimeSecondRandom(activity.getStartTime().getTime()+(int)(Math.random() * 60000));
        activity.setEndTimeSecondRandom(activity.getEndTime().getTime()+(int)(Math.random() * 60000));
        //用户信息统一从redis获取
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        /*if (actDto.getUserId() == null) {
            loginUser = (User) RedisCacheUtil.getObj("loginUser");
        }else {
            loginUser = new User();
            loginUser.setUserId(Integer.valueOf(actDto.getUserId()));
            loginUser = userDao.selectByPrimaryKey(loginUser);
        }*/
        activity.setUserId(String.valueOf(loginUser.getUserId()));
        activity.setUserName(loginUser.getUserName());

        ActSubject actSubject = new ActSubject();
        actSubject.setSubjectId(actDto.getSubjectId());
        actSubject = activitySubjectDao.selectByPrimaryKey(actSubject);
        activity.setSubjectName(actSubject.getSubjectName());
        if (loginUser.getCompanyId() != null && !"".equals(loginUser.getCompanyId())) {
            activity.setCompanyId(Integer.valueOf(loginUser.getCompanyId()));
        }
        activity.setCompanyName(loginUser.getCompanyName());

        Province province = new Province();
        province.setProvinceId(actDto.getCityId());
        province = provinceDao.selectByPrimaryKey(province);
        activity.setCityId(province.getProvinceId());
        activity.setCityName(province.getProvinceName());

        //是新活动
        if ("1".equals(actDto.getActType())){
            //活动状态为准备
            activity.setStatus(0);
        }else {
            //活动状态为结束
            activity.setStatus(2);
        }
        activityDao.insertSelective(activity);
    }

    @Override
    public List<ActSubject> getActSubjects(Integer lastSubjectId, Integer pageSize, String sortBy) {
        return activityDao.getActSubjects(lastSubjectId, pageSize);
    }

    @Override
    public ActivityDetailsVo getActDetailsByActId(Integer actId){
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser"); //当前登录用户
        UserSession loginUser =  BizUtils.getCurrentUser();
        Activity activity = new Activity();
        activity.setActId(actId);
        activity = activityDao.selectByPrimaryKey(actId);
        if(activity == null){
            throw new SSEBKRuntimeException(AppReturnCode.UndefinedError);
        }

        ActivityDetailsVo activityDetailsVo = DtoEntityVoUtils.entity2vo(activity,ActivityDetailsVo.class);
        User user = new User();
        user.setUserId(Integer.valueOf(activity.getUserId()));
        user = userDao.selectByPrimaryKey(user);
        if (activity.getActImages()!=null && !"".equals(activity.getActImages())){
            activityDetailsVo.setActImages(BizUtils.toStringArray(activity.getActImages()));
        }else {
            activityDetailsVo.setActImages(new String[0]);
        }
        activityDetailsVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
        activityDetailsVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

        Integer status;

        if ("1".equals(activityDetailsVo.getActType())){        //是新活动
            if (new Date().before(activity.getStartTime())){
                //未开始
                status = 0;
            }else if (new Date().after(activity.getEndTime())){
                //已结束
                status = 2;
            }else{
                //正在进行
                status = 1;
            }

            activityDetailsVo.setStatus(String.valueOf(status));
            //更新活动状态
            Activity updateStatus = new Activity();
            updateStatus.setActId(activity.getActId());
            updateStatus.setStatus(status);
            activityDao.updateByPrimaryKeySelective(updateStatus);
        }else {
            activityDetailsVo.setStatus("2");       //活动状态为已结束
            activityDetailsVo.setIsPublicActor("");    //报名人为空
            activityDetailsVo.setIsAudit("");   //报名是否需审核
        }

        activityDetailsVo.setOwnerRealName(user.getRealName());
        activityDetailsVo.setOwnerUserId(String.valueOf(user.getUserId()));
        activityDetailsVo.setOwnerUserName(user.getUserName());
        activityDetailsVo.setOwnerUserPhoto(user.getUserPhoto());
        activityDetailsVo.setOwnerCompany(user.getCompanyName());
        //是否收藏 1收藏 0未收藏
        String isCollect = Is.NO.getCode();

        Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_ACT + loginUser.getUserId());
        if (forSet!= null && forSet.contains(activity.getActId())){
            isCollect = Is.YES.getCode();
        }

       /* UserCollectAct userCollectAct = new UserCollectAct();
        userCollectAct.setUserId(loginUser.getUserId());
        userCollectAct.setActId(actId);
        isCollect = String.valueOf(userCollectActDao.selectCount(userCollectAct));*/
        activityDetailsVo.setIsCollect(isCollect);
        //是否已关注

        Set<Integer> iconcernUserIds = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());

        String isConcern = Is.NO.getCode();
        if (iconcernUserIds!=null && iconcernUserIds.contains(user.getUserId())){
            isConcern = Is.YES.getCode();
        }

        activityDetailsVo.setOwnerIsConcern(isConcern);

//        activityDetailsVo.setOwnerIsConcern(String.valueOf(userConcernDao.isConcernedUser(loginUser.getUserId(),user.getUserId())));
        /**enrollStatus
         * 0=未报名
         *1=报名审核中
         *2=报名成功
         *3=报名审核不通过
         */
        String enrollStatus = enrollActService.getEnrollStatus(loginUser.getUserId(),actId);
        activityDetailsVo.setEnrollStatus(enrollStatus);
        /**
         * actPersonCount
         * 报名/参与人人数:
         *活动报名中为报名人数；
         *活动进行中&结束为参与人数
         * 活动状态0准备1进行2结束3取消
         */

        //活动还未开始
        String actPersonCount;
        if (new Date().before(activity.getStartTime())){
            actPersonCount = enrollActService.getActivityCount(actId,"3");
        }else{
            actPersonCount = enrollActService.getActivityCount(actId,"1");
        }

        activityDetailsVo.setActPersonCount(actPersonCount);
        //auditWaitCount;  待审核人数
        String auditWaitCount = enrollActService.getActivityCount(actId,"0");
        activityDetailsVo.setAuditWaitCount(auditWaitCount);
        //auditNotCount;  审核不通过人数
        String auditNotCount = enrollActService.getActivityCount(actId,"2");
        activityDetailsVo.setAuditNotCount(auditNotCount);
        //auditPassCount;  审核通过人数
        String auditPassCount = enrollActService.getActivityCount(actId,"1");


        activityDetailsVo.setAuditPassCount(auditPassCount);
        //commentCount;  评论数
        /*ActivityComment activityComment = new ActivityComment();
        activityComment.setActCommentId(actId);
        String commentCount = String.valueOf(activityCommentDao.selectCount(activityComment));*/
        String commentCount = "0";
        Integer forValue = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_ACT + activity.getActId());
        if (forValue!=null){
            commentCount = String.valueOf(forValue);
        }
        activityDetailsVo.setCommentCount(commentCount);
        return activityDetailsVo;
    }

    @Override
    public List<UserEnrollVo> getEnrollUserList(String lastUserId, String actId, String pageSize,String status) {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Integer> columns = activityDao.getEnrollUserColumns(
                Integer.valueOf(lastUserId),
                    Integer.valueOf(actId),
                        Integer.valueOf(pageSize),
                            Integer.valueOf(status));

        List<User> userConcernedMeList;

        if (columns == null || columns.size() == 0){
            userConcernedMeList = new ArrayList<>();
        }else{
            userConcernedMeList= activityDao.getEnrollUserList(
                    columns);
        }

        //        List<UserConcernVo> userConcernVos = DtoEntityVoUtils.entityList2voList(userConcernedMeList, UserConcernVo.class);

        Set<Integer> iconcernUserIds = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());
        List<UserEnrollVo> userEnrollVos = new ArrayList<>();
        UserEnrollVo userEnrollVo;
        for (User user :
                userConcernedMeList) {
            userEnrollVo = DtoEntityVoUtils.entity2vo(user,UserEnrollVo.class);
            userEnrollVo.setCompany(user.getCompanyName());
            userEnrollVo.setUserPhoto(user.getUserPhoto());
            if ("3".equals(status)) {
                //和获取活动详情公用一个接口，但是所需含义不同
                //获取活动详情  getEnrollStatus的含义 *0=未报名 *1=报名审核中 *2=报名成功 *3=报名审核不通过
                //获取参与人列表 *0=报名审核中 *1=报名成功 *2=报名审核不通过
                userEnrollVo.setEnrollStatus(String.valueOf(Integer.valueOf(enrollActService.getEnrollStatus(user.getUserId(), Integer.valueOf(actId)))-1));
            }else {
                userEnrollVo.setEnrollStatus(status);
            }
            String isConcern = Is.NO.getCode();
            if (iconcernUserIds!=null && iconcernUserIds.contains(user.getUserId())){
                isConcern = Is.YES.getCode();
            }
            String rejectReason = enrollActService.getRejectReason(actId,userEnrollVo.getUserId());
            userEnrollVo.setRejectReason(rejectReason);
            userEnrollVo.setIsConcern(isConcern);
           /* boolean isConcern = userConcernDao.isConcernedUser(loginUser.getUserId(),user.getUserId()) == 0 ? false : true ;
            if (isConcern){
                userConcernVo.setIsConcern("1");
            }else{
                userConcernVo.setIsConcern("0");
            }*/
            userEnrollVos.add(userEnrollVo);
        }
        return userEnrollVos;
    }

    /**
     * 4.4.5.	获取某个区域或主题的活动列表
     * @param lastActId
     * @param pageSize
     * @param subjectId
     * @param cityId
     * @param status
     * @return
     */
    @Override
    public List<ActivityCitySubjectVo> getCityOrSubjectActivity(String lastActId, String pageSize, String subjectId, String cityId, String status) {
        List<ActivityCitySubjectVo> activityCitySubjectVos = new ArrayList<>();

        List<Integer> columns;

        List<Activity> activityList;
        if (subjectId == null) {

            columns = activityDao.getCityOrSubjectActivityColumns(Integer.valueOf(lastActId), Integer.valueOf(pageSize), null, Integer.valueOf(cityId), Integer.valueOf(status));

//            activityList = activityDao.getCityOrSubjectActivity();
        }else {
            columns = activityDao.getCityOrSubjectActivityColumns(Integer.valueOf(lastActId), Integer.valueOf(pageSize), Integer.valueOf(subjectId), null, Integer.valueOf(status));
//            activityList = activityDao.getCityOrSubjectActivity(Integer.valueOf(lastActId), Integer.valueOf(pageSize), Integer.valueOf(subjectId), null, Integer.valueOf(status));
        }

        if (columns == null || columns.size() == 0){
            return new ArrayList<>();
        }

        activityList = activityDao.getCityOrSubjectActivity(columns);
        ActivityCitySubjectVo activityCitySubjectVo;
        String[] actUserPhotos;
        for (Activity activity:activityList){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,ActivityCitySubjectVo.class);
            activityCitySubjectVo.setActImages(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            //todo  为了测试方便，热门活动的定义从15改为5 后期会改回来
            if (actUserNum!=null && Integer.valueOf(actUserNum)>=5){
                isHot = "1";
            }

            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);
            activityCitySubjectVos.add(activityCitySubjectVo);
        }
        return activityCitySubjectVos;
    }

    @Override
    public List<UserActivityCitySubjectVo> getMyActivity(String lastActId, String pageSize) {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        List<Activity> activityList = activityDao.getMyActivity(Integer.valueOf(lastActId), Integer.valueOf(pageSize), loginUser.getUserId());
        List<UserActivityCitySubjectVo> activityCitySubjectVos = new ArrayList<>();
        UserActivityCitySubjectVo activityCitySubjectVo;
        String[] actUserPhotos;
        for (Activity activity:activityList){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,UserActivityCitySubjectVo.class);
            activityCitySubjectVo.setActImages(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            if (actUserNum!=null && Integer.valueOf(actUserNum)>= Constants.ISHOTNUM){
                isHot = "1";
            }
            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);
            activityCitySubjectVos.add(activityCitySubjectVo);
        }
        return activityCitySubjectVos;
    }

    @Override
    public List<UserActivityCitySubjectVo> getIJoinActivity(String lastActId,String userId, String pageSize, String status) {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
//        UserSession loginUser =  BizUtils.getCurrentUser();
//        List<Activity> activityList = activityDao.getIJoinActivity(Integer.valueOf(lastActId), Integer.valueOf(pageSize), loginUser.getUserId(), Integer.valueOf(status));
        List<Integer> columns;
        if (status != null){
            columns = activityDao.getIJoinActivityColumns(Integer.valueOf(lastActId), Integer.valueOf(pageSize), Integer.valueOf(userId), Integer.valueOf(status));
        }else {
            columns = activityDao.getIJoinActivityColumns(Integer.valueOf(lastActId), Integer.valueOf(pageSize), Integer.valueOf(userId), null);
        }
        List<Activity> activityList;
        if (columns != null && columns.size() != 0){
            activityList = activityDao.getIJoinActivity(columns);
        }else {
            activityList = new ArrayList<>();
        }
        List<UserActivityCitySubjectVo> activityCitySubjectVos = new ArrayList<>();
        UserActivityCitySubjectVo activityCitySubjectVo;
        String[] actUserPhotos;
        for (Activity activity:activityList){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,UserActivityCitySubjectVo.class);
            activityCitySubjectVo.setActImages(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            if (actUserNum!=null && Integer.valueOf(actUserNum)>=Constants.ISHOTNUM){
                isHot = "1";
            }
            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);

            //和获取活动详情公用一个接口，但是所需含义不同
            //获取活动详情  getEnrollStatus的含义 *0=未报名 *1=报名审核中 *2=报名成功 *3=报名审核不通过
            //获取参与人列表 *0=报名审核中 *1=报名成功 *2=报名审核不通过
            if (status == null) {
                activityCitySubjectVo.setEnrollStatus(String.valueOf(Integer.valueOf(enrollActService.getEnrollStatus(Integer.valueOf(userId), activity.getActId())) - 1));
            }else{
                activityCitySubjectVo.setEnrollStatus(status);
            }
            String rejectReason = enrollActService.getRejectReason(String.valueOf(activity.getActId()),userId);
            activityCitySubjectVo.setRejectReason(rejectReason);
            activityCitySubjectVos.add(activityCitySubjectVo);
        }
        return activityCitySubjectVos;
    }

    /**
     * 获取首页十条热门活动(根据报名人数倒排序)和三条已结束的活动
     * @return
     */
    public Map<String,Object> getHotActivity(){
        //先获取正在进行中的10场热门活动，如果不到10场，则获取未开始的剩下的几场活动
        List<Activity> activityList = activityDao.getHotActivityIng();
        if (activityList.size() < 10){
            List<Activity> hotActivityUnBegin = activityDao.getHotActivityUnBegin(10 - activityList.size());
            if (activityList != null){
                activityList.addAll(hotActivityUnBegin);
            }else {
                activityList = hotActivityUnBegin;
            }
        }
        List<Integer> columns = new ArrayList<>();
        //仍然没有到10条活动，因为有的活动没有人报名，所以没有查出来。直接查活动表，使用 actid not in 查未开始或正在进行中的活动。
        if (activityList.size() < 10){
            for (Activity activity:activityList){
                columns.add(activity.getActId());
            }
            activityList.addAll(activityDao.getActivityNotInColumns(columns
                    ,new String[]{"0","1"}
                    ,10-activityList.size()));
        }

        List<Activity> hotActivityPassed = activityDao.getHotActivityPassed();
        if (hotActivityPassed.size() < 4){
            columns = new ArrayList<>();
            for (Activity activity:hotActivityPassed){
                columns.add(activity.getActId());
            }
            hotActivityPassed.addAll(
                    activityDao.getActivityNotInColumns(columns
                            ,new String[]{"2"}
                            ,4-hotActivityPassed.size())
            );
        }

        Map<String,Object> data = new HashMap<>();
        List<HotActivityCitySubjectVo> actList = new ArrayList<>();
        HotActivityCitySubjectVo activityCitySubjectVo;
        String[] actUserPhotos;
        for (Activity activity:activityList){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,HotActivityCitySubjectVo.class);
            activityCitySubjectVo.setActPoster(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            if (actUserNum!=null && Integer.valueOf(actUserNum)>=Constants.ISHOTNUM){
                isHot = "1";
            }
            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);
            String status;
            if (new Date().before(activity.getStartTime())){
                //未开始
                status = "0";
            }else if (new Date().after(activity.getEndTime())){
                //已结束
                status = "2";
            }else{
                //正在进行
                status = "1";
            }
            activityCitySubjectVo.setStatus(status);
            actList.add(activityCitySubjectVo);
        }
        //热门活动
        data.put("actList",actList);


        List<HotActivityCitySubjectVo> actEndList = new ArrayList<>();
        for (Activity activity:hotActivityPassed){
            activityCitySubjectVo = DtoEntityVoUtils.entity2vo(activity,HotActivityCitySubjectVo.class);
            activityCitySubjectVo.setActPoster(activity.getActPoster());
            activityCitySubjectVo.setStartTime(String.valueOf(activity.getStartTime().getTime()));
            activityCitySubjectVo.setEndTime(String.valueOf(activity.getEndTime().getTime()));

            actUserPhotos = enrollActService.getTop3Photo(activity.getActId());
            //isHot 当报名人数大于15人时 是hot
            String actUserNum = enrollActService.getActivityCount(activity.getActId(),"3");
            String isHot = "0";
            if (actUserNum!=null && Integer.valueOf(actUserNum)>=Constants.ISHOTNUM){
                isHot = "1";
            }
            activityCitySubjectVo.setActUserNum(actUserNum);
            activityCitySubjectVo.setActUserPhotos(actUserPhotos);
            activityCitySubjectVo.setIsHot(isHot);
            String status;
            if (new Date().before(activity.getStartTime())){
                //未开始
                status = "0";
            }else if (new Date().after(activity.getEndTime())){
                //已结束
                status = "2";
            }else{
                //正在进行
                status = "1";
            }
            activityCitySubjectVo.setStatus(status);
            actEndList.add(activityCitySubjectVo);
        }

        data.put("actEndList",actEndList);
        return data;
    }
}
