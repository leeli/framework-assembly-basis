package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.*;
import com.sse.ssbk.dto.UserInfoDto;
import com.sse.ssbk.entity.*;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.MultiTableUpdateService;
import com.sse.ssbk.service.UserInfoService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.utils.SpringContextHolder;
import com.sse.ssbk.vo.UserInfoVo;
import org.apache.ibatis.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by yxf on 2017/7/28.
 */

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProvinceDao provinceDao;


    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private ActivityCommentDao activityCommentDao;

    @Autowired
    private ForumQuestionDao forumQuestionDao;

    @Autowired
    private MultiTableUpdateService multiTableUpdateService;

    @Override
    public void insertSelective(UserInfo userInfo) {
        userInfoDao.insertSelective(userInfo);
    }

    @Override
    public UserInfoVo getUserInfoByUserId() {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(loginUser.getUserId());
        userInfo = userInfoDao.selectByPrimaryKey(userInfo);
        if (userInfo == null){
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        UserInfoVo userInfoVo = DtoEntityVoUtils.entity2vo(userInfo, UserInfoVo.class);
//        userInfoVo.setCompany(loginUser.getCompanyName() == null ?"":loginUser.getCompanyName());

        Province province = new Province();
        province.setProvinceId(userInfo.getProvinceId());
        province = provinceDao.selectByPrimaryKey(province);
        if (province!= null) {
            userInfoVo.setProvince(province.getProvinceName());
        }
//        userInfoVo.setUserPhoto(loginUser.getUserPhoto());
//        userInfoVo.setProvince(loginUser.getProvince() == null ? "":loginUser.getProvince());
        return userInfoVo;
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int updateUserInfo(UserInfoDto userInfoDto) {

        UserSession loginUser =  BizUtils.getCurrentUser();
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(loginUser.getUserId());
        userInfo = userInfoDao.selectByPrimaryKey(userInfo);
//        userInfo = DtoEntityVoUtils.dto2entity(userInfoDto,UserInfo.class);
        userInfo = DtoEntityVoUtils.combineDtoAndEntity(userInfoDto,userInfo);

        if (userInfoDto.getProvince()!= null && !"".equals(userInfoDto.getProvince())){
            userInfo.setProvinceId(Integer.valueOf(userInfoDto.getProvince()));
        }
        userInfoDao.updateByPrimaryKeySelective(userInfo);

        System.out.println(userInfo);
        //todo 会自动更新token中保存的用户信息 不知道会造成什么影响
        syncUserWithUserInfo(userInfo);
        return 1;
    }

    /**
     * 根据user表更新user_info表
     * 顺便更新token中的值
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncUserInfoWithUser(){
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        String token = currentUser.getToken();
        User user = new User();
        user.setUserId(userId);
        user = userDao.selectByPrimaryKey(user);
        if (user == null){
            return;
        }
        UserInfo userInfo = DtoEntityVoUtils.dto2entity(user,UserInfo.class);
        UserInfoDao userInfoDao = (UserInfoDao)SpringContextHolder.getBean("userInfoDao");
        userInfo.setCompany(user.getCompanyName());
        userInfoDao.updateByPrimaryKeySelective(userInfo);
    }


    /**
     * 根据user_info表更新user表
     * 顺便更新token中的值
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncUserWithUserInfo(UserInfo userInfo){
        if (userInfo == null){
            return;
        }
        User user = new User();
        user.setUserId(userInfo.getUserId());
        user = userDao.selectByPrimaryKey(user);
        user.setRealName(userInfo.getRealName());
        user.setUserPhoto(userInfo.getUserPhoto());
        user.setCompanyName(userInfo.getCompany());
        user.setJobPosition(userInfo.getJobPosition());
        if (userInfo.getProvinceId() != null && !"".equals(userInfo.getProvinceId())) {
            user.setProvinceId(String.valueOf(userInfo.getProvinceId()));
            ProvinceDao provinceDao = SpringContextHolder.getBean(ProvinceDao.class);
            Province province = new Province();
            province.setProvinceId(userInfo.getProvinceId());
            province = provinceDao.selectByPrimaryKey(province);
            user.setProvinceName(province.getProvinceName());
        }
        user.setNameCard(userInfo.getNameCard());
        user.setOrgcodeCertificate(userInfo.getOrgcodeCertificate());
        userDao.updateByPrimaryKeySelective(user);
        //更新多张表
        multiTableUpdateService.updateWithUser(user);
        syncRedisUserWithUser(user);
    }

    /**
     * 顺便更新token中的值
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void syncRedisUserWithUser(User user){

        UserSession userSession = BizUtils.getCurrentUser();
        String token = BizUtils.getCurrentToken();
        userSession.setUserName(user.getUserName());
        userSession.setRealName(user.getRealName());
        userSession.setUserPhoto(user.getUserPhoto());
        if (user.getCityId()!= null && !"".equals(user.getCityId())){
            userSession.setCityId(Integer.valueOf(user.getCityId()));
        }else {
            userSession.setCityId(null);
        }
        userSession.setProvince(user.getProvinceId());
        if (user.getCompanyId()!= null && !"".equals(user.getCompanyId())){
            userSession.setCompanyId(Integer.valueOf(user.getCompanyId()));
        }else {
            userSession.setCompanyId(null);
        }
        userSession.setCompanyName(user.getCompanyName());
        RedisCacheUtils.setForValue(token, userSession, Constants.SESSION_TIME_OUT, TimeUnit.MILLISECONDS);
    }


    @Deprecated
    public int updateUserInfo1(UserInfoDto userInfoDto) {
        UserSession loginUser =  BizUtils.getCurrentUser();
        UserInfo userInfo = DtoEntityVoUtils.dto2entity(userInfoDto,UserInfo.class);
//        loginUser = DtoEntityVoUtils.dto2entity(userInfoDto,UserSession.class);
        userInfo.setUserId(loginUser.getUserId());
        User user = DtoEntityVoUtils.dto2entity(userInfoDto,User.class);
        user.setUserId(loginUser.getUserId());
        if (userInfoDto.getProvince()!= null && !"".equals(userInfoDto.getProvince())){
            userInfo.setProvinceId(Integer.valueOf(userInfoDto.getProvince()));
            Province province = new Province();
            province.setProvinceId(userInfo.getProvinceId());
            province = provinceDao.selectByPrimaryKey(userInfo.getProvinceId());
            user.setProvinceId(String.valueOf(province.getProvinceId()));
            user.setProvinceName(province.getProvinceName());
//            loginUser.setProvince(String.valueOf(province.getProvinceId()));
        }
        if (userInfoDto.getCompany()!=null &&!"".equals(userInfoDto.getCompany())){
            user.setCompanyName(userInfoDto.getCompany());
//            loginUser.setCompanyName(userInfoDto.getCompany());
        }



        //todo 更新 session中的值
//        RedisCacheUtils.setForValue(loginUser.getToken(),loginUser, Constants.SESSION_TIME_OUT, TimeUnit.MILLISECONDS);
        if (userInfoDto.getUserPhoto()== null){
            user.setUserPhoto(loginUser.getUserPhoto());        //避免 只更新profession时，user对象没有需要更新的值。
        }
        userDao.updateByPrimaryKeySelective(user);
        return userInfoDao.updateByPrimaryKeySelective(userInfo);
    }
}
