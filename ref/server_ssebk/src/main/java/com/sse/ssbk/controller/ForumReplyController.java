package com.sse.ssbk.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.dto.ForumReplyAddDto;
import com.sse.ssbk.dto.ForumReplyListDto;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ForumReplyService;
import com.sse.ssbk.vo.ForumReplyGetVo;
import com.sse.ssbk.vo.ForumReplyListVo;

/**
 * 你问我答模块回答控制器.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:03:15
 */
@RestController
@RequestMapping("/ssebk/forum")
public class ForumReplyController extends BaseController {
    @Autowired
    private ForumReplyService forumReplyService;

    /**
     * 添加回答.
     * @param dto ForumReplyDto.
     * @return
     * @author    HT-LiChuanbin 
     * @version   2017年7月20日 下午1:46:51
     */
    @PostMapping("/reply")
    @CountScore(ScoreRules.ADD_REPLY)
    @SendNotice
    public Result<?> add(@RequestBody @Valid ForumReplyAddDto dto) {
        forumReplyService.save(dto);
        return ResultUtils.success();
    }

    /**
     * 删除问题.
     * @param id 问题ID.
     * @return
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:47:02
     */
    @DeleteMapping("/reply/{replyId}")
    public Result<?> del(@PathVariable("replyId") Integer id) {
        forumReplyService.del(id);
        return ResultUtils.success();
    }

    /**
     * 获取问题.
     * @return
     * @author   HT-LiChuanbin 
     * @version  2017年7月20日 下午1:47:19
     */
    @GetMapping("/reply/{replyId}")
    public Result<ForumReplyGetVo> get(@PathVariable("replyId") Integer id) {
        ForumReplyGetVo voForumReplyGet = forumReplyService.get(id);
        return ResultUtils.success(voForumReplyGet);
    }


    /**
     * 获取回答列表.
     * @return
     * @author  HT-LiChuanbin
     * @version 2017年7月24日 下午3:06:22
     */
    @GetMapping("/replys")
    public Result<List<ForumReplyListVo>> list(String data) {
        if (StringUtils.isBlank(data)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        ForumReplyListDto dto = this.getForumReplyListDto(data);

        String lastId = dto.getLastId();
        String pageSize = dto.getPageSize();
        String userId = dto.getUserId();
        String questionId = dto.getQuestionId();

        BizUtils.Page page = BizUtils.getBizPage(lastId, pageSize);

        boolean boolUserId = StringUtils.isBlank(userId);
        boolean boolquestionId = StringUtils.isBlank(questionId);

        List<ForumReplyListVo> voForumReplyLists = new ArrayList<ForumReplyListVo>();

        if (boolUserId && !boolquestionId) {        //通过questionid查询  userid为空
            voForumReplyLists = forumReplyService.listByQuesionId(Integer.parseInt(questionId),page);
        } else if (boolquestionId) {  //通过用户id查询
            voForumReplyLists = forumReplyService.listByUserId(userId == null ?  null:Integer.parseInt(userId), page);
        } else if (!boolUserId && !boolquestionId){     //查询用户在某问题下的回答
            voForumReplyLists = forumReplyService.listByUserIdAndQuesionId(Integer.parseInt(userId), Integer.valueOf(questionId), page);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        return ResultUtils.success(voForumReplyLists);
    }

    /**
     *
     * @param data
     * @return
     * @author HT-LiChuanbin
     * @version 2017年7月25日 下午1:34:18
     */
    private ForumReplyListDto getForumReplyListDto(String data) {
        ForumReplyListDto dto = new ForumReplyListDto();
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastId = jData.getString("lastId");
        String pageSize = jData.getString("pageSize");
        String userId = jData.getString("userId");
        String questionId = jData.getString("questionId");
        dto.setQuestionId(questionId);
        dto.setLastId(lastId);
        dto.setPageSize(pageSize);
        dto.setUserId(userId);
        return dto;
    }
}