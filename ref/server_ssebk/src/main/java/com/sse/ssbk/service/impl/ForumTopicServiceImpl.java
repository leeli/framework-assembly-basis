package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ForumTopicDao;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.service.ForumTopicService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ForumTopicsVo;

@Service
public class ForumTopicServiceImpl implements ForumTopicService {
    @Autowired
    private ForumTopicDao forumTopicDao;

    @Override
    public List<ForumTopicsVo> list(Page page) {
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();

        List<ForumTopicsVo> voForumTopicss = new ArrayList<ForumTopicsVo>();
        Example example = new Example(ForumTopic.class);
        example.setOrderByClause("sequence DESC");
        Criteria criteria = example.createCriteria();
        if (page.getLastId() != -1) {
            ForumTopic forumTopic = forumTopicDao.selectByPrimaryKey(page.getLastId());
            if (forumTopic != null) {
                criteria.andLessThan("createTime", forumTopic.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        List<ForumTopic> forumTopics = forumTopicDao.selectByExampleAndRowBounds(example, rowBounds);
        Iterator<ForumTopic> iForumTopics = forumTopics.iterator();
        while (iForumTopics.hasNext()) {
            ForumTopic forumTopic = iForumTopics.next();
            Integer topicId = forumTopic.getTopicId();
            ForumTopicsVo voForumTopic = DtoEntityVoUtils.entity2vo(forumTopic, ForumTopicsVo.class);
            voForumTopic.setImgUrl(forumTopic.getTopicPicture());
            Set<Integer> concernUserTopics = RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER + topicId);
            voForumTopic.setIsConcern(BizUtils.contains(concernUserTopics, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
            voForumTopic.setConcernNum(CollectionUtils.isEmpty(concernUserTopics) ? 0 : concernUserTopics.size());
            Set<Integer> topicQuestions = RedisCacheUtils.getForSet(RedisConstants.TOPIC_QUESTION + topicId);
            Integer questionNum = CollectionUtils.isEmpty(topicQuestions) ? 0 : topicQuestions.size();
            voForumTopic.setQuestionNum(questionNum);
            voForumTopicss.add(voForumTopic);
        }
        return voForumTopicss;
    }

    @Override
    public List<ForumTopicsVo> listByTopicName(String topicName, Page page) {
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();

        List<ForumTopicsVo> voForumTopicss = new ArrayList<ForumTopicsVo>();
        Example example = new Example(ForumTopic.class);
        example.setOrderByClause("create_time DESC");
        Criteria criteria = example.createCriteria();
        criteria.andLike("topicName", "%" + topicName + "%");
        RowBounds rowBounds = new RowBounds(page.getLastId(), page.getPageSize());
        List<ForumTopic> forumTopics = forumTopicDao.selectByExampleAndRowBounds(example, rowBounds);
        Iterator<ForumTopic> iForumTopics = forumTopics.iterator();
        while (iForumTopics.hasNext()) {
            ForumTopic forumTopic = iForumTopics.next();
            Integer topicId = forumTopic.getTopicId();
            ForumTopicsVo voForumTopic = DtoEntityVoUtils.entity2vo(forumTopic, ForumTopicsVo.class);
            voForumTopic.setImgUrl(forumTopic.getTopicPicture());
            Set<Integer> concernUserTopics = RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER + topicId);
            voForumTopic.setIsConcern(BizUtils.contains(concernUserTopics, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
            voForumTopic.setConcernNum(CollectionUtils.isEmpty(concernUserTopics) ? 0 : concernUserTopics.size());
            Set<Integer> topicQuestions = RedisCacheUtils.getForSet(RedisConstants.TOPIC_QUESTION + topicId);
            Integer questionNum = CollectionUtils.isEmpty(topicQuestions) ? 0 : topicQuestions.size();
            voForumTopic.setQuestionNum(questionNum);
            voForumTopicss.add(voForumTopic);
        }
        return voForumTopicss;
    }
}