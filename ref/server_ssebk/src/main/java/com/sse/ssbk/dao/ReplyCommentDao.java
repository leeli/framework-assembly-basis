package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ReplyComment;

public interface ReplyCommentDao extends BaseDao<ReplyComment> {
    /**
     * 
     * @param replyComment
     * @author HT-LiChuanbin 
     * @version 2017年8月2日 下午4:29:38
     */
    void insertSelectiveWithId(ReplyComment replyComment);
}