package com.sse.ssbk.dao;

import java.util.List;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.TopicQuestion;

public interface TopicQuestionDao extends BaseDao<TopicQuestion> {
	/**
	 * 批量插入.
	 * @param topicQuestions TopicQuestion.
	 * @author               HT-LiChuanbin 
	 * @version              2017年7月21日 上午10:41:50
	 */
	void insertBatch(List<TopicQuestion> topicQuestions);
}