package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by work_pc on 2017/7/23.
 */
@Data
public class AuditNewsDto {
    private String reason;
    @NotBlank
    private String status;
}
