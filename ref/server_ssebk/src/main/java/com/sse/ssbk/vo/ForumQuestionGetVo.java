package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ForumQuestionGetVo {
    // t_forum_question
    private String questionId;
    private String title;
    private String userId;
    private String userName;
    private String userPhoto;
    private String company;
    private String content;

    private List<String> imageArr;

    private String isConcern;
    private List<ForumQuestionForumTopicVo> topics;

    private String canDel;
    private String isMine;
    private String hasAnswered;

    private Integer replyNum;
    private Integer likeNum;
    private String isLike;
    private Integer collectNum;
    private String isCollect;

    private String isAnonymous;

    public ForumQuestionGetVo() {
        this.questionId = "";
        this.title = "";
        this.userName = "";
        this.userPhoto = "";
        this.company = "";
        this.content = "";
        this.imageArr = new ArrayList<String>();
        this.isConcern = "";
        this.topics = new ArrayList<ForumQuestionForumTopicVo>();
        this.canDel = "";
        this.isMine = "";
        this.hasAnswered = "";
        this.userId = "";
        this.isLike = "";
        this.isCollect = "";
        this.isAnonymous = "";
    }
}