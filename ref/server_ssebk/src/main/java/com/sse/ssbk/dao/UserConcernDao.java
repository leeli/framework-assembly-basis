package com.sse.ssbk.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ConcernMyQuestionReply;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.entity.UserConcernUser;

/**
 * Created by yxf on 2017/7/7.
 */
public interface UserConcernDao extends BaseDao<UserConcernUser> {
    /**
     * 
     * @param userId
     * @param offset
     * @param limit
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月4日 下午7:53:54
     */
    List<ConcernMyQuestionReply> selectMyQuestionReplys(@Param("userId") Integer userId, @Param("offset") long offset, @Param("limit") long limit);

    List<Integer> getUserConcernedMeColumnInOrder(@Param("userId") Integer userId, @Param("lastUserId") Integer lastUserId, @Param("pageSize") Integer pageSize);

    /**
     * 关注我的人列表接口
     * @Param columns
     * @return
     */
    List<User> getUserConcernedMeList(@Param("columns") List<Integer> columns);

    List<Integer> getIConcernUserColumnInOrder(@Param("userId") Integer userId, @Param("lastUserId") Integer lastUserId, @Param("pageSize") Integer pageSize);

    /**
     * 我关注的人列表接口
     * @param columns  userId 集合
     * @return
     */
    List<User> getIConcernedUserList(@Param("columns") List<Integer> columns);

    List<Integer> getIconcernedTopicColumnInOrder(@Param("userId") Integer userId, @Param("lastTopicId") Integer lastUserId, @Param("pageSize") Integer pageSize);

    /**
     * 我关注的板块列表
     * @param columns userId 集合
     * @return
     */
    List<ForumTopic> getIconcernedTopicList(@Param("columns") List<Integer> columns);

    int getTopicConcernNum(@Param("topicId") Integer topicId);

    /**
     * 获取关注我的人的数目
     * @param userId
     * @return
     */
    int getUserConcernedMeNumber(@Param("userId") Integer userId);

    /**
     * 获取关注我的人的数目
     * @param userId
     * @return
     */
    int getIConcernedUserNumber(@Param("userId") Integer userId);

    /**
     * 关注用户写入接口
     * @param userId  用户id
     * @param userConcernId  用户要关注的人的id
     */
    void userConcernUser(@Param("userId") Integer userId, @Param("userConcernId") Integer userConcernId);

    void userConcernTopic(@Param("userId") Integer userId, @Param("userTopicId") Integer userTopicId);

    /**
     * 取消关注用户接口
     * @param userId  用户id
     * @param userConcernId  用户要取消关注的人的id
     */
    void deleteConcernUser(@Param("userId") Integer userId, @Param("userConcernId") Integer userConcernId);

    void deleteConcernTopic(@Param("userId") Integer userId, @Param("userTopicId") Integer userTopicId);

    int isConcernedUser(@Param("userId") Integer userId, @Param("userConcernId") Integer userConcernId);

    int isConcernedTopic(@Param("userId") Integer userId, @Param("userTopicId") Integer userTopicId);
}
