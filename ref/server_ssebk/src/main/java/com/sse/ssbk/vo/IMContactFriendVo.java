package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/9.
 */
@Data
public class IMContactFriendVo {
    private String userId;
    private String userName;
    private String realName;
    private String pinYin;
    private String company;
    private String userPhoto;
}
