package com.sse.ssbk.service.impl;

import com.sse.ssbk.dao.AppSdkRelationDao;
import com.sse.ssbk.service.AppSdkRelationService;
import com.sse.ssbk.vo.AppSdkRelationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ciyua on 2017/8/2.
 */
@Service
public class AppSdkRelationServiceImpl implements AppSdkRelationService {

    @Autowired
    private AppSdkRelationDao appSdkRelationDao;

    @Override
    public AppSdkRelationVo isExistAppSDK(String bundleId, String sdkId) {
        return appSdkRelationDao.selectByBundleIdSdkId(bundleId, sdkId);
    }
}
