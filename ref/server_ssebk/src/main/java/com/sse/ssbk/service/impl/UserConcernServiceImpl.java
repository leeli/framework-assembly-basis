package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.dao.ForumQuestionDao;
import com.sse.ssbk.dao.TopicQuestionDao;
import com.sse.ssbk.dao.UserConcernDao;
import com.sse.ssbk.dto.QuestionDto;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.TopicQuestion;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.service.UserConcernService;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ForumTopicVo;
import com.sse.ssbk.vo.UserConcernVo;
import org.aspectj.weaver.patterns.TypePatternQuestions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yxf on 2017/7/13.
 */
@Service
public class UserConcernServiceImpl implements UserConcernService {

    @Autowired
    private UserConcernDao userConcernDao;


    @Autowired
    private TopicQuestionDao topicQuestionDao;

    /**
     * 关注我的人
     * @param userId 用户id
     * @return
     */
    @Override
    public List<UserConcernVo> getUserConcernedMeList(String userId,String lastUserId,String pageSize) {

        List<Integer> columns = userConcernDao.getUserConcernedMeColumnInOrder(Integer.valueOf(userId),Integer.valueOf(lastUserId),Integer.valueOf(pageSize));
        List<User> userConcernedMeList;
        if (columns == null || columns.size() == 0){
            userConcernedMeList = new ArrayList<>();
        }else {
            userConcernedMeList = userConcernDao.getUserConcernedMeList(columns);
        }

//        List<UserConcernVo> userConcernVos = DtoEntityVoUtils.entityList2voList(userConcernedMeList, UserConcernVo.class);
        List<UserConcernVo> userConcernVos = new ArrayList<>();
        UserConcernVo userConcernVo;
        for (User user :
                userConcernedMeList) {
            userConcernVo = DtoEntityVoUtils.entity2vo(user,UserConcernVo.class);
            userConcernVo.setCompany(user.getCompanyName());
            userConcernVo.setPhoto(user.getUserPhoto());
            boolean isConcern = isConcernedUser(Integer.valueOf(userId),user.getUserId());
            if (isConcern){
                userConcernVo.setIsConcern("1");
            }else{
                userConcernVo.setIsConcern("0");
            }
            userConcernVos.add(userConcernVo);
        }
        return userConcernVos;
    }

    /**
     * 我关注的人
     * @param userId 用户id
     * @return
     */
    @Override
    public List<UserConcernVo> getIConcernedUserList(String userId,String lastUserId,String pageSize) {

        List<Integer> columns = userConcernDao.getIConcernUserColumnInOrder(Integer.valueOf(userId),Integer.valueOf(lastUserId),Integer.valueOf(pageSize));
        List<User> iConcernedUserList;
        if (columns == null || columns.size() == 0){
            iConcernedUserList = new ArrayList<>();
        }else {
            iConcernedUserList = userConcernDao.getIConcernedUserList(columns);
        }
//        List<UserConcernVo> userConcernVos = DtoEntityVoUtils.entityList2voList(userConcernedMeList, UserConcernVo.class);
        List<UserConcernVo> userConcernVos = new ArrayList<>();
        UserConcernVo userConcernVo;
        for (User user :
                iConcernedUserList) {
            userConcernVo = DtoEntityVoUtils.entity2vo(user,UserConcernVo.class);
            userConcernVo.setCompany(user.getCompanyName());
            userConcernVo.setPhoto(user.getUserPhoto());
            //我关注的人，我肯定关注了他
            userConcernVo.setIsConcern("1");
            /*boolean isConcern = isConcernedUser(Integer.valueOf(userId),user.getUserId());
            if (isConcern){
                userConcernVo.setIsConcern("1");
            }else{
                userConcernVo.setIsConcern("0");
            }*/
            userConcernVos.add(userConcernVo);
        }
        return userConcernVos;
    }

    /**
     * 获取我关注的板块
     * @param userId
     * @return
     */
    @Override
    public List<ForumTopicVo> getIconcernedTopicList(String userId,String lastTopicId,String pageSize) {
        List<Integer> columns = userConcernDao.getIconcernedTopicColumnInOrder(Integer.valueOf(userId),Integer.valueOf(lastTopicId),Integer.valueOf(pageSize));
        List<ForumTopic> iconcernedTopicList;
        if (columns == null || columns.size() == 0){
            iconcernedTopicList = new ArrayList<>();
        }else {
            iconcernedTopicList = userConcernDao.getIconcernedTopicList(columns);
        }

        List<ForumTopicVo> forumTopicVoList = new ArrayList<>();
        ForumTopicVo forumTopicVo;
        for (ForumTopic forumTopic :
                iconcernedTopicList) {
            forumTopicVo = DtoEntityVoUtils.entity2vo(forumTopic,ForumTopicVo.class);
            forumTopicVo.setImgUrl(forumTopic.getTopicPicture());
//            forumTopicVo.setConcernNum(String.valueOf(getTopicConcernNum(String.valueOf(forumTopic.getTopicId()))));
            String concernNum = String.valueOf(RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER+forumTopic.getTopicId()).size());
            forumTopicVo.setConcernNum(concernNum);
            Example topicQuestionNumExample = new Example(TopicQuestion.class);
            Example.Criteria criteria = topicQuestionNumExample.createCriteria();
            criteria.andEqualTo("topicId",forumTopic.getTopicId())
                    .andEqualTo("isDelete", Is.NO.getCode());

            Integer questionNum = topicQuestionDao.selectCountByExample(topicQuestionNumExample);

            if (questionNum == null){
                forumTopicVo.setQuestionNum("0");
            }else {
                forumTopicVo.setQuestionNum(String.valueOf(questionNum));
            }
            forumTopicVoList.add(forumTopicVo);
        }
        return forumTopicVoList;
    }

    /**
     * 某个板块有多少人关注
     * @param topicNum
     * @return
     */
    @Override
    @Deprecated
    public int getTopicConcernNum(String topicNum) {
        return userConcernDao.getTopicConcernNum(Integer.valueOf(topicNum));
    }

    @Override
    public void userConcernUser(Integer userId, Integer userConcernId) {
        userConcernDao.userConcernUser(userId,userConcernId);
    }

    @Override
    public void userConcernTopic(Integer userId, Integer userTopicId) {
        userConcernDao.userConcernTopic(userId,userTopicId);
    }

    @Override
    public void deleteConcernUser(Integer userId, Integer userTopicId) {
        userConcernDao.deleteConcernUser(userId,userTopicId);
    }

    @Override
    public void deleteConcernTopic(Integer userId, Integer userTopicId) {
        userConcernDao.deleteConcernTopic(userId,userTopicId);
    }

    @Override
    public boolean isConcernedUser(Integer userId, Integer userConcernId) {
        int result = userConcernDao.isConcernedUser(userId,userConcernId);
        return result == 0 ? false : true;
    }

    @Override
    public boolean isConcernedTopic(Integer userId, Integer userTopicId) {
        int result = userConcernDao.isConcernedTopic(userId,userTopicId);
        return result == 0 ? false : true;
    }
}
