package com.sse.ssbk.controller;

import java.util.List;

import javax.validation.Valid;

import com.sse.ssbk.utils.SendNotice;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.dto.ConcernAddDto;
import com.sse.ssbk.dto.ConcernDelDto;
import com.sse.ssbk.enums.ConcernTargetType;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ConcernService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.ConcernMyQuestionReplyVo;

@RestController
@RequestMapping("/ssebk")
public class ConcernController {
    @Autowired
    private ConcernService concernService;

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:38
     */
    @PostMapping("/concern")
    @SendNotice
    public Result<?> add(@RequestBody @Valid ConcernAddDto dto) {
        concernService.add(Integer.parseInt(dto.getTargetId()), ConcernTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:41
     */
    @DeleteMapping("/concern")
    public Result<?> del(@RequestBody @Valid ConcernDelDto dto) {
        concernService.del(Integer.parseInt(dto.getTargetId()), ConcernTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }

    /**
     * 
     * @author HT-LiChuanbin 
     * @version 2017年8月4日 下午6:15:52
     */
    @GetMapping("/my/questionreplys")
    public Result<List<ConcernMyQuestionReplyVo>> listMyQuestionReply(String data) {
        if (StringUtils.isBlank(data)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jData = (JSONObject) JSON.parse(data);
        long pageNo = Long.parseLong(jData.getString("pageNo"));
        long pageSize = Long.parseLong(jData.getString("pageSize"));
        return ResultUtils.success(concernService.listMyQuestionReplys(pageNo, pageSize));
    }
}