package com.sse.ssbk.service;

import com.sse.ssbk.dto.ActDto;
import com.sse.ssbk.entity.ActSubject;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.vo.ActSubjectVo;
import com.sse.ssbk.vo.ActivityCityVo;
import com.sse.ssbk.vo.ActivitySubjectVo;
import com.sse.ssbk.vo.CircleActVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/17.
 */
public interface ActSubjectService {
    /**
     * 获取活动主题列表
     * @param lastSubjectId
     * @param pageSize
     * @return
     */
    List<ActSubjectVo> getActSubjects(@Param("lastSubjectId") String lastSubjectId, @Param("pageSize") String pageSize);

    /**
     * 主题活动查询
     * @param lastSubjectId
     * @param pageSize
     * @return
     */
    List<ActivitySubjectVo> getActivitySubjects(String lastSubjectId, String pageSize);

    List<ActivityCityVo> getActivityCity(String lastCityId, String pageSize);

}
