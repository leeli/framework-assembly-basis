package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 后期评论列表修改VO.
 * @author  HT-LiChuanbin 
 * @version 2017年8月15日 上午11:07:57
 */
@Data
public class CommentsAdapterVo {
    private Integer commentNum;
    private List<CommentsVo> comments;

    public CommentsAdapterVo() {
        this.commentNum = 0;
        this.comments = new ArrayList<CommentsVo>();
    }
}