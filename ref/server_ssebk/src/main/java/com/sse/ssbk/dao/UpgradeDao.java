package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Upgrade;

public interface UpgradeDao extends BaseDao<Upgrade> {
}