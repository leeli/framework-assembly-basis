package com.sse.ssbk.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 收藏枚举.
 * @author  HT-LiChuanbin 
 * @version 2017年7月26日 下午8:35:41
 */
public enum CollectionTargetType {
    ACT, QUESTION, REPLY, NEWS;
    public static CollectionTargetType parse(String targetType) {
        if (StringUtils.isBlank(targetType)) {
            return null;
        }
        targetType = targetType.trim();
        for (CollectionTargetType type : CollectionTargetType.values()) {
            if (StringUtils.equalsIgnoreCase(type.toString(), targetType)) {
                return type;
            }
        }
        return null;
    }
}