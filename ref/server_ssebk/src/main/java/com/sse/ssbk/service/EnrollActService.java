package com.sse.ssbk.service;

import com.sse.ssbk.dto.AuditActivityJoinerDto;
import com.sse.ssbk.entity.EnrollActivity;

import java.util.List;

/**
 * Created by yxf on 2017/7/17.
 */
public interface EnrollActService {
    /**
     * 报名活动
     *
     * @param enrollActivity
     */
    EnrollActivity enrollAct(EnrollActivity enrollActivity);

    /**
     * 查看用户报名状态
     *
     * @param userId
     * @param actId
     * @return
     */
    String getEnrollStatus(Integer userId, Integer actId);

    /**
     * 获取活动不同用户状态的数量
     * @param actId
     * @param status 0 状态0报名(未审核)，1批准（审核通过），2被拒(审核不通过)  3 所有报名用户
     * @return
     */
    String getActivityCount(Integer actId, String status);

    /**
     * 获取活动报名人的前三张头像
     * @param actId
     * @return
     */
    String[] getTop3Photo(Integer actId);

    /**
     * 审核报名人 status 0 待审核 1 审核通过  2 审核不通过
     * @param actId
     * @param auditActivityJoinerDto
     * @return
     */
    int auditActivityJoiner(Integer actId,AuditActivityJoinerDto auditActivityJoinerDto);

    String getRejectReason(String actId,String userId);
}