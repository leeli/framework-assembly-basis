package com.sse.ssbk.vo;

import lombok.Data;

import java.util.Date;

@Data
public class AppSdkRelationVo {
    private String appId;
    private String securityKey;
    private Date expireDate;
    private Date createDate;
    private String deviceType;
    private String androidSignature;
    private String bundleId;
    private String sdkId;

}