package com.sse.ssbk.vo;

import lombok.Data;

import java.util.Date;

/**
 * Created by yxf on 2017/7/20.
 */

@Data
public class NewsVo {
    private String title;
    private String content;
    private String[] images;
    private String sourceFromId;
    private String sourceFromName;
    private String createTime;
    private Integer readCount;
    private Integer collectionCount;
    private Integer commentCount;
    private String isCollect;
}
