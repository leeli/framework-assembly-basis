package com.sse.ssbk.service;

import com.sse.ssbk.dto.InvitationDto;

/**
 * Created by work_pc on 2017/8/8.
 */
public interface InvitationService {
    int addInvitation(InvitationDto invitationDto);

    String validateInvitation(String invitationCode,String realName,String loginName);
}
