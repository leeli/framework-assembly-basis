package com.sse.ssbk.dto;

import lombok.Data;

/**
 * Created by ciyua on 2017/8/8.
 */
@Data
public class SmsCaptchaReqDto {

    private String type;
    private String phone;
    private String authCode;
}
