package com.sse.ssbk.controller.base;

import java.util.HashMap;
import java.util.Map;

import com.sse.ssbk.exception.AppReturnCode;

public class BaseController {
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
	public Map<String, Object> dataParamMap = new HashMap<String, Object>();
	public Map<String, Object> baseParamMap = new HashMap<String, Object>();
	protected Map<String, Object> resultMap = new HashMap<String, Object>();

	public Map<String, Object> getResultMap() {
		return resultMap;
	}

	public void setResultMap(Map<String, Object> resultMap) {
		this.resultMap = resultMap;
	}

	public Map<String, Object> getDataParamMap() {
		if (dataParamMap == null) {
			dataParamMap = new HashMap<String, Object>();
		}
		return dataParamMap;
	}

	public void setDataParamMap(Object reqContent) {
		if (reqContent instanceof Map<?, ?>) {
			this.dataParamMap = (Map<String, Object>) reqContent;
		}
	}

	public Map<String, Object> getBaseParamMap() {
		if (baseParamMap == null) {
			baseParamMap = new HashMap<String, Object>();
		}
		return baseParamMap;
	}

	public void setBaseParamMap(Object reqContent) {
		if (reqContent instanceof Map<?, ?>) {
			this.baseParamMap = (Map<String, Object>) reqContent;
		}
	}

	public void setFail(AppReturnCode returnCode) {
		resultMap.clear();
		resultMap.put("status", false);
		//		resultMap.put("RetrunCode", returnCode.getValue());
		resultMap.put("msg", returnCode.getMsg());
	}


	/**
	 * 单点登录
	 * @return
	 */
	public boolean checkUser(){
	// TODO 逻辑待完善

		return true;
	}
}
