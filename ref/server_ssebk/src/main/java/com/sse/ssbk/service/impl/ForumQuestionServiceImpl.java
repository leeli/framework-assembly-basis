package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.ContextLoader;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ForumQuestionDao;
import com.sse.ssbk.dao.ForumReplyDao;
import com.sse.ssbk.dao.ForumTopicDao;
import com.sse.ssbk.dao.TopicQuestionDao;
import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.ForumReply;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.TopicQuestion;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ForumQuestionService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ForumQuestionForumTopicVo;
import com.sse.ssbk.vo.ForumQuestionGetVo;
import com.sse.ssbk.vo.ForumQuestionListVo;
import com.sse.ssbk.vo.ForumQuestionSaveVo;

@Lazy
@Service
public class ForumQuestionServiceImpl implements ForumQuestionService {
    private static ApplicationContext CONTEXT = ContextLoader.getCurrentWebApplicationContext();
    @Autowired
    private ForumQuestionDao forumQuestionDao;
    @Autowired
    private TopicQuestionDao topicQuestionDao;
    @Autowired
    private ForumTopicDao forumTopicDao;
    @Autowired
    private ForumReplyDao forumReplyDao;

    @Override
    @Transactional
    public ForumQuestionSaveVo save(ForumQuestionDto dto) {
        // 保存提问.
        ForumQuestion forumQuestion = DtoEntityVoUtils.dto2entity(dto, ForumQuestion.class);
        String[] images = dto.getImageArr();
        String strImages = BizUtils.getStrImages(images);
        forumQuestion.setImages(strImages);

        UserSession session = BizUtils.getCurrentUser();
        forumQuestion.setUserName(session.getRealName());
        forumQuestion.setUserId(session.getUserId());
        forumQuestion.setUserPhoto(session.getUserPhoto());
        forumQuestion.setCompanyId(session.getCompanyId());
        forumQuestion.setCompanyName(session.getCompanyName());

        Date now = new Date();
        forumQuestion.setCreateTime(now);
        forumQuestionDao.insertSelectiveWithId(forumQuestion);

        Integer questionId = forumQuestion.getQuestionId();
        // 保存问题和板块关联信息.
        String[] topicIds = dto.getTopicIds();
        if (topicIds != null && topicIds.length != 0) {
            List<TopicQuestion> topicQuestions = new ArrayList<TopicQuestion>();
            for (String strTopicId : topicIds) {
                Integer topicId = Integer.parseInt(strTopicId);
                TopicQuestion topicQuestion = new TopicQuestion();
                topicQuestion.setTopicId(topicId);
                topicQuestion.setQuestionId(questionId);
                topicQuestion.setCreateTime(now);
                topicQuestions.add(topicQuestion);
            }
            topicQuestionDao.insertBatch(topicQuestions);

            // redis存储板块问题关系.
            for (int i = 0; i < topicIds.length; i++) {
                RedisCacheUtils.addForSet(RedisConstants.TOPIC_QUESTION + topicIds[i], questionId);
            }
        }
        return DtoEntityVoUtils.entity2vo(forumQuestion, ForumQuestionSaveVo.class);
    }

    @Override
    @Transactional
    @SuppressWarnings({ "unchecked" })
    public void del(Integer id) {
        // this.validateDelOrUpt(id);
        forumQuestionDao.deleteByPrimaryKeyLogic(id);

        // 级联删除t_topic_question_map数据.
        Date now = new Date();
        Example example = new Example(TopicQuestion.class);
        example.createCriteria().andEqualTo("isDelete", Is.NO.getCode()).andEqualTo("questionId", id);
        List<TopicQuestion> topicQuestions = topicQuestionDao.selectByExample(example);
        Integer[] topicIds = new Integer[topicQuestions.size()];
        for (int i = 0; i < topicQuestions.size(); i++) {
            topicIds[i] = topicQuestions.get(i).getTopicId();
        }
        for (int i = 0; i < topicIds.length; i++) {
            TopicQuestion topicQuestion = new TopicQuestion();
            topicQuestion.setIsDelete(Is.YES.getCode());
            topicQuestion.setDeleteTime(now);
            Example example2 = new Example(TopicQuestion.class);
            example2.createCriteria().andEqualTo("topicId", topicIds[i]).andEqualTo("questionId", id).andEqualTo("isDelete", Is.NO.getCode());
            topicQuestionDao.updateByExampleSelective(topicQuestion, example2);
        }
        
        // 级联删除对应Redis数据.
        RedisTemplate<String, Integer> redisTemplate = (RedisTemplate<String, Integer>) CONTEXT.getBean("redisTemplate");
        SetOperations<String, Integer> opsSet = redisTemplate.opsForSet();
        for (int i = 0; i < topicIds.length; i++) {
            opsSet.remove(RedisConstants.TOPIC_QUESTION + topicIds[i], id);
        }
    }

    /**
     * 
     * @param id
     * @author HT-LiChuanbin 
     * @version 2017年8月16日 上午11:00:56
     */
    private void validateDelOrUpt(Integer id) {
        // 要删除|编辑的问题必须存在.
        Example example = new Example(ForumQuestion.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("isDelete", Is.NO.getCode()).andEqualTo("questionId", id);
        List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExample(example);
        if (CollectionUtils.isEmpty(forumQuestions) || forumQuestions.size() != 1) {
            throw new SSEBKRuntimeException(AppReturnCode.NOT_EXIST);
        }

        // 属于本人发布的且没有被人回答或者收藏的问题，方可删除|编辑.
        ForumQuestion forumQuestion = forumQuestions.get(0);
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();
        if (!forumQuestion.getUserId().equals(currentUserId)) {
            throw new SSEBKRuntimeException("无法操作别人的提问！");
        }
        Set<Integer> questionReplys = RedisCacheUtils.getForSet(RedisConstants.QUESTION_REPLY + id);
        if (!CollectionUtils.isEmpty(questionReplys)) {
            throw new SSEBKRuntimeException("已经被回答，无法操作！");
        }
        Set<Integer> collectionQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.COLLECTION_QUESTION_USER + id);
        if (!CollectionUtils.isEmpty(collectionQuestionUsers)) {
            throw new SSEBKRuntimeException("已经被收藏，无法操作！");
        }
    }

    @Override
    public void upt(ForumQuestionDto dto) {
        final Integer questionId = dto.getQuestionId();

        this.validateDelOrUpt(questionId);
        this.validateUpt(dto);

        Date now = new Date();
        // 修改问题内容。
        ForumQuestion forumQuestion = DtoEntityVoUtils.dto2entity(dto, ForumQuestion.class);
        forumQuestion.setImages(BizUtils.getStrImages(dto.getImageArr()));
        forumQuestion.setUpdateTime(now);

        forumQuestionDao.updateByPrimaryKeySelective(forumQuestion);

        // 删除问题和板块关联信息.
        TopicQuestion queryTopicQuestion = new TopicQuestion();
        queryTopicQuestion.setQuestionId(questionId);
        queryTopicQuestion.setIsDelete(Is.NO.getCode());
        List<TopicQuestion> topicQuestions = topicQuestionDao.select(queryTopicQuestion);
        Iterator<TopicQuestion> iTopicQuestion = topicQuestions.iterator();
        while (iTopicQuestion.hasNext()) {
            TopicQuestion topicQuestion = iTopicQuestion.next();
            topicQuestion.setIsDelete(Is.YES.getCode());
            topicQuestion.setDeleteTime(now);
            Example example = new Example(TopicQuestion.class);
            example.createCriteria().andEqualTo("topicId", topicQuestion.getTopicId()).andEqualTo("questionId", topicQuestion.getQuestionId()).andEqualTo("isDelete", Is.NO.getCode());
            topicQuestionDao.updateByExampleSelective(topicQuestion, example);
        }

        // 添加问题和板块关联信息.
        String[] topicIds = dto.getTopicIds();
        if (topicIds != null && topicIds.length != 0) {
            List<TopicQuestion> topicQuestionsForAdd = new ArrayList<TopicQuestion>();
            for (String strTopicId : topicIds) {
                Integer topicId = Integer.parseInt(strTopicId);
                TopicQuestion topicQuestion = new TopicQuestion();
                topicQuestion.setTopicId(topicId);
                topicQuestion.setQuestionId(questionId);
                topicQuestion.setIsDelete(Is.NO.getCode());
                topicQuestion.setCreateTime(now);
                topicQuestionsForAdd.add(topicQuestion);
            }
            topicQuestionDao.insertList(topicQuestionsForAdd);
            // topicQuestionDao.insertBatch(topicQuestionsForAdd);
        }
    }

    /**
     * 
     * @param dto
     * @author HT-LiChuanbin 
     * @version 2017年8月1日 下午5:22:58
     */
    private void validateUpt(ForumQuestionDto dto) {
        final Integer questionId = dto.getQuestionId();
        ForumQuestion forumQuestionForQuery = new ForumQuestion();
        forumQuestionForQuery.setQuestionId(questionId);
        forumQuestionForQuery.setIsDelete(Is.NO.getCode());
        ForumQuestion forumQuestion = forumQuestionDao.selectOne(forumQuestionForQuery);
        if (forumQuestion == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public ForumQuestionGetVo get(Integer id) {
        this.validateExist(id);

        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();
        ForumQuestion forumQuestion = forumQuestionDao.selectByPrimaryKey(id);

        ForumQuestionGetVo voForumQuestion = DtoEntityVoUtils.entity2vo(forumQuestion, ForumQuestionGetVo.class);
        Is isAnonymous = Is.parse(forumQuestion.getIsAnonymous());
        voForumQuestion.setIsAnonymous(forumQuestion.getIsAnonymous());
        if (Is.YES.equals(isAnonymous) && !currentUserId.equals(forumQuestion.getUserId())) {
            voForumQuestion.setUserId("");
            voForumQuestion.setUserName("匿名");
            voForumQuestion.setUserPhoto("");
            voForumQuestion.setCompany("未公开");
        } else {
            voForumQuestion.setCompany(forumQuestion.getCompanyName());
        }
        Set<Integer> setQuestionReplys = RedisCacheUtils.getForSet(RedisConstants.QUESTION_REPLY + id);
        voForumQuestion.setReplyNum(CollectionUtils.isEmpty(setQuestionReplys) ? 0 : setQuestionReplys.size());
        Set<Integer> setLikeQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.LIKE_QUESTION_USER + id);
        voForumQuestion.setIsLike(BizUtils.contains(setLikeQuestionUsers, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
        voForumQuestion.setLikeNum(CollectionUtils.isEmpty(setLikeQuestionUsers) ? 0 : setLikeQuestionUsers.size());

        Set<Integer> setCollectionQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.COLLECTION_QUESTION_USER + id);
        voForumQuestion.setIsCollect(BizUtils.contains(setCollectionQuestionUsers, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
        voForumQuestion.setCollectNum(CollectionUtils.isEmpty(setCollectionQuestionUsers) ? 0 : setCollectionQuestionUsers.size());

        voForumQuestion.setImageArr(BizUtils.getArrImages(forumQuestion.getImages()));
        Set<Integer> setConcernUserUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + currentUserId);
        voForumQuestion.setIsConcern(BizUtils.contains(setConcernUserUsers, forumQuestion.getUserId()) ? Is.YES.getCode() : Is.NO.getCode());
        voForumQuestion.setCanDel(CollectionUtils.isEmpty(setQuestionReplys) ? Is.YES.getCode() : Is.NO.getCode());
        voForumQuestion.setIsMine(currentUserId.equals(forumQuestion.getUserId()) ? Is.YES.getCode() : Is.NO.getCode());

        Example example = new Example(ForumReply.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("questionId", id).andEqualTo("userId", currentUserId).andEqualTo("isDelete", Is.NO.getCode());
        List<ForumReply> forumReplys = forumReplyDao.selectByExample(example);
        voForumQuestion.setHasAnswered(CollectionUtils.isEmpty(forumReplys) ? Is.NO.getCode() : Is.YES.getCode());

        TopicQuestion queryTopicQuestion = new TopicQuestion();
        queryTopicQuestion.setQuestionId(id);
        queryTopicQuestion.setIsDelete(Is.NO.getCode());

        List<ForumQuestionForumTopicVo> forumTopics = new ArrayList<ForumQuestionForumTopicVo>();
        List<TopicQuestion> topicQuestions = topicQuestionDao.select(queryTopicQuestion);
        Iterator<TopicQuestion> iTopicQuestion = topicQuestions.iterator();
        while (iTopicQuestion.hasNext()) {
            TopicQuestion topicQuestion = iTopicQuestion.next();
            Integer topicId = topicQuestion.getTopicId();
            ForumTopic forumTopic = forumTopicDao.selectByPrimaryKey(topicId);
            ForumQuestionForumTopicVo voForumQuestionForumTopic = new ForumQuestionForumTopicVo();
            voForumQuestionForumTopic.setTopicId(forumTopic.getTopicId().toString());
            voForumQuestionForumTopic.setTopicName(forumTopic.getTopicName());
            Set<Integer> setConcernUserTopics = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_TOPIC + currentUserId);
            voForumQuestionForumTopic.setIsConcern(BizUtils.contains(setConcernUserTopics, topicId) ? Is.YES.getCode() : Is.NO.getCode());
            Set<Integer> concernTopicUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER + topicId);
            voForumQuestionForumTopic.setConcernNum(CollectionUtils.isEmpty(concernTopicUsers) ? 0 : concernTopicUsers.size());
            voForumQuestionForumTopic.setImgUrl(forumTopic.getTopicPicture());
            forumTopics.add(voForumQuestionForumTopic);
        }
        voForumQuestion.setTopics(forumTopics);
        return voForumQuestion;
    }

    /**
     * 
     * @param id
     * @author HT-LiChuanbin 
     * @version 2017年8月17日 下午5:45:44
     */
    private void validateExist(Integer id) {
        Example example = new Example(ForumQuestion.class);
        example.createCriteria().andEqualTo("isDelete", Is.NO.getCode()).andEqualTo("questionId", id);
        List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExample(example);
        if (CollectionUtils.isEmpty(forumQuestions)) {
            throw new SSEBKRuntimeException("当前问题已被删除！");
        }
    }

    @Override
    public List<ForumQuestionListVo> list(Page page) {
        return this.listByMultiCons(page, null, null, null);
    }

    @Override
    public List<ForumQuestionListVo> listByTitle(String title, Page page) {
        return this.listByMultiCons(page, null, null, title);
    }

    @Override
    public List<ForumQuestionListVo> listByTopicId(Integer topicId, Page page) {
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();
        List<ForumQuestionListVo> voForumQuestionLists = new ArrayList<ForumQuestionListVo>();
        // 获取板块问题列表.
        Example exmTopicQuestion = new Example(TopicQuestion.class);
        Criteria criTopicQuestion = exmTopicQuestion.createCriteria();
        criTopicQuestion.andEqualTo("topicId", topicId).andEqualTo("isDelete", Is.NO.getCode());
        List<TopicQuestion> topicQuestions = topicQuestionDao.selectByExample(exmTopicQuestion);
        List<Integer> questionIds = new ArrayList<Integer>();
        for (TopicQuestion topicQuestion : topicQuestions) {
            questionIds.add(topicQuestion.getQuestionId());
        }
        if (CollectionUtils.isEmpty(questionIds)) {
            return voForumQuestionLists;
        }

        // 查询问题列表.
        Example exmForumQuestion = new Example(ForumQuestion.class);
        exmForumQuestion.setOrderByClause("create_time DESC");
        Criteria criForumQuestion = exmForumQuestion.createCriteria();
        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode()).andIn("questionId", questionIds);
        ForumQuestion forumQuestionForSort = forumQuestionDao.selectByPrimaryKey(page.getLastId());
        if (page.getLastId() != -1) {
            if (forumQuestionForSort != null) {
                criForumQuestion.andLessThan("createTime", forumQuestionForSort.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExampleAndRowBounds(exmForumQuestion, rowBounds);
        Iterator<ForumQuestion> iForumQuestion = forumQuestions.iterator();
        while (iForumQuestion.hasNext()) {
            ForumQuestion forumQuestion = iForumQuestion.next();
            Integer questionId = forumQuestion.getQuestionId();
            ForumQuestionListVo voForumQuestionList = DtoEntityVoUtils.entity2vo(forumQuestion, ForumQuestionListVo.class);
            voForumQuestionList.setImageArr(BizUtils.getArrImages(forumQuestion.getImages()));
            voForumQuestionList.setImageArr(BizUtils.getArrImages(forumQuestion.getImages()));

            Is isAnonymous = Is.parse(forumQuestion.getIsAnonymous());
            if (Is.YES.equals(isAnonymous) && !currentUserId.equals(forumQuestion.getUserId())) {
                voForumQuestionList.setUserId("");
                voForumQuestionList.setUserName("匿名");
                voForumQuestionList.setUserPhoto("");
                voForumQuestionList.setCompany("未公开");
            } else {
                voForumQuestionList.setUserId(forumQuestion.getUserId() == null ? "" : forumQuestion.getUserId().toString());
                voForumQuestionList.setUserName(forumQuestion.getUserName());
                voForumQuestionList.setUserPhoto(forumQuestion.getUserPhoto());
                voForumQuestionList.setCompany(forumQuestion.getCompanyName());
            }

            Set<Integer> setQuestionReplys = RedisCacheUtils.getForSet(RedisConstants.QUESTION_REPLY + questionId);
            voForumQuestionList.setReplyNum(CollectionUtils.isEmpty(setQuestionReplys) ? 0 : setQuestionReplys.size());
            Set<Integer> setLikeQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.LIKE_QUESTION_USER + questionId);
            voForumQuestionList.setIsLike(BizUtils.contains(setLikeQuestionUsers, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
            voForumQuestionList.setLikeNum(CollectionUtils.isEmpty(setLikeQuestionUsers) ? 0 : setLikeQuestionUsers.size());
            voForumQuestionList.setHasNewReply(null);
            Set<Integer> setConcernUserUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + currentUserId);
            voForumQuestionList.setIsConcern(BizUtils.contains(setConcernUserUsers, forumQuestion.getUserId()) ? Is.YES.getCode() : Is.NO.getCode());
            this.setTopics(voForumQuestionList, questionId, topicId);
            Set<Integer> topicQuestionsRedis = RedisCacheUtils.getForSet(RedisConstants.TOPIC_QUESTION + topicId);
            voForumQuestionList.setQuestionNum(topicQuestionsRedis == null ? 0 : topicQuestionsRedis.size());
            voForumQuestionLists.add(voForumQuestionList);
        }
        return voForumQuestionLists;
    }

    @Override
    public List<ForumQuestionListVo> listByUserId(Integer userId, Page page) {
        return this.listByMultiCons(page, userId, null, null);
    }

    /**
     * 
     * @param page
     * @param userId
     * @param topicId
     * @param title
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月2日 下午6:03:02
     */
    private List<ForumQuestionListVo> listByMultiCons(Page page, Integer userId, Integer topicId, String title) {
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();
        List<ForumQuestionListVo> voForumQuestionLists = new ArrayList<ForumQuestionListVo>();
        Example example = new Example(ForumQuestion.class);
        example.setOrderByClause("create_time DESC");
        Criteria criteria = example.createCriteria();
        if (userId != null) {
            criteria.andEqualTo("userId", userId);
            if (!userId.equals(currentUserId)) {
                criteria.andEqualTo("isAnonymous", Is.NO.getCode());
            }
        } else if (!StringUtils.isBlank(title)) {
            criteria.andLike("title", title);
        } else if (userId == null && topicId == null && title == null) {
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
        criteria.andLike("isDelete", Is.NO.getCode());
        ForumQuestion forumQuestionForSort = forumQuestionDao.selectByPrimaryKey(page.getLastId());
        if (page.getLastId() != -1) {
            if (forumQuestionForSort != null) {
                criteria.andLessThan("createTime", forumQuestionForSort.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExampleAndRowBounds(example, rowBounds);
        Iterator<ForumQuestion> iForumQuestion = forumQuestions.iterator();
        while (iForumQuestion.hasNext()) {
            ForumQuestion forumQuestion = iForumQuestion.next();
            Integer questionId = forumQuestion.getQuestionId();
            ForumQuestionListVo voForumQuestionList = DtoEntityVoUtils.entity2vo(forumQuestion, ForumQuestionListVo.class);
            voForumQuestionList.setImageArr(BizUtils.getArrImages(forumQuestion.getImages()));
            voForumQuestionList.setImageArr(BizUtils.getArrImages(forumQuestion.getImages()));

            Is isAnonymous = Is.parse(forumQuestion.getIsAnonymous());
            if (Is.YES.equals(isAnonymous) && !currentUserId.equals(forumQuestion.getUserId())) {
                voForumQuestionList.setUserId("");
                voForumQuestionList.setUserName("匿名");
                voForumQuestionList.setUserPhoto("");
                voForumQuestionList.setCompany("未公开");
            } else {
                voForumQuestionList.setUserId(forumQuestion.getUserId() == null ? "" : forumQuestion.getUserId().toString());
                voForumQuestionList.setUserName(forumQuestion.getUserName());
                voForumQuestionList.setUserPhoto(forumQuestion.getUserPhoto());
                voForumQuestionList.setCompany(forumQuestion.getCompanyName());
            }
            Set<Integer> setQuestionReplys = RedisCacheUtils.getForSet(RedisConstants.QUESTION_REPLY + questionId);
            voForumQuestionList.setReplyNum(CollectionUtils.isEmpty(setQuestionReplys) ? 0 : setQuestionReplys.size());
            Set<Integer> setLikeQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.LIKE_QUESTION_USER + questionId);
            voForumQuestionList.setIsLike(BizUtils.contains(setLikeQuestionUsers, currentUserId) ? Is.YES.getCode() : Is.NO.getCode());
            voForumQuestionList.setLikeNum(CollectionUtils.isEmpty(setLikeQuestionUsers) ? 0 : setLikeQuestionUsers.size());
            voForumQuestionList.setHasNewReply(null);
            Set<Integer> setConcernUserUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + currentUserId);
            voForumQuestionList.setIsConcern(BizUtils.contains(setConcernUserUsers, forumQuestion.getUserId()) ? Is.YES.getCode() : Is.NO.getCode());
            this.setTopics(voForumQuestionList, questionId, topicId);
            voForumQuestionList.setCreateTime(forumQuestion.getCreateTime() == null ? "" : String.valueOf(forumQuestion.getCreateTime().getTime()));
            Set<Integer> collectionQuestionUsers = RedisCacheUtils.getForSet(RedisConstants.COLLECTION_QUESTION_USER + forumQuestion.getQuestionId());
            voForumQuestionList.setCollectNum(CollectionUtils.isEmpty(collectionQuestionUsers) ? 0 : collectionQuestionUsers.size());

            // 如果有回答，则显示最新的回答。
            Example exampleForReply = new Example(ForumReply.class);
            exampleForReply.setOrderByClause("create_time DESC");
            Criteria criteriaForReply = exampleForReply.createCriteria();
            criteriaForReply.andEqualTo("isDelete", Is.NO.getCode()).andEqualTo("questionId", questionId);
            List<ForumReply> forumReplys = forumReplyDao.selectByExample(exampleForReply);
            if (!CollectionUtils.isEmpty(forumReplys)) {
                ForumReply lastForumReply = forumReplys.get(0);
                voForumQuestionList.setContent(lastForumReply.getContent());
                voForumQuestionList.setReplyId(lastForumReply.getReplyId().toString());
            }
            voForumQuestionLists.add(voForumQuestionList);
        }
        return voForumQuestionLists;
    }

    /**
     * <p>后来添加的，最多两条.
     * @param voForumQuestionList
     * @param questionId
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午6:09:24
     */
    private void setTopics(ForumQuestionListVo voForumQuestionList, Integer questionId, Integer topicId) {
        List<ForumTopic> forumTopics = forumTopicDao.selectByQuestionId(questionId);
        List<String> topics = new ArrayList<String>();
        if (topicId != null) {
            ForumTopic forumTopic = forumTopicDao.selectByPrimaryKey(topicId);
            topics.add(forumTopic.getTopicName());
            for (int i = 0; i < forumTopics.size() && i < 1 && !StringUtils.equals(forumTopics.get(i).getTopicName(), forumTopic.getTopicName()); i++) {
                topics.add(forumTopics.get(i).getTopicName());
            }
        } else {
            for (int i = 0; i < forumTopics.size() && i < 2; i++) {
                topics.add(forumTopics.get(i).getTopicName());
            }
        }
        voForumQuestionList.setTopics(topics);
    }
}