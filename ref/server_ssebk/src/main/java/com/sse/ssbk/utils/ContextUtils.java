package com.sse.ssbk.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 系统山下文工具.
 * @author  HT-LiChuanbin 
 * @version 2017年7月24日 下午1:55:02
 */
public class ContextUtils {
	/**
	 * 获取当前的HttpServletRequest.
	 * @return
	 * @author  HT-LiChuanbin 
	 * @version 2017年7月24日 下午1:56:36
	 */
	public static HttpServletRequest getCurrentRequest() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		return sra.getRequest();
	}
}