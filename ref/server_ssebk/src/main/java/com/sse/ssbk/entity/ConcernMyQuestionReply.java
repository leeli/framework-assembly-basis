package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Data;

/**
 * 查询我关注的问题和回答列表接口VO
 * @author  HT-LiChuanbin 
 * @version 2017年8月3日 下午1:59:03
 */
@Data
@Entity
public class ConcernMyQuestionReply {
    @Column(name = "question_id")
    private Integer questionId;
    @Column(name = "is_anonymous")
    private String isAnonymous;
    @Column(name = "question_title")
    private String questionTitle;
    @Column(name = "question_content")
    private String questionContent;
    @Column(name = "reply_id")
    private Integer replyId;
    @Column(name = "reply_content")
    private String replyContent;
    @Column(name = "images")
    private String images;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "source")
    private String source;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_photo")
    private String userPhoto;
    @Column(name = "company_name")
    private String company;
}