package com.sse.ssbk.service;

import com.sse.ssbk.vo.UpgradeVo;

public interface UpgradeService {
    /**
     * 
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月18日 下午2:39:11
     */
    UpgradeVo getLastestUpgradeInfo();
}