package com.sse.ssbk.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 关注枚举.
 * @author  HT-LiChuanbin 
 * @version 2017年7月26日 下午8:35:41
 */
public enum ConcernTargetType {
    USER, TOPIC;
    public static ConcernTargetType parse(String targetType) {
        if (StringUtils.isBlank(targetType)) {
            return null;
        }
        targetType = targetType.trim();
        for (ConcernTargetType type : ConcernTargetType.values()) {
            if (StringUtils.equalsIgnoreCase(type.toString(), targetType)) {
                return type;
            }
        }
        return null;
    }
}