package com.sse.ssbk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by ciyua on 2017/8/2.
 */
@Data
@Entity
@Table(name = "t_sdk_appsdkrelation")
@NoArgsConstructor
@AllArgsConstructor
public class AppSdkRelation implements Serializable {
    private String appId;
    private String securityKey;
    private Date expireDate;
    private Date createDate;
    private String deviceType;
    private String androidSignature;
    private String bundleId;
    private String sdkId;
}
