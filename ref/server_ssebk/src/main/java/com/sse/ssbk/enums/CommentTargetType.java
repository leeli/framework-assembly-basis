package com.sse.ssbk.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 评论枚举.
 * @author  HT-LiChuanbin 
 * @version 2017年7月26日 下午8:35:41
 */
public enum CommentTargetType {
    REPLY, REPLYCOMMENT, ACT, NEWS;
    public static CommentTargetType parse(String targetType) {
        if (StringUtils.isBlank(targetType)) {
            return null;
        }
        targetType = targetType.trim();
        for (CommentTargetType type : CommentTargetType.values()) {
            if (StringUtils.equalsIgnoreCase(type.toString(), targetType)) {
                return type;
            }
        }
        return null;
    }
}