package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by work_pc on 2017/8/7.
 */

@Data
@Entity
@Table(name = "t_im_friend")
public class IMFriend {
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "friend_user_id")
    private Integer friendUserId;
    @Column(name = "create_time")
    private Date createTime;
}
