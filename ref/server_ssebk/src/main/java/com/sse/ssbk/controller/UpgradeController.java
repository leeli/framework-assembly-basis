package com.sse.ssbk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sse.ssbk.service.UpgradeService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.UpgradeVo;

@RestController
@RequestMapping("/ssebk")
public class UpgradeController {
    @Autowired
    private UpgradeService upgradeService;

    /**
     * 
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月18日 下午2:38:04
     */
    @GetMapping("/upgrade")
    public Result<UpgradeVo> upgrade() {
        UpgradeVo voUpgrade = upgradeService.getLastestUpgradeInfo();
        return ResultUtils.success(voUpgrade);
    }
}