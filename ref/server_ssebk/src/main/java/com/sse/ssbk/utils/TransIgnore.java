/**
 * 
 */
package com.sse.ssbk.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标识在进行dto-entity转换时忽略的字段.
 * 使用方法：在需要忽略的字段上加上@TransIgnore注解，则此字段不会在进行dto-entity转换时进行转换.
 * 此注解用在entity端.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:53:14
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransIgnore {
}