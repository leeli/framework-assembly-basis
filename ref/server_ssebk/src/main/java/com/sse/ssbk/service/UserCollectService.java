package com.sse.ssbk.service;

import com.sse.ssbk.entity.*;
import com.sse.ssbk.vo.ActivityCitySubjectVo;
import com.sse.ssbk.vo.CollectNewsVo;
import com.sse.ssbk.vo.CollectQuestionVo;
import com.sse.ssbk.vo.CollectReplyVo;

import java.util.List;

/**
 * Created by work_pc on 2017/7/31.
 */
public interface UserCollectService {
    /**
     * 获取我收藏的问题
     * @return
     */
    List<CollectQuestionVo> getMyCollectQuestion(String lastQuestionId, String pageSize);

    /**
     * 获取我收藏的回答
     * @return
     */
    List<CollectReplyVo> getMyCollectReply(String lastReply, String pageSize);

    /**
     * 获取我收藏的活动
     * @return
     */
    List<ActivityCitySubjectVo> getMyCollectActivity(String lastActId, String pageSize);

    /**
     * 获取我收藏的资讯
     * @return
     */
    List<CollectNewsVo> getMyCollectNews(String lastNewsId, String pageSize);

}
