package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.MultiTableUpdate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by work_pc on 2017/8/16.
 */

public interface MultiTableUpdateDao extends BaseDao<MultiTableUpdate>{

    void updatePhoto(@Param("userPhoto") String userPhoto, @Param("userId")Integer userId, @Param("table")String table);

    void updateCompanyName(@Param("companyName")String companyName,@Param("userId")Integer userId, @Param("table")String table);

    void updateCompanyId(@Param("companyId")String companyId,@Param("userId")Integer userId, @Param("table")String table);

}
