package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_reply_comment")
public class ReplyComment {
    @Id
    @Column(name = "reply_comment_id")
    private Integer replyCommentId;
    @Column(name = "parent_comment_id")
    private Integer parentCommentId;
    @Column(name = "top_comment_id")
    private Integer topCommentId;
    @Column(name = "reply_id")
    private Integer replyId;
    @Column(name = "content")
    private String content;
    @Column(name = "is_anonymous")
    private String isAnonymous;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_photo")
    private String userPhoto;
    @Column(name = "to_user_id")
    private Integer toUserId;
    @Column(name = "to_user_name")
    private String toUserName;
    @Column(name = "company_id")
    private Integer companyId;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "is_deleted")
    private Integer isDeleted;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
    @Column(name = "relation")
    private String relation;
}