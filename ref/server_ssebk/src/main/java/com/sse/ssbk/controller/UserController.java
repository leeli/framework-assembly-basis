package com.sse.ssbk.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.sse.ssbk.dto.*;
import com.sse.ssbk.entity.UserInfo;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.service.*;
import com.sse.ssbk.utils.*;
import com.sse.ssbk.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.entity.CompanyInfo;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;

/**
 * Created by yxf on 2017/7/7.
 */

@Controller
@RequestMapping("/ssebk")
@Slf4j
public class UserController extends BaseController {

    @Autowired
    private UserConcernService userConcernService;

    @Autowired
    private UserService userService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private ActService actService;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private CompanyInfoService companyInfoService;

    @Autowired
    private UserCollectService userCollectService;

    @Autowired
    private EnrollActService enrollActService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private IMFriendService imFriendService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private NoticeService noticeService;

    /**
     * 我关注的人
     * @return
     */
    @GetMapping(value = "user/concern/users")
    @ResponseBody
    public Result<?> getIConcernUserList(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String userId = jData.getString("userId");
        String lastUserId = jData.getString("lastUserId");
        String pageSize = jData.getString("pageSize");

        if (userId == null || lastUserId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }
        List<UserConcernVo> iConcernedUserList = userConcernService.getIConcernedUserList(userId, lastUserId, pageSize);
        Map<String, Object> dataSource = new HashMap<>();
        if (iConcernedUserList != null && iConcernedUserList.size() != 0) {
            dataSource.put("lastUserId", String.valueOf(iConcernedUserList.get(iConcernedUserList.size() - 1).getUserId()));
        } else {
            dataSource.put("lastUserId", lastUserId);
        }
        dataSource.put("usersList", iConcernedUserList);
        return ResultUtils.success("我关注的人", dataSource);
    }

    /**
     * 关注我的人
     * @return
     */
    @GetMapping(value = "concern/user")
    @ResponseBody
    public Result<?> getUserConcernMeList(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String userId = jData.getString("userId");
        String lastUserId = jData.getString("lastUserId");
        String pageSize = jData.getString("pageSize");

        if (userId == null || lastUserId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }
        List<UserConcernVo> userConcernedMeList = userConcernService.getUserConcernedMeList(userId, lastUserId, pageSize);
        Map<String, Object> dataSource = new HashMap<>();
        if (userConcernedMeList != null && userConcernedMeList.size() != 0) {
            dataSource.put("lastUserId", String.valueOf(userConcernedMeList.get(userConcernedMeList.size() - 1).getUserId()));
        } else {
            dataSource.put("lastUserId", lastUserId);
        }
        dataSource.put("usersList", userConcernedMeList);
        return ResultUtils.success("关注我的人", dataSource);
    }

    /**
     * 我关注的板块
     * @return
     */
    @GetMapping(value = "user/concern/topics")
    @ResponseBody
    public Result<?> getIConcernTopicList(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String userId = jData.getString("userId");
        String lastTopicId = jData.getString("lastTopicId");
        String pageSize = jData.getString("pageSize");

        if (userId == null || lastTopicId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }
        List<ForumTopicVo> iconcernedTopicList = userConcernService.getIconcernedTopicList(userId, lastTopicId, pageSize);
        Map<String, Object> dataSource = new HashMap<>();
        if (iconcernedTopicList != null && iconcernedTopicList.size() != 0) {
            dataSource.put("lastTopicId", String.valueOf(iconcernedTopicList.get(iconcernedTopicList.size() - 1).getTopicId()));
        } else {
            dataSource.put("lastTopicId", lastTopicId);
        }
        dataSource.put("topicList", iconcernedTopicList);
        return ResultUtils.success("我关注的板块", dataSource);
    }

    @GetMapping(value = "user/home")
    @ResponseBody
    public Result<?> home(String data) {
        UserHomeVo userHomeVo;
        if (data == null) {
            userHomeVo = userService.getHomePage(null);
        } else {
            JSONObject jData = (JSONObject) JSON.parse(data);
            String userId = jData.getString("userId"); //可传可不传 不传是当前用户的
            userHomeVo = userService.getHomePage(userId);
        }
        //        data = URLDecoder.decode(data,"UTF8");
        return ResultUtils.success("用户主页", userHomeVo);
    }

    /**
     * 我的资讯列表
     * @return
     */
    @GetMapping(value = "my/news")
    @ResponseBody
    public Result<?> getMyNewsList(String data) throws UnsupportedEncodingException {

        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //        data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastNewsId = jData.getString("lastNewsId");
        String pageSize = jData.getString("pageSize");
        String status = jData.getString("status");
        String isMyPublish = jData.getString("isMyPublish");

        if (lastNewsId == null || pageSize == null || status == null || isMyPublish == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        List<NewsVoList> myNewsList = userService.getMyNewsList(lastNewsId, pageSize, status, isMyPublish);
        Map<String, Object> dataScource = new HashMap<>();

        if (myNewsList == null || myNewsList.size() == 0) {
            dataScource.put("lastNewsId", lastNewsId);
        } else {
            dataScource.put("lastNewsId", String.valueOf(myNewsList.get(myNewsList.size() - 1).getNewsId()));
        }
        //        List<NewsVoList> newsVoList = new ArrayList<>();

        dataScource.put("newsList", myNewsList);
        return ResultUtils.success("我的资讯列表", dataScource);
    }

    /**
     * 审核某条资讯
     * @param newsId
     * @param auditNewsDto
     * @return
     */
    @PutMapping(value = "my/news/{newsId}")
    @ResponseBody
    public Result<?> auditNews(@PathVariable("newsId") Integer newsId, @RequestBody AuditNewsDto auditNewsDto) {
        if (newsService.auditNews(newsId, auditNewsDto) == 1) {
            return ResultUtils.success();
        }
        return ResultUtils.failure("出错了");
    }

    /**
     * 登录
     * @param loginDto
     * @return
     */
    @PostMapping(value = "/login/user")
    @ResponseBody
    @CountScore(ScoreRules.LOGIN)
    public Result<?> login(@RequestBody @Valid LoginDto loginDto) {
        String loginName = loginDto.getLoginName();
        String password = loginDto.getPassword();
        password = Util.encodeBySHA1(password);
        UserVo userVo = userService.login(loginName, password);

        return ResultUtils.success("登录", userVo);
    }

    /**
     * 注册
     * @param data
     * @return
     * @throws UnsupportedEncodingException
     */
    @PostMapping(value = "register/user")
    @ResponseBody
    public Result<?> register(@RequestBody @Valid RegisterDto registerDto) {
        String userName = registerDto.getUserName();
        String smsCaptcha = registerDto.getSmsCaptcha();
        String password = registerDto.getPassword();
        password = Util.encodeBySHA1(password);
        userService.register(userName, password, smsCaptcha);

        return ResultUtils.success("注册");
    }

    /**
     * 登出
     * @param data
     * @return
     * @throws UnsupportedEncodingException
     */
    @PostMapping(value = "logout/user")
    @ResponseBody
    public Result<?> logout() {
        String token = BizUtils.getCurrentToken();
        RedisCacheUtils.del(token);
        // userService.logout(token);
        return ResultUtils.success("登出");
    }

    /**
     * 图形验证码
     * @return
     */
    @GetMapping(value = "piccaptcha")
    @ResponseBody
    public Result<?> getPicCaptcha() {
        PicCaptchaVo picCaptchaVo = captchaService.getPicCaptcha();
        return ResultUtils.success("获取验证码", picCaptchaVo);
    }

    /**
     * 发送手机验证码
     * @param smsCaptchaDto
     * @return
     */
    @PostMapping(value = "smscaptcha")
    @ResponseBody
    public Result<?> smsCaptcha(@RequestBody @Valid SmsCaptchaDto smsCaptchaDto) {
        SmsCaptchaVo smsCaptchaVo = new SmsCaptchaVo();

        String isPhoneExist = captchaService.smsCaptcha(smsCaptchaDto);
//        String isPhoneExist = userService.userPhoneIsExists(smsCaptchaDto.getUserName());
        smsCaptchaVo.setIsPhoneExist(isPhoneExist);
        return ResultUtils.success("获取验证码", smsCaptchaVo);
    }

    @GetMapping(value = "user/activitys")
    @ResponseBody
    public Result<?> getUserActivity(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastActId = jData.getString("lastActId");
        String pageSize = jData.getString("pageSize");

        if (lastActId == null || pageSize == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        Map<String, Object> dataSource = new HashMap<>();
        List<UserActivityCitySubjectVo> cityOrSubjectActivity = actService.getMyActivity(lastActId, pageSize);
        if (cityOrSubjectActivity != null && cityOrSubjectActivity.size() != 0) {
            lastActId = cityOrSubjectActivity.get(cityOrSubjectActivity.size() - 1).getActId();
        }
        dataSource.put("lastActId", lastActId);
        dataSource.put("actList", cityOrSubjectActivity);
        return ResultUtils.success("我的活动列表", dataSource);
    }

    @GetMapping(value = "user/enrolls")
    @ResponseBody
    public Result<?> getIJoinActivity(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastActId = jData.getString("lastActId");
        String pageSize = jData.getString("pageSize");
        String enrollStatus = jData.getString("enrollStatus");
        String userId = jData.getString("userId");

        if (lastActId == null || pageSize == null || userId == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        Map<String, Object> dataSource = new HashMap<>();
        List<UserActivityCitySubjectVo> cityOrSubjectActivity = actService.getIJoinActivity(lastActId, userId, pageSize, enrollStatus);
        if (cityOrSubjectActivity != null && cityOrSubjectActivity.size() != 0) {
            lastActId = cityOrSubjectActivity.get(cityOrSubjectActivity.size() - 1).getActId();
        }
        dataSource.put("lastActId", lastActId);
        dataSource.put("actList", cityOrSubjectActivity);
        return ResultUtils.success("我参与的活动列表", dataSource);
    }

    @GetMapping(value = "user/info")
    @ResponseBody
    public Result<?> getUserInfo() {
        UserInfoVo userInfoVo = userInfoService.getUserInfoByUserId();
        return ResultUtils.success("用户信息", userInfoVo);
    }

    @PutMapping(value = "user/info")
    @ResponseBody
    public Result<?> updateUserInfo(@RequestBody @Valid UserInfoDto userInfoDto) {
        int result = userInfoService.updateUserInfo(userInfoDto);
        if (result == 1) {
            return ResultUtils.success("修改成功");
        } else {
            return ResultUtils.failure(AppReturnCode.UndefinedError);
        }
    }

    @PostMapping(value = "user/financial")
    @ResponseBody
    public Result<?> addCompanyInfo(@RequestBody CompanyInfoDto companyInfoDto) {
        companyInfoService.addCompanyInfo(companyInfoDto);
        return ResultUtils.success("添加成功");
    }

    @PutMapping(value = "user/financial")
    @ResponseBody
    public Result<?> updateCompanyInfo(@RequestBody CompanyInfoDto companyInfoDto) {
        CompanyInfo companyInfo = null;
        UserSession currentUser = BizUtils.getCurrentUser();
        User user = userService.getUserById(currentUser.getUserId());
        if (user == null || user.getCompanyId() == null || "".equals(user.getCompanyId())) {
            throw new SSEBKRuntimeException(AppReturnCode.UndefinedError);
        }
        if (companyInfoService.updateCompanyInfo(companyInfoDto, Integer.valueOf(user.getCompanyId())) == 1) {
            return ResultUtils.success("修改成功");
        } else {
            return ResultUtils.failure(AppReturnCode.UndefinedError);
        }
    }

    @GetMapping(value = "user/financial")
    @ResponseBody
    public Result<?> getCompanyInfobyId() {
        CompanyInfo companyInfo = null;
        UserSession currentUser = BizUtils.getCurrentUser();

        User user = userService.getUserById(currentUser.getUserId());
        if (user == null || user.getCompanyId() == null || "".equals(user.getCompanyId())) {
            return ResultUtils.success(companyInfo);
        }
        companyInfo = companyInfoService.selectCompanyInfoById(Integer.valueOf(user.getCompanyId()));
        return ResultUtils.success(companyInfo);
    }

    @GetMapping(value = "my/collection/questions")
    @ResponseBody
    public Result<?> getMyCollectQuestion(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jsonObject = JSON.parseObject(data);
        String lastQuestionId = jsonObject.getString("lastQuestionId");
        String pageSize = jsonObject.getString("pageSize");

        if (lastQuestionId == null || pageSize == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }

        List<CollectQuestionVo> myCollectQuestions = userCollectService.getMyCollectQuestion(lastQuestionId, pageSize);
        if (myCollectQuestions != null && myCollectQuestions.size() != 0) {
            lastQuestionId = myCollectQuestions.get(myCollectQuestions.size() - 1).getQuestionId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastQuesionId", lastQuestionId);
        dataSource.put("questionList", myCollectQuestions);

        return ResultUtils.success("我收藏的问题列表", dataSource);
    }

    @GetMapping(value = "my/collection/replys")
    @ResponseBody
    public Result<?> getMyCollectReply(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jsonObject = JSON.parseObject(data);
        String lastReplyId = jsonObject.getString("lastReplyId");
        String pageSize = jsonObject.getString("pageSize");

        if (lastReplyId == null || pageSize == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }

        List<CollectReplyVo> myCollectReplys = userCollectService.getMyCollectReply(lastReplyId, pageSize);
        if (myCollectReplys != null && myCollectReplys.size() != 0) {
            lastReplyId = myCollectReplys.get(myCollectReplys.size() - 1).getReplyId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastReplyId", lastReplyId);
        dataSource.put("replyList", myCollectReplys);

        return ResultUtils.success("我收藏的回答列表", dataSource);
    }

    @GetMapping(value = "my/collection/activitys")
    @ResponseBody
    public Result<?> getMyCollectActivity(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jsonObject = JSON.parseObject(data);
        String lastActId = jsonObject.getString("lastActId");
        String pageSize = jsonObject.getString("pageSize");

        if (lastActId == null || pageSize == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        List<ActivityCitySubjectVo> myCollectActivity = userCollectService.getMyCollectActivity(lastActId, pageSize);
        if (myCollectActivity != null && myCollectActivity.size() != 0) {
            lastActId = myCollectActivity.get(myCollectActivity.size() - 1).getActId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastActId", lastActId);
        dataSource.put("actList", myCollectActivity);
        return ResultUtils.success("我收藏的活动列表", dataSource);
    }

    @GetMapping(value = "my/collection/news")
    @ResponseBody
    public Result<?> getMyCollectNews(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jsonObject = JSON.parseObject(data);
        String lastNewsId = jsonObject.getString("lastNewsId");
        String pageSize = jsonObject.getString("pageSize");

        if (lastNewsId == null || pageSize == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        List<CollectNewsVo> myCollectNews = userCollectService.getMyCollectNews(lastNewsId, pageSize);
        if (myCollectNews != null && myCollectNews.size() != 0) {
            lastNewsId = myCollectNews.get(myCollectNews.size() - 1).getNewsId();
        }
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("lastNewsId", lastNewsId);
        dataSource.put("newsList", myCollectNews);
        return ResultUtils.success("我收藏的资讯列表", dataSource);
    }

    @GetMapping(value = "user/requireinfo")
    @ResponseBody
    @Deprecated
    /**
     * 接口文档中未找到  ：可能是个人必填信息是否完善接口
     */
    public Result<?> requireInfo() {
        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("isExist", userService.requireInfo());
        return ResultUtils.success(dataSource);
    }

    @GetMapping(value = "user")
    @ResponseBody
    public Result<?> userPhoneIsExists(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jsonObject = JSON.parseObject(data);
        String loginName = jsonObject.getString("loginName");
        if (loginName == null) {
            return ResultUtils.failure(AppReturnCode.ParaReqMiss);
        }

        Map<String, Object> dataSource = new HashMap<>();
        dataSource.put("isExist", userService.userPhoneIsExists(loginName));
        return ResultUtils.success(dataSource);
    }

    /**
     * 审核报名人
     * @return
     */
    @PutMapping(value = "my/enroll/{actId}")
    @ResponseBody
    @CountScore(ScoreRules.AUDIT_ACTIVITY_JOINER)
    @SendNotice
    public Result<?> auditActivityJoiner(@PathVariable("actId") Integer actId, @RequestBody AuditActivityJoinerDto auditActivityJoinerDto) {
        if (enrollActService.auditActivityJoiner(actId, auditActivityJoinerDto) != 0) {
            return ResultUtils.success();
        }
        throw new SSEBKRuntimeException(AppReturnCode.UndefinedError);
    }

    @PostMapping(value = "invitationcode")
    @ResponseBody
    public Result<?> addInvitation(@RequestBody InvitationDto invitationDto) {
        if (invitationService.addInvitation(invitationDto) == 1) {
            return ResultUtils.success();
        } else {
            return ResultUtils.failure(AppReturnCode.UndefinedError);
        }
    }

    @GetMapping(value = "invitationcode")
    @ResponseBody
    public Result<?> validateInvitation(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        JSONObject jsonObject = JSON.parseObject(data);
        String invitationCode = jsonObject.getString("invitationCode");
        String realName = jsonObject.getString("realName");
        String loginName = jsonObject.getString("loginName");
        String password = jsonObject.getString("password");

        if (invitationCode == null || realName == null || loginName == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        Map<String, Object> dataSource = new HashMap<>();
        String isMatch = invitationService.validateInvitation(invitationCode, realName, loginName);
        UserVo userVo;
        //邀请码验证成功
        if ("1".equals(isMatch)){
            User user = new User();
            user.setUserName(loginName);
            user.setRealName(realName);
            user.setStatus("1");
            userService.validateSuccessInsertUser(user);

            UserInfo userInfo = new UserInfo();
            userInfo.setUserId(user.getUserId());
            userInfo.setRealName(user.getRealName());
            userInfoService.insertSelective(userInfo);
            System.out.println(user);
            System.out.println(user.getUserId());
            password = Util.encodeBySHA1(password);
            userVo = userService.login(loginName, password);
            return ResultUtils.success(userVo);
        }
        return ResultUtils.failure(AppReturnCode.NoSuchInvitationCodeOrName);
//        dataSource.put("isMatch", isMatch);
//        return ResultUtils.success(dataSource);
    }

    @PostMapping(value = "im/friend/req")
    @ResponseBody
    public Result<?> requestAddFriend(@RequestBody @Valid RequestFriendDto requestFriendDto) {
        imFriendService.requesTAddFrined(requestFriendDto);
        return ResultUtils.success();
    }

    @PostMapping(value = "im/friend")
    @ResponseBody
    public Result<?> agreeAddFriend(@RequestBody @Valid AgreeFriendDto agreeFriendDto) {
        if (imFriendService.agressAddFrined(agreeFriendDto) == 1) {
            return ResultUtils.success();
        } else {
            return ResultUtils.failure(AppReturnCode.UndefinedError);
        }
    }

    @GetMapping(value = "im/friend/find")
    @ResponseBody
    public Result<?> searchFriend(String data) {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jsonObject = JSON.parseObject(data);
        String searchKeys = jsonObject.getString("searchKeys");

        if (searchKeys == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }

        List<IMFriendVo> imFriendVoList = imFriendService.searchFriend(searchKeys);

        if (imFriendVoList == null || imFriendVoList.size() == 0) {
            throw new SSEBKRuntimeException(AppReturnCode.NoSuchUser);
        }
        return ResultUtils.success(imFriendVoList);
    }

    /**
     * 好友请求列表
     * @return
     */
    @GetMapping(value = "im/req/friends")
    @ResponseBody
    public Result<?> reqFriends() {

        List<IMReqFriendsVo> imReqFriendsVos = imFriendService.reqFriends();

        return ResultUtils.success(imReqFriendsVos);
    }

    /**
     * 好友列表
     * @return
     */
    @GetMapping(value = "im/friends")
    @ResponseBody
    public Result<?> contactList() {
        return ResultUtils.success(imFriendService.contactList());
    }

    /**
     * 修改密码
     * @param changePswDto
     * @return
     */
    @PostMapping(value = "user/changepassword")
    @ResponseBody
    public Result<?> changePsw(@RequestBody @Valid ChangePswDto changePswDto) {
        String oriPswd = changePswDto.getOldPwd();
        String newPswd = changePswDto.getNewPwd();
        oriPswd = Util.encodeBySHA1(oriPswd);
        newPswd = Util.encodeBySHA1(newPswd);
        userService.changePsw(oriPswd, newPswd);
        return ResultUtils.success();
    }

    /**
     * 忘记密码
     * @param forgetPswDto
     * @return
     */
    @PostMapping(value = "user/forgotpassword")
    @ResponseBody
    public Result<?> forgetPsw(@RequestBody @Valid ForgetPswDto forgetPswDto) {
        String username = forgetPswDto.getPhone();
        String code = forgetPswDto.getCode();
        String newPswd = forgetPswDto.getNewPwd();
        newPswd = Util.encodeBySHA1(newPswd);
        userService.forgetPsw(username, code, newPswd);
        return ResultUtils.success();
    }

    /**
     * 是否完善个人信息
     * @return
     */
    @GetMapping(value = "user/initinfo")
    @ResponseBody
    public Result<?> isCompleteFinancial(){

        UserSession userInfo = BizUtils.getCurrentUser();

        String isCompleteRequiredUserInfo = userService.requireInfo();

        Map<String,Object> dataSource = new HashMap<>();
        dataSource.put("isCompleteRequiredUserInfo",isCompleteRequiredUserInfo);
        return ResultUtils.success(dataSource);
    }

    @GetMapping(value = "my/notices")
    @ResponseBody
    public Result<?> myNotices(String data){
        if (data == null){
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jObject = (JSONObject)JSON.parse(data);
        String isSystemNotice = jObject.getString("isSystemNotice");
        String lastId = jObject.getString("lastId");
        String pageSize = jObject.getString("pageSize");

        if (isSystemNotice == null){
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        BizUtils.Page page = BizUtils.getBizPage(lastId,pageSize);

        return ResultUtils.success(noticeService.list(isSystemNotice,page));
    }

    @DeleteMapping(value = "my/notices")
    @ResponseBody
    public Result<?> deleteNotices(@RequestBody DeleteNoticeDto deleteNoticeDto){
        noticeService.delete(deleteNoticeDto);
        return ResultUtils.success();
    }


    @GetMapping(value = "user/scoreindex")
    @ResponseBody
    public Result<?> scoreindex(String data){
        if (data == null){
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        JSONObject jsonObject = (JSONObject) JSON.parse(data);
        String userId = jsonObject.getString("userId");
        if (userId == null){
            throw new SSEBKRuntimeException(AppReturnCode.ParaReqMiss);
        }
        ScoreIndexVo scoreIndexVo = userService.scoreIndex(userId);
        return ResultUtils.success(scoreIndexVo);
    }

    @GetMapping(value = "my/notice")
    @ResponseBody
    public Result<?> hasNewNotices(){
        Map<String,Object> dataSource = new HashMap<>();
        dataSource.put("hasNewNotice",noticeService.hasNewNotices());
        return ResultUtils.success(dataSource);
    }
}
