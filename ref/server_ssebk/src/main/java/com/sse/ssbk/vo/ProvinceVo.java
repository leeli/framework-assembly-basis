package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class ProvinceVo {
    private String provinceId;
    private String provinceName;
    private String shortName;
}