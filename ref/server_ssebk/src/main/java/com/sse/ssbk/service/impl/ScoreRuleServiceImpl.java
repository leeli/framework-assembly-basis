package com.sse.ssbk.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sse.ssbk.dao.ScoreRuleDao;
import com.sse.ssbk.entity.ScoreRule;
import com.sse.ssbk.service.ScoreRuleService;

@Service
public class ScoreRuleServiceImpl implements ScoreRuleService {
    @Autowired
    private ScoreRuleDao scoreRuleDao;

    @Override
    public List<ScoreRule> list() {
        return scoreRuleDao.selectAll();
    }
}