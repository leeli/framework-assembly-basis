package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.NewsDao;
import com.sse.ssbk.dto.AuditNewsDto;
import com.sse.ssbk.dto.NewsDto;
import com.sse.ssbk.dto.NewsPageDto;
import com.sse.ssbk.entity.News;
import com.sse.ssbk.entity.SourceFrom;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.service.NewsService;
import com.sse.ssbk.service.SourceFromService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by yxf on 2017/7/12.
 */
@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private SourceFromService sourceFromService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    public News getNewsById(String id) {
        News news = new News();
        news.setNewsId(Integer.valueOf(id));
        return newsDao.selectByPrimaryKey(news);
    }
    @Override
    public int addNews(NewsDto newsDto) {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        News news = DtoEntityVoUtils.dto2entity(newsDto,News.class);
        String images = BizUtils.getStrImages(newsDto.getImages());
        if (images!=null && images.length() != 0){
            news.setImages(BizUtils.getStrImages(newsDto.getImages()));
        }
        news.setUserPhoto(loginUser.getUserPhoto());
        news.setCompanyName(loginUser.getCompanyName());
        news.setCompanyId(loginUser.getCompanyId() == null ? null:Integer.valueOf(loginUser.getCompanyId()));
        news.setUserName(loginUser.getUserName());
        news.setUserId(loginUser.getUserId());
        if (loginUser.getPerm().get("zxsh")){       //是否有资讯审核权
            news.setStatus("1");
        }else {
            news.setStatus("0");
        }
        news.setCreateTime(new Date());
        SourceFrom sourceFrom = sourceFromService.getSourceFromById(news.getSourceFromId());
        news.setSourceFrom(sourceFrom.getSourceName());
        return newsDao.insertSelective(news);
    }
    @Override
    public List<News> getNews(NewsPageDto newsPageDto) {
        return newsDao.getNews(newsPageDto);
    }

    /**
     * 用户是否收藏某个资讯 收藏 1 没有收藏 0
     * @param userId
     * @param newsId
     * @return
     */
    @Override
    public String isCollect(Integer userId, Integer newsId) {
        return newsDao.isCollect(userId,newsId) == 0? "0":"1";
    }

    @Override
    public int auditNews(Integer newsId, AuditNewsDto auditNewsDto) {
        return newsDao.auditNews(newsId,auditNewsDto.getReason(),auditNewsDto.getStatus());
    }
}
