package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.EnrollActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by work_pc on 2017/7/15.
 */
public interface EnrollActivityDao extends BaseDao<EnrollActivity> {
    /**
     * 查看用户报名状态
     * @param userId
     * @param actId
     * @return
     */
    String getEnrollStatus(Integer userId,Integer actId);

    /**
     * 获取活动报名人的前三张头像
     * @param actId
     * @return
     */
    String[] getTop3Photo(Integer actId);


    /**
     * 审核报名人 status 0 待审核 1 审核通过  2 审核不通过
     * @param userIds
     * @param reason
     * @param enrollStatus
     * @return
     */
    int auditActivityJoiner(@Param("actId") Integer actId,@Param("userIds") List<Integer> userIds, @Param("reason") String reason, @Param("enrollStatus") String enrollStatus);
}
