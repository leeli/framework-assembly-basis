package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/7/31.
 */
@Data
public class CollectActivityVo {

    private String actId;
    private String cityName;
    private String activityImage;  //活动海报
    private String startTime;
    private String actNum;
    private String isCollect;
}
