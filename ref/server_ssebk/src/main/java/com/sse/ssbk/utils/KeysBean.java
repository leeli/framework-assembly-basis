/*
* FileName：KeysBean.java
*
* Description： 
*
*/
package com.sse.ssbk.utils;

public class KeysBean {

    public KeysBean(String server, long bytes, long expiry) {
        this.server = server;
        this.bytes = bytes;
        this.expiry = expiry;
    }

    private String server;

    private long bytes;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public long getBytes() {
        return bytes;
    }

    public void setBytes(long bytes) {
        this.bytes = bytes;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    private long expiry;


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;

}
