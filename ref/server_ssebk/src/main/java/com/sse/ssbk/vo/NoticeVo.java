package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/18.
 */
@Data
public class NoticeVo {
    private String noticeId;
    private String noticeType;
    private String content;
    private String sourceUserId;
    private String sourceActionId;
    private String sourceUserPhoto;
    private String isRead;
    private String createTime;
    private String noticeTypeDesc;
}
