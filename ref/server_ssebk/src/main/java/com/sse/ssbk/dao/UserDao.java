package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.dto.NewsPageDto;
import com.sse.ssbk.entity.News;
import com.sse.ssbk.entity.RolePerm;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.vo.PermExistsVo;
import com.sse.ssbk.vo.RolePermVo;
import com.sse.ssbk.vo.UserHomeVo;
import com.sse.ssbk.vo.UserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/7.
 */
public interface UserDao extends BaseDao<User> {
    /**
     * userId从session中获取
     * @param userId
     * @return
     */
    UserHomeVo getHomePage(String userId);

    List<News> getMyNewsList(@Param("lastNewsId")String lastNewsId,@Param("pageSize")Integer pageSize
            ,@Param("Status")String Status,@Param("isMyPublish")Integer isMyPublish,@Param("userId") String userId);

    User getUserByUserName(@Param("userName")String userName);

    List<RolePermVo> getUserRoleByUserId(@Param("userId")Integer userId);
    List<PermExistsVo> getUserPermByUserId(@Param("userId")Integer userId);
    List<PermExistsVo> getAllPermit(@Param("userId")Integer userId);

    int selectCountByUserName(@Param("loginName") String loginName);

    /**
     * 当用户验证邀请码成功后，需要将用户信息插入user和user_info表中
     * @return
     */
    int validateSuccessInsertUser(User user);
}
