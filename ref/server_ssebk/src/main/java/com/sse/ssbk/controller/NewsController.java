package com.sse.ssbk.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.dto.NewsDto;
import com.sse.ssbk.dto.NewsPageDto;
import com.sse.ssbk.entity.News;
import com.sse.ssbk.entity.SourceFrom;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.NewsService;
import com.sse.ssbk.service.SourceFromService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.CountScore;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.NewsVo;
import com.sse.ssbk.vo.NewsVoList;
import com.sse.ssbk.vo.SourceFromVo;

/**
 * Created by yxf on 2017/7/7.
 */

@Controller
@RequestMapping("/ssebk")
public class NewsController extends BaseController {
    @Autowired
    private NewsService newsService;

    @Autowired
    private SourceFromService sourceFromService;

    @GetMapping("/news/sourceFroms")
    @ResponseBody
    public Result<?> getSourceFromList() {
        List<SourceFromVo> sourceFromList = sourceFromService.getSourceFromList();
        return ResultUtils.success("资讯来源列表", sourceFromList);
    }

    /**
     * 写入资讯
     * @param newsDto
     * @return
     */
    @PostMapping("/news")
    @ResponseBody
    @CountScore(ScoreRules.ADD_NEWS)
    public Result<?> addNews(@RequestBody @Valid NewsDto newsDto) {
        newsService.addNews(newsDto);
        return ResultUtils.success(AppReturnCode.NEWS_PUBLISH_SUCCESS.getMsg(), null);
    }

    @GetMapping(value = "/news/{newsId}")
    @ResponseBody
    public Result<?> getNewsById(@PathVariable("newsId") String id) {
        News news = newsService.getNewsById(id);

        if (news == null) {
            return ResultUtils.failure("00001", "无此资讯");
        }

        Integer readCount = RedisCacheUtils.getInt(RedisConstants.User.ReadCount.READ_COUNT_NEWS + id);
        Integer collectionCount = RedisCacheUtils.getInt(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + id);
        Integer commentCount = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS + id);

        if (readCount == null) {
            readCount = 0;
        }

        if (collectionCount == null) {
            collectionCount = 0;
        }

        if (commentCount == null) {
            commentCount = 0;
        }

        SourceFrom sourceFrom = sourceFromService.getSourceFromById(news.getSourceFromId());
        NewsVo newsVo = DtoEntityVoUtils.entity2vo(news, NewsVo.class);
        if (news.getImages() != null) {
            newsVo.setImages(BizUtils.toStringArray(news.getImages()));
        } else {
            newsVo.setImages(new String[0]);
        }
        readCount++;
        RedisCacheUtils.setInt(RedisConstants.User.ReadCount.READ_COUNT_NEWS + id, readCount); //修复查看资讯详情需要刷新，查看数才加一的问题
        newsVo.setReadCount(readCount.intValue());
        newsVo.setCollectionCount(collectionCount.intValue());
        newsVo.setCommentCount(commentCount.intValue());
        newsVo.setSourceFromName(sourceFrom.getSourceName());

        UserSession loginUser = BizUtils.getCurrentUser();

        //是否收藏
        String isCollect = Is.NO.getCode();
        Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_NEWS + loginUser.getUserId());
        if (forSet != null && forSet.contains(news.getNewsId())) {
            isCollect = Is.YES.getCode();
        }
        //            String isCollect = newsService.isCollect(loginUser.getUserId(), Integer.valueOf(news.getNewsId()));

        //        String isCollect = newsService.isCollect(loginUser.getUserId(), Integer.valueOf(id));
        newsVo.setIsCollect(isCollect);
        return ResultUtils.success("资讯详情", newsVo);
    }

    @GetMapping(value = "news")
    @ResponseBody
    public Result<?> getNews(String data) throws UnsupportedEncodingException {
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        //		data = URLDecoder.decode(data,"UTF8");

        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastNewsId = jData.getString("lastNewsId");
        String pageSize = jData.getString("pageSize");
        String sourceFromId = jData.getString("sourceFromId");

        //		User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser = BizUtils.getCurrentUser();
        NewsPageDto newsPageDto = new NewsPageDto(Integer.valueOf(lastNewsId), Integer.valueOf(pageSize), sourceFromId);

        List<News> newsList = newsService.getNews(newsPageDto);

        if (newsList == null || newsList.size() == 0) {
            lastNewsId = lastNewsId;
            //			return ResultUtils.failure("00001","无数据");
        } else {
            lastNewsId = String.valueOf(newsList.get(newsList.size() - 1).getNewsId());
        }

        Map<String, Object> dataSource = new HashMap<>();
        List<NewsVoList> newsVoList = new ArrayList<>();
        dataSource.put("lastNewsId", lastNewsId);
        NewsVoList newsVo;
        for (News news : newsList) {
            newsVo = DtoEntityVoUtils.entity2vo(news, NewsVoList.class);
            if (news.getImages() != null && !"".equals(news.getImages())) {
                newsVo.setImages(BizUtils.toStringArray(news.getImages()));
            } else {
                newsVo.setImages(new String[0]);
            }

            String soureFromName = sourceFromService.getSourceFromById(news.getSourceFromId()).getSourceName();
            newsVo.setSourceFromName(soureFromName);
            /* Integer readCount = RedisCacheUtil.getInt("readCount" + "News" + news.getNewsId());
             Integer collectionCount = RedisCacheUtil.getInt("collectionCount" + "News" + news.getNewsId());
            //            Integer collectionCount = RedisCacheUtil.getForSet("user_collection_news"+ news.getNewsId()).size();

             Integer commentCount = RedisCacheUtil.getInt("commentCount" + "News" + news.getNewsId());*/
            Integer readCount = RedisCacheUtils.getInt(RedisConstants.User.ReadCount.READ_COUNT_NEWS + news.getNewsId());
            Integer collectionCount = RedisCacheUtils.getInt(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + news.getNewsId());
            Integer commentCount = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS + news.getNewsId());

            if (readCount == null) {
                readCount = 0;
            }

            if (collectionCount == null) {
                collectionCount = 0;
            }

            if (commentCount == null) {
                commentCount = 0;
            }

            //			RedisCacheUtil.setInt("readCount"+"News"+news.getNewsId(),readCount);

            newsVo.setReadCount(String.valueOf(readCount));
            newsVo.setCollectionCount(String.valueOf(collectionCount));
            newsVo.setCommentCount(String.valueOf(commentCount));

            //是否收藏
            String isCollect = Is.NO.getCode();

            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_NEWS + loginUser.getUserId());
            if (forSet != null && forSet.contains(news.getNewsId())) {
                isCollect = Is.YES.getCode();
            }
            //            String isCollect = newsService.isCollect(loginUser.getUserId(), Integer.valueOf(news.getNewsId()));
            newsVo.setIsCollect(isCollect);
            newsVoList.add(newsVo);
        }
        dataSource.put("newsList", newsVoList);
        return ResultUtils.success("资讯列表", dataSource);
    }
}
