package com.sse.ssbk.service;

import com.sse.ssbk.vo.AppSdkRelationVo;
import org.springframework.stereotype.Service;

/**
 * Created by ciyua on 2017/8/2.
 */
@Service
public interface AppSdkRelationService {

    public AppSdkRelationVo isExistAppSDK(String bundleId, String sdkId);
}
