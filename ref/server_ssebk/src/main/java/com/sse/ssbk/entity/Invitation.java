package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by work_pc on 2017/8/8.
 */
@Data
@Entity
@Table(name = "t_invitation")
public class Invitation {
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "phone")
    private String phone;
    @Column(name = "invitation_code")
    private String invitationCode;
    @Column(name = "to_user_real_name")
    private String toUserRealName;
    @Column(name = "status")
    private String status;
    @Column(name = "message")
    private String message;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
}
