/*
* FileName：PropetiesInfo.java
*
* Description： 读取  properties 里的配置
*
* History：
* 版本号 作者 日期 简介
* 1.0 kong  2015/9/14 Create
*/
package com.sse.ssbk.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class PropetiesInfo {

    public final static String CONFIG   = "application.properties";

    
    /***
     * 加密请求配置信息
     */
    private static List<String> encryptlist=null;
    public static List<String> getEncryptlist() {
        if(encryptlist==null){
            encryptlist=new ArrayList<>();
            String str= (String) PropetiesInfo.getKeyInfo(PropetiesInfo.CONFIG, "ENCRYPTList");
            String[] strs=str.split(",");
            for(String item : strs){
                encryptlist.add(item);
            }
        }
        return encryptlist;
    }
    
    
    public static Object getKeyInfo(String filePath,String keyName){
        InputStream proIn = PropetiesInfo.class.getClassLoader().getResourceAsStream(filePath);
        Properties pro = new Properties();
        try {
            pro.load(proIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //back = pro.getProperty(keyName);
        return pro.get(keyName);
    }


    public static Map<String,String> getAllKeyinfo(String filePath){
        InputStream proIn = PropetiesInfo.class.getClassLoader().getResourceAsStream(filePath);
        Properties pro = new Properties();
        try {
            pro.load(proIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, String> map = new HashMap<String, String>((Map)pro);
        return map;
    }
}