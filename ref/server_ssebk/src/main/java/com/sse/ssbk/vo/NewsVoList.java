package com.sse.ssbk.vo;

import lombok.Data;

import java.util.Date;

/**
 * Created by yxf on 2017/7/20.
 */

@Data
public class NewsVoList {
    private String newsId;
    private String title;
    private String content;
    private String[] images;
//    private String sourceFromID;
    private String sourceFromName;
    private String createTime;
    private String readCount;
    private String collectionCount;
    private String commentCount;
    //获取资讯列表有用到
    private String isCollect;
    //获取我的资讯列表有用到
    private String reason;
}
