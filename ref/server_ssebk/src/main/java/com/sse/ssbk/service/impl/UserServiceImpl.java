package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.sse.ssbk.entity.*;
import com.sse.ssbk.utils.*;
import com.sse.ssbk.vo.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.IMFriendDao;
import com.sse.ssbk.dao.ProvinceDao;
import com.sse.ssbk.dao.SourceFromDao;
import com.sse.ssbk.dao.UserConcernDao;
import com.sse.ssbk.dao.UserDao;
import com.sse.ssbk.dao.UserScoreDao;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.CompanyInfoService;
import com.sse.ssbk.service.ScoreLevelService;
import com.sse.ssbk.service.UserService;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * Created by work_pc on 2017/7/23.
 */
@Service
public class UserServiceImpl implements UserService {

    // 日志
    private static org.slf4j.Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserConcernDao userConcernDao;

    @Autowired
    private UserScoreDao userScoreDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private SourceFromDao sourceFromDao;

    @Autowired
    private ScoreLevelService scoreLevelService;

    @Autowired
    private CompanyInfoService companyInfoService;

    @Autowired
    private ProvinceDao provinceDao;

    @Autowired
    private IMFriendDao imFriendDao;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public UserHomeVo getHomePage(String userId) {
        //redis中的信息可能会有延时误差
        //        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        String isConcern = Is.NO.getCode();
        String isFriend = Is.NO.getCode();
        UserSession loginUser = BizUtils.getCurrentUser();
        if (userId == null) {
            //自己查看自己的主页
            userId = String.valueOf(loginUser.getUserId());
        } else {
            //查看别人的主页
            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());
            if (forSet != null && forSet.contains(Integer.valueOf(userId))) {
                isConcern = Is.YES.getCode();
            }
            if (imFriendDao.isFriend(Integer.parseInt(userId), loginUser.getUserId()) != 0) {
                isFriend = Is.YES.getCode();
            }
        }

        User user = new User();
        user.setUserId(Integer.valueOf(userId));
        user = userDao.selectByPrimaryKey(user);

        UserHomeVo homeVo = DtoEntityVoUtils.entity2vo(user, UserHomeVo.class);

        homeVo.setCompany(user.getCompanyName());
        homeVo.setProvince(user.getProvinceName());
        //用户获取用户得分
        /*UserScore userScore = new UserScore();
        userScore.setUserId(Integer.valueOf(userId));
        userScore = userScoreDao.selectByPrimaryKey(userScore);*/

        //        Double score = redisTemplate.opsForZSet().score(RedisConstants.User.USER_SCORE_RANK, userId);
        //todo 使用redisTemplate不行。。
        Double score = stringRedisTemplate.opsForZSet().score(RedisConstants.User.USER_SCORE_RANK, userId);
        int userScore;
        if (score == null) {
            userScore = 0;
        } else {
            userScore = score.intValue();
        }

        Long rank = redisTemplate.opsForZSet().count(RedisConstants.User.USER_SCORE_RANK, userScore + 1, Integer.MAX_VALUE) + 1;

        //        Integer userScore = RedisCacheUtils.getInt(RedisConstants.User.USER_SCORE + userId);
        String levelId;
        homeVo.setScore(String.valueOf(userScore));
        levelId = (userScore + 100) / 100 > 8 ? "8" : String.valueOf((userScore + 100) / 100);
        //积分表中查不到，没有积分
        /*if (userScore == null) {
            homeVo.setScore(String.valueOf(0));
            levelId = "1";
        } else {
            homeVo.setScore(String.valueOf(userScore));
            levelId = (userScore + 100) / 100 > 8 ? "8" : String.valueOf((userScore + 100) / 100);
        }*/

        ScoreLevel scoreLevel = scoreLevelService.getScoreLevelByLevelId(levelId);
        if (scoreLevel != null) {
            homeVo.setLevelId(levelId);
            homeVo.setLevelName(scoreLevel.getLevelName());
            homeVo.setLevelImg(scoreLevel.getLevelImg());
        }
        //关注我的人
        Integer concernMeNum = userConcernDao.getUserConcernedMeNumber(Integer.valueOf(userId));

        if (concernMeNum == null) {
            concernMeNum = 0;
        }

        homeVo.setConcernToMyNum(String.valueOf(concernMeNum));

        //我关注的人
        Integer iConcernNum = userConcernDao.getIConcernedUserNumber(Integer.valueOf(userId));

        if (iConcernNum == null) {
            concernMeNum = 0;
        }
        //我的关注人数
        homeVo.setConcernOfMyNum(String.valueOf(iConcernNum));
        //初始化null
        homeVo = DtoEntityVoUtils.voFieldFromNull2Empty(homeVo);
        homeVo.setIsConcern(isConcern);
        homeVo.setIsFriend(isFriend);

        homeVo.setRank(rank.toString());
        //        homeVo.setRank("9999");
        return homeVo;
    }

    @Override
    public List<NewsVoList> getMyNewsList(String lastNewsId, String pageSize, String Status, String isMyPublish) {
        //        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser = BizUtils.getCurrentUser();
        List<News> myNewsList = userDao.getMyNewsList(lastNewsId, Integer.valueOf(pageSize), Status, Integer.valueOf(isMyPublish), String.valueOf(loginUser.getUserId()));
        List<NewsVoList> newsVoLists = new ArrayList<>();
        NewsVoList newsVo;
        for (News news : myNewsList) {
            newsVo = DtoEntityVoUtils.entity2vo(news, NewsVoList.class);
            if (news.getImages() != null) {
                newsVo.setImages(BizUtils.toStringArray(news.getImages()));
            } else {
                newsVo.setImages(new String[0]);
            }

            SourceFrom sourceFrom = new SourceFrom();
            sourceFrom.setSourceFromId(Integer.valueOf(news.getSourceFromId()));
            String soureFromName = sourceFromDao.selectByPrimaryKey(sourceFrom).getSourceName();
            newsVo.setSourceFromName(soureFromName);
            sourceFrom = null;

            Integer readCount = RedisCacheUtils.getInt(RedisConstants.User.ReadCount.READ_COUNT_NEWS + news.getNewsId());
            Integer collectionCount = RedisCacheUtils.getInt(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + news.getNewsId());
            Integer commentCount = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS + news.getNewsId());

            if (readCount == null) {
                readCount = 0;
            }

            if (collectionCount == null) {
                collectionCount = 0;
            }

            if (commentCount == null) {
                commentCount = 0;
            }
            /*String isCollect = newsDao.isCollect(loginUser.getUserId(),Integer.valueOf(news.getNewsId())) == 1 ?"1":"0";
            newsVo.setIsCollect();*/
            newsVo.setReadCount(String.valueOf(readCount));
            newsVo.setCollectionCount(String.valueOf(collectionCount));
            newsVo.setCommentCount(String.valueOf(commentCount));
            newsVo.setReason(news.getReason() == null ? "" : news.getReason());
            newsVoLists.add(newsVo);
        }
        return newsVoLists;
    }

    @Override
    public UserVo login(String loginName, String password) {
        UserVo userVo = new UserVo();
        Map<String, Object> params = new HashMap<>();
        params.put("loginName", loginName);
        params.put("password", password);

        //TODO 地址需要配置
/*        String reqUrl = "https://10.10.11.11/casserver/m_login/login";
        String r = null;
        try{
            r = ServiceUtil.DoHttpPost(reqUrl, params);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }

         JSONObject json = JSONObject.parseObject(r);

         String status = json.get("status") + "";
         String reason = json.getString("reason");
         String token = json.getString("ticket");

        // SSO验证有误（用户不存在或密码不正确等）
        if("0".equals(status)){
            log.info("登录失败，原因：" + json.getString("reason"));
            throw new SSEBKRuntimeException(AppReturnCode.CustomError.getCode(), reason);
        }*/
        // TODO 发布到内网需要解开以上代码，注释token 设置固定值，此代码需删除
        String sha1loginName = Util.encodeBySHA1(loginName);
        String token = sha1loginName + "TGT-1-mDpbnd7bRfbgTqtXo6bcAr7eN42QHJbhJ3gBO9mIJlV2Ty07Fl-cas01.example.org"+new Random().nextDouble();
        Set<String> keys = RedisCacheUtils.keys(sha1loginName+"*");
        for (String key:keys){
            RedisCacheUtils.expire(key,1);
        }
        userVo.setToken(token);
        userVo.setUserName(loginName);

        // 继续验证上市百科用户信息
        // 通过用户名查询百科用户信息
        User userInfo = userDao.getUserByUserName(loginName);
        // 百科用户不存在
        if (userInfo == null) {
            userVo.setUserStatus("0");
            return userVo;
        }
        // 查询用户相关角色
        List<PermExistsVo> permExistsVoList = userDao.getUserPermByUserId(userInfo.getUserId());
        List<PermExistsVo> allPerm = userDao.getAllPermit(userInfo.getUserId());
        Map<String, Boolean> perm = new HashMap<>();
        for (PermExistsVo permExistsVo : allPerm) {
            if (permExistsVoList.contains(permExistsVo)) {
                perm.put(permExistsVo.getPermNickName(), true);
            } else {
                perm.put(permExistsVo.getPermNickName(), false);
            }
        }
        // 通过公司ID查询是否完成财务指标

        String companyId = userInfo.getCompanyId();

        CompanyInfo companyInfo = null;
        if (companyId != null && !"".equals(companyId)) {
            companyInfo = companyInfoService.selectCompanyInfoById(Integer.valueOf(userInfo.getCompanyId()));
        }

        String completeFinancial = companyInfo == null ? "0" : "1"; // 0=未完善，1=已完善

        userVo.setPerm(perm);
        userVo.setUserId(userInfo.getUserId());
        userVo.setRealName(userInfo.getRealName());
        userVo.setUserPhoto(userInfo.getUserPhoto());
        userVo.setProvince(userInfo.getCityId());
        userVo.setCompany(userInfo.getCompanyName());
        userVo.setUserStatus(userInfo.getStatus());
        userVo.setIsCompleteFinancial(completeFinancial);

        UserSession session = new UserSession();
        session.setUserId(userInfo.getUserId());
        session.setUserName(userInfo.getUserName());
        session.setRealName(userInfo.getRealName());
        session.setUserPhoto(userInfo.getUserPhoto());
        session.setUserStatus(userInfo.getStatus());
        session.setProvince(userInfo.getCityId());
        session.setCompanyName(userInfo.getCompanyName());
        session.setToken(token);
        session.setPerm(perm);
        if (companyId != null && "".equals(companyId)) {
            session.setCompanyId(Integer.valueOf(companyId));
        }
        session.setCompanyName(userInfo.getCompanyName());

        RedisCacheUtils.setForValue(token, session, Constants.SESSION_TIME_OUT, TimeUnit.MILLISECONDS);
        return userVo;
    }

    @Override
    public void register(String userName, String password, String smsCaptcha) {
        // 向通行证注册
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("loginName", userName);
        params.put("nickName", "ssbk_" + userName); // TODO 昵称生成逻辑
        params.put("password", password);
        params.put("activeCode", smsCaptcha);
        params.put("version", "0");
        // TODO 需要配置
        params.put("serviceUrl", "http://www108.sse.com.cn/");

        //TODO 地址需要配置
        String reqUrl = "https://10.10.11.11/casserver/m_login/register";
        String r = null;
        try {
            r = ServiceUtil.DoHttpPost(reqUrl, params);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
        JSONObject json = JSONObject.parseObject(r);
        // 注册不成功
        if ("0".equals(json.getString("status"))) {
            log.info("注册失败，原因：" + json.getString("reason"));
            throw new SSEBKRuntimeException(AppReturnCode.CustomError.getCode(), json.getString("reason"));
        }
    }

    public void logout(String ticket) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("CASTGC", ticket);
        // TODO 需要抽取为配置信息
        String reqUrl = "https://10.10.11.11/casserver/logout";
        try {
            ServiceUtil.DoHttpPost(reqUrl, paramMap);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
    }

    @Override
    public void changePsw(String oriPswd, String newPswd) {
        UserSession loginUser = BizUtils.getCurrentUser();
        String username = loginUser.getUserName();
        Map<String, Object> params = new HashMap<>();
        params.put("username", username);
        params.put("oriPswd", oriPswd);
        params.put("newPswd", newPswd);

        String reqUrl = "https://10.10.11.11/casserver/m_login/updatePswd";
        String r = null;
        try {
            r = ServiceUtil.DoHttpGet(reqUrl, params, "GET");
        } catch (Exception e) {
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }

        JSONObject json = JSONObject.parseObject(r);

        Integer status = json.getInteger("status");
        String reason = json.getString("reason");

        if (0 == status) {
            throw new SSEBKRuntimeException(AppReturnCode.UndefinedError.getCode(), reason);
        }
    }

    @Override
    public void forgetPsw(String username, String code, String newPswd) {
        Map<String, Object> params = new HashMap<>();
        params.put("username", username);
        params.put("code", code);
        params.put("newPswd", newPswd);

        String reqUrl = "https://10.10.11.11/casserver/m_login/updatePswdByCode";
        String r = null;
        try {
            r = ServiceUtil.DoHttpGet(reqUrl, params, "GET");
        } catch (Exception e) {
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }

        JSONObject json = JSONObject.parseObject(r);

        Integer status = json.getInteger("status");
        String reason = json.getString("reason");

        if (0 == status) {
            throw new SSEBKRuntimeException(AppReturnCode.UndefinedError.getCode(), reason);
        }
    }

    @Override
    public String requireInfo() {
        UserSession loginUser = BizUtils.getCurrentUser();
        User user = new User();
        user.setUserId(loginUser.getUserId());
        user = userDao.selectByPrimaryKey(user);
        if (user.getUserPhoto() != null && user.getProvinceId() != null && user.getCompanyName() != null && user.getJobPosition() != null && user.getNameCard() != null && user.getOrgcodeCertificate() != null) {
            user.setStatus("2");
            userDao.updateByPrimaryKeySelective(user);
            return Is.YES.getCode();
        }
        return Is.NO.getCode();
    }

    /**
     * @param loginName  登录名 用户手机号
     * @return 0:存在;1:不存在;
     */
    @Override
    public String userPhoneIsExists(String loginName) {
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("username", loginName);
        // TODO 需要抽取为配置信息
        String reqUrl = "http://10.10.11.11:8080/casserver/remote/isUserNameUnique";
        String r = null;
        try {
            r = ServiceUtil.DoHttpGet(reqUrl, paramMap, "GET");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
        JSONObject json = JSONObject.parseObject(r);
        String status = json.getString("status");

        return status;
    }

    @Override
    public User getUserById(Integer userId) {
        User user = new User();
        user.setUserId(userId);
        return userDao.selectByPrimaryKey(user);
    }

    @Override
    public int validateSuccessInsertUser(User user) {
        return userDao.validateSuccessInsertUser(user);
    }

    @Override
    public ScoreIndexVo scoreIndex(String userId) {
        UserSession loginUser = BizUtils.getCurrentUser();

        User user = new User();
        user.setUserId(Integer.valueOf(userId));
        user = userDao.selectByPrimaryKey(user);

        ScoreIndexVo scoreIndexVo = DtoEntityVoUtils.entity2vo(user, ScoreIndexVo.class);

        scoreIndexVo.setCompany(user.getCompanyName() == null ?"":user.getCompanyName());

        Double score = stringRedisTemplate.opsForZSet().score(RedisConstants.User.USER_SCORE_RANK, userId);
        int userScore;
        if (score == null) {
            userScore = 0;
        } else {
            userScore = score.intValue();
        }

        Long rank = redisTemplate.opsForZSet().count(RedisConstants.User.USER_SCORE_RANK, userScore+1, Integer.MAX_VALUE) + 1;

        //        Integer userScore = RedisCacheUtils.getInt(RedisConstants.User.USER_SCORE + userId);
        String currentlevelId;
        scoreIndexVo.setCurrentScore(String.valueOf(userScore));
        currentlevelId = (userScore + 100) / 100 > Constants.MAX_SCORE_LEVEL ? String.valueOf(Constants.MAX_SCORE_LEVEL) : String.valueOf((userScore + 100) / 100);


        String lastLevelId = String.valueOf(Integer.valueOf(currentlevelId) -1);
        String upgradeLevelId = String.valueOf(Integer.valueOf(currentlevelId) +1);
        String next2LevelId = String.valueOf(Integer.valueOf(currentlevelId) +2);

        //当前等级的
        ScoreLevel currentScoreLevel = scoreLevelService.getScoreLevelByLevelId(currentlevelId);
        if (currentScoreLevel != null) {
            scoreIndexVo.setCurrentId(currentlevelId);
            scoreIndexVo.setLevelName(currentScoreLevel.getLevelName());
//            scoreIndexVo.setCurrentScore(currentScoreLevel.getLevelScore());
            scoreIndexVo.setCurrentImg(currentScoreLevel.getLevelImg());
        }

        if (!String.valueOf(Constants.MIN_SCORE_LEVEL).equals(currentlevelId)) {
            //上一等级
            ScoreLevel lastScoreLevel = scoreLevelService.getScoreLevelByLevelId(lastLevelId);
            if (lastScoreLevel != null) {
                scoreIndexVo.setLastLevelId(lastLevelId);
                scoreIndexVo.setLastLevelName(lastScoreLevel.getLevelName());
                scoreIndexVo.setLastLevelScore(String.valueOf(Integer.valueOf(lastScoreLevel.getLevelScore()) - 100));
                scoreIndexVo.setLastLevelImg(lastScoreLevel.getLevelImg());
            }
        }

        //下一等级
        if (!String.valueOf(Constants.MAX_SCORE_LEVEL).equals(currentlevelId)) {
            ScoreLevel upgradeScoreLevel = scoreLevelService.getScoreLevelByLevelId(upgradeLevelId);
            if (upgradeScoreLevel != null) {
                scoreIndexVo.setUpgradeId(upgradeLevelId);
                scoreIndexVo.setUpgradeName(upgradeScoreLevel.getLevelName());
                scoreIndexVo.setUpgradeLevelScore(String.valueOf(Integer.valueOf(upgradeScoreLevel.getLevelScore()) - 100));
                scoreIndexVo.setUpgradeImg(upgradeScoreLevel.getLevelImg());
                scoreIndexVo.setUpgradeToScore(String.valueOf(Integer.valueOf(upgradeScoreLevel.getLevelScore()) - userScore - 100));
            }
        }

        if (String.valueOf(Constants.MIN_SCORE_LEVEL).equals(currentlevelId)) {
            //下两个等级
            ScoreLevel next2ScoreLevel = scoreLevelService.getScoreLevelByLevelId(next2LevelId);
            scoreIndexVo.setNext2LevelId(next2LevelId);
            scoreIndexVo.setNext2LevelName(next2ScoreLevel.getLevelName());
            scoreIndexVo.setNext2LevelScore(String.valueOf(Integer.valueOf(next2ScoreLevel.getLevelScore()) - 100));
            scoreIndexVo.setNext2LevelImg(next2ScoreLevel.getLevelImg());
            scoreIndexVo.setNext2LevelUpgradeToScore(String.valueOf(Integer.valueOf(next2ScoreLevel.getLevelScore()) - userScore -100));
        }


        Set<String> userIds = stringRedisTemplate.opsForZSet().reverseRange(RedisConstants.User.USER_SCORE_RANK, 0, 9);

        List<UserTopVo> userTopVos = new ArrayList<>();
        for (String id: userIds){
            user = new User();
            user.setUserId(Integer.valueOf(id));
            user = userDao.selectByPrimaryKey(user);
            if (user == null){
                continue;
            }

            UserTopVo userTopVo = DtoEntityVoUtils.entity2vo(user, UserTopVo.class);
            userTopVo.setUserCompany(user.getCompanyName() == null ? "":user.getCompanyName());
            Double topScore = stringRedisTemplate.opsForZSet().score(RedisConstants.User.USER_SCORE_RANK, id);
            if (topScore == null) {
                userScore = 0;
            } else {
                userScore = topScore.intValue();
            }

            Long topRank = redisTemplate.opsForZSet().count(RedisConstants.User.USER_SCORE_RANK, userScore+1, Integer.MAX_VALUE) + 1;
            userTopVo.setRank(topRank.toString());


            String topLevelId = (userScore + 100) / 100 > Constants.MAX_SCORE_LEVEL ? String.valueOf(Constants.MAX_SCORE_LEVEL) : String.valueOf((userScore + 100) / 100);
            ScoreLevel topScoreLevel = scoreLevelService.getScoreLevelByLevelId(topLevelId);
            /*userTopVo.setLevelId(levelId);
            userTopVo.setLevelName(scoreLevel.getLevelName());*/
            userTopVo.setUserLevelImg(topScoreLevel.getLevelImg());


            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());
            String isConcern = Is.NO.getCode();
            if (forSet != null && forSet.contains(Integer.valueOf(id))) {
                isConcern = Is.YES.getCode();
            }
            userTopVo.setInConcern(isConcern);
            userTopVos.add(userTopVo);
        }
        scoreIndexVo.setUserTops(userTopVos);
        return scoreIndexVo;
    }

    public static void main(String[] args) {
        System.out.println(new Random().nextDouble());
    }
}
