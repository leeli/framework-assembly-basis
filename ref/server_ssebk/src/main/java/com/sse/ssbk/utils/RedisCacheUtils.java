package com.sse.ssbk.utils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.ContextLoader;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by yxf on 2017/7/7.
 */
public class RedisCacheUtils {
    private static Integer OVER_TIME = 15 * 60 * 1000;
    private static ApplicationContext CONTEXT = ContextLoader.getCurrentWebApplicationContext();
    private static StringRedisTemplate stringRedisTemplate = SpringContextHolder.getBean("stringRedisTemplate");
    private static RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");

    @SuppressWarnings("unchecked")
    public static <T> void setForValue(String key, T obj) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ValueOperations<String, T> opsValue = redisTemplate.opsForValue();
        opsValue.set(key, obj);
    }

    @SuppressWarnings("unchecked")
    public static <T> void setForValue(String key, T obj, long timeout, TimeUnit unit) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ValueOperations<String, T> opsValue = redisTemplate.opsForValue();
        opsValue.set(key, obj);
        redisTemplate.expire(key, timeout, unit);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getForValue(String key) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ValueOperations<String, T> opsValue = redisTemplate.opsForValue();
        return opsValue.get(key);
    }

    @SuppressWarnings("unchecked")
    public static <T> void addForSet(String key, T obj) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        SetOperations<String, T> opsSet = redisTemplate.opsForSet();
        opsSet.add(key, obj);
    }

    @SuppressWarnings("unchecked")
    public static <T> void delForSet(String key, T... obj) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        SetOperations<String, T> opsSet = redisTemplate.opsForSet();
        opsSet.remove(key, obj);
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> getForSet(String key) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        SetOperations<String, T> opsSet = redisTemplate.opsForSet();
        return opsSet.members(key);
    }

    @SuppressWarnings("unchecked")
    public static <T> void addForZSet(String key, T obj, double score) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ZSetOperations<String, T> opsZSet = redisTemplate.opsForZSet();
        opsZSet.add(key, obj, score);
    }

    @SuppressWarnings("unchecked")
    public static <T> void delForZSet(String key, T obj) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ZSetOperations<String, T> opsZSet = redisTemplate.opsForZSet();
        opsZSet.remove(key, obj);
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> getForZSet(String key) {
        RedisTemplate<String, T> redisTemplate = (RedisTemplate<String, T>) CONTEXT.getBean("redisTemplate");
        ZSetOperations<String, T> opsZSet = redisTemplate.opsForZSet();
        return opsZSet.range(key, 0, opsZSet.size(key));
    }

    /**
     * 删除缓存<br>
     * 根据key精确匹配删除
     * @param key
     */
    @SuppressWarnings("unchecked")
    public static void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }

    /**
     * 批量删除<br>
     * （该操作会执行模糊查询，请尽量不要使用，以免影响性能或误删）
     * @param pattern
     */
    public static void batchDel(String... pattern) {
        for (String kp : pattern) {
            redisTemplate.delete(redisTemplate.keys(kp + "*"));
        }
    }

    /**
     * 取得缓存（int型）
     * @param key
     * @return
     */
    public static Integer getInt(String key) {
        String value = stringRedisTemplate.boundValueOps(key).get();
        if (StringUtils.isNotBlank(value)) {
            return Integer.valueOf(value);
        }
        return null;
    }

    /**
     * 取得缓存（字符串类型）
     * @param key
     * @return
     */
    public static String getStr(String key) {
        return stringRedisTemplate.boundValueOps(key).get();
    }

    /**
     * 取得缓存（字符串类型）
     * @param key
     * @return
     */
    public static String getStr(String key, boolean retain) {
        String value = stringRedisTemplate.boundValueOps(key).get();
        if (!retain) {
            redisTemplate.delete(key);
        }
        return value;
    }

    /**
     * 获取缓存<br>
     * 注：基本数据类型(Character除外)，请直接使用get(String key, Class<T> clazz)取值
     * @param key
     * @return
     */
    public static Object getObj(String key) {
        return redisTemplate.boundValueOps(key).get();
    }

    /**
     * 获取缓存<br>
     * 注：java 8种基本类型的数据请直接使用get(String key, Class<T> clazz)取值
     * @param key
     * @param retain    是否保留
     * @return
     */
    public static Object getObj(String key, boolean retain) {
        Object obj = redisTemplate.boundValueOps(key).get();
        if (!retain) {
            redisTemplate.delete(key);
        }
        return obj;
    }

    /**
     * 获取缓存<br>
     * 注：该方法暂不支持Character数据类型
     * @param key   key
     * @param clazz 类型
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T get(String key, Class<T> clazz) {
        return (T) redisTemplate.boundValueOps(key).get();
    }

    /**
     * 获取缓存json对象<br>
     * @param key   key
     * @param clazz 类型
     * @return
     */
    public static <T> T getJson(String key, Class<T> clazz) {
        return JsonMapper.fromJsonString(stringRedisTemplate.boundValueOps(key).get(), clazz);
    }

    /**
     * 将value对象写入缓存
     * @param key
     * @param value
     */
    /*public static void set(String key,Object value,ExpireTime time){
        if(value.getClass().equals(String.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Integer.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Double.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Float.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Short.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Long.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else if(value.getClass().equals(Boolean.class)){
            stringRedisTemplate.opsForValue().set(key, value.toString());
        }else{
            redisTemplate.opsForValue().set(key, value);
        }
        if(time.getTime() > 0){
            redisTemplate.expire(key, time.getTime(), TimeUnit.SECONDS);
        }
    }*/

    public static void set(String key, Object value) {
        if (value.getClass().equals(String.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Integer.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Double.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Float.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Short.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Long.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else if (value.getClass().equals(Boolean.class)) {
            stringRedisTemplate.opsForValue().set(key, value.toString());
        } else {
            redisTemplate.opsForValue().set(key, value);
        }
        redisTemplate.expire(key, OVER_TIME, TimeUnit.SECONDS);
    }

    /**
     * 将value对象以JSON格式写入缓存
     * @param key
     * @param value
     */
    public static void setJson(String key, Object value) {
        stringRedisTemplate.opsForValue().set(key, JsonMapper.toJsonString(value));
        stringRedisTemplate.expire(key, OVER_TIME, TimeUnit.SECONDS);
    }

    /**
     * 更新key对象field的值
     * @param key   缓存key
     * @param field 缓存对象field
     * @param value 缓存对象field值
     */
    public static void setJsonField(String key, String field, String value) {
        JSONObject obj = JSON.parseObject(stringRedisTemplate.boundValueOps(key).get());
        obj.put(field, value);
        stringRedisTemplate.opsForValue().set(key, obj.toJSONString());
    }

    /**
     * 递减操作
     * @param key
     * @param by
     * @return
     */
    public static double decr(String key, double by) {
        return redisTemplate.opsForValue().increment(key, -by);
    }

    /**
     * 递增操作
     * @param key
     * @param by
     * @return
     */
    public static double incr(String key, double by) {
        return redisTemplate.opsForValue().increment(key, by);
    }

    /**
     * 获取double类型值
     * @param key
     * @return
     */
    public static double getDouble(String key) {
        String value = stringRedisTemplate.boundValueOps(key).get();
        if (StringUtils.isNotBlank(value)) {
            return Double.valueOf(value);
        }
        return 0d;
    }

    public static double incr(String key, long delta) {
        return redisTemplate.opsForValue().increment(key, delta);
    }

    public static long getLong(String key) {
        String value = stringRedisTemplate.boundValueOps(key).get();
        if (StringUtils.isBlank(value)) {
            return 0;
        }
        return Long.valueOf(value);
    }

    /**
     * 设置double类型值
     * @param key
     * @param value
     */
    public static void setDouble(String key, double value) {
        stringRedisTemplate.opsForValue().set(key, String.valueOf(value));
        stringRedisTemplate.expire(key, OVER_TIME, TimeUnit.SECONDS);
    }

    /**
     * 设置double类型值
     * @param key
     * @param value
     */
    public static void setInt(String key, int value) {
        stringRedisTemplate.opsForValue().set(key, String.valueOf(value));
        stringRedisTemplate.expire(key, OVER_TIME, TimeUnit.SECONDS);
    }

    /**
     * 将map写入缓存
     * @param key
     * @param map
     */
    public static <T> void setMap(String key, Map<String, T> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 将map写入缓存
     * @param key
     */
    @SuppressWarnings("unchecked")
    public static <T> void setMap(String key, T obj) {
        Map<String, String> map = (Map<String, String>) JsonMapper.parseObject(obj, Map.class);
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 向key对应的map中添加缓存对象
     * @param key
     * @param map
     */
    public static <T> void addMap(String key, Map<String, T> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 向key对应的map中添加缓存对象
     * @param key   cache对象key
     * @param field map对应的key
     * @param value     值
     */
    public static void addMap(String key, String field, String value) {
        redisTemplate.opsForHash().put(key, field, value);
    }

    /**
     * 向key对应的map中添加缓存对象
     * @param key   cache对象key
     * @param field map对应的key
     * @param obj   对象
     */
    public static <T> void addMap(String key, String field, T obj) {
        redisTemplate.opsForHash().put(key, field, obj);
    }

    /**
     * 获取map缓存
     * @param key
     * @param clazz
     * @return
     */
    public static <T> Map<String, T> mget(String key, Class<T> clazz) {
        BoundHashOperations<String, String, T> boundHashOperations = redisTemplate.boundHashOps(key);
        return boundHashOperations.entries();
    }

    /**
     * 获取map缓存
     * @param key
     * @param clazz
     * @return
     */
    public static <T> T getMap(String key, Class<T> clazz) {
        BoundHashOperations<String, String, String> boundHashOperations = redisTemplate.boundHashOps(key);
        Map<String, String> map = boundHashOperations.entries();
        return JsonMapper.parseObject(map, clazz);
    }

    /**
     * 获取map缓存中的某个对象
     * @param key
     * @param field
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getMapField(String key, String field, Class<T> clazz) {
        return (T) redisTemplate.boundHashOps(key).get(field);
    }

    /**
     * 删除map中的某个对象
     * @author lh
     * @date 2016年8月10日
     * @param key   map对应的key
     * @param field map中该对象的key
     */
    public void delMapField(String key, String... field) {
        BoundHashOperations<String, String, ?> boundHashOperations = redisTemplate.boundHashOps(key);
        boundHashOperations.delete(field);
    }

    /**
     * 指定缓存的失效时间
     *
     * @author FangJun
     * @date 2016年8月14日
     * @param key 缓存KEY
     */
    public static void expire(String key, long seconds) {
        if (seconds > 0) {
            redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
        }
    }

    /**
     * 添加set
     * @param key
     * @param value
     */
    public static void sadd(String key, String... value) {
        redisTemplate.boundSetOps(key).add(value);
    }

    /**
     * 删除set集合中的对象
     * @param key
     * @param value
     */
    public static void srem(String key, String... value) {
        redisTemplate.boundSetOps(key).remove(value);
    }

    /**
     * set重命名
     * @param oldkey
     * @param newkey
     */
    public static void srename(String oldkey, String newkey) {
        redisTemplate.boundSetOps(oldkey).rename(newkey);
    }

    /**
     * 短信缓存
     * @author fxl
     * @date 2016年9月11日
     * @param key
     * @param value
     * @param time
     */
    public static void setIntForPhone(String key, Object value, int time) {
        stringRedisTemplate.opsForValue().set(key, JsonMapper.toJsonString(value));
        if (time > 0) {
            stringRedisTemplate.expire(key, time, TimeUnit.SECONDS);
        }
    }

    /**
     * 模糊查询keys
     * @param pattern
     * @return
     */
    public static Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }
}
