package com.sse.ssbk.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class CollectionAddDto {
    @NotBlank
    private String targetId;
    @NotBlank
    private String targetType;
}