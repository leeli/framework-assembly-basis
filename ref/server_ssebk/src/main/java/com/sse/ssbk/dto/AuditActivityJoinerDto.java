package com.sse.ssbk.dto;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by work_pc on 2017/7/23.
 */
@Data
public class AuditActivityJoinerDto {
    @NotEmpty
    private List<Integer> userIds;
    private String reason;
    @NotBlank
    private String enrollStatus;
}
