package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by yxf on 2017/7/28.
 */
@Data
public class UserInfoDto {
    private String userPhoto;       //头像
    private String realName;
    private String jobPosition;  //职务
    private String profession;  //行业
    private String company;
    private String province;        //省市id
    private String email;
    private String education;
    private String sex;
    private String nameCard;
    private String orgcodeCertificate;
}
