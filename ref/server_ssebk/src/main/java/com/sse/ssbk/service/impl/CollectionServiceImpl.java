package com.sse.ssbk.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import tk.mybatis.mapper.entity.Example;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ActivityDao;
import com.sse.ssbk.dao.ForumQuestionDao;
import com.sse.ssbk.dao.ForumReplyDao;
import com.sse.ssbk.dao.NewsDao;
import com.sse.ssbk.dao.UserCollectActDao;
import com.sse.ssbk.dao.UserCollectNewsDao;
import com.sse.ssbk.dao.UserCollectQuestionDao;
import com.sse.ssbk.dao.UserCollectReplyDao;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.ForumReply;
import com.sse.ssbk.entity.News;
import com.sse.ssbk.entity.UserCollectAct;
import com.sse.ssbk.entity.UserCollectNews;
import com.sse.ssbk.entity.UserCollectQuestion;
import com.sse.ssbk.entity.UserCollectReply;
import com.sse.ssbk.enums.CollectionTargetType;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.CollectionService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.RedisCacheUtils;

@Slf4j
@Service
public class CollectionServiceImpl implements CollectionService {
    @Autowired
    private UserCollectActDao userCollectActDao;
    @Autowired
    private UserCollectNewsDao userCollectNewsDao;
    @Autowired
    private UserCollectReplyDao userCollectReplyDao;
    @Autowired
    private UserCollectQuestionDao userCollectQuestionDao;
    @Autowired
    private ActivityDao activityDao;
    @Autowired
    private ForumQuestionDao forumQuestionDao;
    @Autowired
    private ForumReplyDao forumReplyDao;
    @Autowired
    private NewsDao newsDao;

    @Override
    @Transactional
    public void add(Integer targetId, CollectionTargetType targetType) {
        this.validateExist(targetId, targetType);
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        Date now = new Date();
        if (CollectionTargetType.ACT.equals(targetType)) {
            Set<Integer> userCollectionActs = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_ACT + userId);
            Boolean boolUserCollectionAct = BizUtils.contains(userCollectionActs, targetId);
            if (boolUserCollectionAct) {
                log.error("用户已经收藏了" + "targetType：" + targetType + ", targetId：" + targetId);
                return;
            }

            UserCollectAct record = new UserCollectAct();
            record.setActId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            userCollectActDao.insertSelective(record);
            RedisCacheUtils.addForSet(RedisConstants.User.Collection.USER_COLLECTION_ACT + userId, targetId);
            RedisCacheUtils.incr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_ACT + targetId, 1);
        } else if (CollectionTargetType.QUESTION.equals(targetType)) {
            Set<Integer> userCollectionActs = RedisCacheUtils.getForSet(RedisConstants.COLLECTION_USER_QUESTION + userId);
            Boolean boolUserCollectionAct = BizUtils.contains(userCollectionActs, targetId);
            if (boolUserCollectionAct) {
                log.error("用户已经收藏了" + "targetType：" + targetType + ", targetId：" + targetId);
                return;
            }

            UserCollectQuestion record = new UserCollectQuestion();
            record.setQuestionId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            userCollectQuestionDao.insertSelective(record);
            RedisCacheUtils.addForSet(RedisConstants.COLLECTION_USER_QUESTION + userId, targetId);
            RedisCacheUtils.addForSet(RedisConstants.COLLECTION_QUESTION_USER + targetId, userId);
        } else if (CollectionTargetType.REPLY.equals(targetType)) {
            Set<Integer> userCollectionActs = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + userId);
            Boolean boolUserCollectionAct = BizUtils.contains(userCollectionActs, targetId);
            if (boolUserCollectionAct) {
                log.error("用户已经收藏了" + "targetType：" + targetType + ", targetId：" + targetId);
                return;
            }

            UserCollectReply record = new UserCollectReply();
            record.setReplyId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            userCollectReplyDao.insertSelective(record);
            RedisCacheUtils.addForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + userId, targetId);
            RedisCacheUtils.incr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_REPLY + targetId, 1);
        } else if (CollectionTargetType.NEWS.equals(targetType)) {
            Set<Integer> userCollectionActs = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_NEWS + userId);
            Boolean boolUserCollectionAct = BizUtils.contains(userCollectionActs, targetId);
            if (boolUserCollectionAct) {
                log.error("用户已经收藏了" + "targetType：" + targetType + ", targetId：" + targetId);
                return;
            }

            UserCollectNews record = new UserCollectNews();
            record.setNewsId(targetId);
            record.setUserId(userId);
            record.setCreateTime(now);
            RedisCacheUtils.addForSet(RedisConstants.User.Collection.USER_COLLECTION_NEWS + userId, targetId);
            RedisCacheUtils.incr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + targetId, 1);
            userCollectNewsDao.insertSelective(record);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    /**
     * 
     * @param targetId
     * @author HT-LiChuanbin 
     * @version 2017年8月16日 下午7:13:13
     */
    private void validateExist(Integer targetId, CollectionTargetType targetType) {
        if (CollectionTargetType.ACT.equals(targetType)) {
            Example example = new Example(Activity.class);
            example.createCriteria().andEqualTo("actId", targetId);
            List<Activity> activitys = activityDao.selectByExample(example);
            if (CollectionUtils.isEmpty(activitys)) {
                throw new SSEBKRuntimeException("当前活动已被取消，请刷新重试！");
            }
        } else if (CollectionTargetType.QUESTION.equals(targetType)) {
            Example example = new Example(ForumQuestion.class);
            example.createCriteria().andEqualTo("questionId", targetId).andEqualTo("isDelete", Is.NO.getCode());
            List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExample(example);
            if (CollectionUtils.isEmpty(forumQuestions)) {
                throw new SSEBKRuntimeException("当前问题已被删除，请刷新重试！");
            }
        } else if (CollectionTargetType.REPLY.equals(targetType)) {
            Example example = new Example(ForumReply.class);
            example.createCriteria().andEqualTo("replyId", targetId).andEqualTo("isDelete", Is.NO.getCode());
            List<ForumReply> forumReplys = forumReplyDao.selectByExample(example);
            if (CollectionUtils.isEmpty(forumReplys)) {
                throw new SSEBKRuntimeException("当前回答已被删除，请刷新重试！");
            }
        } else if (CollectionTargetType.NEWS.equals(targetType)) {
            Example example = new Example(News.class);
            example.createCriteria().andEqualTo("newsId", targetId);
            List<News> newss = newsDao.selectByExample(example);
            if (CollectionUtils.isEmpty(newss)) {
                throw new SSEBKRuntimeException("当前咨询已被删除，请刷新重试！");
            }
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public void del(Integer targetId, CollectionTargetType targetType) {
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        if (CollectionTargetType.ACT.equals(targetType)) {
            UserCollectAct record = new UserCollectAct();
            record.setActId(targetId);
            record.setUserId(userId);
            userCollectActDao.delete(record);
            RedisCacheUtils.delForSet(RedisConstants.User.Collection.USER_COLLECTION_ACT + userId, targetId);
            RedisCacheUtils.decr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_ACT + targetId, 1);
        } else if (CollectionTargetType.QUESTION.equals(targetType)) {
            UserCollectQuestion record = new UserCollectQuestion();
            record.setQuestionId(targetId);
            record.setUserId(userId);
            userCollectQuestionDao.delete(record);
            RedisCacheUtils.delForSet(RedisConstants.COLLECTION_USER_QUESTION + userId, targetId);
            RedisCacheUtils.delForSet(RedisConstants.COLLECTION_QUESTION_USER + targetId, userId);
            RedisCacheUtils.decr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_QUESTION + targetId, 1);
        } else if (CollectionTargetType.REPLY.equals(targetType)) {
            UserCollectReply record = new UserCollectReply();
            record.setReplyId(targetId);
            record.setUserId(userId);
            userCollectReplyDao.delete(record);
            RedisCacheUtils.delForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + userId, targetId);
            RedisCacheUtils.decr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_REPLY + targetId, 1);
        } else if (CollectionTargetType.NEWS.equals(targetType)) {
            UserCollectNews record = new UserCollectNews();
            record.setNewsId(targetId);
            record.setUserId(userId);
            userCollectNewsDao.delete(record);
            RedisCacheUtils.delForSet(RedisConstants.User.Collection.USER_COLLECTION_NEWS + userId, targetId);
            RedisCacheUtils.decr(RedisConstants.User.CollectionCount.COLLECTION_COUNT_NEWS + targetId, 1);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    /**
     * 
     * @param session
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:27:25
     */
    private void validateSession(UserSession session) {
        if (session == null || session.getUserId() == null) {
            throw new SSEBKRuntimeException(AppReturnCode.LoginOutDate);
        }
    }
}