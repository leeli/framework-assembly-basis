package com.sse.ssbk.service;

import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.vo.ForumTopicVo;
import com.sse.ssbk.vo.UserConcernVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/13.
 */

public interface UserConcernService {

    /**
     * 获取我关注的人列表接口
     * @param userId 用户id
     * @return
     */
    List<UserConcernVo> getUserConcernedMeList(String userId,String lastUserId,String pageSize);

    /**
     * 获取关注我的人列表接口
     * @param userId 用户id
     * @return
     */
    List<UserConcernVo> getIConcernedUserList(String userId,String lastUserId,String pageSize);

    int getTopicConcernNum(String topicNum);

    List<ForumTopicVo> getIconcernedTopicList(String userId,String lastTopicId,String pageSize);

    void userConcernUser( Integer userId, Integer userConcernId);

    void userConcernTopic(Integer userId,Integer userTopicId);

    void deleteConcernUser(Integer userId,Integer userTopicId);

    void deleteConcernTopic(Integer userId,Integer userTopicId);

    boolean isConcernedUser( Integer userId,Integer userConcernId);

    boolean isConcernedTopic(Integer userId,Integer userTopicId);
}
