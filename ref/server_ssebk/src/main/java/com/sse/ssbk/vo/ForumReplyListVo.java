package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class ForumReplyListVo {
    private String replyId;
    private String userId;
    private String isAnonymous;
    private String userName;
    private String userPhoto;
    private String company;
    private String content;
    private String createTime;
    private String isConcern;
    private String likeNum;
    private String commentNum;
    private String questionTitle;
    private String questionId;
    private String isLike;
    private String[] imageArr;
}