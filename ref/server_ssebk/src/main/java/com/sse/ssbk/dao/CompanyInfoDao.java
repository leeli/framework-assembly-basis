package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.CompanyInfo;

/**
 * Created by yxf on 2017/7/27.
 */
public interface CompanyInfoDao extends BaseDao<CompanyInfo> {

    void insertSelectiveWithId(CompanyInfo companyInfo);
}
