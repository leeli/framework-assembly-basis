package com.sse.ssbk.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sse.ssbk.dto.CollectionAddDto;
import com.sse.ssbk.dto.CollectionDelDto;
import com.sse.ssbk.enums.CollectionTargetType;
import com.sse.ssbk.service.CollectionService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;

@RestController
@RequestMapping("/ssebk")
public class CollectionController {
    @Autowired
    private CollectionService collectionService;

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:38
     */
    @PostMapping("/collection")
    public Result<?> add(@RequestBody @Valid CollectionAddDto dto) {
        collectionService.add(Integer.parseInt(dto.getTargetId()), CollectionTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }

    /**
     * 
     * 
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:06:41
     */
    @DeleteMapping("/collection")
    public Result<?> del(@RequestBody @Valid CollectionDelDto dto) {
        collectionService.del(Integer.parseInt(dto.getTargetId()), CollectionTargetType.parse(dto.getTargetType()));
        return ResultUtils.success();
    }
}