package com.sse.ssbk.service;

import com.sse.ssbk.dto.CompanyInfoDto;
import com.sse.ssbk.entity.CompanyInfo;

/**
 * Created by yxf on 2017/7/27.
 */
public interface CompanyInfoService {

    void addCompanyInfo(CompanyInfoDto companyInfoDto);

    int updateCompanyInfo(CompanyInfoDto companyInfoDto,Integer finanicalId);

    CompanyInfo selectCompanyInfoById(Integer companyId);
}
