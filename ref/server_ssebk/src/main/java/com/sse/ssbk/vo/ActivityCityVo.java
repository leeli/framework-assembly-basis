package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
public class ActivityCityVo {
    private String cityId;
    private String cityName;
    private String actNum;
}
