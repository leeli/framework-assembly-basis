package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
public class ForumTopicVo {
	private String topicId;
	private String topicName;
	private String imgUrl;
	//关注数
	private String concernNum;
	//该板块下的问题数
	private String questionNum;
}
