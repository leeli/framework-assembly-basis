package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

/**
 * Created by yxf on 2017/7/13.
 */
@Data
@Entity
@Table(name = "t_user_concern_user")
public class UserConcernUser {
    @Column(name = "user_id")
    private Integer userId; //用户id
    @Column(name = "concern_user_id")
    private Integer concernUserId; //关注的用户id
    @Column(name = "create_time")
    private Date createTime;
}