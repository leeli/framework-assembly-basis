package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
public class UserEnrollVo {
    private String userId;
    private String realName;
    private String company;
    private String userPhoto;
    private String isConcern;
    private String enrollStatus;
    private String rejectReason;

    public UserEnrollVo(){
        this.rejectReason = "";
    }
}
