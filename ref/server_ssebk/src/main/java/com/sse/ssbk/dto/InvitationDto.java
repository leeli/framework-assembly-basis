package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by work_pc on 2017/8/8.
 */
@Data
public class InvitationDto {
    /*private Integer userId;
    private String userName;*/
    @NotBlank
    private String phone;
    @NotBlank
    private String invitationCode;
    @NotBlank
    private String toUserRealName;
    private String status;
    private String message;
}
