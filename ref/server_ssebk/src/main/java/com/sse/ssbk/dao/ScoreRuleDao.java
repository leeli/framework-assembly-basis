package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ScoreRule;

public interface ScoreRuleDao extends BaseDao<ScoreRule> {
}