package com.sse.ssbk.vo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
public class ActSubjectVo {
    private String subjectId;
    private String subjectName;
    private String picture;
}
