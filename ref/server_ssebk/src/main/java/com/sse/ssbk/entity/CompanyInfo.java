package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
@Entity
@Table(name = "t_company_info")
public class CompanyInfo {
    @Id
    @Column(name = "company_id")
    private Integer companyId;
    @Column(name = "company_name")
    private String company;
    @Column(name = "chairman")
    private String president;
    @Column(name = "regist_address")
    private String registAddress;
    @Column(name = "contact_user_name")
    private String linkMan;
    @Column(name = "phone")
    private String telephone;
    @Column(name = "mail_address")
    private String address;
    @Column(name = "main_business")
    private String business;
    @Column(name = "market_share")
    private String markOccupancy;
    @Column(name = "industry_rank")
    private String industryRanking;
    @Column(name = "total_assets")
    private String totalAssets;
    @Column(name = "net_assets")
    private String netAssets;
    @Column(name = "before_total_stock")
    private String beforeEquity;
    @Column(name = "after_total_stock")
    private String releaseEquity;
    @Column(name = "last_year_taxed_profit")
    private String lastYearProfits;
    @Column(name = "predict_taxed_profit")
    private String predictProfits;
    @Column(name = "predict_publish_time")
    private String expectedIssueTime;
    @Column(name = "total_finance_amount")
    private String fundingAmount;
    @Column(name = "agency")
    private String intermediaryInstitutions;
    @Column(name = "progress")
    private String listedProgress;
}
