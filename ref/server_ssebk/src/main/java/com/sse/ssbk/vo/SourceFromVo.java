package com.sse.ssbk.vo;

import lombok.Data;

import java.util.Date;

/**
 * Created by yxf on 2017/7/19.
 */
@Data
public class SourceFromVo {
    private Integer sourceFromId;
    private String sourceName;
    private Integer sort;
}
