package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
public class ActivitySubjectVo {
    private String subjectId;
    private String subjectName;
    private String picture;
    private String actNum;
}
