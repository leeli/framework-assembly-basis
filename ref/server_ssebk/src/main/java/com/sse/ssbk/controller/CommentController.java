package com.sse.ssbk.controller;

import javax.validation.Valid;

import com.sse.ssbk.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.dto.CommentAddDto;
import com.sse.ssbk.dto.CommentsDto;
import com.sse.ssbk.enums.CommentTargetType;
import com.sse.ssbk.enums.ScoreRules;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.CommentService;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.vo.CommentsAdapterVo;

@RestController
@RequestMapping("/ssebk")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @PostMapping("/comment")
    @CountScore(ScoreRules.ADD_COMMNET)
    @SendNotice
    public Result<?> add(@RequestBody @Valid CommentAddDto dto) {
        return ResultUtils.success(commentService.add(dto));
    }

    @GetMapping("/comments")
    public Result<CommentsAdapterVo> list(String data) {
        CommentsDto dto = this.getCommentsDto(data);
        Integer targetId = Integer.parseInt(dto.getTargetId());
        String targetType = dto.getTargetType();
        String lastId = dto.getLastId();
        String pageSize = dto.getPageSize();
        Page page = BizUtils.getBizPage(lastId, pageSize);
        CommentsAdapterVo voCommentsAdapter = commentService.list(page, targetId, CommentTargetType.parse(targetType));
        return ResultUtils.success(voCommentsAdapter);
    }

    /**
     * 
     * @param data
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月25日 下午1:34:18
     */
    private CommentsDto getCommentsDto(String data) {
        CommentsDto dto = new CommentsDto();
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jData = (JSONObject) JSON.parse(data);
        String lastId = jData.getString("lastId");
        String pageSize = jData.getString("pageSize");
        String targetId = jData.getString("targetId");
        String targetType = jData.getString("targetType");
        dto.setTargetId(targetId);
        dto.setTargetType(targetType);
        dto.setLastId(lastId);
        dto.setPageSize(pageSize);
        return dto;
    }
}