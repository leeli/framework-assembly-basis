
/*
* FileName：ServiceUtil.java
*
* Description：接口请求类
*
* History：
* 版本号 作者 日期 简介
* 1.0 knogxiangrun 2015-09-09 Create
*/

package com.sse.ssbk.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sun.net.httpserver.Headers;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.*;

public class ServiceUtil {



    //日志
    private static org.slf4j.Logger log = LoggerFactory.getLogger(ServiceUtil.class);


    /***
     * 做post请求
     * @param url
     * @param params
     * @return
     */
    public static String DoHttpPost(String url, Map<String, Object> params) throws Exception {

        HttpClient httpclient = new DefaultHttpClient();

        try {

            if (url.startsWith("https")) {
                httpclient = new SSLClient();
            }


            // 构造一个post对象
            HttpPost httpPost = new HttpPost(url);
            // 添加所需要的post内容

            List<NameValuePair> nvps = new ArrayList<NameValuePair>();

            params.remove("ip");
            params.remove("userAgent");
            Iterator<Map.Entry<String, Object>> it = params.entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, Object> e = it.next();
                nvps.add(new BasicNameValuePair(e.getKey(), e.getValue().toString()));
            }


            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            // 发送验证码特殊处理
            if(url.contains("ComInfoServer/authImage.do")){
                String cookie = params.get("session") + "";
                httpPost.setHeader("Cookie",cookie);
                params.remove("session");
            }


            Date st = new Date();
            StringBuffer log_sb = new StringBuffer();
            log_sb.append("msgId:");
            log_sb.append(params.get("msgId"));
            log_sb.append("      ");
            log_sb.append("URL:");
            log_sb.append(url);
            log_sb.append("?");
            log_sb.append(JoinParm(params));
            log_sb.append("      ");
            log_sb.append("starttime:");
            log_sb.append(Util.getDate(st, "yyyy-MM-dd HH:mm:ss.SSS"));
            log_sb.append("      ");

            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            int statuscode = response.getStatusLine().getStatusCode();
            String result = EntityUtils.toString(entity, "utf-8");


            Date et = new Date();
            log_sb.append("endtime:");
            log_sb.append(Util.getDate(et, "yyyy-MM-dd HH:mm:ss.SSS"));
            log_sb.append("      ");
            log_sb.append("calltime:");
            log_sb.append(et.getTime() - st.getTime());
            log_sb.append("      ");
            log_sb.append("statusCode:");
            log_sb.append(statuscode);
            log_sb.append("      ");
            if (params.containsKey("sr")||statuscode != 200) {
                log_sb.append("result:");
                log_sb.append(result);
                log_sb.append("      ");
            }
            log.warn(log_sb.toString());
            httpPost.abort();
            return result;
        }finally {
            httpclient.getConnectionManager().shutdown();
        }
    }


    /***
     * 做get请求
     * @param url
     * @param params
     * @return
     */
    public  static String DoHttpGet(String url, Map<String, Object> params, String method) throws Exception {

        HttpClient httpclient = new DefaultHttpClient();
        try {

            if(url.startsWith("https")){
                httpclient=new SSLClient();
            }




            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            String ip=null;
            String userAgent=null;
            if(params.get("userAgent")!=null){
            	userAgent=params.get("userAgent").toString();
           }
            if(params.get("ip")!=null){
            	 ip=params.get("ip").toString();
            }
            params.remove("ip");
            params.remove("userAgent");
            Iterator<Map.Entry<String,Object>> it= params.entrySet().iterator();

            while (it.hasNext()){
                Map.Entry<String,Object> e= it.next();
                nvps.add(new BasicNameValuePair(e.getKey(),Util.getObjStrV(e.getValue())));
            }

           //图片解析单独处理(图片解析接口不用重新拼接url)
            if(!method.equals("parsePicture"))
            {
            	url+="?"+EntityUtils.toString(new UrlEncodedFormEntity(nvps,"utf-8"));
            }

            Date st= new Date();
            StringBuffer log_sb=new StringBuffer();
            log_sb.append("msgId:");
            log_sb.append(params.get("msgId"));
            log_sb.append("      ");
            log_sb.append("URL:");
            log_sb.append(url);
            log_sb.append("      ");
            log_sb.append("starttime:");
            log_sb.append(Util.getDate(st,"yyyy-MM-dd HH:mm:ss.SSS"));
            log_sb.append("      ");

            // 构造一个get对象
            HttpGet httpGet=new HttpGet(url);
            if(ip!=null){
//            	httpGet.setHeader("Proxy-Client-IP", ip);
            	httpGet.setHeader("APP-IP",ip);
            }
            if(userAgent!=null){
            	httpGet.setHeader("User-Agent", userAgent);
            }
            HttpResponse response = httpclient.execute(httpGet);
            Header[] headers = response.getHeaders("Set-Cookie");

            HttpEntity entity = response.getEntity();
            int statuscode=response.getStatusLine().getStatusCode();
            String result=EntityUtils.toString(entity, "utf-8");

            Date et= new Date();
            log_sb.append("endtime:");
            log_sb.append(Util.getDate(et,"yyyy-MM-dd HH:mm:ss.SSS"));
            log_sb.append("      ");
            log_sb.append("calltime:");
            log_sb.append(et.getTime()-st.getTime());
            log_sb.append("      ");
            log_sb.append("statusCode:");
            log_sb.append(statuscode);
            log_sb.append("      ");
            if(params.containsKey("sr")||statuscode!=200){
                log_sb.append("result:");
                log_sb.append(result);
                log_sb.append("      ");
            }
            log.warn(log_sb.toString());

            // 获取图形验证码特殊处理
            if(url.contains("ComInfoServer/getAuthImage.do")){
                Header cookieHeader = headers[0];
                String cookieValue = cookieHeader.getValue();
                int index = cookieValue.indexOf("JSESSIONID");
                String sessionStr = cookieValue.substring(index,index + 43);
                result = "{\"session\":\"" + sessionStr + "\",\"picCaptcha\":\"" + result + "\"}";
            }

            httpGet.abort();
            System.out.println("result:"+result);
            return result;
        }finally {
            httpclient.getConnectionManager().shutdown();
        }
    }

    /***
     * 拼接map参数
     * @param params
     * @return
     */
    public static String JoinParm(Map<String, Object> params){

        Iterator iter = params .entrySet().iterator();
        StringBuffer sb=new StringBuffer();
        while (iter.hasNext()){
            Map.Entry e = (Map.Entry)iter.next();

            String key =Util.getObjStrV(e.getKey());
            String val = Util.getObjStrV(e.getValue());


            try {
                key = URLEncoder.encode(key, "utf-8");
                val = URLEncoder.encode(val, "utf-8");
            } catch (UnsupportedEncodingException ee) {
                ee.printStackTrace();
            }

            sb.append(key);
            sb.append("=");
            sb.append(val);
            if(iter.hasNext()){
                sb.append("&");
            }
        }
        return sb.toString();
    }
}
