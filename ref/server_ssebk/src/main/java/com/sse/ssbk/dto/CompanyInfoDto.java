package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
public class CompanyInfoDto {
    @NotBlank
    private String company;
    @NotBlank
    private String president;
    @NotBlank
    private String registAddress;
    @NotBlank
    private String linkMan;
    @NotBlank
    private String telephone;
    @NotBlank
    private String address;
    @NotBlank
    private String business;
    @NotBlank
    private String markOccupancy;
    @NotBlank
    private String industryRanking;
    @NotBlank
    private String totalAssets;
    @NotBlank
    private String netAssets;
    @NotBlank
    private String beforeEquity;
    @NotBlank
    private String releaseEquity;
    @NotBlank
    private String lastYearProfits;
    @NotBlank
    private String predictProfits;
    @NotBlank
    private String expectedIssueTime;
    @NotBlank
    private String fundingAmount;
    @NotBlank
    private String intermediaryInstitutions;
    @NotBlank
    private String listedProgress;
}
