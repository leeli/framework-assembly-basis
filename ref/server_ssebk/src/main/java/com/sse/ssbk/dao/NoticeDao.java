package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Notice;

/**
 * Created by work_pc on 2017/8/18.
 */
public interface NoticeDao extends BaseDao<Notice> {
}
