package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/7/31.
 */
@Data
public class CollectQuestionVo {
    private String questionId;
    private String userId;
    private String userName;
    private String userPhoto;
    private String company;
    private String title;
    private String isAnonymous;
    private String replyNum;   //回复数
    private String isCollect;
    private String createTime;
    private String isConcern;
    //新增
    private String likeNum; //赞同数
}
