package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_forum_reply")
public class ForumReply {
    @Id
    @Column(name = "reply_id")
    private Integer replyId;
    @Column(name = "content")
    private String content;
    @Column(name = "images")
    private String images;
    @Column(name = "is_anonymous")
    private String isAnonymous;
    @Column(name = "question_id")
    private Integer questionId;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_photo")
    private String userPhoto;
    @Column(name = "company_id")
    private Integer companyId;
    @Column(name = "company_name")
    private String companyName;
    @Column(name = "is_delete")
    private String isDelete;
    @Column(name = "delete_time")
    private Date deleteTime;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
}