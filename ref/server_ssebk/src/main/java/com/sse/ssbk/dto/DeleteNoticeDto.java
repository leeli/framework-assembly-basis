package com.sse.ssbk.dto;

import com.sse.ssbk.utils.SendNotice;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by work_pc on 2017/8/18.
 */
@Data
public class DeleteNoticeDto {
    @NotNull
    private String noticeId;
    @NotBlank
    private String isSystemNotice;
}
