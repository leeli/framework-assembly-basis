package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ActSubject;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.vo.ActSubjectVo;
import com.sse.ssbk.vo.ActivityCityVo;
import com.sse.ssbk.vo.ActivitySubjectVo;
import com.sse.ssbk.vo.CircleActVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by work_pc on 2017/7/15.
 */
public interface ActivitySubjectDao extends BaseDao<ActSubject> {

	/**
	 * 2017.8.9 废弃
	 * 获取主题列表
	 * @param lastSubjectId
	 * @param pageSize
	 * @return
	 */
	@Deprecated
	List<ActSubject> getActSubjects(@Param("lastSubjectId") Integer lastSubjectId, @Param("pageSize") Integer pageSize);


	/**
	 * 通过id获取主题列表
	 * @param columns
	 * @return
	 */
	List<ActSubject> getActSubejectsInColumns(@Param("columns")List<Integer> columns);
	/**
	 * 主题活动查询
	 * @param lastSubjectId
	 * @param pageSize
	 * @return
	 */
	List<Integer> getActivitySubjectsColumnInOrder(@Param("lastSubjectId") Integer lastSubjectId, @Param("pageSize") Integer pageSize);

	List<ActivitySubjectVo> getActivitySubjects(@Param("columns") List<Integer> columns);



	/**
	 * 更多区域活动查询接口
	 * @param lastCityId
	 * @param pageSize
	 * @return
	 */
	List<ActivityCityVo> getActivityCity(@Param("lastCityId") Integer lastCityId, @Param("pageSize") Integer pageSize);


}
