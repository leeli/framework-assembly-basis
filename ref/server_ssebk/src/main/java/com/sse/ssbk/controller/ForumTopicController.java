package com.sse.ssbk.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ForumTopicService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.ForumTopicsDto;
import com.sse.ssbk.vo.ForumTopicsVo;

/**
 * 板块控制器.
 * @author  HT-LiChuanbin 
 * @version 2017年7月26日 下午7:16:17
 */
@RestController
@RequestMapping("/ssebk/forum")
public class ForumTopicController {
    @Autowired
    private ForumTopicService forumTopicService;

    /**
     * 
     * @param data
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午7:39:23
     */
    @GetMapping("/topics")
    public Result<List<ForumTopicsVo>> list(String data) {
        ForumTopicsDto dto = this.getForumQuestionListDto(data);
        String lastId = dto.getLastId();
        String pageSize = dto.getPageSize();
        String topicName = dto.getTopicName();
        Page page = BizUtils.getBizPage(lastId, pageSize);
        boolean boolTopicName = StringUtils.isBlank(topicName);

        List<ForumTopicsVo> voForumTopics = new ArrayList<ForumTopicsVo>();
        if (boolTopicName) {
            voForumTopics = forumTopicService.list(page);
        } else {
            voForumTopics = forumTopicService.listByTopicName(topicName, page);
        }
        return ResultUtils.success(voForumTopics);
    }

    /**
     * 
     * @param data
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月1日 下午4:38:14
     */
    private ForumTopicsDto getForumQuestionListDto(String data) {
        ForumTopicsDto dto = new ForumTopicsDto();
        if (data == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        JSONObject jData = (JSONObject) JSON.parse(data);
        String topicName = jData.getString("topicName");
        String lastId = jData.getString("lastId");
        String pageSize = jData.getString("pageSize");
        dto.setTopicName(topicName);
        dto.setLastId(lastId);
        dto.setPageSize(pageSize);
        return dto;
    }
}