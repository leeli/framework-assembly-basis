package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.ForumReply;
import lombok.Data;

@Data
public class ForumReplyGetVo {
    /* 板块信息 */
    private List<ForumReplyGetForumTopicVo> topics;

    /* 问题相关字段 */
    private String questionId;
    private String questionUserName;
    private String questionUserPhoto;
    private String questionCompany;
    private String questionUserId;
    private String questionTitle;
    private String questionUserIsConcern;
    private Integer questionReplyNum;

    /* 回答相关字段 */
    private String replyId;
    private String replyUsername;
    private String replyUserPhoto;
    private String replyCompany;
    private String replyUserId;
    private String replyUserIsConcern;
    private String replyContent;
    private String replyCreateTime;
    private Integer replyLikeNum;
    private Integer replyCommentNum;
    private Integer replyCollectNum;
    private String[] imageArr;

    /* 和当前用户相关字段. */
    private String isLike;
    private String isCollect;

    public ForumReplyGetVo() {
        this.questionId = "";
        this.questionUserName = "";
        this.questionUserPhoto = "";
        this.questionCompany = "";
        this.questionUserId = "";
        this.questionTitle = "";
        this.questionUserIsConcern = "";
        this.replyId = "";
        this.questionReplyNum = 0;
        this.replyUsername = "";
        this.replyUserPhoto = "";
        this.replyCompany = "";
        this.replyUserId = "";
        this.replyUserIsConcern = "";
        this.replyContent = "";
        this.replyCreateTime = "";
        this.replyLikeNum = 0;
        this.replyCommentNum = 0;
        this.replyCollectNum = 0;
        this.isLike = "";
        this.isCollect = "";
        this.topics = new ArrayList<ForumReplyGetForumTopicVo>();
    }

    public void  setForumReplyGetVoFromForumReply(ForumReply forumReply){
        this.replyId = String.valueOf(forumReply.getReplyId());
        this.replyUsername = String.valueOf(forumReply.getUserName() == null?"":forumReply.getUserName());
        this.replyUserPhoto = forumReply.getUserPhoto();
        this.replyCompany = forumReply.getCompanyName() == null ?"":forumReply.getCompanyName();
        this.replyUserId = String.valueOf(forumReply.getUserId());
        this.replyContent = forumReply.getContent();
        this.replyContent = forumReply.getContent();
        this.replyCreateTime = String.valueOf(forumReply.getCreateTime().getTime());
    }

    public void  setForumReplyGetVoFromForumQuestion(ForumQuestion forumQuestion){
        this.questionId = String.valueOf(forumQuestion.getQuestionId());
        this.questionUserName = forumQuestion.getUserName();
        this.questionUserPhoto = forumQuestion.getUserPhoto();
        this.questionCompany = forumQuestion.getCompanyName();
        this.questionUserId = String.valueOf(forumQuestion.getUserId()== null?"":forumQuestion.getUserId());
        this.questionTitle = forumQuestion.getTitle();
    }
}