package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by ciyua on 2017/8/3.
 */
@Data
public class RegisterVo {

    private String isRegister;
}
