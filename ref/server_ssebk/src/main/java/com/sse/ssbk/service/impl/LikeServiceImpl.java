package com.sse.ssbk.service.impl;

import org.springframework.stereotype.Service;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.enums.LikeTargetType;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.LikeService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.RedisCacheUtils;

@Service
public class LikeServiceImpl implements LikeService {
    @Override
    public void add(Integer targetId, LikeTargetType targetType) {
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        if (LikeTargetType.REPLY.equals(targetType)) {
            RedisCacheUtils.addForSet(RedisConstants.LIKE_USER_REPLY + userId, targetId);
            RedisCacheUtils.addForSet(RedisConstants.LIKE_REPLY_USER + targetId, userId);
        } else if (LikeTargetType.REPLYCOMMENT.equals(targetType)) {
            // RedisCacheUtils.incr(RedisConstants.LIKE_COMMENT_USER + targetId, userId);

            RedisCacheUtils.addForSet(RedisConstants.LIKE_USER_REPLYCOMMENT + userId, targetId);
            RedisCacheUtils.addForSet(RedisConstants.LIKE_REPLYCOMMENT_USER + targetId, userId);
        } else if (LikeTargetType.QUESTION.equals(targetType)) {
            RedisCacheUtils.addForSet(RedisConstants.LIKE_USER_QUESTION + userId, targetId);
            RedisCacheUtils.addForSet(RedisConstants.LIKE_QUESTION_USER + targetId, userId);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public void del(Integer targetId, LikeTargetType targetType) {
        UserSession session = BizUtils.getCurrentUser();
        this.validateSession(session);
        Integer userId = session.getUserId();
        if (LikeTargetType.REPLY.equals(targetType)) {
            RedisCacheUtils.delForSet(RedisConstants.LIKE_USER_REPLY + userId, targetId);
            RedisCacheUtils.delForSet(RedisConstants.LIKE_REPLY_USER + targetId, userId);
        } else if (LikeTargetType.REPLYCOMMENT.equals(targetType)) {
            // RedisCacheUtils.decr(RedisConstants.LIKE_COMMENT_USER + targetId, userId);

            RedisCacheUtils.delForSet(RedisConstants.LIKE_USER_REPLYCOMMENT + userId, targetId);
            RedisCacheUtils.delForSet(RedisConstants.LIKE_REPLYCOMMENT_USER + targetId, userId);
        } else if (LikeTargetType.QUESTION.equals(targetType)) {
            RedisCacheUtils.delForSet(RedisConstants.LIKE_USER_QUESTION + userId, targetId);
            RedisCacheUtils.delForSet(RedisConstants.LIKE_QUESTION_USER + targetId, userId);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    /**
     * @param session
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:27:25
     */
    private void validateSession(UserSession session) {
        if (session == null || session.getUserId() == null) {
            throw new SSEBKRuntimeException(AppReturnCode.LoginOutDate);
        }
    }
}