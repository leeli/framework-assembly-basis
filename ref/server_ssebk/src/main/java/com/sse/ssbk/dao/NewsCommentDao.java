package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.NewsComment;

public interface NewsCommentDao extends BaseDao<NewsComment> {
}