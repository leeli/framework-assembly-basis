package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ScoreBlotter;

/**
 * Created by work_pc on 2017/8/10.
 */
public interface ScoreBlotterDao extends BaseDao<ScoreBlotter> {
}
