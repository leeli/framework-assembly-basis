package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by ciyua on 2017/8/8.
 */
@Data
public class SmsCaptchaVo {

    private String isPhoneExist; // 0:存在;1:不存在;
}
