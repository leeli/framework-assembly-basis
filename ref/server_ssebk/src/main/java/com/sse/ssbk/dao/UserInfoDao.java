package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.UserInfo;

/**
 * Created by yxf on 2017/7/28.
 */
public interface UserInfoDao extends BaseDao<UserInfo>{

}
