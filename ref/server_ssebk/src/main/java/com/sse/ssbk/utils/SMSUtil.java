package com.sse.ssbk.utils;

/**
 * Created by work_pc on 2017/8/17.
 */
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;


public class SMSUtil {

    private static Logger logger = Logger.getLogger(SMSUtil.class);

    public static void sendSMS(String phoneNums, String content) {

        logger.info("短信通知");
        logger.info("短信内容:"+content);
        logger.info("手机号码:"+phoneNums);

        if(phoneNums !=null && !"".equals(phoneNums)){
            HttpClient httpClient = new HttpClient();
            httpClient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");
            /*final String smsUrl = Config.getValue("message_url");
            final String smsUser = Config.getValue("message_user");
            final String smsPwd = Config.getValue("message_pwd");*/
            final String smsUrl = "http://10.10.11.60/msg/newmessage.do";
            final String smsUser = "app2";
            final String smsPwd = "app1";
            PostMethod postMethod = new PostMethod(smsUrl);
            NameValuePair[] data = {new NameValuePair("content", content),
                    new NameValuePair("type", "sendMsg"),
                    new NameValuePair("userName", smsUser),
                    new NameValuePair("userPassword", smsPwd),
                    new NameValuePair("phoneNums", phoneNums),
                    new NameValuePair("productId", "724"),
                    new NameValuePair("channelId", "0"),
                    new NameValuePair("sendTime", null),
                    new NameValuePair("needReport", "1")};
            logger.info(Arrays.toString(data));

            postMethod.setRequestBody(data);
            try {
                int statusCode = httpClient.executeMethod(postMethod);
                if (statusCode != HttpStatus.SC_OK) {
                    logger.error("msg send failed: " + postMethod.getStatusLine());
                }
                // 读取内容
                byte[] responseBody = postMethod.getResponseBody();
                // 处理内容
                String html = new String(responseBody);
                logger.error(html);
            } catch (Exception e) {
                logger.error(e);
            }
            postMethod.releaseConnection();
        }else{
            logger.error("手机号码为空，不发送短信");
        }

    }


    public static void main(String[] args) {
        SMSUtil.sendSMS("17602109057", "hello");
    }
}
