package com.sse.ssbk.vo;

import com.sse.ssbk.utils.TransIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by work_pc on 2017/7/15.
 */

@Data
public class ActivityDetailsVo {
    private String actId;
    private String actName;
    private String describ;  //活动简介
    private String status;  //活动状态0准备1进行2结束3取消
    private String address;
    private String[] actImages;   //活动图片
    private String actPoster;   //活动海报
    private String startTime;
    private String endTime;
    private String cityId;
    private String cityName;
    private String subjectName;
    private String ownerUserId;
    private String ownerUserName;
    //新增
    private String ownerRealName;
    private String ownerUserPhoto;
    private String ownerCompany;
    private String ownerIsConcern;   //是否关注活动发布者
    private String isCollect;
    /**
     * 0=未报名
     *1=报名审核中
     *2=报名成功
     *3=报名审核不通过
     */
    private String enrollStatus;
    /**
     * 报名/参与人人数:
     *活动报名中为报名人数；
     *活动进行中&结束为参与人数
     */
    private String actPersonCount;
    private String auditWaitCount;  //待审核人数
    private String auditNotCount;  //审核不通过人数
    private String auditPassCount;  //审核通过人数
    private String commentCount;  //评论数
    private String isAudit;  //是否需要审核 0=不需要，1=需要审核
    private String actType;  //活动类型：1=新活动，2=活动回顾
    private String isPublicActor; //是否公开参与者
}
