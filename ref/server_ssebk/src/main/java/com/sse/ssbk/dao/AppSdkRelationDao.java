package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.AppSdkRelation;
import com.sse.ssbk.vo.AppSdkRelationVo;
import org.apache.ibatis.annotations.Param;

/**
 * Created by ciyua on 2017/8/2.
 */
public interface AppSdkRelationDao extends BaseDao<AppSdkRelation> {

    AppSdkRelationVo selectByBundleIdSdkId(@Param("bundleId")String bundleId, @Param("sdkId")String sdkId);
}
