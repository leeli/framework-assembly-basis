package com.sse.ssbk.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import lombok.Data;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.common.BaseInfo;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;

/**
 * 处理业务工具类.
 * @author  HT-LiChuanbin 
 * @version 2017年7月20日 下午3:45:17
 */
public class BizUtils {
    @Data
    public class Page {
        private Integer lastId;
        private Integer pageSize;
    }

    /**
     * 
     * @param sets
     * @param value
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月2日 下午1:51:55
     */
    public static boolean contains(Set<Integer> sets, Integer value) {
        if (sets == null || value == null) {
            return false;
        }
        for (Integer setValue : sets) {
            if (setValue.equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月28日 下午6:16:42
     */
    public static UserSession getCurrentUser() {
        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String base = req.getHeader("base");
        if (StringUtils.isBlank(base)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        BaseInfo baseInfo = getBaseInfo();
        String token = baseInfo.getToken();
        return (UserSession) RedisCacheUtils.getForValue(token);
    }

    /**
     *
     * @return
     */
    public static String getCurrentToken() {
        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String base = req.getHeader("base");
        if (StringUtils.isBlank(base)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        BaseInfo baseInfo = getBaseInfo();
        String token = baseInfo.getToken();
        return token;
    }

    /**
     * 
     * @param data
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月25日 下午1:34:18
     */
    public static BaseInfo getBaseInfo() {
        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String base;
        try {
            base = URLDecoder.decode(req.getHeader("base"), "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError.getMsg(), e);
        }
        if (base == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        BaseInfo baseInfo = new BaseInfo();
        JSONObject jBaseInfo = (JSONObject) JSON.parse(base);
        String deviceId = jBaseInfo.getString("deviceId");
        String token = jBaseInfo.getString("token");
        String OSType = jBaseInfo.getString("OSType");
        String deviceType = jBaseInfo.getString("deviceType");
        String version = jBaseInfo.getString("version");

        boolean boolDeviceId = !StringUtils.isBlank(deviceId);
        boolean boolToken = !StringUtils.isBlank(token);
        boolean boolOSType = !StringUtils.isBlank(OSType);
        boolean booldeviceType = !StringUtils.isBlank(deviceType);
        boolean boolVersion = !StringUtils.isBlank(version);
        if (!(boolDeviceId && boolToken && boolOSType && booldeviceType && boolVersion)) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }

        baseInfo.setDeviceId(deviceId);
        baseInfo.setToken(token);
        baseInfo.setOSType(OSType);
        baseInfo.setDeviceType(deviceType);
        baseInfo.setVersion(version);
        return baseInfo;
    }

    /**
     * 获取业务与分页信息.
     * @param lastId
     * @param pageSize
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月24日 下午3:42:16
     */
    public static Page getBizPage(String lastId, String pageSize) {
        BizUtils.Page page = new BizUtils().new Page();
        if (StringUtils.isBlank(lastId) || Integer.parseInt(lastId) < 1) {
            page.setLastId(-1);
        } else {
            page.setLastId(Integer.parseInt(lastId));
        }
        if (StringUtils.isBlank(pageSize)) {
            page.setPageSize(20);
        } else {
            page.setPageSize(Integer.parseInt(pageSize));
        }
        return page;
    }

    /**
     * 图片字符串数组组装成"url1,url2"的格式.
     * @param images 图片字符串数组.
     * @return
     * @author       HT-LiChuanbin 
     * @version      2017年7月20日 下午3:43:46
     */
    public static String getStrImages(String[] images) {
        String strImages = null;
        if (images != null && images.length != 0) {
            strImages = "";
            for (String image : images) {
                strImages += image + ",";
            }
            strImages = strImages.substring(0, strImages.length() - 1);
        } else if (images != null && images.length == 0) {
            strImages = "";
        }
        return strImages;
    }

    /**
     * 
     * @param images
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午5:17:48
     */
    public static List<String> getArrImages(String images) {
        List<String> imageArr = new ArrayList<String>();
        if (StringUtils.isBlank(images)) {
            return new ArrayList<String>();
        }
        for (String image : images.split(",")) {
            imageArr.add(image);
        }
        return imageArr;
    }

    /**
     * 图片字符串转换为字符串数组 entity to vo
     * @param images
     * @return
     */
    public static String[] toStringArray(String images) {
        return images.split(",");
    }
}