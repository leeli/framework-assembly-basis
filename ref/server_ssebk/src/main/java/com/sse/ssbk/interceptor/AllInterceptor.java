package com.sse.ssbk.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sse.ssbk.service.AppSdkRelationService;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.AppSdkRelationVo;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.secret.NativeEnAndDeAPI;
import com.sse.ssbk.utils.StringUtil;
import com.sse.ssbk.utils.Util;

/**
 * 加密拦截器
 * @version 2017年7月18日 下午5:38:11
 */
public class AllInterceptor extends HandlerInterceptorAdapter {

	// 日志
	private static org.slf4j.Logger log = LoggerFactory.getLogger(AllInterceptor.class);

	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Autowired
	private AppSdkRelationService appSdkRelationService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		// TODO 验证用户登录状态 chekUser

		BaseController baseController = null;
		Object controller = null;
		if (HandlerMethod.class.equals(handler.getClass())) {
			// 获取controller，判断是不是实现登录接口的控制器
			HandlerMethod method = (HandlerMethod) handler;
			controller = method.getBean();
		}

		if (controller instanceof BaseController) {

			// POST/PUT/DELETE 直接返回
			if(!"GET".equals(request.getMethod())){
				return true;
			}

			String base = request.getHeader("base");
			String data = request.getParameter("data");

			log.info("RemoteIP:" + Util.getRemortIP(request));
			baseController = (BaseController) controller;
			response.setContentType(CONTENT_TYPE);
			log.info("解密前base:" + base);
			log.info("解密前data:" + data);

			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> mapBase = null;
			Map<String, Object> mapData = null;
			String strBase = null;
			String strData = null;

			// 解密部分
			try {
				if (base == null || data == null) {
					baseController.setFail(AppReturnCode.ParaError);
					WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
					return false;
				}

				strBase = NativeEnAndDeAPI.decrypt_data(base);
				strData = NativeEnAndDeAPI.decrypt_data(data);
				log.info("base解密后:" + strBase);
				log.info("data解密后:" + strData);
				if (strBase == null || strData == null) {
					baseController.setFail(AppReturnCode.DecryptError);
					WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (StringUtil.isNotEmpty(strBase) && StringUtil.isNotEmpty(strData)) {
				try {
					mapBase = mapper.readValue(strBase, Map.class);
					mapData = mapper.readValue(strData, Map.class);

					// :检查签名是否正确，要取出securitykey
					// 验证签名
					String sId = mapBase.get("sid") != null ? mapBase.get("sid").toString() : null;
					String bundleId = mapBase.get("bid") != null ? mapBase.get("bid").toString() : null;
					if (StringUtil.isEmpty(sId) || StringUtil.isEmpty(bundleId)) {
						baseController.setFail(AppReturnCode.ParaReqMiss);
						WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
						return false;
					}
					String key = sId + "_" + bundleId;
					String securitykey = Util.getObjStrV(RedisCacheUtils.getForValue(key));

					if (StringUtil.isEmpty(securitykey)) {
						AppSdkRelationVo appSdkRelationVo = new AppSdkRelationVo();
						appSdkRelationVo=appSdkRelationService.isExistAppSDK(bundleId, sId);
						if(appSdkRelationVo==null){
							baseController.setFail(AppReturnCode.NoExist);
							WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
							return false;
						}
						securitykey=appSdkRelationVo.getSecurityKey();
						RedisCacheUtils.setForValue(key, securitykey);
						// base&data验签
						if (NativeEnAndDeAPI.check_signeddata(base, securitykey, strBase) != 1
								|| NativeEnAndDeAPI.check_signeddata(data, securitykey, strData) != 1) {
							baseController.setFail(AppReturnCode.signedError);
							WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
							return false;
						}
					}
					request.setAttribute("SECURITYKEY", securitykey);

					baseController.setBaseParamMap(mapBase);
					baseController.setDataParamMap(mapData);
				} catch (Exception e) {
					baseController.setFail(AppReturnCode.ParaError);
					WriteResult(response, new JSONObject(baseController.getResultMap()).toJSONString());
					e.printStackTrace();
					return false;
				}
			} else {
				baseController.setBaseParamMap(null);
				baseController.setDataParamMap(null);
			}
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

		BaseController baseController = null;
		Object controller = null;
		if (HandlerMethod.class.equals(handler.getClass())) {
			// 获取controller，判断是不是实现登录接口的控制器
			HandlerMethod method = (HandlerMethod) handler;
			controller = method.getBean();
		}

		if (controller instanceof BaseController) {
			baseController = (BaseController) controller;

			Map<String, Object> map = baseController.getResultMap();
			log.info("response:" + map);
			// securitykey
			String securitykey = (String) request.getAttribute("SECURITYKEY");
			WriteResult(response, NativeEnAndDeAPI.encryptData(new JSONObject(map).toJSONString(), securitykey));
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

	public void WriteResult(HttpServletResponse response, String result) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(result);
		out.flush();
		out.close();
	}
}
