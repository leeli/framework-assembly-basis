package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
public class HotActivityCitySubjectVo {
    private String actId;
    private String actName;
    private String status;
    private String actPoster;
    private String startTime;
    private String endTime;
    private String[] actUserPhotos;
    //新增
    private String actType;  //活动类型：1=新活动，2=活动回顾
    //新增
    private String isPublicActor; //是否公开参与者
    private String actUserNum;
    private String cityId;
    private String cityName;
    private String isHot;
}
