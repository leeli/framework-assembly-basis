package com.sse.ssbk.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sse.ssbk.dao.ProvinceDao;
import com.sse.ssbk.entity.Province;
import com.sse.ssbk.service.ProvinceService;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.vo.ProvinceVo;

@Service
public class ProvinceServiceImpl implements ProvinceService {
    @Autowired
    private ProvinceDao provinceDao;

    @Override
    public List<ProvinceVo> list() {
        List<Province> provinces = provinceDao.selectAll();
        return DtoEntityVoUtils.entityList2voList(provinces, ProvinceVo.class);
    }
}