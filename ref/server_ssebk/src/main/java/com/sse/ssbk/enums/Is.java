package com.sse.ssbk.enums;

import org.apache.commons.lang3.StringUtils;

public enum Is {
    YES("1", "是"), NO("0", "否");
    private String code;
    private String desc;

    public String getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }

    private Is(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 
     * @param code
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月9日 上午11:10:06
     */
    public static Is parse(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        code = code.trim();
        for (Is is : Is.values()) {
            if (StringUtils.equals(is.getCode(), code)) {
                return is;
            }
        }
        return null;
    }
}