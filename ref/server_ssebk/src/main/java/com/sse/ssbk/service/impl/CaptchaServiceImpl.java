package com.sse.ssbk.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.dto.PicCaptchaDto;
import com.sse.ssbk.dto.SmsCaptchaDto;
import com.sse.ssbk.dto.SmsCaptchaReqDto;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.CaptchaService;
import com.sse.ssbk.utils.ServiceUtil;
import com.sse.ssbk.vo.PicCaptchaVo;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by ciyua on 2017/8/8.
 */
@Service
public class CaptchaServiceImpl implements CaptchaService {

    // 日志
    private static org.slf4j.Logger log = LoggerFactory.getLogger(CaptchaServiceImpl.class);

    /**
     * 获取图形验证码
     * @return
     */
    public PicCaptchaVo getPicCaptcha(){
        Map<String, Object> paramMap = new HashMap<>();
        // TODO 地址需要做配置
        String reqUrl = "http://10.10.12.55:8080/ComInfoServer/getAuthImage.do";
        String r = null;
        try{
            r = ServiceUtil.DoHttpGet(reqUrl, paramMap, "GET");
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
        PicCaptchaVo picCaptchaVo = JSON.parseObject(r,PicCaptchaVo.class);
        return picCaptchaVo;
    }

    /**
     * 发送手机验证码
     * @param smsCaptchaDto
     */
    public String smsCaptcha(SmsCaptchaDto smsCaptchaDto){
        SmsCaptchaReqDto smsCaptchaReqDto = new SmsCaptchaReqDto();
        smsCaptchaReqDto.setType(smsCaptchaDto.getType());
        smsCaptchaReqDto.setPhone(smsCaptchaDto.getUserName());
        smsCaptchaReqDto.setAuthCode(smsCaptchaDto.getPicCaptcha());

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("data", JSON.toJSONString(smsCaptchaReqDto));
        paramMap.put("session", smsCaptchaDto.getSession());
        // TODO 地址需要做配置
        String reqUrl = "http://10.10.12.55:8080/ComInfoServer/authImage.do";
        String r = null;
        try{
            r = ServiceUtil.DoHttpPost(reqUrl, paramMap);
            log.info("r: " + r);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }
        JSONObject json = JSONObject.parseObject(r);
        String status = json.getString("status");
        String reason = json.getString("reason");
        if("0".equals(status)){
            log.info("发送手机验证码失败，原因：" + reason);
            if (reason.contains("用户已成功注册")){
                return "0";
            }
            throw new SSEBKRuntimeException(AppReturnCode.CustomError.getCode(), reason);
        }
        return "1";
    }

}
