package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by ciyua on 2017/7/26.
 */
@Data
public class PermVo {
    private Integer permId;
    private String permName;
}
