package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by work_pc on 2017/8/21.
 */
@Data
public class UserTopVo {
    private String userId;
    private String userPhoto;
    private String userName;
    private String realName;
    private String userCompany;
    private String userLevelImg;        //用户等级徽章
    private String rank;
    private String inConcern;
}
