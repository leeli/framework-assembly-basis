package com.sse.ssbk.dto;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by ciyua on 2017/8/8.
 */
@Data
public class PicCaptchaDto {
    @NotBlank
    private String authCode;
    @NotBlank
    private String type;
    @NotBlank
    private String phone;
}
