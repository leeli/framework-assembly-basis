package com.sse.ssbk.service;

import com.sse.ssbk.dto.CommentAddDto;
import com.sse.ssbk.enums.CommentTargetType;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.vo.CommentsAdapterVo;

public interface CommentService {
    /**
     * 
     * @param dto
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午8:28:53
     */
    Integer add(CommentAddDto dto);

    /**
     * 
     * @param page
     * @param targetId
     * @param targetType
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午8:30:35
     */
    CommentsAdapterVo list(Page page, Integer targetId, CommentTargetType targetType);
}