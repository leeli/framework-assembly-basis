package com.sse.ssbk.utils;

import java.net.InetAddress;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.JsonNode;

public class Util {

	
	/***
     * 获取ip
     * @param request
     * @return
     */
    public static String getRemortIP(HttpServletRequest request) { String ipAddress = null;
        //ipAddress = request.getRemoteAddr();
        ipAddress = request.getHeader("x-forwarded-for");
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if(ipAddress.equals("127.0.0.1")){
                //根据网卡取本机配置的IP
                InetAddress inet=null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (java.net.UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress= inet.getHostAddress();
            }

        }

        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if(ipAddress!=null && ipAddress.length()>15){ //"***.***.***.***".length() = 15
            if(ipAddress.indexOf(",")>0){
                ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }
    
    /***
     * 获取时间字符串
     * @param format
     * @return
     */
    public static Long getDate(){
        return new Date().getTime();
    }

    /***
     * 获取时间字符串
     * @param currentTime
     * @param format
     * @return
     */
    public static String getDate(Date currentTime,String format){
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(currentTime);
    }
    
    public static void main(String[] args) {
    	System.out.println(getDate());
		System.out.println(getDate(new Date(),"yyyyMMdd HHmmss"));
		System.out.println(getDate(new Date(1469424664490L),"yyyyMMdd HHmmss"));
	}
  //处理空值
    /***
     * 处理空字符串
     * @param o
     * @param dv
     * @return
     */
    public static String getJNStrV(JsonNode o,String dv){
        return o==null?dv:o.textValue();
    }

    /***
     * 处理空字符串
     * @param o
     * @return
     */
    public static String getJNStrV(JsonNode o){
        return getJNStrV(o,"");
    }
    public static String getObjStrV(Object o,String dv){
        if(o instanceof JsonNode){
            return getJNStrV((JsonNode)o,dv);
        }
        return o==null?dv:o.toString();
    }
    public static String getObjStrV(Object o){
        return getObjStrV(o,"");
    }

    public static String encodeBySHA1(String str){

        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.update(str.getBytes());
            return getFormattedText(messageDigest.digest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private static String getFormattedText(byte[] bytes) {
        int len = bytes.length;
        StringBuilder buf = new StringBuilder(len * 2);
        // 把密文转换成十六进制的字符串形式
        for (int j = 0; j < len; j++) {
            buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
            buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
        }
        return buf.toString();
    }

    public static Long getTimestamp(){
        return new Date().getTime()/1000;
    }

}
