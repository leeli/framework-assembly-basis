package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/28.
 */
@Data
public class UserInfoVo {
    private String userPhoto;       //头像
    private String realName;
    private String jobPosition;  //职务
    private String profession;  //行业
    private String company;  //公司名
    private String province;    //省名
    private String email;
    private String education;
    private String sex;
    private String nameCard;
    private String orgcodeCertificate;
}
