package com.sse.ssbk.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.secret.NativeEnAndDeAPI;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 上证百科自定义JSON消息转换器.
 * @author  Thomas Lee
 * @version 2017年7月18日 下午3:33:11
 */
public class SSEBKFastJsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

	// 日志
	private static org.slf4j.Logger log = LoggerFactory.getLogger(SSEBKFastJsonHttpMessageConverter.class);

	public final static Charset UTF8 = Charset.forName("UTF-8");
	private Charset charset = UTF8;
	private SerializerFeature[] features = new SerializerFeature[0];

	public SSEBKFastJsonHttpMessageConverter() {
		super(new MediaType("application", "json", UTF8), new MediaType("application", "*+json", UTF8));
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return true;
	}

	public Charset getCharset() {
		return this.charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public SerializerFeature[] getFeatures() {
		return features;
	}

	public void setFeatures(SerializerFeature... features) {
		this.features = features;
	}

	@Override
	protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream in = inputMessage.getBody();
		List<String> headerList = inputMessage.getHeaders().get("base");
		byte[] buf = new byte[1024];
		for (;;) {
			int len = in.read(buf);
			if (len == -1) {
				break;
			}
			if (len > 0) {
				baos.write(buf, 0, len);
			}
		}
		String base = headerList.get(0);
		String data = baos.toString();

		if(StringUtil.isEmpty(base) || StringUtil.isEmpty(data)){
			throw new SSEBKRuntimeException(AppReturnCode.ParaError);
		}

		log.info("base解密前：" + base);
		log.info("data解密前：" + data);

		String baseStr = NativeEnAndDeAPI.decrypt_data(base);
		String dataStr = NativeEnAndDeAPI.decrypt_data(data);

		log.info("base解密后:" + baseStr);
		log.info("data解密后:" + dataStr);
		if (StringUtil.isEmpty(baseStr) || StringUtil.isEmpty(dataStr)) {
			throw new SSEBKRuntimeException(AppReturnCode.DecryptError);
		}

		// TODO 缺少验签

		byte[] bytes = dataStr.getBytes();
		return JSON.parseObject(bytes, 0, bytes.length, charset.newDecoder(), clazz);
	}

	@Override
	protected void writeInternal(Object obj, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
		OutputStream out = outputMessage.getBody();
		String text = JSON.toJSONString(obj, features);
		// 加密.
//		text = IdHandler.idEncrypt(text);
		byte[] bytes = text.getBytes(charset);
		out.write(bytes);
	}
}