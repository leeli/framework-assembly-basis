package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 查询我关注的问题和回答列表接口VO
 * @author  HT-LiChuanbin 
 * @version 2017年8月3日 下午1:59:03
 */
@Data
public class ConcernMyQuestionReplyVo {
    private String questionId;
    private String replyId;
    private String questionTitle;
    private String isAnonymous;
    private String content;
    //    private String questionContent;
    //    private String replyContent;
    private List<String> imageArr;

    private String userId;
    private String userName;
    private String userPhoto;
    private String company;

    private String topicId;
    private String topicName;
    private String imgUrl;
    private List<String> topics;

    private String source;
    // private String isConcern;
    private Integer concernNum;
    private String isLike;
    private Integer likeNum;
    private Integer replyNum;

    public ConcernMyQuestionReplyVo() {
        this.questionId = "";
        this.replyId = "";
        this.questionTitle = "";
        this.isAnonymous = "";
        //        this.questionContent = "";
        //        this.replyContent = "";
        this.content = "";
        this.imageArr = new ArrayList<String>();

        this.userId = "";
        this.userName = "";
        this.userPhoto = "";
        this.company = "";

        this.topicId = "";
        this.topicName = "";
        this.imgUrl = "";
        this.topics = new ArrayList<String>();

        this.source = "";
        // this.isConcern = "";
        this.concernNum = 0;
        this.isLike = "";
        this.likeNum = 0;
        this.replyNum = 0;
    }
}