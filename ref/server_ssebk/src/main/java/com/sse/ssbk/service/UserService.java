package com.sse.ssbk.service;

import com.sse.ssbk.entity.User;
import com.sse.ssbk.vo.*;

import java.util.List;

/**
 * Created by yxf on 2017/7/13.
 */

public interface UserService {
    /**
     * @return
     */
    UserHomeVo getHomePage(String userId);

    List<NewsVoList> getMyNewsList(String lastNewsId, String pageSize
            , String Status, String isMyPublish);

    UserVo login(String loginName, String password);

    void register(String userName, String password, String smsCaptcha);

    void logout(String ticket);

    void changePsw(String oriPswd,String newPswd);

    void forgetPsw(String username,String code,String newPswd);

    String requireInfo();

    String userPhoneIsExists(String loginName);

    User getUserById(Integer userId);

    int validateSuccessInsertUser(User user);

    ScoreIndexVo scoreIndex(String userId);
}
