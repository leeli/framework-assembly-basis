package com.sse.ssbk.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务运行异常.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午1:20:46
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SSEBKRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String code;
    private String message;

    public SSEBKRuntimeException() {
        super(AppReturnCode.SERVER_ERROR.getMsg());
    }

    public SSEBKRuntimeException(String message) {
        super(message);
        this.code = AppReturnCode.SERVER_ERROR.getCode();
        this.message = message;
    }

    public SSEBKRuntimeException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public SSEBKRuntimeException(AppReturnCode AppReturnCode) {
        this(AppReturnCode.getCode(), AppReturnCode.getMsg());
    }

    public SSEBKRuntimeException(Throwable cause) {
        super(cause);
        this.code = AppReturnCode.SERVER_ERROR.getCode();
        this.message = cause.getMessage();
    }

    public SSEBKRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.code = AppReturnCode.SERVER_ERROR.getCode();
        this.message = message;
    }

    public SSEBKRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.code = AppReturnCode.SERVER_ERROR.getCode();
        this.message = message;
    }
}