package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.entity.UserCollectAct;
import com.sse.ssbk.vo.CollectReplyVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/27.
 */
public interface UserCollectActDao extends BaseDao<UserCollectAct> {

    List<Integer> getCollectActColumnsInOrder(@Param("userId") Integer userId, @Param("lastActId") Integer lastActId, @Param("pageSize") Integer pageSize);

    List<Activity> getCollectAct(@Param("columns") List<Integer> columns);
}
