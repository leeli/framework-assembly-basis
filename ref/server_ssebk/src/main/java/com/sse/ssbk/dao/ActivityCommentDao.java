package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ActivityComment;

public interface ActivityCommentDao extends BaseDao<ActivityComment> {
}