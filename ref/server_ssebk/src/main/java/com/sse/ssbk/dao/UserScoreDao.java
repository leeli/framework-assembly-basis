package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.entity.UserConcernUser;
import com.sse.ssbk.entity.UserScore;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/7.
 */
public interface UserScoreDao extends BaseDao<UserScore> {

}
