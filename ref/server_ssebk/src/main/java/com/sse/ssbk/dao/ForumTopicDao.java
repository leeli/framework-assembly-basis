package com.sse.ssbk.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ForumTopic;

public interface ForumTopicDao extends BaseDao<ForumTopic> {
    /**
     * 
     * @param questionId
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午4:04:39
     */
    List<ForumTopic> selectByQuestionId(@Param("questionId") Integer questionId);

    /**
     * 
     * @param questionId
     * @param userId
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月9日 下午7:09:10
     */
    List<ForumTopic> selectByQuestionIdUserId(@Param("questionId") Integer questionId, @Param("userId") Integer userId);
}