package com.sse.ssbk.common;

/**
 * Redis Constants.
 * <p>静态变量取名默认约定：XXX_YYY_ZZZ，其中XXX为功能（可省略），YYY为key值，ZZZ为value值。
 * @author HT-LiChuanbin 
 * @version 2017年7月31日 下午1:13:21
 */
public class RedisConstants {
    public static final String LIKE_REPLYCOMMENT_USER = "like_replycomment_user";
    public static final String LIKE_USER_REPLYCOMMENT = "like_user_replycomment";

    public static final String LIKE_QUESTION_USER = "like_question_user";
    public static final String LIKE_USER_QUESTION = "like_user_question";
    public static final String LIKE_USER_REPLY = "like_user_reply";
    public static final String LIKE_REPLY_USER = "like_reply_user";

    public static final String QUESTION_REPLY = "question_reply";

    public static final String COLLECTION_USER_QUESTION = "collection_user_question";
    public static final String COLLECTION_QUESTION_USER = "collection_question_user";

    public static final String CONCERN_USER_USER = "concern_user_user";
    public static final String CONCERN_USER_TOPIC = "concern_user_topic";
    public static final String CONCERN_TOPIC_USER = "concern_topic_user";

    public static final String REPLY_COMMENT = "reply_comment";
    public static final String ACT_COMMENT = "act_comment";
    public static final String NEWS_COMMENT = "news_comment";

    public static final String SUBJECT_ORDERBY_SORT_DESC = "subject_orderby_sort_desc";

    public static final String TOPIC_QUESTION = "topic_question";

    /**
     * 你问我答模块.
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:18:52
     */
    public class Forum {
        /** 回答评论点赞表. */
        public static final String REPLY_COMMENLIKES_MAP = "reply_commenlikes_map";
        /** 回答点赞表. */

    }

    /**
     * 咨询模块.
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:19:02
     */
    public class News {
        /** 咨询评论点赞表. */
        public static final String NEWS_COMMENLIKES_MAP = "news_commenlikes_map";
    }

    /**
     * 活动模块.
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:19:04
     */
    public class Activity {
        /** 活动评论点赞表. */
        public static final String LIKE_ACT_COMMENT_USER = "like_act_comment_user";
    }

    /**
     * 用户相关模块.
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:19:07
     */
    public class User {
        /** 用户积分表. */
        public static final String USER_SCORE = "user_score";

        public static final String USER_SCORE_RANK = "user_score_rank";

        /**
         * 用户关注
         * @author HT-LiChuanbin 
         * @version 2017年7月31日 下午3:00:29
         */
        public class Concern {

            /** 用户关注板块表. */

        }

        /**
         * 用户收藏.
         * @author HT-LiChuanbin 
         * @version 2017年7月31日 下午3:00:50
         */
        public class Collection {
            public static final String USER_COLLECTION_ACT = "user_collection_act";
            public static final String USER_COLLECTION_NEWS = "user_collection_news";
            public static final String USER_COLLECTION_REPLY = "user_collection_reply";
        }

        /**
         * 用户收藏数.
         */
        public class CollectionCount {
            public static final String COLLECTION_COUNT_ACT = "collection_count_act";
            public static final String COLLECTION_COUNT_NEWS = "collection_count_news";
            public static final String COLLECTION_COUNT_QUESTION = "collection_count_question";
            public static final String COLLECTION_COUNT_REPLY = "collection_count_reply";
        }

        /**
         * 阅读数
         */
        public class ReadCount {
            public static final String READ_COUNT_NEWS = "read_count_news";
        }

        /**
         * 评论数
         */
        public class CommentCount {
            public static final String COMMENT_COUNT_ACT = "comment_count_act";
            public static final String COMMENT_COUNT_NEWS = "comment_count_news";
            // public static final String COMMENT_COUNT_QUESTION = "comment_count_question";
            public static final String COMMENT_COUNT_REPLY = "comment_count_reply";
        }

        /**
         * 好友请求
         */
        public class Frined {
            /**
             * fuser 请求 tuser
             */
            public static final String REQUEST_FUSER_TUSER = "request_fuser_tuser";
            /**
             * 值为user对象
             */
            public static final String REQUEST_TUSER_FUSER = "request_tuser_fuser";
            /**
             * 值为userid
             */
            public static final String REQUEST_TUSER_FUSERID = "request_tuer_fuserid";
        }
    }

    public class Notice{
        public static final String LAST_VIEW_NOTICE_TIME = "last_view_notice_time";
    }

}
