package com.sse.ssbk.service;

import com.sse.ssbk.dto.AuditNewsDto;
import com.sse.ssbk.dto.NewsDto;
import com.sse.ssbk.dto.NewsPageDto;
import com.sse.ssbk.entity.News;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/12.
 */
public interface NewsService {
    News getNewsById(String id);

    int addNews(NewsDto newsDto);

    List<News> getNews(NewsPageDto newsPageDto);

    String isCollect(Integer userId,Integer newsId);

    int auditNews(Integer newsId, AuditNewsDto auditNewsDto);
}
