package com.sse.ssbk.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by ciyua on 2017/7/26.
 */
@Data
@Entity
@Table(name = "t_role_perm_map")
@NoArgsConstructor
@AllArgsConstructor
public class RolePerm implements Serializable {
    @Id
    private Integer roleId;
    @Id
    private Integer permId;
}
