package com.sse.ssbk.aop;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.*;
import com.sse.ssbk.dto.*;
import com.sse.ssbk.entity.*;
import com.sse.ssbk.enums.*;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.CountScore;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.utils.SendNotice;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by work_pc on 2017/8/21.
 */

@Aspect
@Component
@Slf4j
public class NoticeAop {

    @Autowired
    private NoticeDao noticeDao;
    @Autowired
    private ActivityDao activityDao;
    @Autowired
    private ForumQuestionDao forumQuestionDao;
    @Autowired
    private ForumReplyDao forumReplyDao;
    @Autowired
    private ReplyCommentDao replyCommentDao;
    @Autowired
    private UserDao userDao;

    @Pointcut("@annotation(com.sse.ssbk.utils.SendNotice)")
    public void sendNotice() {

    }

    @AfterReturning("sendNotice()")
    private void sendNoticeAfter(JoinPoint pj) throws Throwable {
        Object[] args = pj.getArgs();
        UserSession currentUser = BizUtils.getCurrentUser();
        //通知来源id
        Integer sourceUserId = currentUser.getUserId();
        String sourceUserPhoto = currentUser.getUserPhoto();
        NoticeTypes noticeTypes = NoticeTypes.Other;
        String sourceActionId = "";
        List<Integer> targerUserId = new ArrayList<>();
        String content = "";
        try {
            for (Object arg : args) {
                //活动报名 若活动需审核，则向问题的发出者推送需待审核消息
                if (arg instanceof EnrollActivity){
                    Integer actId = ((EnrollActivity) arg).getActId();
                    Activity activity = new Activity();
                    activity.setActId(actId);
                    activity = activityDao.selectByPrimaryKey(activity);
                    //活动报名需审核
                    if (Is.YES.getCode().equals(activity.getIsAudit())){
                        sourceActionId = String.valueOf(actId);
                        noticeTypes = NoticeTypes.ActivityNeedAudit;
                        content = "您的活动 \""+activity.getActName()+" \"有了新的报名者需要审核！";
                        targerUserId.add(Integer.valueOf(activity.getUserId()));
                    }
                }

                //审核活动报名者
                if (arg instanceof AuditActivityJoinerDto) {
                    sourceActionId = String.valueOf((Integer) args[0]);
                    //审核参与人 通过
                    if ("1".equals(((AuditActivityJoinerDto) arg).getEnrollStatus())) {
                        Activity activity = new Activity();
                        activity.setActId(Integer.valueOf(sourceActionId));
                        activity = activityDao.selectByPrimaryKey(activity);
                        noticeTypes = NoticeTypes.ActivityAuditPass;
                        content = "您申请参与的活动: \""+activity.getActName()+" \" ，被通过了！";
                        targerUserId = ((AuditActivityJoinerDto) arg).getUserIds();
                    }
                    //审核参与人 不通过
                    if ("2".equals(((AuditActivityJoinerDto) arg).getEnrollStatus())){
                        Activity activity = new Activity();
                        activity.setActId(Integer.valueOf(sourceActionId));
                        activity = activityDao.selectByPrimaryKey(activity);
                        noticeTypes = NoticeTypes.ActivityAuditUnpass;
                        content = "您申请参与的活动: \""+activity.getActName()+"\" 未被审核通过！";
                        targerUserId = ((AuditActivityJoinerDto) arg).getUserIds();
                    }
                }

                //问题被回答
                if (arg instanceof ForumReplyAddDto){
                    sourceActionId = ((ForumReplyAddDto) arg).getQuestionId();
                    ForumQuestion forumQuestion = new ForumQuestion();
                    forumQuestion.setQuestionId(Integer.valueOf(sourceActionId));
                    forumQuestion = forumQuestionDao.selectByPrimaryKey(forumQuestion);
                    noticeTypes = NoticeTypes.QuestionHasReply;
                    content = "您的问题: \" "+forumQuestion.getTitle()+"\" 有了新回答!";
                    targerUserId.add(forumQuestion.getUserId());
                }

                //评论
                if (arg instanceof CommentAddDto){
                    String targetType = ((CommentAddDto) arg).getTargetType();
                    sourceActionId = ((CommentAddDto) arg).getTargetId();
                    CommentTargetType eTargetType = CommentTargetType.parse(targetType);
                    //回答被评论 回答的评论被评论
                    if (CommentTargetType.REPLY.equals(eTargetType)){
                        noticeTypes = NoticeTypes.ReplyHasComment;
                        ForumReply forumReply = new ForumReply();
                        forumReply.setReplyId(Integer.valueOf(sourceActionId));
                        forumReply = forumReplyDao.selectByPrimaryKey(forumReply);
                        int length = forumReply.getContent().length();
                        content = "您的回答: \" "+forumReply.getContent().substring(0,Math.min(20,length))+"...\" 有了新评论!";
                        targerUserId.add(forumReply.getUserId());
                    }else if (CommentTargetType.REPLYCOMMENT.equals(eTargetType)){
                        noticeTypes = NoticeTypes.CommentHasComment;
                        ReplyComment replyComment = new ReplyComment();
                        replyComment.setReplyCommentId(Integer.valueOf(sourceActionId));
                        replyComment = replyCommentDao.selectByPrimaryKey(replyComment);
                        int length = replyComment.getContent().length();
                        content ="您的评论: \" "+replyComment.getContent().substring(0,Math.min(20,length))+"...\" 有了新评论";
                        targerUserId.add(replyComment.getUserId());
                    }
                }
                //点赞
                if (arg instanceof LikeAddDto){
                    String targetType = ((LikeAddDto) arg).getTargetType();
                    sourceActionId = ((LikeAddDto) arg).getTargetId();
                    LikeTargetType eTargetType = LikeTargetType.parse(targetType);
                    //回答被点赞
                    if (LikeTargetType.QUESTION.equals(eTargetType)){
                        noticeTypes = NoticeTypes.QuestionHasLike;
                        ForumQuestion forumQuestion = new ForumQuestion();
                        forumQuestion.setQuestionId(Integer.valueOf(sourceActionId));
                        forumQuestion = forumQuestionDao.selectByPrimaryKey(forumQuestion);
                        content = "您的问题: \" "+forumQuestion.getTitle()+"\" 被点赞了！";
                        targerUserId.add(forumQuestion.getUserId());
                    }else if (LikeTargetType.REPLY.equals(eTargetType)){
                        noticeTypes = NoticeTypes.ReplyHasLike;
                        ForumReply forumReply = new ForumReply();
                        forumReply.setReplyId(Integer.valueOf(sourceActionId));
                        forumReply = forumReplyDao.selectByPrimaryKey(forumReply);
                        // todo 回答的前20个字符
                        int length = forumReply.getContent().length();
                        content = "您的回答: \""+forumReply.getContent().substring(0,Math.min(20,length))+"...\" 被点赞了!";
                        targerUserId.add(forumReply.getUserId());
                    }else if (LikeTargetType.REPLYCOMMENT.equals(eTargetType)){
                        noticeTypes = NoticeTypes.CommentHasLike;
                        ReplyComment replyComment = new ReplyComment();
                        replyComment.setReplyCommentId(Integer.valueOf(sourceActionId));
                        replyComment = replyCommentDao.selectByPrimaryKey(replyComment);
                        content = "您的评论: \" "+replyComment.getContent()+"...\" 被点赞了!";
                        targerUserId.add(replyComment.getUserId());
                    }
                }
                //用户被关注
                if (arg instanceof ConcernAddDto){
                    String targetType = ((ConcernAddDto) arg).getTargetType();
                    sourceActionId = ((ConcernAddDto) arg).getTargetId();
                    ConcernTargetType eTargetType = ConcernTargetType.parse(targetType);
                    if (ConcernTargetType.USER.equals(eTargetType)){
                        noticeTypes = NoticeTypes.UserHasConcerned;
                        content = "您被用户"+currentUser.getRealName()+"关注了！";
                        targerUserId.add(Integer.valueOf(sourceActionId));
                    }
                }
            }

            String reqMethod = pj.getTarget().getClass().getName() + "." + pj.getSignature().getName() + "()";
            for (Integer userId : targerUserId) {
                log.info("=====通知=====");
                log.info(reqMethod);
                Notice notice = new Notice();
                notice.setNoticeType(noticeTypes.getType());
                notice.setNoticeTypeDesc(noticeTypes.getTypeDesc());
                notice.setContent(content);
                notice.setUserId(userId);
                notice.setSourceUserId(sourceUserId);
                notice.setSourceActionId(Integer.valueOf(sourceActionId));
                notice.setSourceUserPhoto(sourceUserPhoto);
                notice.setCreateTime(new Date());
                notice.setIsRead("0");
                notice.setIsDeleted("0");
                noticeDao.insertSelective(notice);
                log.info("用户"+currentUser.getUserId());
                log.info(noticeTypes.getDesc());
                log.info("对用户"+userId);
                log.info(content);
                log.info("====通知结束====");
            }
        } catch (Exception e) {
            log.error("=====通知异常=====");
            log.error("异常信息:{}", e.getMessage());
        }
    }
}
