package com.sse.ssbk.dto;

import lombok.Data;

@Data
public class ForumReplyListDto {
	private String userId;
	private String pageSize;
	private String questionId;
	private String lastId;
}