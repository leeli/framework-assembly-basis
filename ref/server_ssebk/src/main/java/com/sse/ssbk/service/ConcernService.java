package com.sse.ssbk.service;

import java.util.List;

import com.sse.ssbk.enums.ConcernTargetType;
import com.sse.ssbk.vo.ConcernMyQuestionReplyVo;

/**
 * 
 * @author HT-LiChuanbin 
 * @version 2017年7月31日 下午1:35:26
 */
public interface ConcernService {
    /**
     * 
     * @param targetId
     * @param targetType
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:11:15
     */
    public void add(Integer targetId, ConcernTargetType targetType);

    /**
     * 
     * @param targetId
     * @param targetType
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午1:11:18
     */
    public void del(Integer targetId, ConcernTargetType targetType);

    /**
     * 
     * @param page
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月4日 下午6:14:40
     */
    public List<ConcernMyQuestionReplyVo> listMyQuestionReplys(long pageNo, long pageSize);
}