package com.sse.ssbk.service;

import com.sse.ssbk.dto.PicCaptchaDto;
import com.sse.ssbk.dto.SmsCaptchaDto;
import com.sse.ssbk.vo.PicCaptchaVo;

import java.util.Map;

/**
 * Created by ciyua on 2017/8/8.
 */
public interface CaptchaService {

    PicCaptchaVo getPicCaptcha();

    String smsCaptcha(SmsCaptchaDto smsCaptchaDto);
}
