package com.sse.ssbk.entity;

import com.sse.ssbk.utils.TransIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by work_pc on 2017/7/15.
 */

@Data
@Entity
@Table(name = "t_activity")
public class Activity {
    @Id
    @Column(name = "act_id")
    private Integer actId;
    @Column(name = "activity_name")
    private String actName;
    @Column(name = "activity_images")
    private String actPoster; //活动海报
    private String userName;
    private String userId;
    private Integer status; //活动状态0准备1进行2结束3取消
    @TransIgnore
    private Date startTime;
    @TransIgnore
    private Date endTime;
    private Date createTime;
    private Date updateTime;
    @Column(name = "act_subject_name")
    private String subjectName;
    private String address;
    @Column(name = "is_approve_need")
    private String isAudit; //是否需要审核 0=不需要，1=需要审核
    @Column(name = "is_new")
    private String actType; //活动类型：1=新活动，2=活动回顾
    private String describ; //活动简介
    @Column(name = "is_member_public")
    private String isPublicActor; //是否公开参与者
    @Column(name = "activity_photos")
    @TransIgnore
    private String actImages; //活动图片
    private String cityName;
    private Integer cityId;
    @Column(name = "act_subject_id")
    private String subjectId;
    private String companyName;
    private Integer companyId;

    //区域活动查询排序用 时间戳
    @TransIgnore
    private Long startTimeSecondRandom;
    @TransIgnore
    private Long endTimeSecondRandom;
}
