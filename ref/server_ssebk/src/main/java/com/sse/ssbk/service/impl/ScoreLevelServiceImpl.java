package com.sse.ssbk.service.impl;

import com.sse.ssbk.dao.ScoreLevelDao;
import com.sse.ssbk.entity.ScoreLevel;
import com.sse.ssbk.service.ScoreLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yxf on 2017/7/26.
 */
@Service
public class ScoreLevelServiceImpl implements ScoreLevelService {

    @Autowired
    private ScoreLevelDao scoreLevelDao;

    @Override
    public ScoreLevel getScoreLevelByLevelId(String levelId) {
        ScoreLevel scoreLevel = new ScoreLevel();
        scoreLevel.setLevelId(Integer.valueOf(levelId));
        scoreLevel = scoreLevelDao.selectByPrimaryKey(scoreLevel);
        return scoreLevel;
    }
}
