package com.sse.ssbk.dto;

import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.utils.TransIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
public class ActDto{
    @NotBlank
    private String actName;
    @NotBlank
    private String describ;
    @NotBlank
    private String address;
    @NotBlank
    private String actPoster;   //活动海报
    private String[] actImages;   //活动图片
    @NotBlank
    private String startTime;
    @NotBlank
    private String endTime;
    @NotNull
    private Integer cityId;
    @NotBlank
    private String subjectId;
//    @NotBlank  兼容活动回顾 没有是否公开和是否审核
    private String isPublicActor; //是否公开参与者
//    @NotBlank
    private String isAudit;  //是否需要审核 0=不需要，1=需要审核
    @NotBlank
    private String actType;  //活动类型：1=新活动，2=活动回顾
    private String userId;
}
