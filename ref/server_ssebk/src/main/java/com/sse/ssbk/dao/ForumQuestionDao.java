package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ForumQuestion;

public interface ForumQuestionDao extends BaseDao<ForumQuestion> {
	/**
	 * 可选择插入数据，并返回ID.
	 * @param forumQuestion ForumQuestion
	 * @author              HT-LiChuanbin 
	 * @version             2017年7月21日 上午11:05:22
	 */
	void insertSelectiveWithId(ForumQuestion forumQuestion);

	/**
	 * 逻辑删除.
	 * @param id 问题ID.
	 * @author   HT-LiChuanbin 
	 * @version  2017年7月21日 上午11:05:48
	 */
	void deleteByPrimaryKeyLogic(Integer id);
}