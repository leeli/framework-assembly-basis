package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.print.attribute.standard.MediaSize;
import java.util.Date;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
@Entity
@Table(name = "t_user_collection_act")
public class UserCollectAct {
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "act_id")
    private Integer actId;
    @Column(name = "create_time")
    private Date createTime;
}
