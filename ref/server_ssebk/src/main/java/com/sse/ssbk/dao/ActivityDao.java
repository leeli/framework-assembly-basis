package com.sse.ssbk.dao;

import java.util.List;

import com.sse.ssbk.entity.User;
import org.apache.ibatis.annotations.Param;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ActSubject;
import com.sse.ssbk.vo.CircleActVo;
import com.sse.ssbk.entity.Activity;

/**
 * Created by work_pc on 2017/7/15.
 */
public interface ActivityDao extends BaseDao<Activity> {

	/**
	 * 获取活动轮播图
	 * @return
	 */
	List<CircleActVo> getCircleAct();

	/**
	 * 获取活动主题列表  设计图02e
	 * @param lastSubjectId
	 * @param pageSize
	 * @return
	 */
	List<ActSubject> getActSubjects(@Param("lastSubjectId") Integer lastSubjectId, @Param("pageSize") Integer pageSize);


	/**
	 * 获取参与人、报名人列表
	 * @param lastUserId
	 * @param actId
	 * @param pageSize
	 * @param status
	 * @return
	 */
	List<Integer> getEnrollUserColumns(@Param("lastUserId") Integer lastUserId, @Param("actId") Integer actId, @Param("pageSize") Integer pageSize,@Param("enrollStatus") Integer status);

	List<User> getEnrollUserList(@Param("columns") List<Integer> columns);


	/**
	 * 获取某个区域或主题的活动列表
	 * @param subjectId
	 * @param cityId
	 * @return
	 */
	List<Integer> getCityOrSubjectActivityColumns(@Param("lastActId")Integer lastActId,@Param("pageSize")Integer pageSize,@Param("subjectId")Integer subjectId,@Param("cityId")Integer cityId,@Param("status")Integer status);


	List<Activity> getCityOrSubjectActivity(@Param("columns")List<Integer> columns);

	/**
	 * 获取某个区域或主题的活动列表
	 * @param lastActId
	 * @param pageSize
	 * @param subjectId
	 * @param cityId
	 * @param status
	 * @return
	 */
	@Deprecated
	List<Activity> getCityOrSubjectActivity(@Param("lastActId")Integer lastActId,@Param("pageSize")Integer pageSize,@Param("subjectId")Integer subjectId,@Param("cityId")Integer cityId,@Param("status")Integer status);


	/**
	 * 获取我的活动列表
	 * @param lastActId
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	List<Activity> getMyActivity(@Param("lastActId")Integer lastActId,@Param("pageSize")Integer pageSize,@Param("userId") Integer userId);


	List<Integer> getIJoinActivityColumns(@Param("lastActId")Integer lastActId,@Param("pageSize")Integer pageSize,@Param("userId") Integer userId,@Param("status")Integer status);

	/**
	 * 获取我参与的活动列表
	 * @param lastActId
	 * @param pageSize
	 * @return
	 */
	List<Activity> getIJoinActivity(@Param("columns") List<Integer> columns);


	/**
	 * 正在进行中的10场活动，
	 * @return
	 */
	List<Activity> getHotActivityIng();


	/**
	 * 正在未开始中的几场活动，
	 * @return
	 */
	List<Activity> getHotActivityUnBegin(@Param("size") Integer size);

	/**
	 * 已结束的三场活动，
	 * @return
	 */
	List<Activity> getHotActivityPassed();

	/**
	 *
	 * @param columns
	 * @param status  状态0,1,2,3
	 * @return
	 */
	List<Activity> getActivityNotInColumns(@Param("columns") List<Integer> columns,@Param("status") String[] status,@Param("size") Integer size);
}
