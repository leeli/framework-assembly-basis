package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;

/**
 * Created by work_pc on 2017/8/18.
 */
@Data
@Entity
@Table(name = "t_notice")
public class Notice {
    @Id
    @Column(name = "notice_id")
    private Integer noticeId;
    @Column(name = "notice_type")
    private String noticeType;
    @Column(name = "content")
    private String content;
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "is_read")
    private String isRead;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "is_deleted")
    private String isDeleted;
    @Column(name = "source_user_id")
    private Integer sourceUserId;
    @Column(name = "source_action_id")
    private Integer sourceActionId;
    @Column(name = "source_user_photo")
    private String sourceUserPhoto;
    @Column(name = "notice_type_desc")
    private String noticeTypeDesc;
}
