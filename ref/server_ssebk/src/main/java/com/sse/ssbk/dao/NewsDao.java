package com.sse.ssbk.dao;

import java.util.List;

import com.sse.ssbk.dto.NewsPageDto;
import org.apache.ibatis.annotations.Param;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.News;

/**
 * Created by yxf on 2017/7/7.
 */
public interface NewsDao extends BaseDao<News> {

	/**
	 * 获取资讯列表
	 * @param newsPageDto
	 * @return
	 */
	List<News> getNews(NewsPageDto newsPageDto);

	/**
	 * 用户是否收藏某个资讯
	 * @param userId
	 * @param newsId
	 * @return
	 */
	int isCollect(@Param("userId") Integer userId,@Param("newsId")Integer newsId);

	/**
	 * 审核资讯 status 0 待审核 1 审核通过  2 审核不通过
	 * @param newsId
	 * @param reason
	 * @param status
	 * @return
	 */
	int auditNews(@Param("newsId") Integer newsId,@Param("reason") String reason,@Param("status") String status);
}
