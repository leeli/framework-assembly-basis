package com.sse.ssbk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sse.ssbk.controller.base.BaseController;
import com.sse.ssbk.service.ProvinceService;
import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;
import com.sse.ssbk.vo.ProvinceVo;

/**
 * 省份控制器.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:03:15
 */
@RestController
@RequestMapping("/ssebk/common")
public class ProvinceController extends BaseController {
    @Autowired
    private ProvinceService provinceService;

    /**
     * 获取问题列表.
     * @return
     * @author  HT-LiChuanbin 
     * @version 2017年7月24日 下午3:06:22
     */
    @GetMapping("/provinces")
    public Result<List<ProvinceVo>> list() {
        List<ProvinceVo> voProvinces = provinceService.list();
        return ResultUtils.success(voProvinces);
    }
}