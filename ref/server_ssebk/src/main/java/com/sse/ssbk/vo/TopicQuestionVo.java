package com.sse.ssbk.vo;

import java.util.Date;

import lombok.Data;

@Data
public class TopicQuestionVo {
	private Integer topicId;
	private Integer questionId;
	private Date createTime;
}