package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.News;
import com.sse.ssbk.entity.RolePerm;
import com.sse.ssbk.entity.ScoreLevel;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.vo.UserHomeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/7.
 */
public interface ScoreLevelDao extends BaseDao<ScoreLevel> {

}
