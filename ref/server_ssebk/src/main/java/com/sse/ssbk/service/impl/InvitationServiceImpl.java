package com.sse.ssbk.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.InvitationDao;
import com.sse.ssbk.dto.InvitationDto;
import com.sse.ssbk.entity.Invitation;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.InvitationService;
import com.sse.ssbk.service.UserService;
import com.sse.ssbk.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by work_pc on 2017/8/8.
 */
@Service
public class InvitationServiceImpl implements InvitationService {

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private UserService userService;

    @Override
    public int addInvitation(InvitationDto invitationDto) {
        Invitation invitation = DtoEntityVoUtils.dto2entity(invitationDto, Invitation.class);

        //用户手机号已存在
        if (!"1".equals(userService.userPhoneIsExists(invitation.getPhone()))){
            throw new SSEBKRuntimeException("用户已存在");
        }

        UserSession currentUser = BizUtils.getCurrentUser();
        invitation.setUserId(currentUser.getUserId());
        invitation.setUserName(currentUser.getUserName());
        invitation.setCreateTime(new Date());

        String companyName = currentUser.getCompanyName();
        String realName = currentUser.getRealName();
        String code = invitation.getInvitationCode();

        String telephone = invitation.getPhone();

        String content;
        if (companyName == null || "".equals(companyName)){
            content = "您好:\n  我是"+  realName+",现邀请您使用上证百科app,邀请码为"+code+",有效期为7天。";
        }else {
            content = "您好:\n  我是"+companyName+"的"+ realName+",现邀请您使用上证百科app,邀请码为"+code+",有效期为7天。";
        }
        //todo 183服务器不部署
        SMSUtil.sendSMS(telephone,content);
        //短信发送成功后才插入数据库
        invitationDao.insertSelective(invitation);
        /*Map<String, Object> params = new HashMap<>();
        params.put("mobileNum", invitation.getPhone());
        params.put("type", 3);
        params.put("content","[测试短信功能]");

        String reqUrl = "http://10.10.12.55:8080/ComInfoServer/authImage.do";
        String r = null;
        try{
            r = ServiceUtil.DoHttpGet(reqUrl, params, "GET");
        }catch (Exception e){
            throw new SSEBKRuntimeException(AppReturnCode.SERVER_ERROR);
        }

        JSONObject json = JSONObject.parseObject(r);

        int status = json.getInteger("status");
        String reason = json.getString("reason");

        if(0 == status){
            throw new SSEBKRuntimeException(AppReturnCode.UndefinedError.getCode(), reason);
        }*/
        return 1;
    }

    @Override
    public String validateInvitation(String invitationCode,String realName,String loginName) {
        Example invExample = new Example(Invitation.class);
        Example.Criteria criteria = invExample.createCriteria();
        criteria.andEqualTo("phone",loginName);
        criteria.andEqualTo("toUserRealName",realName);
//        criteria.andEqualTo("invitationCode",invitationCode);
        criteria.andGreaterThan("createTime",new Date(new Date().getTime() - 7 * Constants.MILLISECONDS_OF_DAY));

        List<Invitation> invitations = invitationDao.selectByExample(invExample);

        Map<Integer, List<Invitation>> collect = invitations.stream().collect(Collectors.groupingBy(Invitation::getUserId));


        //同一个用户以最新的为准 不同用户的均有效
        for (Integer userId:collect.keySet()){
            List<Invitation> invi = collect.get(userId);
            if ( invi.size() == 1 && invi.get(0).getInvitationCode().equals(invitationCode)){
                return Is.YES.getCode();
            }else {
                Optional<Invitation> max = invi.stream().max(Comparator.comparing(Invitation::getCreateTime));
                if (max.get().getInvitationCode().equals(invitationCode)){
                    return Is.YES.getCode();
                }
            }
        }

        return Is.NO.getCode();
    }

    public static void main(String[] args) {
        System.out.println(LocalDate.now().minusDays(7));
        System.out.println(LocalDate.now().minusDays(7).toString());
        System.out.println(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        System.out.println(new Date());
    }
}
