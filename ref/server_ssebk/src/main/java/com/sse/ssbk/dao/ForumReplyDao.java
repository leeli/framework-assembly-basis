package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ForumReply;

public interface ForumReplyDao extends BaseDao<ForumReply> {

    /**
     * 
     * @param forumReply
     * @author HT-LiChuanbin 
     * @version 2017年7月28日 下午1:27:31
     */
    void insertSelectiveWithId(ForumReply forumReply);

    /**
     * 逻辑删除.
     * @param id ID.
     * @author   HT-LiChuanbin 
     * @version  2017年7月21日 上午11:05:48
     */
    void deleteByPrimaryKeyLogic(Integer id);
}