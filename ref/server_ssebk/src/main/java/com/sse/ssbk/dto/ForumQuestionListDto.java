package com.sse.ssbk.dto;

import lombok.Data;

@Data
public class ForumQuestionListDto {
	private String lastId;
	private String pageSize;
	private String title;
	private String topicId;
	private String userId;
}