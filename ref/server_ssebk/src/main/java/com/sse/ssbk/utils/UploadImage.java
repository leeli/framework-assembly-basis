package com.sse.ssbk.utils;

import lombok.Data;

@Data
public class UploadImage {
    private String filename;
    private String index;
    private String url;
    private String thumbnailUrl;
}