package com.sse.ssbk.service;

import com.sse.ssbk.dto.DeleteNoticeDto;
import com.sse.ssbk.entity.Notice;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.vo.NoticeVo;

import java.util.List;

/**
 * Created by work_pc on 2017/8/18.
 */
public interface NoticeService {

    List<NoticeVo> list(String isSystemNotice, BizUtils.Page page);

    void delete(DeleteNoticeDto deleteNoticeDto);

    String hasNewNotices();

    void readNotice(List<String> noticesId);
}
