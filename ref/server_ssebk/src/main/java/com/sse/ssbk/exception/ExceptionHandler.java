package com.sse.ssbk.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sse.ssbk.utils.Result;
import com.sse.ssbk.utils.ResultUtils;

/**
 * 统一异常处理.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午2:02:16
 */
@Slf4j
@ControllerAdvice(basePackages = "com.sse.ssbk.controller")
public class ExceptionHandler {
    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    public Result<?> exception(Exception e, HttpServletRequest request, HttpServletResponse response) {
        this.error(e, request);
        // log.error("请求URI：\n" + request.getRequestURI(), e);
        // 避免乱码.
        response.setCharacterEncoding("UTF-8");
        if (e instanceof MethodArgumentNotValidException) {
            return this.failure(AppReturnCode.ParaError.getCode(), AppReturnCode.ParaError.getMsg());
        } else if (e instanceof DuplicateKeyException) {
            Pattern pattern = Pattern.compile("Duplicate entry '(.*)' for key '(.*)'");
            Matcher matcher = pattern.matcher(e.getCause().getMessage());
            if (matcher.matches()) {
                String key = matcher.group(2);
                String value = matcher.group(1);
                return this.failure(key.replace("_UNIQUE", "") + ":" + value + " 已存在，请修改后重新添加");
            } else {
                return this.failure(e.getCause().getMessage());
            }
        } else if (e instanceof DataIntegrityViolationException) {
            if (e.getCause().getMessage().contains("Data too long for column")) {
                Pattern pattern = Pattern.compile(".*Data too long for column '(.*)' at row.*");
                Matcher matcher = pattern.matcher(e.getCause().getMessage());
                if (matcher.matches()) {
                    String key = matcher.group(1);
                    return this.failure("字段[" + key + "]长度超出限制");
                }
                return this.failure("某些字段长度超出限制");
            } else if (e.getCause().getMessage().contains("doesn't have a default value")) {
                Pattern pattern = Pattern.compile(".*Field '(.*)' doesn't have a default value.*");
                Matcher matcher = pattern.matcher(e.getCause().getMessage());
                if (matcher.matches()) {
                    String key = matcher.group(1);
                    return this.failure("字段[" + key + "]没有默认值");
                }
                return this.failure("某些字段没有默认值");
            } else {
                return this.failure(e.getCause().getMessage());
            }
        } else if (e instanceof MissingServletRequestParameterException) {
            return this.failure("参数缺失");
        } else if (e instanceof SSEBKRuntimeException) {
            SSEBKRuntimeException ssebkRE = (SSEBKRuntimeException) e;
            return this.failure(ssebkRE.getCode(), ssebkRE.getMessage());
        } else {
            return this.failure(AppReturnCode.SERVER_ERROR.getMsg());
        }
    }

    /**
     * 打印错误日志.
     * <p>目的：为了能够直接通过日志解决问题，不需要场景重现。
     * <p>打印
     *      1. uri；
     *      2. 请求参数；
     *      3. Ruquest Header中的base；
     *      4. Request Body中的data。
     * @param e
     * @param request
     * @author HT-LiChuanbin 
     * @version 2017年8月11日 上午11:08:20
     */
    private void error(Exception e, HttpServletRequest request) {
        try {
            String base = request.getHeader("base");
            String strParam = "";
            String method = request.getMethod();
            if (RequestMethod.GET.toString().equals(method)) {
                Map<String, String[]> mParams = request.getParameterMap();
                Set<Map.Entry<String, String[]>> smParams = mParams.entrySet();
                Iterator<Map.Entry<String, String[]>> ismParams = smParams.iterator();
                while (ismParams.hasNext()) {
                    Map.Entry<String, String[]> entryParam = ismParams.next();
                    strParam += entryParam.getKey() + "=" + entryParam.getValue()[0].toString();
                }
            } else if (RequestMethod.POST.toString().equals(method) || RequestMethod.PUT.toString().equals(method)) {
                try (ServletInputStream sis = request.getInputStream()) {
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(sis))) {
                        StringBuilder sb = new StringBuilder();
                        String line = "";
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        strParam += sb.toString();
                    }
                }
            } else if (RequestMethod.DELETE.toString().equals(method)) {
            } else {
                log.error("不支持的请求方法！");
                return;
            }
            log.info("base信息：\n" + URLDecoder.decode(base, "utf-8"));
            log.info("请求信息：\n" + strParam.substring(0, strParam.length()));
            log.error("请求URI：\n" + method.toString() + " " + request.getRequestURI(), e);
        } catch (IOException ioe) {
            log.error("", ioe);
        }
    }

    private Result<?> failure(String message) {
        return ResultUtils.failure(message);
    }

    private Result<?> failure(String code, String message) {
        return ResultUtils.failure(code, message);
    }
}