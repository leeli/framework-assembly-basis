package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
public class UserHomeVo {
    //我关注的人数
    private String concernOfMyNum;
    //关注我的人数
    private String concernToMyNum;
    private String userName;
    private String realName;
    private String userPhoto;
    private String company;
    private String province;
    //积分
    private String score;
    //排名
    private String rank;
    //用户等级
    private String levelId;
    //用户等级名
    private String levelName;
    //用户等级徽章
    private String levelImg;

    private String isConcern;

    private String isFriend;
}
