package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.SourceFrom;
import com.sse.ssbk.vo.SourceFromVo;

import java.util.List;

public interface SourceFromDao extends BaseDao<SourceFrom> {
    List<SourceFromVo> getSourceFromList();
}