package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by work_pc on 2017/8/10.
 */
@Entity
@Data
@Table(name = "t_user_score_blotter")
public class ScoreBlotter {
    @Id
    @Column(name = "score_blotter_id")
    private Integer scoreBlotterId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "score_rule_id")
    private Integer scoreRuleId;

    @Column(name = "create_time")
    private Date createTime;
}
