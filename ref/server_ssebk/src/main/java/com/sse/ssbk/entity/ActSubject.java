package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by yxf on 2017/7/17.
 */
@Data
@Entity
@Table(name = "t_activity_subject")
public class ActSubject {
    @Id
    @Column(name = "act_subject_id")
    private String subjectId;
    @Column(name = "act_subject_name")
    private String subjectName;
    @Column(name = "act_subject_picture")
    private String picture;
    private Date createTime;
    private Date updateTime;
    private String sort;
}
