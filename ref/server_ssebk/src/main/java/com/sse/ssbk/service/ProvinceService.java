package com.sse.ssbk.service;

import java.util.List;

import com.sse.ssbk.vo.ProvinceVo;

public interface ProvinceService {
    /**
     * 获取省份列表.
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午6:37:06
     */
    List<ProvinceVo> list();
}