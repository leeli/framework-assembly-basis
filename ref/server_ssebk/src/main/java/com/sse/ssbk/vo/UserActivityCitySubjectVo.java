package com.sse.ssbk.vo;

import lombok.Data;

/**
 * Created by yxf on 2017/7/27.
 */
@Data
public class UserActivityCitySubjectVo {
    private String actId;
    private String actName;
    private String status;
    private String actImages;
    private String startTime;
    private String endTime;
    private String[] actUserPhotos;
    private String actUserNum;
    private String cityId;
    private String cityName;
    private String isHot;
    private String enrollStatus;
    private String rejectReason;
    //新增:是否公开参与者
    private String isPublicActor;

    //新增
    private String actType;  //活动类型：1=新活动，2=活动回顾

    public UserActivityCitySubjectVo(){
        this.enrollStatus = "";
        this.rejectReason = "";
    }
}
