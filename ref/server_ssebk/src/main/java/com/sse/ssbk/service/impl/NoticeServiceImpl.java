package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.NoticeDao;
import com.sse.ssbk.dto.DeleteNoticeDto;
import com.sse.ssbk.entity.Notice;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.enums.NoticeTypes;
import com.sse.ssbk.service.NoticeService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.NoticeVo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by work_pc on 2017/8/18.
 */
@Service
@Scope(scopeName = "prototype")
public class NoticeServiceImpl implements NoticeService {

   @Autowired
    private NoticeDao noticeDao;

    @Override
    public List<NoticeVo> list(String isSystemNotice, BizUtils.Page page) {
        Example noticeExample = new Example(Notice.class);
        noticeExample.setOrderByClause("create_time desc");
        Example.Criteria criteria = noticeExample.createCriteria();
        criteria.andEqualTo("isDeleted",0);
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        RedisCacheUtils.setForValue(RedisConstants.Notice.LAST_VIEW_NOTICE_TIME+userId,new Date());
        criteria.andEqualTo("userId",userId);
        if (Is.YES.getCode().equals(isSystemNotice)){
            criteria.andEqualTo("noticeType", NoticeTypes.SystemMessage.getType());
        }else {
            criteria.andNotEqualTo("noticeType",NoticeTypes.SystemMessage.getType());
        }
        RowBounds rowBounds;
        if (page.getLastId() != -1) {
            Notice notice = new Notice();
            notice.setNoticeId(page.getLastId());
            notice = noticeDao.selectByPrimaryKey(notice);

            if (notice != null){
                Date createTime = notice.getCreateTime();
                criteria.andLessThan("createTime", createTime);
            }
        }
        rowBounds = new RowBounds(0,page.getPageSize());
        List<Notice> notices = noticeDao.selectByExampleAndRowBounds(noticeExample,rowBounds);

        List<NoticeVo> noticeVos = DtoEntityVoUtils.entityList2voList(notices, NoticeVo.class);

        //将通知置为已读
        List<String> noticesId = noticeVos.stream().map(NoticeVo::getNoticeId).collect(toList());
        if (noticesId.size() > 0){
            readNotice(noticesId);
        }
        return noticeVos;
    }

    @Override
    public void delete(DeleteNoticeDto deleteNoticeDto) {
        String noticeId = deleteNoticeDto.getNoticeId();
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        Notice notice = new Notice();
        notice.setIsDeleted(Is.YES.getCode());
        Example noticeExample =  new Example(Notice.class);
        Example.Criteria criteria = noticeExample.createCriteria();
        criteria.andEqualTo("isDeleted",Is.NO.getCode());
        criteria.andEqualTo("userId",userId);
        if (Is.YES.getCode().equals(deleteNoticeDto.getIsSystemNotice())){
            criteria.andEqualTo("noticeType", NoticeTypes.SystemMessage.getType());
        }else {
            criteria.andNotEqualTo("noticeType",NoticeTypes.SystemMessage.getType());
        }
        if (!"-1".equals(noticeId)){
            criteria.andEqualTo("noticeId",Integer.valueOf(noticeId));
        }
        noticeDao.updateByExampleSelective(notice,noticeExample);
    }



    @Override
    public String hasNewNotices(){
        UserSession currentUser = BizUtils.getCurrentUser();
        Integer userId = currentUser.getUserId();
        Date lastViewNoticeTime = RedisCacheUtils.getForValue(RedisConstants.Notice.LAST_VIEW_NOTICE_TIME+userId);
        if (lastViewNoticeTime == null){
            lastViewNoticeTime = new Date(0);
        }
        Notice notice = new Notice();
        notice.setIsDeleted(Is.YES.getCode());
        Example noticeExample =  new Example(Notice.class);
        Example.Criteria criteria = noticeExample.createCriteria();
        criteria.andEqualTo("isDeleted",Is.NO.getCode());
        criteria.andEqualTo("userId",userId);
        criteria.andGreaterThan("createTime",lastViewNoticeTime);
        int i = noticeDao.selectCountByExample(noticeExample);
        if (i > 0){
            return Is.YES.getCode();
        }else {
            return Is.NO.getCode();
        }
    }

    @Override
    public void readNotice(List<String> noticesId){
        Example example = new Example(Notice.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("noticeId",noticesId);
        Notice notice = new Notice();
        notice.setIsRead("1");
        noticeDao.updateByExampleSelective(notice,example);
    }
}
