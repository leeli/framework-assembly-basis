package com.sse.ssbk.vo;

import lombok.Data;

@Data
public class ForumTopicsVo {
    private String topicId;
    private String topicName;
    private String imgUrl;
    private String isConcern;
    private Integer concernNum;
    private Integer questionNum;

    public ForumTopicsVo() {
        this.topicId = "";
        this.topicName = "";
        this.imgUrl = "";
        this.isConcern = "";
        this.concernNum = 0;
        this.questionNum = 0;
    }
}