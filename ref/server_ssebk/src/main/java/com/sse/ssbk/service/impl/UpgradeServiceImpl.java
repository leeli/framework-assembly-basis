package com.sse.ssbk.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import tk.mybatis.mapper.entity.Example;

import com.sse.ssbk.common.BaseInfo;
import com.sse.ssbk.dao.UpgradeDao;
import com.sse.ssbk.entity.Upgrade;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.service.UpgradeService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.vo.UpgradeVo;

@Service
public class UpgradeServiceImpl implements UpgradeService {
    @Autowired
    private UpgradeDao upgradeDao;

    @Override
    public UpgradeVo getLastestUpgradeInfo() {
        Example example = new Example(Upgrade.class);
        example.setOrderByClause("create_time DESC");
        RowBounds rowBounds = new RowBounds(0, 1);
        List<Upgrade> upgrades = upgradeDao.selectByExampleAndRowBounds(example, rowBounds);
        if (CollectionUtils.isEmpty(upgrades)) {
            return new UpgradeVo();
        }
        UpgradeVo voUpgrade = DtoEntityVoUtils.entity2vo(upgrades.get(0), UpgradeVo.class);
        Is is = this.isUpgrade(voUpgrade.getVersionNo());
        if (Is.YES.equals(is)) {
            voUpgrade.setIsUpgrade(Is.YES.getCode());
        } else {
            voUpgrade.setUpgradeDesc("");
            voUpgrade.setVersionNo("");
            voUpgrade.setIsForce("");
            voUpgrade.setIsUpgrade(Is.NO.getCode());
            voUpgrade.setUrl("");
        }
        return voUpgrade;
    }

    /**
     * 
     * @param versionNo
     * @return
     * @author HT-LiChuanbin 
     * @version 2017年8月18日 下午2:59:10
     */
    private Is isUpgrade(String lastedVersionNo) {
        BaseInfo baseInfo = BizUtils.getBaseInfo();
        return lastedVersionNo.compareTo(baseInfo.getVersion()) > 0 ? Is.YES : Is.NO;
    }
}