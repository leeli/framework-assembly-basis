package com.sse.ssbk.service;

import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.UserInfoDao;
import com.sse.ssbk.dto.UserInfoDto;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.entity.UserInfo;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.utils.SpringContextHolder;
import com.sse.ssbk.vo.UserInfoVo;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yxf on 2017/7/28.
 */
public interface UserInfoService {

    void insertSelective(UserInfo userInfo);

    UserInfoVo getUserInfoByUserId();

    int updateUserInfo(UserInfoDto userInfoDto);

    /**
     * 根据user表更新user_info表
     * 顺便更新token中的值
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void syncUserInfoWithUser();


    /**
     * 根据user_info表更新user表
     * 顺便更新token中的值
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void syncUserWithUserInfo(UserInfo userInfo);

    @Transactional(propagation = Propagation.REQUIRED)
    void syncRedisUserWithUser(User user);
}
