package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.UserCollectAct;
import com.sse.ssbk.entity.UserCollectQuestion;
import com.sse.ssbk.vo.CollectQuestionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/27.
 */
public interface UserCollectQuestionDao extends BaseDao<UserCollectQuestion> {

    List<Integer> getCollectQuestionColumnsInOrder(@Param("userId") Integer userId,@Param("lastQuestionId") Integer lastQuestionId,@Param("pageSize")Integer pageSize);

    List<CollectQuestionVo> getCollectQuestion(@Param("columns") List<Integer> columns);
}
