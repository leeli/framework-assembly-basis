package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
@Entity
@Table(name = "t_user_info")
public class UserInfo {
    @Id
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "real_name")
    private String realName;
    @Column(name = "photo")
    private String userPhoto;       //头像
    @Column(name = "company_name")
    private String company;
    @Column(name = "job_position")
    private String jobPosition;  //职务
    @Column(name = "province_id")
    private Integer provinceId;  //省份id
    @Column(name = "profession")
    private String profession;  //行业
    @Column(name = "education")
    private String education;
    @Column(name = "sex")
    private String sex;
    @Column(name = "email")
    private String email;
    @Column(name = "name_card")
    private String nameCard;    //名片
    @Column(name = "orgcode_certificate")
    private String orgcodeCertificate;
}
