package com.sse.ssbk.service;

import com.sse.ssbk.entity.User;
import com.sse.ssbk.entity.UserInfo;

/**
 * Created by work_pc on 2017/8/16.
 */
public interface MultiTableUpdateService {

    void updateWithUser(User user);

    @Deprecated
    void updateWithUserInfo(UserInfo user);
}
