package com.sse.ssbk.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ForumQuestionListVo {
    // t_forum_question
    private String questionId;
    private String title;
    private String content;
    private List<String> imageArr;

    private String userId;
    private String userName;
    private String userPhoto;
    private String company;

    private Integer replyNum;
    private String isLike;
    private Integer likeNum;
    private String hasNewReply;
    private String IsConcern;

    /** 最多两个. */
    private List<String> topics;

    // 遗漏.
    private String createTime;
    private Integer collectNum;

    private String replyId;
    private Integer questionNum;

    public ForumQuestionListVo() {
        this.questionId = "";
        this.userPhoto = "";
        this.userId = "";
        this.userName = "";
        this.company = "";
        this.title = "";
        this.imageArr = new ArrayList<String>();
        this.content = "";
        this.replyNum = 0;
        this.likeNum = 0;
        this.hasNewReply = "";
        this.IsConcern = "";
        this.isLike = "";
        this.topics = new ArrayList<String>();
        this.createTime = "";
        this.collectNum = 0;
        this.replyId = "";
        this.questionNum = 0;
    }
}