package com.sse.ssbk.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by yxf on 2017/7/21.
 */
@Data
public class UserConcernVo {
    //我关注的人数
    private String userId;
    //关注我的人数
    private String userName;
    private String realName;
    private String company;
    private String photo;
    private String isConcern;
    /*//报名状态
    private String enrollStatus;*/
}
