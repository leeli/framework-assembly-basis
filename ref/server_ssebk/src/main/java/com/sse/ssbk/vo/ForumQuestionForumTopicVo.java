package com.sse.ssbk.vo;

import lombok.Data;

/**
 * 供ForumQuestionVo中的topics属性使用.
 * @author  HT-LiChuanbin 
 * @version 2017年7月25日 上午11:25:22
 */
@Data
public class ForumQuestionForumTopicVo {
    private String topicId;
    private String topicName;
    private String isConcern;
    private Integer concernNum;
    private String imgUrl;

    public ForumQuestionForumTopicVo() {
        this.topicId = "";
        this.topicName = "";
        this.isConcern = "";
        this.concernNum = 0;
        this.imgUrl = "";
    }
}