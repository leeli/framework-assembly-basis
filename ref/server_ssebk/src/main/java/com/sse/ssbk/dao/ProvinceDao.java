package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.Province;

public interface ProvinceDao extends BaseDao<Province> {
}