package com.sse.ssbk.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class ForumReplyAddDto {
    @NotBlank
    private String questionId;
    @NotBlank
    private String content;
    private String[] imageArr;
}