package com.sse.ssbk.dto;

import lombok.Data;

@Data
public class QuestionDto {
	private String title;
	private String content;
	private String isAnonymous;
	private String[] images;
	private String topicId;
}