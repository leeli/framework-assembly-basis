package com.sse.ssbk.dao;

import com.sse.ssbk.dao.base.BaseDao;
import com.sse.ssbk.entity.UserCollectQuestion;
import com.sse.ssbk.entity.UserCollectReply;
import com.sse.ssbk.vo.CollectQuestionVo;
import com.sse.ssbk.vo.CollectReplyVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by yxf on 2017/7/27.
 */
public interface UserCollectReplyDao extends BaseDao<UserCollectReply> {

    List<Integer> getCollectReplyColumnsInOrder(@Param("userId") Integer userId, @Param("lastReplyId") Integer lastReplyId, @Param("pageSize") Integer pageSize);

    List<CollectReplyVo> getCollectReply(@Param("columns") List<Integer> columns);
}
