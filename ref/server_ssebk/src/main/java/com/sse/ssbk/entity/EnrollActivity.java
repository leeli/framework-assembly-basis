package com.sse.ssbk.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by yxf on 2017/7/20.
 */
@Data
@Entity
@Table(name = "t_activity_user_map")
public class EnrollActivity {
    @Id
    @NotNull
    private Integer actId;
    private Integer userId;
    private String status;
    private String userName;
    private Date createTime;
    private Date updateTime;
    private String rejectReason;
}
