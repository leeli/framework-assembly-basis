package com.sse.ssbk.entity;

import com.sse.ssbk.utils.TransIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import sun.rmi.runtime.Log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by yxf on 2017/7/13.
 */
@Data
@Entity
@Table(name = "t_user")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    @TransIgnore
    private static final long serialVersionUID = 7503259038193180654L;
    @Id
    private Integer userId;
    private String userName;
    private Date createTime;
    private Date updateTime;
    private String realName;
    private String companyId;
    private String nameCard;
    private String orgcodeCertificate;
    private String userPhoto;
    private String provinceId;
    private String cityId;
    private String phone;
    private String provinceName;
    private String cityName;
    private String jobPosition;
    private String companyName;
    @Column(name = "latest_login_time")
    private Date lastestLoginTime;
    @Column(name = "clientEndInfo")
    private String clientEndInfo;
    private String status;
}
