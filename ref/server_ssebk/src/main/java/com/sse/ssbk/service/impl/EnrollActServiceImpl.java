package com.sse.ssbk.service.impl;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ActivityDao;
import com.sse.ssbk.dao.EnrollActivityDao;
import com.sse.ssbk.dto.AuditActivityJoinerDto;
import com.sse.ssbk.entity.Activity;
import com.sse.ssbk.entity.EnrollActivity;
import com.sse.ssbk.entity.TopicQuestion;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.service.EnrollActService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by yxf on 2017/7/17.
 */
@Service
public class EnrollActServiceImpl implements EnrollActService {

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private EnrollActivityDao enrollActivityDao;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 报名活动
     * @param enrollActivity
     */
    @Override
    public EnrollActivity enrollAct(EnrollActivity enrollActivity) {
//        User loginUser = (User) RedisCacheUtil.getObj("loginUser");
        UserSession loginUser =  BizUtils.getCurrentUser();
        enrollActivity.setCreateTime(new Date());
        enrollActivity.setUserId(loginUser.getUserId());
        enrollActivity.setUserName(loginUser.getUserName());
        //todo 获取活动详情
        Activity activity = new Activity();
        activity.setActId(enrollActivity.getActId());
        activity = activityDao.selectByPrimaryKey(activity);
//        activity = activityDao.getActByPrimaryKey(enrollActivity.getActId());
        //判断活动是否需要审核
        if("0".equals(activity.getIsAudit())){
            //不需要审核，变为报名成功状态
            enrollActivity.setStatus("1");
        }else{
            //需要审核,变为待审核状态
            enrollActivity.setStatus("0");
        }

        if(stringRedisTemplate.opsForZSet().score("actId", enrollActivity.getCreateTime().toString()) != null){
            stringRedisTemplate.opsForZSet().incrementScore("actId",enrollActivity.getCreateTime().toString(),1);
        }else {
            stringRedisTemplate.opsForZSet().add("actId",enrollActivity.getCreateTime().toString(),1);
        }
        //报名失败
        try{
            enrollActivityDao.insertSelective(enrollActivity);
        }catch(Exception ex){
            enrollActivity.setStatus("2");
        }
        return enrollActivity;
    }

    /**
     * 查看用户报名状态
     * @param userId
     * @param actId
     * @return
     */
    @Override
    public String getEnrollStatus(Integer userId, Integer actId) {
        EnrollActivity enrollActivity = new EnrollActivity();
        enrollActivity.setUserId(userId);
        enrollActivity.setActId(actId);
        enrollActivity = enrollActivityDao.selectOne(enrollActivity);
        if (enrollActivity == null || enrollActivity.getStatus().equals("")){
            //未报名
            return "0";
        }
        //其他情况
        return String.valueOf(Integer.valueOf(enrollActivity.getStatus()) + 1);
    }


    /**
     * 获取活动不同用户状态的数量
     * @param actId
     * @param status 0 状态0报名(未审核)，1批准（审核通过），2被拒(审核不通过)  3 所有报名用户
     * @return
     */
    public String getActivityCount(Integer actId,String status){
        EnrollActivity enrollActivity = new EnrollActivity();
        enrollActivity.setActId(actId);
        //获取活动报名人数
        if ("3".equals(status)){
            return String.valueOf(enrollActivityDao.selectCount(enrollActivity));
        }
        enrollActivity.setStatus(status);
        return String.valueOf(enrollActivityDao.selectCount(enrollActivity));
    }

    /**
     * 获取活动报名人的前三张头像
     * @param actId
     * @return
     */
    public String[] getTop3Photo(Integer actId){
        String[] top3Photos = enrollActivityDao.getTop3Photo(actId);
        return top3Photos;
    }

    @Override
    public int auditActivityJoiner(Integer actId,AuditActivityJoinerDto auditActivityJoinerDto) {
        return enrollActivityDao.auditActivityJoiner(actId,auditActivityJoinerDto.getUserIds(),auditActivityJoinerDto.getReason(),auditActivityJoinerDto.getEnrollStatus());
    }

    @Override
    public String getRejectReason(String actId, String userId) {
        Example enrollActivityExample = new Example(EnrollActivity.class);
        Example.Criteria criTopicQuestion = enrollActivityExample.createCriteria();
        criTopicQuestion.andEqualTo("actId", actId).andEqualTo("userId", userId);
        EnrollActivity enrollActivity = enrollActivityDao.selectByExample(enrollActivityExample).get(0);
        if (enrollActivity == null){
            return "";
        }
        return enrollActivity.getRejectReason() == null ? "":enrollActivity.getRejectReason();
    }
}
