package com.sse.ssbk.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_forum_topic")
public class ForumTopic {
    @Id
    @Column(name = "topic_id")
    private Integer topicId;
    @Column(name = "topic_name")
    private String topicName;
    @Column(name = "topic_picture")
    private String topicPicture;
    @Column(name = "create_time")
    private Date createTime;
    @Column(name = "update_time")
    private Date updateTime;
    @Column(name = "sequence")
    private Short sequence;
    @Column(name = "app_id")
    private String appId;
}