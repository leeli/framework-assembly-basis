package com.sse.ssbk.common;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final String DEFAULT_STRING_VALUE = "";
    public static final Integer DEFAULT_INTEGER_VALUE = 0;
    /** 会话默认过期时间，单位毫秒. */
    public static final Long SESSION_TIME_OUT = 24 * 60 * 60 * 1000L;

    public static final Long MILLISECONDS_OF_DAY = 24 * 60 * 60 * 1000L;

    public static final Long MILLISECONDS_OF_MINUTE = 60 * 1000L;

    public static final Long SECONDS_OF_MINUTE = 60L;

    public static final int ISHOTNUM = 5;

    public static final List<String> UPDATE_PHOTO_TABLE = Arrays.asList("t_news","t_reply_comment","t_news_comment","t_forum_reply","t_forum_question","t_activity_comment");
    public static final List<String> UPDATE_COMPANY_NAME_TABLE = Arrays.asList("t_news","t_reply_comment","t_news_comment","t_forum_reply","t_forum_question","t_activity");
    public static final List<String> UPDATE_COMPANY_ID_TABLE = Arrays.asList("t_news","t_reply_comment","t_news_comment","t_forum_reply","t_forum_question","t_activity");

    public static final int MIN_SCORE_LEVEL = 1;
    public static final int MAX_SCORE_LEVEL = 8;

}