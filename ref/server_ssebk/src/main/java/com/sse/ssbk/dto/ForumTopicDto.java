package com.sse.ssbk.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "t_forum_topic")
public class ForumTopicDto {
	private Integer topicId;
	private String topicName;
	private Date createTime;
	private Date updateTime;
	private Short sequence;
	private String appId;
}