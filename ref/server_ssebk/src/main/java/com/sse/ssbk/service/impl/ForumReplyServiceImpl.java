package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import tk.mybatis.mapper.entity.Example;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ForumQuestionDao;
import com.sse.ssbk.dao.ForumReplyDao;
import com.sse.ssbk.dao.ForumTopicDao;
import com.sse.ssbk.dao.TopicQuestionDao;
import com.sse.ssbk.dto.ForumReplyAddDto;
import com.sse.ssbk.entity.ForumQuestion;
import com.sse.ssbk.entity.ForumReply;
import com.sse.ssbk.entity.ForumTopic;
import com.sse.ssbk.entity.TopicQuestion;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.ForumReplyService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.utils.EmojiUtils;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ForumReplyGetForumTopicVo;
import com.sse.ssbk.vo.ForumReplyGetVo;
import com.sse.ssbk.vo.ForumReplyListVo;

@Service
public class ForumReplyServiceImpl implements ForumReplyService {
    @Autowired
    private ForumReplyDao forumReplyDao;
    @Autowired
    private ForumQuestionDao forumQuestionDao;
    @Autowired
    private TopicQuestionDao topicQuestionDao;
    @Autowired
    private ForumTopicDao forumTopicDao;

    @Override
    public ForumReplyGetVo get(Integer id) {
        this.validateGet(id);
        UserSession loginUser = BizUtils.getCurrentUser();
        ForumReplyGetVo voForumReplyGet = new ForumReplyGetVo();
        ForumReply forumReply = forumReplyDao.selectByPrimaryKey(id);
        Integer questionId = forumReply.getQuestionId();

        voForumReplyGet.setForumReplyGetVoFromForumReply(forumReply);
        if (forumReply.getImages() == null || "".equals(forumReply.getImages())) {
            voForumReplyGet.setImageArr(new String[0]);
        } else {
            voForumReplyGet.setImageArr(BizUtils.toStringArray(forumReply.getImages()));
        }
        voForumReplyGet.setReplyContent(EmojiUtils.decode(forumReply.getContent()));
        // 获取板块信息.
        TopicQuestion topicQuestionForQuery = new TopicQuestion();
        topicQuestionForQuery.setQuestionId(questionId);
        topicQuestionForQuery.setIsDelete(Is.NO.getCode());
        List<ForumTopic> forumTopics = forumTopicDao.selectByQuestionId(questionId);
        //        List<ForumReplyGetForumTopicVo> voForumReplyGetForumTopics = DtoEntityVoUtils.entityList2voList(forumTopics, ForumReplyGetForumTopicVo.class);
        List<ForumReplyGetForumTopicVo> voForumReplyGetForumTopics = new ArrayList<>();
        for (ForumTopic forumTopic : forumTopics) {
            ForumReplyGetForumTopicVo forumReplyGetForumTopicVo = DtoEntityVoUtils.entity2vo(forumTopic, ForumReplyGetForumTopicVo.class);
            forumReplyGetForumTopicVo.setImgUrl(forumTopic.getTopicPicture() == null ? "" : forumTopic.getTopicPicture());

            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_TOPIC_USER + forumTopic.getTopicId());
            String isConcern = Is.NO.getCode();
            if (forSet != null && forSet.contains(loginUser.getUserId())) {
                isConcern = Is.YES.getCode();
            }
            forumReplyGetForumTopicVo.setIsConcern(isConcern);
            forumReplyGetForumTopicVo.setConcernNum(String.valueOf(forSet.size()));
            voForumReplyGetForumTopics.add(forumReplyGetForumTopicVo);
        }

        voForumReplyGet.setTopics(voForumReplyGetForumTopics);

        // 获取问题信息.
        ForumQuestion forumQuestion = forumQuestionDao.selectByPrimaryKey(questionId);

        //        voForumReplyGet.setQuestionId(String.valueOf(questionId));
        voForumReplyGet.setForumReplyGetVoFromForumQuestion(forumQuestion);

        //是否关注问题发布者，是否关注回答者，是否点赞，是否收藏

        Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());

        String questionUserIsConcern;
        String replyUserIsConcern;
        if (forSet != null && forSet.size() != 0) {
            if (forSet.contains(forumQuestion.getUserId())) {
                questionUserIsConcern = Is.YES.getCode();
            } else {
                questionUserIsConcern = Is.NO.getCode();
            }

            if (forSet.contains(forumReply.getUserId())) {
                replyUserIsConcern = Is.YES.getCode();
            } else {
                replyUserIsConcern = Is.NO.getCode();
            }
        } else {
            questionUserIsConcern = Is.NO.getCode();
            replyUserIsConcern = Is.NO.getCode();
        }

        String isLike;
        forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
        if (forSet != null && forSet.contains(loginUser.getUserId())) {
            isLike = Is.YES.getCode();
        } else {
            isLike = Is.NO.getCode();
        }
        String isCollect;
        forSet = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + loginUser.getUserId());
        if (forSet != null && forSet.contains(forumReply.getReplyId())) {
            isCollect = Is.YES.getCode();
        } else {
            isCollect = Is.NO.getCode();
        }
        Integer replyLikeNum = 0;
        forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
        if (forSet != null) {
            replyLikeNum = forSet.size();
        }

        Integer replyCommentNum = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + forumReply.getReplyId());
        if (replyCommentNum == null) {
            replyCommentNum = 0;
        }

        Integer replyCollectNum = RedisCacheUtils.getInt(RedisConstants.User.CollectionCount.COLLECTION_COUNT_REPLY + forumReply.getReplyId());
        if (replyCollectNum == null) {
            replyCollectNum = 0;
        }

        voForumReplyGet.setReplyCommentNum(replyCommentNum);
        voForumReplyGet.setReplyCollectNum(replyCollectNum);
        voForumReplyGet.setReplyLikeNum(replyLikeNum);
        voForumReplyGet.setQuestionUserIsConcern(questionUserIsConcern);
        voForumReplyGet.setReplyUserIsConcern(replyUserIsConcern);
        voForumReplyGet.setIsLike(isLike);
        voForumReplyGet.setIsCollect(isCollect);
        // 获取回答信息.
        // 获取和当前用户相关信息.
        return DtoEntityVoUtils.voFieldFromNull2Empty(voForumReplyGet);
    }

    @Override
    public List<ForumReplyListVo> listByQuesionId(Integer questionId, Page page) {
        List<ForumReplyListVo> voForumReplyLists = new ArrayList<ForumReplyListVo>();

        Example exmForumQuestion = new Example(ForumReply.class);
        exmForumQuestion.setOrderByClause("create_time DESC");
        Example.Criteria criForumQuestion = exmForumQuestion.createCriteria();
        UserSession loginUser = BizUtils.getCurrentUser();
        criForumQuestion.andEqualTo("questionId", questionId);
        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode());
        ForumReply forumReplyForSort = forumReplyDao.selectByPrimaryKey(page.getLastId());
        if (page.getLastId() != -1) {
            if (forumReplyForSort != null) {
                criForumQuestion.andLessThan("createTime", forumReplyForSort.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        //        RowBounds rowBounds = new RowBounds(page.getLastId(), page.getPageSize());
        List<ForumReply> forumReplys = forumReplyDao.selectByExampleAndRowBounds(exmForumQuestion, rowBounds);
        Iterator<ForumReply> iForumReply = forumReplys.iterator();
        while (iForumReply.hasNext()) {
            ForumReply forumReply = iForumReply.next();
            ForumReplyListVo voForumReplyList = DtoEntityVoUtils.entity2vo(forumReply, ForumReplyListVo.class);
            if (forumReply.getIsAnonymous().equals(Is.YES.getCode())) { //该回答已匿名
                voForumReplyList.setUserId("");
                voForumReplyList.setUserName("匿名");
                voForumReplyList.setUserPhoto("");
                voForumReplyList.setCompany("未公开");
            } else {
                if (forumReply.getCompanyName() == null) {
                    voForumReplyList.setCompany("");
                } else {
                    voForumReplyList.setCompany(forumReply.getCompanyName());
                }
            }

            if (voForumReplyList.getQuestionTitle() == null) {
                voForumReplyList.setQuestionTitle("");
            }
            if (forumReply.getImages() == null || "".equals(forumReply.getImages())) {
                voForumReplyList.setImageArr(new String[0]);
            } else {
                voForumReplyList.setImageArr(BizUtils.toStringArray(forumReply.getImages()));
            }

            //            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + forumReply.getUserId());
            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + loginUser.getUserId());

            String isConcern;
            if (forSet != null && forSet.contains(forumReply.getUserId())) {
                isConcern = Is.YES.getCode();
            } else {
                isConcern = Is.NO.getCode();
            }

            String likeNum = "0";
            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null) {
                likeNum = String.valueOf(forSet.size());
            }

            String commentNum = "0";

            Integer comNum = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + forumReply.getReplyId());
            if (comNum != null) {
                commentNum = String.valueOf(comNum);
            }

            String isLike;
            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null && forSet.contains(loginUser.getUserId())) {
                isLike = Is.YES.getCode();
            } else {
                isLike = Is.NO.getCode();
            }

            voForumReplyList.setIsConcern(isConcern);
            voForumReplyList.setLikeNum(likeNum);
            voForumReplyList.setCommentNum(commentNum);
            voForumReplyList.setIsLike(isLike);
            voForumReplyLists.add(voForumReplyList);
        }
        return voForumReplyLists;
    }

    @Override
    public List<ForumReplyListVo> listByUserId(Integer userId, Page page) {
        List<ForumReplyListVo> voForumReplyLists = new ArrayList<ForumReplyListVo>();

        /*List<Integer> replyIds = new ArrayList<Integer>();
        Set<Integer> setReplyIds = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + userId);
        for (Integer objReplyId : setReplyIds) {
            replyIds.add(Integer.valueOf(objReplyId.toString()));
        }*/

        Example exmForumQuestion = new Example(ForumReply.class);
        exmForumQuestion.setOrderByClause("create_time DESC");
        Example.Criteria criForumQuestion = exmForumQuestion.createCriteria();

        UserSession loginUser = BizUtils.getCurrentUser();

        if (userId == null) {
            userId = loginUser.getUserId();
        } else if (Integer.compare(loginUser.getUserId(), userId) != 0) {
            criForumQuestion.andEqualTo("isAnonymous", "0"); //非当前用户只能查看不匿名的回答
        }

        //        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode()).andIn("replyId", replyIds);
        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode());
        criForumQuestion.andEqualTo("userId", userId);
        ForumReply forumReplyForSort = forumReplyDao.selectByPrimaryKey(page.getLastId());
        if (page.getLastId() != -1) {
            if (forumReplyForSort != null) {
                criForumQuestion.andLessThan("createTime", forumReplyForSort.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        List<ForumReply> forumReplys = forumReplyDao.selectByExampleAndRowBounds(exmForumQuestion, rowBounds);
        Iterator<ForumReply> iForumReply = forumReplys.iterator();
        while (iForumReply.hasNext()) {
            ForumReply forumReply = iForumReply.next();
            ForumReplyListVo voForumReplyList = DtoEntityVoUtils.entity2vo(forumReply, ForumReplyListVo.class);
            ForumQuestion forumQuestion = new ForumQuestion();
            forumQuestion.setQuestionId(forumReply.getQuestionId());
            forumQuestion = forumQuestionDao.selectByPrimaryKey(forumQuestion);

            voForumReplyList.setQuestionId(String.valueOf(forumQuestion.getQuestionId()));
            voForumReplyList.setQuestionTitle(forumQuestion.getTitle());

            if (forumReply.getImages() == null || "".equals(forumReply.getImages())) {
                voForumReplyList.setImageArr(new String[0]);
            } else {
                voForumReplyList.setImageArr(BizUtils.toStringArray(forumReply.getImages()));
            }

            if (forumReply.getCompanyName() == null) {
                voForumReplyList.setCompany("");
            } else {
                voForumReplyList.setCompany(forumReply.getCompanyName());
            }

            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + userId);

            String isConcern;
            if (forSet != null && forSet.contains(forumReply.getUserId())) {
                isConcern = Is.YES.getCode();
            } else {
                isConcern = Is.NO.getCode();
            }
            String isLike;
            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null && forSet.contains(userId)) {
                isLike = Is.YES.getCode();
            } else {
                isLike = Is.NO.getCode();
            }

            String likeNum = "0";
            //            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null) {
                likeNum = String.valueOf(forSet.size());
            }
            String commentNum = "0";
            Integer comment = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + forumReply.getReplyId());
            if (comment != null) {
                commentNum = String.valueOf(comment);
            }
            voForumReplyList.setIsLike(isLike);
            voForumReplyList.setIsConcern(isConcern);
            voForumReplyList.setLikeNum(likeNum);
            voForumReplyList.setCommentNum(commentNum);
            voForumReplyList.setCompany(forumReply.getCompanyName());
            //            voForumReplyLists.add(voForumReplyList);
            voForumReplyLists.add(DtoEntityVoUtils.voFieldFromNull2Empty(voForumReplyList));
        }

        return voForumReplyLists;
    }

    @Override
    public List<ForumReplyListVo> listByUserIdAndQuesionId(Integer userId, Integer questionId, Page page) {
        List<ForumReplyListVo> voForumReplyLists = new ArrayList<ForumReplyListVo>();

        /*List<Integer> replyIds = new ArrayList<Integer>();
        Set<Integer> setReplyIds = RedisCacheUtils.getForSet(RedisConstants.User.Collection.USER_COLLECTION_REPLY + userId);
        for (Integer objReplyId : setReplyIds) {
            replyIds.add(Integer.valueOf(objReplyId.toString()));
        }*/

        Example exmForumQuestion = new Example(ForumReply.class);
        exmForumQuestion.setOrderByClause("create_time DESC");
        Example.Criteria criForumQuestion = exmForumQuestion.createCriteria();

        UserSession loginUser = BizUtils.getCurrentUser();

        if (userId == null) {
            userId = loginUser.getUserId();
        } else if (Integer.compare(loginUser.getUserId(), userId) != 0) {
            criForumQuestion.andEqualTo("isAnonymous", "0"); //非当前用户只能查看不匿名的回答
        }

        //        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode()).andIn("replyId", replyIds);
        criForumQuestion.andEqualTo("isDelete", Is.NO.getCode());
        criForumQuestion.andEqualTo("userId", userId);
        criForumQuestion.andEqualTo("questionId", questionId);
        ForumReply forumReplyForSort = forumReplyDao.selectByPrimaryKey(page.getLastId());
        if (page.getLastId() != -1) {
            if (forumReplyForSort != null) {
                criForumQuestion.andLessThan("createTime", forumReplyForSort.getCreateTime());
            }
        }
        RowBounds rowBounds = new RowBounds(0, page.getPageSize());
        List<ForumReply> forumReplys = forumReplyDao.selectByExampleAndRowBounds(exmForumQuestion, rowBounds);
        Iterator<ForumReply> iForumReply = forumReplys.iterator();
        while (iForumReply.hasNext()) {
            ForumReply forumReply = iForumReply.next();
            ForumReplyListVo voForumReplyList = DtoEntityVoUtils.entity2vo(forumReply, ForumReplyListVo.class);
            ForumQuestion forumQuestion = new ForumQuestion();
            forumQuestion.setQuestionId(forumReply.getQuestionId());
            forumQuestion = forumQuestionDao.selectByPrimaryKey(forumQuestion);

            voForumReplyList.setQuestionId(String.valueOf(forumQuestion.getQuestionId()));
            voForumReplyList.setQuestionTitle(forumQuestion.getTitle());

            if (forumReply.getImages() == null || "".equals(forumReply.getImages())) {
                voForumReplyList.setImageArr(new String[0]);
            } else {
                voForumReplyList.setImageArr(BizUtils.toStringArray(forumReply.getImages()));
            }

            if (forumReply.getCompanyName() == null) {
                voForumReplyList.setCompany("");
            } else {
                voForumReplyList.setCompany(forumReply.getCompanyName());
            }

            Set<Integer> forSet = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + userId);

            String isConcern;
            if (forSet != null && forSet.contains(forumReply.getUserId())) {
                isConcern = Is.YES.getCode();
            } else {
                isConcern = Is.NO.getCode();
            }
            String isLike;
            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null && forSet.contains(userId)) {
                isLike = Is.YES.getCode();
            } else {
                isLike = Is.NO.getCode();
            }

            String likeNum = "0";
            forSet = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLY_USER + forumReply.getReplyId());
            if (forSet != null) {
                likeNum = String.valueOf(forSet.size());
            }
            String commentNum = "0";
            Integer commentN = RedisCacheUtils.getInt(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + forumReply.getReplyId());
            if (commentN != null) {
                commentNum = String.valueOf(forSet.size());
            }

            voForumReplyList.setIsLike(isLike);
            voForumReplyList.setIsConcern(isConcern);
            voForumReplyList.setLikeNum(likeNum);
            voForumReplyList.setCommentNum(commentNum);

            //            voForumReplyLists.add(voForumReplyList);
            voForumReplyLists.add(DtoEntityVoUtils.voFieldFromNull2Empty(voForumReplyList));
        }
        return voForumReplyLists;
    }

    /**
     * 
     * @param id
     * @author HT-LiChuanbin 
     * @version 2017年7月31日 下午3:58:18
     */
    private void validateGet(Integer id) {
        if (id == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public void save(ForumReplyAddDto dto) {
        this.validateBizSave(dto);
        UserSession session = BizUtils.getCurrentUser();
        Date now = new Date();
        ForumReply forumReply = DtoEntityVoUtils.dto2entity(dto, ForumReply.class);
        forumReply.setContent(EmojiUtils.encode(dto.getContent()));
        forumReply.setImages(BizUtils.getStrImages(dto.getImageArr()));
        forumReply.setUserId(session.getUserId());
        forumReply.setUserName(session.getRealName());
        forumReply.setUserPhoto(session.getUserPhoto());
        forumReply.setCompanyId(session.getCompanyId());
        forumReply.setCompanyName(session.getCompanyName());
        forumReply.setCreateTime(now);
        forumReplyDao.insertSelectiveWithId(forumReply);

        String questionId = dto.getQuestionId();
        Integer replyId = forumReply.getReplyId();
        RedisCacheUtils.addForSet(RedisConstants.QUESTION_REPLY + questionId, replyId);
    }

    /**
     * 
     * @param dto
     * @author HT-LiChuanbin 
     * @version 2017年8月17日 下午5:46:29
     */
    private void validateBizSave(ForumReplyAddDto dto) {
        if (dto == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        Integer quesionId = Integer.parseInt(dto.getQuestionId());
        Example example = new Example(ForumQuestion.class);
        example.createCriteria().andEqualTo("questionId", quesionId).andEqualTo("isDelete", Is.NO.getCode());
        List<ForumQuestion> forumQuestions = forumQuestionDao.selectByExample(example);
        if (CollectionUtils.isEmpty(forumQuestions)) {
            throw new SSEBKRuntimeException("当前问题已被删除，请刷新重试！");
        }
    }

    @Override
    public void del(Integer id) {
        this.validateDel(id);
        ForumReply forumReply = forumReplyDao.selectByPrimaryKey(id);
        Integer questionId = forumReply.getQuestionId();
        RedisCacheUtils.delForSet(RedisConstants.QUESTION_REPLY + questionId, id);
        forumReplyDao.deleteByPrimaryKeyLogic(id);
    }

    /**
     * 
     * @param id
     * @author HT-LiChuanbin 
     * @version 2017年8月16日 上午11:09:45
     */
    private void validateDel(Integer id) {
        // 要删除的回答必须存在.
        Example example = new Example(ForumReply.class);
        example.createCriteria().andEqualTo("isDelete", Is.NO.getCode()).andEqualTo("replyId", id);
        List<ForumReply> forumReplys = forumReplyDao.selectByExample(example);
        if (CollectionUtils.isEmpty(forumReplys) || forumReplys.size() != 1) {
            throw new SSEBKRuntimeException(AppReturnCode.NOT_EXIST);
        }

        // 属于本人的回答的且没有被人评论或者收藏的回答，方可删除。
        ForumReply forumReply = forumReplys.get(0);
        UserSession session = BizUtils.getCurrentUser();
        Integer currentUserId = session.getUserId();
        if (!forumReply.getUserId().equals(currentUserId)) {
            throw new SSEBKRuntimeException("无法操作别人的回答！");
        }
        Set<Integer> replyComments = RedisCacheUtils.getForSet(RedisConstants.REPLY_COMMENT + id);
        if (!CollectionUtils.isEmpty(replyComments)) {
            throw new SSEBKRuntimeException("已经被评论，无法操作！");
        }
        long collectionCountReplys = RedisCacheUtils.getLong(RedisConstants.User.CollectionCount.COLLECTION_COUNT_REPLY + id);
        if (collectionCountReplys > 0) {
            throw new SSEBKRuntimeException("已经被收藏，无法操作！");
        }
    }

    @Override
    @Deprecated
    public void upt(ForumReplyAddDto dto) {
    }
}
