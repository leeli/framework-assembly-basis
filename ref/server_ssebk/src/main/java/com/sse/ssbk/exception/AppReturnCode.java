package com.sse.ssbk.exception;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;

/**
 * 前后端交互枚举定义.
 * @author  HT-LiChuanbin 
 * @version 2017年7月19日 下午1:31:23
 */
@Slf4j
public enum AppReturnCode {
	OK("200", "成功", ""),
	NEWS_PUBLISH_SUCCESS("201","资讯发布成功",""),
	SERVER_ERROR("500", "服务器繁忙", ""),
	
	CONCERN_YET("00035","已经关注过", ""),
	
    ParaError("00001","参数错误","参数格式错误"),
    OutDate("00002","appid已过期","该appid注册的SDK已过期"),
    NoExist("00003","appid不合法","该appid没有注册相应SDK"),
    LoginOutDate("00004","登录已过期","时间超过五分钟"),
    ParaReqMiss("00005","缺少必填参数","reqContent里的参数不全"),
	DataNotExist("00006", "无符合条件数据","通过条件查询数据库无符合条件数据"),
	FriendExists("00007", "好友已存在","通过条件查询数据库无符合条件数据"),
	FriendReqExists("00008", "您已发送了好友请求","通过条件查询数据库无符合条件数据"),
	FriendReqNotExists("00009", "该用户未发送好友请求","通过条件查询数据库无符合条件数据"),
	FriendReqSelf("00010", "不能添加自己为好友","通过条件查询数据库无符合条件数据"),
	NoSuchUser("00011","用户不存在","通过条件查询数据库无符合条件数据"),
	NoSuchInvitationCodeOrName("00012","邀请码或用户名不正确","通过条件查询数据库无符合条件数据"),
    DecryptError("00200","解密失败","解密失败"),
    signedError("00300","签名错误","签名验证失败"),
	CustomError("00400","占位符自定义异常","错误信息使用传入信息"),
    UndefinedError("00000","未定义异常","出现未定义异常"),//
    
	NOT_EXIST("00500","资源不存在","操作对象资源不存在");
    private final String code;
    private final String msg;
    private final String desc;

    AppReturnCode(String value,String msg,String desc) {
        this.code = value;
        this.msg = msg;
        this.desc = desc;
    }
    
	public String getCode() {
		return code;
	}
	public String getMsg() {
		return msg;
	}
	public String getDesc() {
		return desc;
	}
	
	/**
	 * 通过code解析枚举.
	 * @param code 枚举code.
	 * @return     对应的枚举，如果没有则为null.
	 * @author     HT-LiChuanbin 
	 * @version    2017年7月19日 下午1:30:15
	 */
	public AppReturnCode parse(String code) {
		if(StringUtils.isBlank(code)){
			log.warn("入参code：" + code + "不合理！");
			return null;
		}
		code = code.trim();
		for(AppReturnCode appReturn : AppReturnCode.values()){
			if(StringUtils.equals(code, appReturn.getCode())){
				return appReturn;
			}
		}
		log.warn("入参code：" + code + "超出业务范围！");
		return null;
	}
}