package com.sse.ssbk.service.impl;

import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.CompanyInfoDao;
import com.sse.ssbk.dao.UserDao;
import com.sse.ssbk.dto.CompanyInfoDto;
import com.sse.ssbk.entity.CompanyInfo;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.service.CompanyInfoService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yxf on 2017/7/27.
 */
@Service
public class CompanyInfoServiceImpl implements CompanyInfoService{

    @Autowired
    private CompanyInfoDao companyInfoDao;

    @Autowired
    private UserDao userDao;

    @Override
    public void addCompanyInfo(CompanyInfoDto companyInfoDto) {
        CompanyInfo companyInfo = DtoEntityVoUtils.dto2entity(companyInfoDto, CompanyInfo.class);
        companyInfoDao.insertSelectiveWithId(companyInfo);
        Integer companyId = companyInfo.getCompanyId();
        UserSession currentUser = BizUtils.getCurrentUser();
        User user = new User();
        user.setUserId(currentUser.getUserId());
        user.setCompanyId(String.valueOf(companyId));
        userDao.updateByPrimaryKeySelective(user);
    }

    @Override
    public int updateCompanyInfo(CompanyInfoDto companyInfoDto,Integer finanicalId) {
        CompanyInfo companyInfo = DtoEntityVoUtils.dto2entity(companyInfoDto, CompanyInfo.class);
        companyInfo.setCompanyId(finanicalId);
        return companyInfoDao.updateByPrimaryKeySelective(companyInfo);
    }

    @Override
    public CompanyInfo selectCompanyInfoById(Integer companyId) {
        CompanyInfo companyInfo = new CompanyInfo();
        companyInfo.setCompanyId(companyId);

        return companyInfoDao.selectByPrimaryKey(companyInfo);
    }
}
