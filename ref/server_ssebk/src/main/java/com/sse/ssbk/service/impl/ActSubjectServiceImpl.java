package com.sse.ssbk.service.impl;

import java.util.*;
import java.util.concurrent.TimeUnit;

import com.sse.ssbk.common.Constants;
import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.service.ActSubjectService;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.ActivityCityVo;
import com.sse.ssbk.vo.ActivitySubjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sse.ssbk.dao.ActivitySubjectDao;
import com.sse.ssbk.entity.ActSubject;
import com.sse.ssbk.utils.DtoEntityVoUtils;
import com.sse.ssbk.vo.ActSubjectVo;
import tk.mybatis.mapper.entity.Example;

/**
 * Created by yxf on 2017/7/17.
 */
@Service
public class ActSubjectServiceImpl implements ActSubjectService {

	@Autowired
	private ActivitySubjectDao activitySubjectDao;

	/**
	 * 主题列表 按sort进行排序
	 * @param lastSubjectId
	 * @param pageSize
	 * @return
	 */
	@Override
	public List<ActSubjectVo> getActSubjects(String lastSubjectId, String pageSize) {
		LinkedHashSet<Integer> actIds = RedisCacheUtils.getForValue(RedisConstants.SUBJECT_ORDERBY_SORT_DESC);
		Example example;
		if (actIds == null || actIds.size() ==0){
			actIds = new LinkedHashSet<>();
			example = new Example(ActSubject.class);
			example.setOrderByClause("sort desc");
			example.selectProperties("subjectId");
			List<ActSubject> actSubjects = activitySubjectDao.selectByExample(example);
			for (ActSubject actSubject:actSubjects){
				actIds.add(Integer.valueOf(actSubject.getSubjectId()));
			}
			//设置超时时间10分钟
			RedisCacheUtils.setForValue(RedisConstants.SUBJECT_ORDERBY_SORT_DESC,actIds,Constants.SECONDS_OF_MINUTE * 10,TimeUnit.SECONDS);
		}
		List<Integer> ids = new ArrayList<>(actIds);
		int index;
		if("-1".equals(lastSubjectId)){
			index = -1;
		}else {
			index = ids.indexOf(Integer.valueOf(lastSubjectId));
		}
		index += 1;

		ids = ids.subList(index, Math.min(ids.size(),index + Integer.valueOf(pageSize)));

		if (ids.size() == 0){
			return new ArrayList<>();
		}

		List<ActSubject> actSubjects = activitySubjectDao.getActSubejectsInColumns(ids);


		/* 此种方式没办法达到 order by field的效果  会乱序
		example = new Example(ActSubject.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andIn("subjectId",ids);
		List<ActSubject> actSubjects = activitySubjectDao.selectByExample(example);*/
		List<ActSubjectVo> actSubjectVoList = DtoEntityVoUtils.entityList2voList(actSubjects, ActSubjectVo.class);
		return actSubjectVoList;
	}

	@Override
	public List<ActivitySubjectVo> getActivitySubjects(String lastSubjectId, String pageSize) {
		List<Integer> columns = activitySubjectDao.getActivitySubjectsColumnInOrder(Integer.valueOf(lastSubjectId), Integer.valueOf(pageSize));
		
		if (columns != null && columns.size() != 0){
			List<ActivitySubjectVo> activitySubjects = activitySubjectDao.getActivitySubjects(columns);
			//todo  有图片之后可能需要去掉 picture为空
			for (ActivitySubjectVo activitySubjectVo:activitySubjects){
				DtoEntityVoUtils.voFieldFromNull2Empty(activitySubjectVo);
			}
			return activitySubjects;
		}else {
			return new ArrayList<ActivitySubjectVo>();
		}
	}

	@Override
	public List<ActivityCityVo> getActivityCity(String lastCityId, String pageSize) {
		List<ActivityCityVo> activityCity = activitySubjectDao.getActivityCity(Integer.valueOf(lastCityId), Integer.valueOf(pageSize));
		return activityCity;
	}
}
