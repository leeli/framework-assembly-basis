package com.sse.ssbk.vo;

import java.util.Map;

import lombok.Data;

/**
 * Created by ciyua on 2017/7/25.
 */
@Data
public class UserVo {
    // 用户ID
    private Integer userId;
    // 用户名
    private String userName;
    // 真实姓名
    private String realName;
    // 用户头像
    private String userPhoto;
    // 状态
    private String userStatus;
    // 身份凭证
    private String token;
    // 权限相关
    private Map<String, Boolean> perm;
    // private List<RoleVo> roleList;
    // 用户所在城市ID
    private String province;
    // 公司ID
    private String company;
    // 财务指标，0=未完善，1=已完善
    private String isCompleteFinancial;
}