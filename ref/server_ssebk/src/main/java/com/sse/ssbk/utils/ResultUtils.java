package com.sse.ssbk.utils;

import com.sse.ssbk.exception.AppReturnCode;

/**
 * 通用返回结果工具类.
 * @author  Thomas Lee
 * @version 2017年6月8日 上午12:20:46
 */
public class ResultUtils {
	/**
	 * 返回通用成功信息.
	 * @param data       泛型.
	 * @return Result<T> 通用返回结果.
	 * @author  		  Thomas Lee
	 * @version 		  2017年6月8日 上午12:23:33
	 * @version          2017年6月26日 下午3:00:28
	 */
	public static <T> Result<T> success(T data) {
		Result<T> result = new Result<T>();
		result.setCode(AppReturnCode.OK.getCode());
		result.setMsg(AppReturnCode.OK.getMsg());
		result.setData(data);
		return result;
	}

	/**
	 * 返回通用成功信息.
	 * @param data       泛型.
	 * @return Result<T> 通用返回结果.
	 * @author  		  Thomas Lee
	 * @version 		  2017年6月8日 上午12:23:33
	 * @version          2017年6月26日 下午3:00:28
	 */
	public static <T> Result<T> success(String msg, T data) {
		Result<T> result = new Result<T>();
		result.setCode(AppReturnCode.OK.getCode());
		result.setMsg(msg);
		result.setData(data);
		return result;
	}

	public static <T> Result<T> success() {
		Result<T> result = new Result<T>();
		result.setCode(AppReturnCode.OK.getCode());
		result.setMsg(AppReturnCode.OK.getMsg());
		result.setData(null);
		return result;
	}

    /**
     * 返回通用失败信息.
     * @param resultEnum 错误结果枚举.
     * @return Result<T> 通用返回结果.
     * @author 			  Thomas Lee
     * @version 		  2017年6月8日 上午12:25:38
     * @version          2017年6月26日 下午3:00:21
     */
	public static <T> Result<T> failure(AppReturnCode returnCode) {
		Result<T> result = new Result<T>();
		result.setCode(returnCode.getCode());
		result.setMsg(returnCode.getMsg());
		return result;
	}

	/**
	 * 返回通用失败结果.
	 * @param code 		    错误代码.
	 * @param msg 	            错误信息.
	 * @return Result<T> 通用返回结果.
	 * @author 			  Thomas Lee
	 * @version    	  2017年6月26日 下午3:01:39
	 */
	public static <T> Result<T> failure(String code, String msg) {
		Result<T> result = new Result<T>();
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

	/**
	 * 返回通用失败结果.
	 * @param code 		    错误代码.
	 * @param msg 	            错误信息.
	 * @return Result<T> 通用返回结果.
	 * @author 			  Thomas Lee
	 * @version    	  2017年6月26日 下午3:01:39
	 */
	public static <T> Result<T> failure(String msg) {
		Result<T> result = new Result<T>();
		result.setCode(AppReturnCode.SERVER_ERROR.getCode());
		result.setMsg(msg);
		return result;
	}
}