package com.sse.ssbk.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dao.ActivityCommentDao;
import com.sse.ssbk.dao.NewsCommentDao;
import com.sse.ssbk.dao.ReplyCommentDao;
import com.sse.ssbk.dto.CommentAddDto;
import com.sse.ssbk.entity.ActivityComment;
import com.sse.ssbk.entity.NewsComment;
import com.sse.ssbk.entity.ReplyComment;
import com.sse.ssbk.enums.CommentTargetType;
import com.sse.ssbk.enums.Is;
import com.sse.ssbk.exception.AppReturnCode;
import com.sse.ssbk.exception.SSEBKRuntimeException;
import com.sse.ssbk.service.CommentService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.EmojiUtils;
import com.sse.ssbk.utils.BizUtils.Page;
import com.sse.ssbk.utils.RedisCacheUtils;
import com.sse.ssbk.vo.CommentsAdapterVo;
import com.sse.ssbk.vo.CommentsVo;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private NewsCommentDao newsCommentDao;
    @Autowired
    private ActivityCommentDao activityCommentDao;
    @Autowired
    private ReplyCommentDao replyCommentDao;

    @Override
    public Integer add(CommentAddDto dto) {
        this.validateAdd(dto);
        Date now = new Date();
        UserSession session = BizUtils.getCurrentUser();
        Integer userId = session.getUserId();
        String realName = session.getRealName();
        String userPhoto = session.getUserPhoto();
        Integer companyId = session.getCompanyId();
        String companyName = session.getCompanyName();
        String targetType = dto.getTargetType();
        Integer targetId = Integer.parseInt(dto.getTargetId());
        CommentTargetType eTargetType = CommentTargetType.parse(targetType);
        String conent = dto.getContent();
        if (CommentTargetType.ACT.equals(eTargetType)) {
            ActivityComment comment = new ActivityComment();
            comment.setContent(EmojiUtils.encode(conent));
            comment.setActCommentImages(BizUtils.getStrImages(dto.getImageArr()));
            comment.setUserId(userId);
            comment.setUserName(realName);
            comment.setUserPhoto(userPhoto);
            comment.setActId(targetId);
            comment.setCreateTime(now);
            activityCommentDao.insertSelective(comment);
            RedisCacheUtils.incr(RedisConstants.User.CommentCount.COMMENT_COUNT_ACT + targetId, 1L);
            return 0;
        } else if (CommentTargetType.NEWS.equals(eTargetType)) {
            NewsComment comment = new NewsComment();
            comment.setContent(EmojiUtils.encode(conent));
            comment.setNewsId(targetId);
            comment.setUserId(userId);
            comment.setUserName(realName);
            comment.setUserPhoto(userPhoto);
            comment.setCompanyId(companyId);
            comment.setCompanyName(companyName);
            comment.setCreateTime(now);
            newsCommentDao.insertSelective(comment);
            RedisCacheUtils.incr(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS + targetId, 1L);
            return 0;
        } else if (CommentTargetType.REPLYCOMMENT.equals(eTargetType)) {
            ReplyComment comment = new ReplyComment();
            comment.setParentCommentId(targetId);
            ReplyComment toComment = replyCommentDao.selectByPrimaryKey(targetId);
            if (toComment == null) {
                throw new SSEBKRuntimeException(AppReturnCode.ParaError);
            }
            comment.setTopCommentId(toComment.getTopCommentId());
            comment.setReplyId(toComment.getReplyId());
            comment.setContent(EmojiUtils.encode(conent));
            comment.setIsAnonymous(dto.getIsAnonymous());
            comment.setUserId(userId);
            comment.setUserName(realName);
            comment.setUserPhoto(userPhoto);
            comment.setToUserId(toComment.getUserId());
            comment.setToUserName(toComment.getUserName());
            comment.setCompanyId(companyId);
            comment.setCompanyName(companyName);
            comment.setCreateTime(now);
            replyCommentDao.insertSelectiveWithId(comment);
            RedisCacheUtils.incr(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + toComment.getReplyId(), 1);
            return comment.getReplyCommentId();
        } else if (CommentTargetType.REPLY.equals(eTargetType)) {
            ReplyComment comment = new ReplyComment();
            comment.setContent(EmojiUtils.encode(conent));
            replyCommentDao.insertSelectiveWithId(comment);
            Integer commentId = comment.getReplyCommentId();
            comment.setTopCommentId(commentId);
            comment.setReplyId(targetId);
            comment.setIsAnonymous(dto.getIsAnonymous());
            comment.setUserId(userId);
            comment.setUserName(realName);
            comment.setUserPhoto(userPhoto);
            comment.setCompanyId(companyId);
            comment.setCompanyName(companyName);
            comment.setCreateTime(now);
            replyCommentDao.updateByPrimaryKeySelective(comment);
            RedisCacheUtils.addForSet(RedisConstants.REPLY_COMMENT + targetId, commentId);
            RedisCacheUtils.incr(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + targetId, 1);
            return commentId;
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    /**
     * 
     * @param dto
     * @author HT-LiChuanbin 
     * @version 2017年7月26日 下午8:41:46
     */
    private void validateAdd(CommentAddDto dto) {
        String targetType = dto.getTargetType();
        CommentTargetType eTargetType = CommentTargetType.parse(targetType);
        if (eTargetType == null) {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
    }

    @Override
    public CommentsAdapterVo list(Page page, Integer targetId, CommentTargetType targetType) {
        // TODO validate
        UserSession session = BizUtils.getCurrentUser();
        Integer userId = session.getUserId();
        CommentsAdapterVo voCommentsAdapter = new CommentsAdapterVo();
        List<CommentsVo> voCommentss = new ArrayList<CommentsVo>();
        if (CommentTargetType.ACT.equals(targetType)) {
            Example example = new Example(ActivityComment.class);
            example.setOrderByClause("create_time DESC");
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("actId", targetId);

            if (page.getLastId() != -1) {
                ActivityComment activityCommentForSort = activityCommentDao.selectByPrimaryKey(page.getLastId());
                if (activityCommentForSort != null) {
                    criteria.andLessThan("createTime", activityCommentForSort.getCreateTime());
                }
            }

            RowBounds rowBounds = new RowBounds(0, page.getPageSize());
            List<ActivityComment> activityComments = activityCommentDao.selectByExampleAndRowBounds(example, rowBounds);
            Iterator<ActivityComment> iActivityComment = activityComments.iterator();
            while (iActivityComment.hasNext()) {
                ActivityComment comment = iActivityComment.next();
                CommentsVo voComments = new CommentsVo();
                voComments.setCommentId(String.valueOf(comment.getActCommentId()));
                voComments.setContent(EmojiUtils.decode(comment.getContent()));
                voComments.setImageArr(BizUtils.getArrImages(comment.getActCommentImages()));

                Set<Integer> setUserConcernUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + session.getUserId());
                boolean boolUserConcernUser = BizUtils.contains(setUserConcernUsers, comment.getUserId());
                voComments.setIsConcern(boolUserConcernUser ? Is.YES.getCode() : Is.NO.getCode());
                voComments.setCreateTime(String.valueOf(comment.getCreateTime().getTime()));

                voComments.setUserId(comment.getUserId().toString());
                voComments.setUserName(comment.getUserName());
                voComments.setUserPhoto(comment.getUserPhoto());
                voCommentss.add(voComments);
            }
            Long commentNum = RedisCacheUtils.getLong(RedisConstants.User.CommentCount.COMMENT_COUNT_ACT + targetId);
            voCommentsAdapter.setCommentNum(commentNum.intValue());
            voCommentsAdapter.setComments(voCommentss);
        } else if (CommentTargetType.NEWS.equals(targetType)) {
            Example example = new Example(NewsComment.class);
            example.setOrderByClause("create_time DESC");
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("newsId", targetId);
            NewsComment newsCommentForSort = newsCommentDao.selectByPrimaryKey(page.getLastId());
            if (page.getLastId() != -1) {
                if (newsCommentForSort != null) {
                    criteria.andLessThan("createTime", newsCommentForSort.getCreateTime());
                }
            }
            RowBounds rowBounds = new RowBounds(0, page.getPageSize());
            List<NewsComment> newsComments = newsCommentDao.selectByExampleAndRowBounds(example, rowBounds);
            Iterator<NewsComment> iNewsComments = newsComments.iterator();
            while (iNewsComments.hasNext()) {
                NewsComment comment = iNewsComments.next();
                CommentsVo voComments = new CommentsVo();
                voComments.setCommentId(String.valueOf(comment.getNewsCommentId()));
                voComments.setContent(EmojiUtils.decode(comment.getContent()));
                Set<Integer> userConcernUsers = RedisCacheUtils.getForSet(RedisConstants.CONCERN_USER_USER + session.getUserId());
                Integer commentUserId = comment.getUserId();
                boolean boolUserConcernUser = BizUtils.contains(userConcernUsers, commentUserId);
                voComments.setIsConcern(boolUserConcernUser ? Is.YES.getCode() : Is.NO.getCode());
                voComments.setCreateTime(String.valueOf(comment.getCreateTime().getTime()));
                voComments.setUserId(commentUserId.toString());
                voComments.setUserName(comment.getUserName());
                voComments.setUserPhoto(comment.getUserPhoto());
                voComments.setCompany(comment.getCompanyName());
                voCommentss.add(voComments);
            }
            Long commentNum = RedisCacheUtils.getLong(RedisConstants.User.CommentCount.COMMENT_COUNT_NEWS + targetId);
            voCommentsAdapter.setCommentNum(commentNum.intValue());
            voCommentsAdapter.setComments(voCommentss);
        } else if (CommentTargetType.REPLY.equals(targetType)) {
            Example example = new Example(ReplyComment.class);
            example.setOrderByClause("create_time DESC");
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("replyId", targetId);
            ReplyComment replyCommentForSort = replyCommentDao.selectByPrimaryKey(page.getLastId());
            if (page.getLastId() != -1) {
                if (replyCommentForSort != null) {
                    criteria.andLessThan("createTime", replyCommentForSort.getCreateTime());
                }
            }
            RowBounds rowBounds = new RowBounds(0, page.getPageSize());
            List<ReplyComment> replyComments = replyCommentDao.selectByExampleAndRowBounds(example, rowBounds);
            Iterator<ReplyComment> iReplyComments = replyComments.iterator();
            while (iReplyComments.hasNext()) {
                CommentsVo voComments = new CommentsVo();
                ReplyComment comment = iReplyComments.next();
                Integer parentCommentId = comment.getParentCommentId();
                Integer commentId = comment.getReplyCommentId();

                voComments.setCommentId(String.valueOf(comment.getReplyCommentId()));

                // 如果父评论为null，则其为父评论.
                voComments.setParentCommentId(parentCommentId == null ? commentId.toString() : parentCommentId.toString());
                voComments.setCommentTopId(comment.getTopCommentId().toString());
                voComments.setContent(EmojiUtils.decode(comment.getContent()));

                Set<Integer> setLikeCommentUsers = RedisCacheUtils.getForSet(RedisConstants.LIKE_REPLYCOMMENT_USER + comment.getReplyCommentId());
                voComments.setLikeCount(CollectionUtils.isEmpty(setLikeCommentUsers) ? 0 : setLikeCommentUsers.size());
                voComments.setIsLike(BizUtils.contains(setLikeCommentUsers, userId) ? Is.YES.getCode() : Is.NO.getCode());

                voComments.setCreateTime(String.valueOf(comment.getCreateTime().getTime()));
                voComments.setUserId(comment.getUserId().toString());
                voComments.setUserName(comment.getUserName());
                voComments.setUserPhoto(comment.getUserPhoto());
                voComments.setCompany(comment.getCompanyName());
                voComments.setToUserName(comment.getToUserName());
                voCommentss.add(voComments);
            }

            Long commentNum = RedisCacheUtils.getLong(RedisConstants.User.CommentCount.COMMENT_COUNT_REPLY + targetId);
            voCommentsAdapter.setCommentNum(commentNum.intValue());
            voCommentsAdapter.setComments(voCommentss);
        } else {
            throw new SSEBKRuntimeException(AppReturnCode.ParaError);
        }
        return voCommentsAdapter;
    }
}