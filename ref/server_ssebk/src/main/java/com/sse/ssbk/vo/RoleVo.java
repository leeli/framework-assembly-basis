package com.sse.ssbk.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by ciyua on 2017/7/26.
 */
@Data
public class RoleVo {
    private Integer roleId;
    private String roleName;
    private List<PermVo> permList;
}
