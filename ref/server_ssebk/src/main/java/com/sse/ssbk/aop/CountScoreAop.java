package com.sse.ssbk.aop;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sse.ssbk.dao.ScoreBlotterDao;
import com.sse.ssbk.dao.UserDao;
import com.sse.ssbk.entity.ScoreBlotter;
import com.sse.ssbk.entity.User;
import com.sse.ssbk.enums.ScoreRules;
import lombok.extern.slf4j.Slf4j;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.sse.ssbk.common.RedisConstants;
import com.sse.ssbk.common.UserSession;
import com.sse.ssbk.dto.AuditActivityJoinerDto;
import com.sse.ssbk.dto.CommentAddDto;
import com.sse.ssbk.dto.ForumQuestionDto;
import com.sse.ssbk.dto.ForumReplyAddDto;
import com.sse.ssbk.dto.LikeAddDto;
import com.sse.ssbk.dto.LoginDto;
import com.sse.ssbk.dto.NewsDto;
import com.sse.ssbk.service.UserService;
import com.sse.ssbk.utils.BizUtils;
import com.sse.ssbk.utils.CountScore;
import com.sse.ssbk.utils.RedisCacheUtils;

/**
 * Created by work_pc on 2017/8/10.
 */

@Aspect
@Component
@Slf4j
public class CountScoreAop {

    @Autowired
    private ScoreBlotterDao scoreBlotterDao;

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private UserDao userDao;

    @Pointcut("@annotation(com.sse.ssbk.utils.CountScore)")
    public void addScore() {

    }

    /* @Pointcut("@annotation(com.sse.ssbk.utils.CountScore) && execution(com.sse.ssbk.controller.UserController.auditActivityJoiner(int,AuditActivityJoinerDto))args(userId1,auditActivityJoinerDto)")
     public void addScoreWithParam(){
     }*/

    @AfterReturning("addScore()")
    private void addScoreAround(JoinPoint pj) throws Throwable {
        List<Integer> targerUserId = new ArrayList<>();
        Object[] args = pj.getArgs();
        try {
            for (Object arg : args) {
                //审核参与人 如果通过的话加分
                if (arg instanceof AuditActivityJoinerDto) {

                    if ("1".equals(((AuditActivityJoinerDto) arg).getEnrollStatus())) {
                        targerUserId = ((AuditActivityJoinerDto) arg).getUserIds();
                    }
                }

                if (arg instanceof LoginDto){
                    String loginName = ((LoginDto) arg).getLoginName();
                    User user = userDao.getUserByUserName(loginName);
                    Integer userId = user.getUserId();
                    targerUserId.add(userId);
                }

                //新增资讯 发布即加分
                if (arg instanceof NewsDto || arg instanceof ForumQuestionDto || arg instanceof ForumReplyAddDto || arg instanceof CommentAddDto || arg instanceof LikeAddDto) {
                    UserSession user = BizUtils.getCurrentUser();
                    String currentToken = BizUtils.getCurrentToken();
                    Integer userId = user.getUserId();
                    targerUserId.add(userId);
                }
            }
            ScoreRules scoreRules = getControllerMethodScoreRules(pj);
            String description = scoreRules.getDesc();
            int score = scoreRules.getScore();
            int scoreRuleId = scoreRules.getRuleId();

            String reqMethod = pj.getTarget().getClass().getName() + "." + pj.getSignature().getName() + "()";
            for (Integer userId : targerUserId) {
                log.info("=====累积积分=====");
                RedisCacheUtils.incr(RedisConstants.User.USER_SCORE + userId, score);
//                redisTemplate.opsForZSet().incrementScore(RedisConstants.User.USER_SCORE_RANK,userId.intValue(),score);
                stringRedisTemplate.opsForZSet().incrementScore(RedisConstants.User.USER_SCORE_RANK,userId.toString(),score);
                ScoreBlotter scoreBlotter = new ScoreBlotter();
                scoreBlotter.setUserId(userId);
                scoreBlotter.setScoreRuleId(scoreRuleId);
                scoreBlotter.setCreateTime(new Date());
                scoreBlotterDao.insert(scoreBlotter);
                log.info("方法描述:" + description);
                log.info("添加分数:" + score);
                log.info("用户 " + userId + "执行 " + reqMethod + " " + description + " 增加了 " + score + " 分");
                log.info("=====累积积分结束=====");
            }
        } catch (Exception e) {
            log.error("=====累积积分异常=====");
            log.error("异常信息:{}", e.getMessage());
        }

    }


    public static ScoreRules getControllerMethodScoreRules(JoinPoint joinPoint) throws ClassNotFoundException {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        ScoreRules scoreRules = ScoreRules.NONE;
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    scoreRules = method.getAnnotation(CountScore.class).value();
                    break;
                }
            }
        }
        return scoreRules;
    }


    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param joinPoint 切点
     * @return 方法描述
     * @throws Exception
     */
    public static String getControllerMethodDescription(JoinPoint joinPoint) throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        String description = "";
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    description = method.getAnnotation(CountScore.class).value().getDesc();
                    break;
                }
            }
        }
        return description;
    }

    /**
     * 获取注解中对方法的分数 用于Controller层注解
     *
     * @param joinPoint 切点
     * @return 方法描述
     * @throws Exception
     */
    public static int getControllerMethodScore(JoinPoint joinPoint) throws Exception {
        String targetName = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Class targetClass = Class.forName(targetName);
        Method[] methods = targetClass.getMethods();
        int score = 0;
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                Class[] clazzs = method.getParameterTypes();
                if (clazzs.length == arguments.length) {
                    score = method.getAnnotation(CountScore.class).value().getScore();
                    break;
                }
            }
        }
        return score;
    }
}
