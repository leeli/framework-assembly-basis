set -x
set -e
mvn clean install -DskipTests
chmod 766 "./target/framework-assembly-basis.war"
scp -p ./target/framework-assembly-basis.war root@192.168.176.129:/indi/software/xxx-api-8083/ROOT.war
chmod 766 ./deploy.sh
scp -p ./deploy.sh root@192.168.176.129:/indi/software/script
ssh root@192.168.176.129 "/indi/software/script/deploy.sh"