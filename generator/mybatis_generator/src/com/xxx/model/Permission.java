package com.xxx.model;

public class Permission {
    private Long permId;

    private String description;

    private Long parentPermId;

    private String permName;

    public Long getPermId() {
        return permId;
    }

    public void setPermId(Long permId) {
        this.permId = permId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Long getParentPermId() {
        return parentPermId;
    }

    public void setParentPermId(Long parentPermId) {
        this.parentPermId = parentPermId;
    }

    public String getPermName() {
        return permName;
    }

    public void setPermName(String permName) {
        this.permName = permName == null ? null : permName.trim();
    }
}