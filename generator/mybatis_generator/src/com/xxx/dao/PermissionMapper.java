package com.xxx.dao;

import com.xxx.model.Permission;

public interface PermissionMapper {
    int deleteByPrimaryKey(Long permId);

    int insert(Permission record);

    int insertSelective(Permission record);

    Permission selectByPrimaryKey(Long permId);

    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);
}